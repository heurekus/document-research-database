#Conventions

In 'Details View', a small 'Info' icon is shown on the left of each field. When moving the mouse over it text can be shown, e.g. to inform the user which conventions he should observe when entering information in this field. The 'convention' text can be changed by clicking on the info icon.

![Screenshot](img/conventions.png)
