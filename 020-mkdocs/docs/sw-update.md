#Software Update Procedure

##Option 1: Docker

### Update

 * Do a backup of required

 * On the shell, go the the project directory.

 * Run the update script to download the latest [release version](https://gitlab.com/heurekus/document-research-database/-/releases).

```text
sudo ./00-pull-and-build-update.sh
```
 * Once the script is finished, restart the DRDB instance. 

```text
docker-compose down; docker-compose up -d
```

 * Done

### Update to the Latest Beta Version

 * Note: By default the update script installs the latest **release** version. To install the latest beta version, run the script as follows:

```text
sudo ./00-pull-and-build-update.sh master
```

### Rollback


 * Should it be necessary to roll back to a previous version, you can run the update script with a particular version string as the first parameter. 

 * Important: Rollbacks are generally possible, but NOT if the database scheme in the code was changed. Read the relase notes for details.

 * To go 'back' to v20.1, for example, run ```sudo ./00-pull-and-build-update.sh v20.1```. This will pull all files for the v20.1 release and set the docker 'latest' tag to this version. This way, the docker-compose.yml file does not have to be changed to bring-up this version. 

##Option 2: Manual Installation

###Basics

The easiest way to do a software update is to grab the latest project TAR file from the project development repository and then perform the software update on the production server as follows:

 * Rename the current DRDB project directory to serve as a backup

 * Untar the DRDB update file in the web directory and then rename the new directory to the name of your previous DRDB folder

 * Restore your configuration files and logs from the backup folder


###Details

 * The following bash script automates the actions above. 

 * IMPORTANT: When using the script, deny access to the backup directory in the web path in apache2.conf as follows:

```
<Directory /var/www/drdb.bak>
deny from all
</Directory>
```

 * Note 1: If there was a problem extracting the tar file the original state is restored before the script exits.

 * Note 2: The script leaves the backup on the server after it has executed. It is only deleted when the script is run again. This way, the backup continues to be available and can be renamed to restore the previous version. WARNING: If the SQL database scheme was changed (see release notes) it is not advisable to go back to the previous software version.

```
#!/bin/bash

# Directories, adapt as required
WWW_PATH="/var/www"
HOME_PATH="/home/martin"
DRDB_DIR_NAME="drdb"

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
fi

if [[ $# -eq 0 ]] ; then
    echo 'filename of tar file missing'
    exit 1
fi

echo 'extracting TAR archive...'

cd $WWW_PATH
sudo rm -rf $DRDB_DIR_NAME".bak"
sudo mv $DRDB_DIR_NAME $DRDB_DIR_NAME".bak"
sudo mkdir $DRDB_DIR_NAME
cd $DRDB_DIR_NAME

if sudo tar -xf $HOME_PATH/$1
then
    echo 'success!'
else
    echo 'tar extract error, rolling back'
    cd $WWW_PATH
    sudo rm -rf $DRDB_DIR_NAME
    sudo mv $DRDB_DIR_NAME".bak" $DRDB_DIR_NAME
    cd $HOME_PATH
    exit 1
fi

cd $WWW_PATH

echo 'copying log and config files and adapting access permissions...'

# Get drdb log file from the backup and put it into new directory
sudo cp "./"$DRDB_DIR_NAME".bak/log/drdb-logfile.txt" "./"$DRDB_DIR_NAME"/log/"

# Images also need to be copied from the backup to the new directory
sudo cp "./"$DRDB_DIR_NAME".bak/images/db-front-pic.jpg" "./"$DRDB_DIR_NAME"/images/"
sudo cp "./"$DRDB_DIR_NAME".bak/images/db-front-pic-xs.jpg" "./"$DRDB_DIR_NAME"/images/"

# Get the configuration files from the backup and put them into the new directory
sudo cp -R "./"$DRDB_DIR_NAME".bak/config" "./"$DRDB_DIR_NAME


sudo chmod -R 755 $DRDB_DIR_NAME
sudo chown -R www-data:www-data $DRDB_DIR_NAME

echo 'done!'

```

