# **Document Research Database** Installation Instructions (Docker)

There are two ways of installing the Document Research Database (DRDB):

 * **Option 1:** Installation in a Docker container (very simple, recommended)

 * **Option 2:** Installation on a server with manual configuration of Apache, Mysql, certificates, database creation, etc. etc.

This page describes the Docker approach.


# Option 1: Docker

For a test installation without SSL encryption, get the latest DRDB docker installation and update file from the repository, extract it, run the build and update script, and then launch the container:

```text

cd ~
mkdir doc-db-docker
cd doc-db-docker
wget https://gitlab.com/heurekus/document-research-database/-/raw/master/DRDB--Docker-installation-and-update-files.tar.gz
tar xvzf DRDB--Docker-installation-and-update-files.tar.gz .

# Copy the sample Docker Compose file. Have a look if changes are required.
# Probably not for a first test install.
cp docker-compose.yml.sample docker-compose.yml

# Build the drdb docker container with the latest 
./00-pull-and-build-update.sh

docker-compose up -d

```

 * When run for the first time, it will take a few seconds for a demo database to be created. 

 * Once startup is complete, the installation can be reached on TCP port 8500.

 * Finally, the database needs to be initialized by calling the following URL with ```localhost``` or the ```IP address``` of the server that runs the Docker container:

```text
http://localhost:8500/drdb/index.php?content=bs_first_use_init
```
 
 * At this point the installation is fully usable via the network and localy via:

```text
 http://localhost:8500/
```

 * Preconfigured **username/password**: admin/helloworld

 * For details how to create a database with your own fields, etc. etc., have a look at the other entries in the 'installation section'.

## Docker - Persistent data storage, backup and restore

 * When the container is created with docker-compose for the first time, a ```volume``` directory is created in the doc-db-docker directory. Here, all configuration data and the database itself is stored.

 * A backup can be made of the complete installation with all data by shutting down the container (docker-compose down) and then backing up the complete doc-db-docker directory. Use tar to preserve access permissions of the files.

 * The database can be restored on the same or on another server by extracting the backup archive. If done on another server, run the docker build command as shown above to crate the drdb image again. If a 'volume' directory is present (as in this backup/restore scenario, the existing database and configuration will be used!

## Docker - TLS/SSL encryption
 
 * For a production deployment, it is strongly recommended to use TLS/SSL encryption between the browser and the web server (https).

 * The web server in the container does NOT provide any SSL encryption for web requests.

 * Adding TLS/SSL encryption is done by using a reverse-proxy in front of the container. A project that has been tested [can be found here](https://github.com/evertramos/docker-compose-letsencrypt-nginx-proxy-companion). Another option (but untested) might be the [Traefik project](https://doc.traefik.io/traefik/getting-started/quick-start/).

