#Multiple Databases on the Same Server

## Option 1: Docker Setup

Create a second directory, and create another docker-compose project. If run without TLS/SSL proxy front-end (not recommended) use a different TCP port in the configuration file. If a TLS/SSL proxy front-end is used, use a different domain name.

## Option 2: Traditional Setup

It is possible to install several independent DRDB databases on the same web server. **HOWEVER, it is of utmost importance** that each DRDB database is accessed with an individual domain name!

Here's why: Using the same domain name and just a different directory name in the URL will not work well if the same usernames are used. This is because the combination of the web server, PHP and the DRDB database use the domain name + username to store data in memory for individual users. If the same domain and usernames are used for two databases, the individual configuration of the user of one database is used for the other. This will result in issues because, for example, name and visibility of database fields can be configured on a per user basis which is stored in memory.
