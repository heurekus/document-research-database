#Screenshots

**Note:** Right-Click + 'View Image' on an image enlarges a screenshot!

##The Main Menu

![Screenshot](img/01-main-menu.png)

##Record Detail View

![Screenshot](img/05-record-details.png)

##Editing a Field

![Screenshot](img/07-editing-a-field.png)


##Search Options

![Screenshot](img/02-search-options.png)


##Multi Search

![Screenshot](img/03-multi-search.png)


##Search and Replace

![Screenshot](img/04-search-replace.png)


##Select and Order Fields to Show

![Screenshot](img/06-select-and-order-fields-to-show.png)


##Export Records or Search Results

![Screenshot](img/08-export-search-result.png)


