| | | |
|:----------- |:-------------:| -----------:|
| | | v22.1, 23.7.2021 |

# About

Imagine you have an Excel document that contains a taxonomy of documents. The Excel file has 10 tabs which contain 2 MB of pure text in 12.000 cells. Some cells contain text of several A4 pages and in many cases, text is formatted in bold, italics, color, etc. In addition, several people are working on the project simultaneously and nobody has any idea anymore who has done what and which file contains the latest version... 

At this point the **Document Research Database (DRDB)** might be helpful with the following features:

## The Basics

![Screenshot](img/record-details.png)

* Many people can work on the project simultaneously. Record locking makes sure nobody tramples over work of others accidentally.

* Text in fields of records can by styled in bold, italics, color, etc., and of any length. Several A4 pages per field are no problem.

* The number of fields of a record is not limited. Projects this database system is used for have 30+ record fields. Note: The picture above shows just 4 fields of a database record to fit into this page more easily.

* Records can be grouped, e.g. all records for documents written between certain dates, documents of certain libraries, etc. Searches can be performed over the whole database, or be limited to one or more groups.

* New database records can be inserted at the end or after any existing record. Records can also be re-ordered via the GUI.

* The order in which fields are displayed when viewing the details of a record can be changed with via drag and drop, individual fields can be shown or hidden depending on the current needs. These parameters are user specific as there is no one-size-fits-all view for everybody on the project.

* Any changes to record fields are recorded and a history with comparison to previous versions can be shown. 

* Touch UI integration, e.g. swipe to go from one database record to the next, up- and down swiping for scrolling and single touch enter the field editor.


## Search Information and List Results

* A wide variety of search possibilities to search for records, including AND/OR search combinations, search for records with particular empty fields, non-empty fields, etc. etc.

* The output of search results is configurable to only show fields where a match is found or extra fields to set the results in context.

## Export Data

Search results or the complete database can be exported to LibreOffice Writer and Calc. Macros in those documents that are part of the project can automatically format the exported result to fit particular needs.


## Import Data

Import existing information from LibreOffice Calc documents with LibreOffice Basic Macros that are part of the project.

### User Access Model

* Access to the database via a web browser requires a username/password that is enforced by the web server and NOT the application itself (HTTPS digest authentication). This way, it is possible to deploy the system on the Internet and, depending on username and password strength, only have a very small attack surface.

* Access without a username/password for 'visitors/guests' is NOT possible, everybody needs an account.

* Users can can have read/write access or read-only access.  In any case, however, a password is required, the application is not intended to act as portal for the general public!


## Configurability

As every project is different, the number and names of fields of a database record can be configured at installation time and modified later when necessary. 

## License

The source code of this project is freely available and licensed under the **GNU General Public License v3**. For details see [here](https://www.gnu.org/licenses/gpl-3.0.en.html)


## How To Start

Welcome to the **Document Research Database (DRDB)** project!

You can find a fully working demo installation [here](https://demo.doc-rdb.de/drdb) to test things at your leisure. **You will be asked for a username / password. Use gituser / hellogit**

## Installation

The preferred method to install DRDB is in a Docker container which can be done in just a few minutes. Have a look at the [installation instructions page](https://demo.doc-rdb.de/drdb-manual/installation-docker/) for details.

## Contributing

If you are interested in extending and changing the functionality, feel free to [fork the code](https://gitlab.com/heurekus/document-research-database) and send pull requests for new features you have developed.

## Issues and Feature Requests

Issues and feature requests [can be raised on Gitlab](https://gitlab.com/heurekus/document-research-database).

