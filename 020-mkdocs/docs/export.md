# Export to Libreoffice Writer

While single database records or fields could theoretically be exported to Libreoffice Writer manually one by one, exporting complete result lists and formatting them in Writer in a consistent way can be a daunting task if done that way. DRDB offers an automated way to export all records found in a result list to a Libreoffice Writer document as follows:

### General Procedure

In the result list of a search, click on the "Export" Button on the left of the list. The dialog box as shown below is then presented with the following options:

 * **Quick print**: Outputs the content of all database records that were found into a Libreoffice Writer document with minimal formatting. Only two paragraph formats are used, 'Standard' and 'Standard-indentation'. The former is a default paragraph style, the second one has been derived from it. It is used to indent the content of each field of a record while leaving the field names to the right.

 * **Writer**: Also outputs the database record to a Libreoffice Writer document, but uses a different paragraph format for each field name and field content. This way, each field can be formatted with a different style in Libreoffice Writer and it is possible to change the text style, tabulation, indentation, etc. of each field individually and apply it to the overall document without having to do the changes record by record. 

![Screenshot](img/libreoffice-export-dialog-box.png)

### Limiting the number of fields shown in the output

Only the fields that have been selected to be shown in the search result will be shown in the Quick Print / Libreoffice Writer output. By default, only the record IDs will be shown in the output. To select the fields that are exported, close the print window and the result window, go back to the search / list option and click on the "Select Fields" button. Then run the search again and start Quick Print / Libreoffice Export again. 

![Screenshot](img/select-fields.png)

### Other Output Options

As shown in the first screenshot above, there are some pieces of information of each record that can be included or omitted by selecting/unselecting the corresponding checkbox:

 * **Show ID**: Includes the ID of each database record in the export result

 * **Show ID with link**: Makes the ID of each database in the export result clickable. This way, one can jump from the Libreoffice Writer document directly back to the database record in the browser.

 * **Page break**: Insert page break between records in case the export document should only contain one document record per page.


### Extra Formatting Options per Field

 * For Libreoffice Writer export, some fields should be printed next to the field name rather than in a new line. This is for example useful for fields which only contain a short text. This can be configured in /config/database-structure.php by the project administrator with the ```PRINT_INLINE``` parameter for each database field. Note: This is NOT configurable via the web browser (yet). 

### Writer Template File

The Libreoffice Writer export uses a Writer document template in the /template directory of the project. Paragraph styles can be modified in this template document so the output can be styled as per the projects requirements. A new template file can be uploaded from the 'Administrator' page.

# Export to Libreoffice Calc

### General Procedure

It is also possible to output a search result list directly to Libreoffice Calc. Each field selected for output in the result list and, as a consequence, in the export result, becomes a column in Calc. Text formats in individual fields is preserved. 

### Calc Template File

The Libreoffice Calc export uses a Calc document template in the /template directory of the project. Many cell properties such as its horizontal size, text wrapping in the cells, etc. can be modified in this template document. Note that any exisiting cell text in the template file is discarded during export! A new template file can be uploaded from the 'Administrator' page.

### Specifying the Fields for Table Columns

To select the fields which become columns in the output use the same procedure as described above for selecting fields for Writer export.
