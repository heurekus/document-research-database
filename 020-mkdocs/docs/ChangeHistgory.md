# Change History of Fields

Each field of every record has a change history of all changes made to it since the record was created. It can be accessed by clicking/tapping on the little 'clock' icon on the lower right of a field. If the field was only once edited, no change history is yet available. In case the field has been edited at least twice, the changes can be compared as shown in the screenshot below. 

 * Changes are always compared to the LATEST version of the field which is always shown on the right of the screen.

 * The latest version and the change currently viewed always contain the change date and the author of the change.

 * Lines in light green have been added while lines in light red have been removed.

![Screenshot](img/change-history.png)
