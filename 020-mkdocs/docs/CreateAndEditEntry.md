# Create, Edit and Move Document Records

## Create New Entry

There are several way to create a new database entry (also referred to as a document in the text below):

### Create New Entry At the End of the Database or a Category

The first is to use the "Add new document" button from the main page. By default, this will create a new document and will be put at the end of the database.

The database works with numerical document IDs which allows to sort and group documents. For details see id-structure.php or ask your admin.

By selecting a category at the top of the edit screen, the new document can also be created at the end of a category. Again, this is described in more detail in id-structure.php.

![Screenshot](img/create-new-doc.png)


## Create a New Entry AFTER an Existing Entry

To insert a new document after an existing document, go to the detail view of that document and then click on the "Add new AFTER" button. 

## Edit an Existing Entry

There are two ways to edit an existing entry:

 * Click on any field to edit the particular field

 * Click on the "Edit" button on the left to edit ALL fields at once without having to save changes in one field first before making changes in the next field.

## Document Record Locking While Editing

While editing a field or a full entry the document record in the database is locked to the user. If another user wants to modify the same document an error is shown and editing is not enabled.

## Move document records

If a document record is not at the right place in respect to other documents records, use the "Move a document" button on the left of a search or list result to start the moving process. A yellow "Move" button is shown next to each document of the list. Press the yellow button of the document to be moved.

![Screenshot](img/move1.png)


Now scroll through the list and click on the green "Insert After This" button. After a confirmation dialog the document record will then be moved.

![Screenshot](img/move2.png)

![Screenshot](img/move3.png)


