# Add, Change or Delete a Field (Column) after Installation

Once the database is up and running, admin users can modify the database field configuration via the web interface. The configuration page can be accessed via the 'DB Management' button on the administration page.

## Administration Management Page

![Screenshot](img/admin-page-db-button.png)


## Field Management Page

![Screenshot](img/db-management-page.png)


