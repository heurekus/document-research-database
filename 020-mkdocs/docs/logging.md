#Logging

### DRDB Logs

 * Detailed log entries are produced for every page access in the ```/log``` folder of the project.

 * In a Docker environment, the log can be viewed from outside the container as follows:

```text
docker logs CONTAINER-NAME --follow
```

 * Log entries show how the server side PHP code for the page request is traversed. Also, SQL statements are logged and errors are reported. For debugging purposes, or to see what a user has done when something doesn't work as expected, the log file is a great resource. 

 * DRDB checks the log file size and archives the file into a .tar.gz file once its size exceeds 10 MB.


### PHP and Apache Error Logs

 * PHP and Apache error logs are available **inside** the container:

```text
# Get to bash inside the container
docker container exec -it CONTAINER_NAME bash 

# Tail the Apache error log for Apache and PHP errors
tail -f /var/log/apache2/error.log
```
