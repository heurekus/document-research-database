# Database Structure

As every document research project is different the database structure of DRDB is flexible and can be changed during the installation process. 

Some aspects of the database structure can also be changed later-on. However, depending on the change, manual changes in the SQL database might be required as described below (e.g. manual addition and deletion of fields in the main database table).

## The Structure of a Database Record

The database structure that is used for a DRDB project can be found in *config/database-structure.php*. The array is structured as follows:

 * Each line contains a field that is to be displayed. 

 * The order of this array determines the default order of fields shown on in the GUI.

 * The first parameter (the named index) represents the name of the database field as shown in the GUI It can contain any character, including space and special characters.

 * The GUI name parameter then references an array with three parameters in each line:

 * 1st reference parameter: Name of the field in the database. It **must not** contain spaces, special characters, etc.)
  
 * 2nd reference parameter: SHOW_FIELD: By default all fields are visible (=1) but the configuration can be changed to hide the field by setting this field to 0.
  
 * 3rd reference parameter: PRINT_INLINE: The content of a field can be printed in a separate row (default) or, if it is short, it can also be printed in-line (together with the fieldname) when exporting to Libreoffice. Can be 'true = 1' for inline printing or 'false = 0' for a separate line.

## Modifications After The Initial Project Installation 

The following changes can be made to the structure array can be made that do NOT require SQL database changes.

 * Order of the entries in the array can be changed. 

 * The index names (see above) can be changed to modify what's printed on the page

**IMPORTANT:** When the array is changed, increase 'version' in config.php so users will get the latest version of the field configuration when they load a page. Also, their current field order configuration that is stored in the database is overwritten as names and the number of fields might have changed.

## Things That Require Manual SQL Database Changes

 * Entries can be added and deleted in the array. This requires a corresponding change in the main database table.

 * Once a database is in use, it's advisable to NOT change the the database field name (first string in the sub arrays). Should this nevertheless become necessary, the field name needs to be changed in the main database table as well. Also entries in the modification history table need to be changed as well!

**IMPORTANT:** Change the version number variable as described above so user settings based on the old configuration are removed next time the user accesses the database!

## Example Configuration

The following array is taken from the example installation. The names are not really very useful for a practical applications as all GUI field names start with the word 'DEMO' and all database field names are numbered consecutively for simplicity's sake. Note, for example, that GUI field names can contain spaces while database field name MUST NOT contain spaces and any kind of special characters!

```
// field constants for the sub-array contents
define ('DB_FIELD_NAME', 0);
define ('SHOW_FIELD', 1);
define ('PRINT_INLINE', 2);

// Array that describes which fields to display. Have a look at the constants 
// directly above for information on what the numbers in the sub-array 
// represent.
$doc_db_description = array(
		'Demo Field 01'  => array('F0K', 1, 0),
		'Demo Field 02'  => array('F0A', 1, 0),
		'Demo Field 03'  => array('F0B', 1, 0),
		'Demo Field 04'  => array('F0C', 1, 0),
		'Demo Field 05'  => array('F0D', 1, 0),
		'Demo Field 06'  => array('F0E', 1, 0),
		'Demo Field 07'  => array('F0F', 1, 0),
		'Demo Field 08'  => array('F0G', 1, 0),
		'Demo Field 09'  => array('F0H', 1, 0),
		'Demo Field 10'  => array('F0I', 1, 0),
		'Demo Field 11'  => array('F0J', 1, 0),
		'Demo Field 12'  => array('F0L', 1, 0),
		'Demo Field 13'  => array('F0M', 1, 0),
		'Demo Field 14'  => array('F0N', 1, 0),
		'Demo Field 15'  => array('F0O', 1, 0),
		'Demo Field 16'  => array('F0P', 1, 0),
		'Demo Field 17'  => array('F0Q', 1, 0),
		'Demo Field 18'  => array('F0R', 1, 0),
		'Demo Field 19)' => array('F0S', 1, 0),
		'Demo Field 20'  => array('F0T', 1, 0),
		'Demo Field 21'  => array('F0U', 1, 0),
		'Demo Field 22'  => array('F0V', 1, 0),
		'Demo Field 23'  => array('F0W', 1, 0),
		'Demo Field 24'  => array('F0X', 1, 0),
		'Demo Field 25'  => array('F0Y', 1, 0),
		'Demo Field 26'  => array('F0Z', 1, 0),
		'Demo Field 27'  => array('FAA', 1, 0),
		'Demo Field 28'  => array('FAB', 1, 0),
		'Demo Field 29'  => array('FAC', 1, 0),
		'Demo Field 30'  => array('FAD', 1, 0),
		'Demo Field 31'  => array('FAE', 1, 0),
		'Demo Field 32'  => array('FAF', 1, 0),
		'Demo Field 33'  => array('FAG', 1, 0),
		'Demo Field 34'  => array('FAH', 1, 0),
		'Demo Field 35'  => array('FAI', 1, 0),
);

```
