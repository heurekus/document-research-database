#Optional Configuration Options


## Adapt Header Images

The images used for the normal and small headers can be found and replaced in the *images* folder on the server. The large header for desktop view is around 1000 pixels wide and 100 pixels high. The extra small image used for small screens has a width of around 160 pixels and is also around 100 pixels high.

## Autocorrect French Punctuation

In French text, double chevrons (« and ») are used as quotation marks. Also, a non-breaking space characters are used before ! and ? characters. To automatically insert the non-breaking spaces and convert normal double quotes to double chevrons (e.g. because you don't have a French keyboard), set '*fix_french_punctuation*' in ```config/config.php```  to true. Whenever text is edited and saved, the text is scanned and changes are made automatically. 

## Field(s) used for the Record Headers in List View

By default the content of the first database field of a record is used in the list view page as header for the individual record. This can be changed in ```config/database-structure.php``` with the following two variables:

```
$list_view_header_fields = array (
    'F0A',
    'F0B',
    'F0X',
);
```

If empty or missing, the first entry of $doc_db_description will be used. Several fields can be used as header. 

IMPORTANT: The array contains the DATABASE FIELD NAME(s) and NOT the names presented to the user!!!

If in list view the header text for each record is expected to be very long, the following variable can be set to true to use small text. Otherwise, text is formated as ```<h3>``` to appear larger. This is also the default in case the variable is false or not declared at all.


```
$list_view_header_small_text_size = true;
```
