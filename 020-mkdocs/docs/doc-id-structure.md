##Document-ID Structure

Some research projects have the requirement to group the records into categories that can represent things like, for example, location, time periods, a combination of both or anything else that can be used as a category. This can be done in DRDB with the document IDs and is configured in *config/id-structure.php*.

This file contains the structure of the database id field which has a number of ranges that represent different pieces of data. The structure can be used in various places for things such as, for example, generating a list of 'categories' to limit database searches to subsets of the database content.

Here is an example:

```text
In the database, the ID has 10 digits, i.e. up to 1.999.999.999.
As an example, the ID could be split as follows for data that was
imported from several sheets in an Excel file. Each sheet contains a
number of documents that were written in a certain decade and in 
a certain country. In other words, the sheets represent decades/countries
and thus became the category identifier in the DRDB database.

1.    100.     000.100

Excel File   : 1 Digit  (up to 9)     --> Always 1. Was initially thought to 
                                          be used to to separate documents of 
                                          different countries. However, PHP
                                          uses 32 bit integers and resorts to
                                          floats for higher numbers. Therefore
                                          this is a future option. 
                                          
                                      --> DANGER! DANGER! MYSQL also 
                                          stores the ID as 32-bit integer limit
                                          here with 2.147.483.647. BIGINT data
                                          type is available that should fix
                                          things. Not implemented so far!

Sheet Number : 3 digits (up to 999)   --> to distinguish decades and countries 
										  
										  Decades + Country 1 is assinged 001-030
										  Decades + Country 2 is assigned 031-050
										  
Doc. Number: 6 digits (up to 99999)   --> 99 document ids in between each
                                          document to have room to insert 
                                          documents later on

Special numbers:

In addition, all numbers in the array 0 <= 1000 are special numbers used to
group several categories together for search and display purposes. A
multi-category group then includes all groups from the first special number
to the next special number (or the end of the array if non follows). 

Note: The special numbers are handled in the getAndSetSearchLimit() function that
is also contained in the configuration file (id-structure.php).

*/

$id_categories = array(
		'Demo - All'                         => 1,
		'Demo - set 1'                       => 1001000000,
		'Demo - set 2'                       => 1002000000,
		
		'--------'                           => 2,
		'More Demo - All'                    => 2,		
		'More demo - set a'                  => 1031000000,
		'More demo - set b'                  => 1032000000,
);


```
