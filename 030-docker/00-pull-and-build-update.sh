#!/bin/bash

echo "### DRDB Updater v1.0"
echo

if [ "$EUID" -ne 0 ]
  then echo "Can't proceed, please run as root"; echo
  exit
fi

rm DRDB--Docker-installation-and-update-files.tar.gz

# If no version is given in the command line, update to the latest released version from the repository.
# Othwerwise update to the version given in the command line.
if [ -z "$1" ]
  then
    VERSION=`git ls-remote --tags https://codeberg.org/heurekus/Document-Research-Database.git | grep -o 'v.*' | sort -V | tail -1`
else
  VERSION=$1
fi


echo "Updating to version $VERSION"
echo
echo

FILE_URL="https://codeberg.org/heurekus/Document-Research-Database/raw/tag/$VERSION/DRDB--Docker-installation-and-update-files.tar.gz"

wget $FILE_URL

if [ $? -ne 0 ]; then
  echo "Version given not found, aborting the update process"
  echo
  exit
fi

tar xvzf DRDB--Docker-installation-and-update-files.tar.gz

# build with the build date in a tag, so a fallback to a previous image
# version is possible by modifying the tag in docker-compose.yml should
# this every become necessary.
docker pull mysql:5.7
docker pull ubuntu:20.04
docker build -t doc-rdb:$VERSION -t doc-rdb:latest -t doc-rdb:$(date -u +'%Y-%m-%dT%H.%M.%SZ') .
