# WE HAVE MOVED

This project is now maintained at https://codeberg.org/heurekus/Document-Research-Database. Please go there to get the latest vesion of the code, for feature requests and for opening issues.

This presence is maintained for some time so the update process will continue to work. The last version in this repository will autmatically change the update URLs of your instance and further updates will automatically be downloaded from the repository on Codeberg.org
