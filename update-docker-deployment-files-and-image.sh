#!/bin/bash

if [[ $EUID -ne 0 ]]; then

   echo
   echo "ERROR: This script must be run as root, please try again!"
   echo
   exit 1
fi

# Go to the directory where the script is located, so it can be called
# from anywhere

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

# Create TAR file of source code for docker build process
cd ./010-src
tar --exclude="log/*.tar" --exclude="log/*" -cvzf \
   ../030-docker/install-files/00-drdb-installation.tar.gz --exclude '.git' .

# Now build and tar the documentation
cd ../020-mkdocs
mkdocs build
cd site
tar -cvzf ../../030-docker/install-files/02-docdb-manual.tar.gz --exclude '.git' .

# Now build the image locally
cd ../../030-docker
docker pull ubuntu:20.04
#docker build -t doc-rdb .
docker build -t doc-rdb:latest -t doc-rdb:$(date -u +'%Y-%m-%dT%H.%M.%SZ') .


# Create tar file for deploying a live Docker installation or updating an existing one.
sudo tar --exclude=volumes --exclude=nohup.out -cvzf ../DRDB--Docker-installation-and-update-files.tar.gz .
