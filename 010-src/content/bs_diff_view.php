<?php
/**
 * bs_diff_view.php
 *
 * Content for viewing modification differences of a field of a particular
 * database record
 *
 * @version    1.0 2017-12-21
 * @package    DRDB
 * @copyright  Copyright (c) 2014-17 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


// extra styles just for this page for the HTML table that shows the diff.

?>

<style>
.diff td{
	padding:0 0.667em;
}

.diffDeleted span{
	border:1px solid rgb(255,192,192);
	background:rgb(255,224,224);
}

.diffInserted span{
	border:1px solid rgb(192,255,192);
	background:rgb(224,255,224);
}
 
</style>

<script type="text/javascript" src="/jquery.touch.swipe/jquery.touchSwipe.min.js"></script>

<script src="js/diff_view.js"></script>
  
<!-- Modal dialog box that is shown if no diff was found -->
<div class="modal fade" id="noDiffFound" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">No Version History For This Field...</h4>
      </div>
      <div class="modal-body">
        <p id="modal-replace-text">The current version of this field is the 
        only version in the database so far. Therefore, no diff is available! 
           Click on 'close' to return to the tab that shows the record details.</p>
      </div>
      <div class="modal-footer">      
        <button type="button" id="closetab" class="btn btn-default" data-dismiss="modal" 
                style="float:left">Close</button>      
      </div>
    </div>
    
  </div>
</div>  
   
  
<?php

$log = new Logging();

if (!UserPermissions::hasAccess('history')) {
    $log->lwrite("User has no 'history' permission, aborting");
    exit;
}

if (!isset($_GET['id'])) {
	echo '<h1> ERROR: Parameter "id" missing! </h1>';
	$log->lwrite('bs_diff_view.php: Error, "id" parameter missing');
	exit;
}
$id = (int) $_GET['id'];

if (!isset($_GET['field_name'])) {
	echo '<h1> ERROR: Parameter "field_name" missing! </h1>';
	$log->lwrite('bs_diff_view.php: Error, "field_name" parameter missing');
	exit;
}
$field_name = $_GET['field_name'];

// Get all versions of the requested record/field combination
$all_versions = ModHistoryStorage::getPreviousFieldContents($id, $field_name);

// var_dump($all_versions);
// echo sizeof($all_versions);

// Which versions are to be compared? If no info is given in URL, compare
// the two latest ones.
if (!isset($_GET['v1'])) {
	$first_version = 0;
}
else {
	$first_version = (int) $_GET['v1']; // newer version
}

if (!isset($_GET['v2'])) {
	$second_version = 1;
}
else {
	$second_version = (int) $_GET['v2']; // older version
}

if (sizeof($all_versions) < $second_version+1) {
	
	// Show the 'no diff found' modal dialog box by activating it via Javascript
	echo '<script src="js/diff_view_show_dbox.js"></script>';
	return;	
}

// Get the current version of the field from the modification database.
// The current version is always the first record returned.
$text2 =  $all_versions[$first_version]['content'];

// get the latest old version of the field
$text1 =  $all_versions[$second_version]['content'];

// Replace paragraphs and breaks with standard line feeds as the diff
// algorithms works with standard lines
$text1 = str_replace("<p>", "", $text1);
$text1 = str_replace("</p>", "\n", $text1);
$text1 = str_replace("<P>", "", $text1);
$text1 = str_replace("</P>", "\n", $text1);
$text1 = str_replace("<br>", "\n", $text1);
$text1 = str_replace("<br />", "\n", $text1);

$text2 = str_replace("<p>", "", $text2);
$text2 = str_replace("</p>", "\n", $text2);
$text2 = str_replace("<P>", "", $text2);
$text2 = str_replace("</P>", "\n", $text2);
$text2 = str_replace("<br>", "\n", $text2);
$text2 = str_replace("<br />", "\n", $text2);

?>
<div class="container-fluid">

  <div id="swipe">

  <div class="row content">
    <div class="col-sm-2 sidenav">      
      <br>

      <ul id="menu_area" class="nav nav-pills nav-stacked custom">
        <li class="active" id="closetab_menu">
          <a href="#a1" id="menu_text">
          <span class="glyphicon glyphicon-asterisk"></span> Close tab</a>
        </li>
        <br>
                
        
<?php 

        $prev_diff_url = 'index.php?content=bs_diff_view&id=' . $id .
        '&field_name=' . $field_name . '&v2=' .
        ($second_version - 1);
        
        $next_diff_url = 'index.php?content=bs_diff_view&id=' . $id .
        '&field_name=' . $field_name . '&v2=' .
        ($second_version + 1);
        
        // Show button to go to previous diff or disable the button
        // if this is already the first diff.
        if ($second_version > 1) {

        	// Insert reference for Javascript arrow + swipe functionality
        	echo '<span id="prev_diff_url" style="display:none">' .
        	     $prev_diff_url . '</span>' . PHP_EOL;
        	
        	// Insert button with link
        	echo  '<li class="active">';
        	echo '<a href="' . $prev_diff_url . '" ';
        	
        }
        else {
        	// Insert empty reference for Javascript arrow + swipe functionality
        	echo '<span id="prev_diff_url" style="display:none">#2</span>' . PHP_EOL;
        	     
        	// Insert button with empty link
        	echo '<li class="disabled">';
        	echo '<a href="#2" ';

        }        
        echo 'id="menu_text"><span class="glyphicon glyphicon-backward"></span> Newer</a>';
        
        echo '</li><br>';               
        
        echo PHP_EOL;
        echo PHP_EOL;
        
        // Show button to go to next diff if one is available in the datbase or
        // disable if the current diff is the last diff
        if (sizeof($all_versions)-1 > $second_version) {

        	// Insert reference for Javascript arrow + swipe functionality
        	echo '<span id="next_diff_url" style="display:none">' .
        			$next_diff_url . '</span>' . PHP_EOL;        	
        	
        	// Insert button with link
        	echo  '<li class="active">';
        	echo '<a href="' . $next_diff_url . '" ';
        }
        else { 

        	// Insert empty reference for Javascript arrow + swipe functionality
        	echo '<span id="next_diff_url" style="display:none">#2</span>' . PHP_EOL;        	
        	
        	// Insert button with empty link
        	echo '<li class="disabled">';
        	echo '<a href="#2" ';
        	
        }
        
        echo 'id="menu_text"><span class="glyphicon glyphicon-forward"></span> Older</a>';
        echo '</li>';

        if (UserPermissions::hasAccess('help')) {
            echo ('
                <br>
                <li class="active">
                <a href="#a3" id="diff-help"
                    id="menu_text">
                <span class="glyphicon glyphicon-question-sign"></span> Help</a>
                </li>  
            ');
        }
?>  
        
       </ul>
    </div>
   
    <!-- end of menu side panel, start with the right panel -->
       
    <div class="col-sm-10">

<?php 

echo "<br>";

echo PHP_EOL;

echo '<div class="panel-group">';
echo '<div class="panel panel-info">';
echo '<div class="panel-heading">';

echo PHP_EOL;

echo '<table style="width:100%">';
echo '<tr>';
echo '<td align="left" ><b>Older version: ' . 
      date('Y-m-d H:i:s', $all_versions[$second_version]['mod_time']) . ' by ' .
      ucfirst($all_versions[$second_version]['mod_user']) . 
      '</b></td>';

echo '<td align="right"><b>Latest version: ' . 
     date('Y-m-d H:i:s', $all_versions[$first_version]['mod_time']) . ' by ' .
     ucfirst($all_versions[$first_version]['mod_user']) .      
     '</b></td>';

echo '</tr>';
echo '</table>';

echo '  </div>'; //end of panel-heading

echo PHP_EOL;

echo '<div class="panel-body">';

// Create HTML table that shows the difference between the two database
// field versions.
echo Diff::toTable(Diff::compare($text1, $text2));



echo '</div>'; // panel-body
echo '</div>'; // end of pannel
echo '</div>'; // end of swipe area
echo '</div>'; // end of pannel group

?>

    </div> <!-- end of right column -->
  </div> <!-- end of row -->
</div> <!-- end of container --> 
