<?php
/**
 * bs_user_management.php
 *
 * Content for User Management page
 *
 * @version    1.0 2021-04-08
 * @package    DRDB
 * @copyright  Copyright (c) 2014-21 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('User management page accessed');

if (!UserPermissions::hasAccess('admin')) {
    $log->lwrite("User has no admin rights, aborting");
    return;
}

// Protect POST operations againt CSRF by checking the token that must be present
if (isset($_POST['operation'])) {

    if (checkAntiCsrfProtection() == false) {
        $err_msg = "There was a security error, unable to modify user config";
        $log->lwrite($err_msg);
        return false;
    }
}

// Check if POST information is present and permissions of a user need to be updated
if (isset($_POST['operation']) && $_POST['operation'] == 'change_permissions') {
    UserPermissions::updatePermissionsOfSingleUser($log);
}

// Check if POST information is present and a new user is to be created
if (isset($_POST['operation']) && $_POST['operation'] == 'add_user') {
    UserPermissions::addUser($log);
}

// Check if POST information is present and password hsa to be changed
if (isset($_POST['operation']) && $_POST['operation'] == 'change_pwd') {
    UserPermissions::changePasswordOfUser($log);
}

// Check if POST information is present and password hsa to be changed
if (isset($_POST['operation']) && $_POST['operation'] == 'del_user') {
    UserPermissions::deleteUser($log);
}

// Create token if not already done in a previous iteration or somewhere else
// by another action. The token is sent when the save button is pressed
// and then analyzed in createOrModifiyRecord() to prevent Cross Site
// Request Forgery (CSRF) attacks.
$token = createOrReuseSecurityToken();

?>
<script src="js/user_management.js"></script>

<div class="container-fluid">

  <div class="row content">
    <div class="col-sm-2 sidenav">      
      <br>

      <ul id="menu_area" class="nav nav-pills nav-stacked custom">
        <li class="active" id="closetab">
          <a href="#a1" id="menu_text">
             <span class="glyphicon glyphicon-asterisk"></span> Close tab</a><br>
        </li>
       
        <li class="active">
           <a href="index.php?content=bs_user_add" id="add_user">
              <span class="glyphicon glyphicon-plus"></span> Add user</a>
        </li>
       </ul>
    </div>
   
    <div class="col-sm-10"> 

<br>
<h2>User Management</h2>
<br>


<?php 

$local_users_and_permissions = UserPermissions::getUsersAndPermissions();

// For each user
foreach ($local_users_and_permissions as $user => $permissions):

    //$log->lwrite("User: " . $user);
    
    echo '<div class="panel-group">';
    echo '<div class="panel panel-info">';     
    echo '<div class="panel-heading">';

    echo '<strong> ';
    echo $user;
    echo '</strong>';
    
    echo '</div>'; // end of pannel-heading

    
    echo '<div class="panel-body">';
    echo '<div class="form-check">';
    
    // Checkbox for each type of permission
    echo '<div class="row">';
    echo PHP_EOL;
    
    // Create a form for the checkboxes and the update button to submit
    // changes back to the server.
    echo PHP_EOL . PHP_EOL . PHP_EOL;
    echo '<form action="index.php?content=bs_user_management" method="post">' . PHP_EOL;
    echo '<input type="hidden" id="id-token" name="token" value="' .  $token . '">';
    // include the username as a hidden form element so it gets sent back
    // together with the permissions.
    echo '<input type="hidden" name="user" value="' . $user . '">';
    
    $permission_categories = UserPermissions::getPermissionCategories();
    foreach ($permission_categories as $permission_category):
        //$log->lwrite("Permission: " . $permission_category);   
        echo '<div class="col-sm-2">' . PHP_EOL;
        
        // add below for 'input' depending on permission:
        // checked="false", disabled="false" (--> can't be changed)

        echo '<input type="checkbox" class="form-check-input" name="'  
             .$permission_category . '"';
        
        echo (in_array($permission_category, $permissions)) ? 'checked>' : '>';

        echo PHP_EOL . '<label class="form-check-label" for="exampleCheck1">&nbsp;' . 
             $permission_category . ' </label>';

        echo PHP_EOL . "</div> <!--  end of of column -->" . PHP_EOL . PHP_EOL;

    endforeach;

    
    echo '<div class="col-sm-2">'; // column for the buttons
    
    // Update
    echo PHP_EOL;
    echo '<button type="submit" class="btn btn-success update-user" ' . 
         'name="operation" value="change_permissions">' .
         '<span class="glyphicon glyphicon-ok"></span> ' .
         'Update&nbsp;&nbsp;&nbsp;&nbsp;</button><br><br>';
    echo PHP_EOL;

    
    // Change password button
    echo PHP_EOL;
    echo '<button type="button" user="' . $user . '" class="btn btn-primary password-user">' .
        '<span class="glyphicon glyphicon-edit"></span> ' .
        'Password</button><br><br>';
    
    // Delete button
    echo PHP_EOL;
    echo '<button type="button" user="' . $user . '" class="btn btn-warning del-user">' .
        '<span class="glyphicon glyphicon-remove-circle"></span> ' .
        'Delete&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>';    
    
    echo '</div>'; // end of column for the buttons
    
    echo PHP_EOL;
    echo "</form>";
    echo PHP_EOL;

    echo PHP_EOL;
    echo '</div> <!-- end of form for this user -->';
    echo PHP_EOL;
    echo '</div> <!-- end of row -->';

    echo PHP_EOL;
    echo '</div>'; //panel-body
    echo '</div>'; //end of pannel
    echo '</div>'; //end of pannel group
    echo PHP_EOL;
    
    echo '<br>';
    

endforeach; // for each user

?>
</div> <!-- end of right column -->
</div> <!-- end of row -->
</div> <!-- end of container -->


<!-- Modal dialog box for changing the password of a user -->
<div class="modal fade" id="pwdChangeModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Title</h4>
      </div>
      <div class="modal-body">
         <form action="index.php?content=bs_user_management" method="post" id="pwdchange">
            <input type="hidden" name="operation" value="change_pwd">
            <input type="hidden" id="id-token" name="token" value="<?php echo $token?>">              
            <div class="form-group">
              <label for="pwd">New password:</label>
              <input type="text" class="form-control" name="password">
            </div>              
            <button type="submit" class="btn btn-success change-pwd" style="float:right">
                <span class="glyphicon glyphicon-ok"></span>
                Change
            </button>            
            <button type="button" class="btn btn-primary" data-dismiss="modal" 
                style="float:left">Cancel</button>           
            <br><br>
         </form>
      </div>
    </div>    
  </div>
</div>


<!-- Modal dialog box for deleting a user user -->
<div class="modal fade" id="userDeleteModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title-del-user">Title</h4>
      </div>
      <div class="modal-body">
         <form action="index.php?content=bs_user_management" method="post" id="deluser">
            <input type="hidden" name="operation" value="del_user">
            <input type="hidden" id="id-token" name="token" value="<?php echo $token?>">                         
            <button type="button" class="btn btn-primary" data-dismiss="modal" 
                style="float:left">Cancel</button>
            <button type="submit" class="btn btn-warning del-user" 
                style="float:right">Delete</button>         
            <br><br>
         </form>
      </div>
    </div>    
  </div>
</div>




