<?php
/**
 * record_list_view_print-table.php
 *
 * Print version of a database record list. All content of each record is output
 * and an HTML table is created that can be used to import the information
 * to Libreoffice CALC!
 * 
 * The process takes URL parameters to decide what kind of list to generate,
 * calls the appropriate method in the database class (which contains the 
 * SQL statement for the query) and displays the resulting record list
 * in HTML table format.
 * 
 *
 * @version    2.0 2019-03-03
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'x-3rd-party-includes/libreoffice-export/lo-calc-export.php';

// Exporting large databases to LibreOffice can require a significant
// amount of memory. The default php memory limit of 128 MB is too small.
// Let's be a bit more generous.
ini_set('memory_limit', '1024M');

$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

$log = new Logging();
$log->lwrite('Document List View - Print Table page accessed');

if (!UserPermissions::hasAccess('export')) {
    $log->lwrite('Export aborted, user does not have export rights');
    return;
}

$lo_export = new LO_Calc_Export;
$log_buf = "";

$banner_text = '';
$doc_list_type = 'empty';

//Find out what kind of list the calling page desires and act accordingly
if (isset($_GET['doc_list_type'])) {
	$doc_list_type = $_GET['doc_list_type'];	
}

// Find out if a case sensitive search is requested
if (isset($_POST['case_sensitive'])) {
	$case_sensitive_search = true;
} else {
	$case_sensitive_search = false;
}

// Use the getListOfRecords() to get a list of docs and to set the banner 
// text and return URL depending on the list type extracted from the GET 
// request.
list ($items, 
	  $banner_text, 
	  $footer_text, 
	  $replacement_term,
	  $fields_and_search_terms) = getListOfRecords($doc_list_type, 
	  		                                       $local_doc_db_description,
	  		                                       $case_sensitive_search);

$str = '<p>' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . ' Overview - Table</p>';  
$lo_export->New_Table_Row();
$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($str);
$lo_export->End_Of_Table_Row();

if (!$items){
	$str = '<p>No ' . DatabaseConfigStorage::getCfgParam('doc-name-ui-plural') . ' found...</p>';
	$lo_export->New_Table_Row();
	$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($str);
	$lo_export->End_Of_Table_Row();
	
	// The document will be empty except for the message but the user
	// should get some output.
	SendLibreofficeCalcDocOverHttp($log, $lo_export);
	exit;		
}

$str = '<p>'. $banner_text . ', ' . count($items) . ' entries found.</p>';	
$lo_export->New_Table_Row();
$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($str);
$lo_export->End_Of_Table_Row();

// At this point we need to find out if the output is to be limited
// to a certain year. If so the function outputs the info into the calc
// file and returns the year in $limit_to_year.
$limit_to_year = CheckAndOutputSingleYearLimitForCalc($log, $lo_export);

// ======================================================================
//
// Generate the table header with the names of all fields that are to
// be exported.
//
// ======================================================================

// If the current user (session) has an addapted database record field
// sort order load it. Otherwise use the default from database-structure.php
$fieldArrayWithOptions = selectFieldsToPrint(true);
if ($fieldArrayWithOptions === false) {
	$log->lwrite('ERROR: Unable to load db_field_order_for_list_output!');
	return;
}

$lo_export->New_Table_Row();

// If the document ID is to be included, print a header for it
if (( 
		(isset($_SESSION['blt_user_includeLink']) &&
	           $_SESSION['blt_user_includeLink'] == true)
    ) 
     	||		
    (
   	    (isset($_SESSION['blt_user_showId']) &&
	           $_SESSION['blt_user_showId'] == true)
   )) {
	
	$str = '<p><strong>ID</strong></p>';
	$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($str);	
}

foreach ($fieldArrayWithOptions as $field_print_name=>$field_options){

	// Don't show the field if it has been configured by the user as
	// invisible
	if ($field_options[SHOW_FIELD] == 0){
		continue;
	}

    $str = '<p><strong>' . $field_print_name . '</strong></p>';
    $log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($str);    

}

$lo_export->End_Of_Table_Row();


// ======================================================================
//
// Now output each document in the search result.
//
// ======================================================================

$found_first_doc_of_year = false;

foreach ($items as $i=>$item){
  	
    if ($limit_to_year != 0) {
        
        if (!IsDocumentNotFromThisYear($item, $limit_to_year,
            $found_first_doc_of_year)) {
                continue;
        }
    }

    $lo_export->New_Table_Row();

	if (isset($_SESSION['blt_user_includeLink']) &&
	 		  $_SESSION['blt_user_includeLink'] == true){		       	
	 		      
		// document ID output with link to database
		$str = '<p>' .		  		
		  	   GetHrefForHashId($item) . 
			   $item->getField('id') .
			   '</a></p>';
        $log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($str);
		 			
	}
	elseif (isset($_SESSION['blt_user_showId']) &&
		          $_SESSION['blt_user_showId'] == true){
		
		// Simple document ID output
		$str =  '<p><b>' . $item->getField('id') .
		        '</b></p>';		
		$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($str);
	}
	 	
	//Output all fields of the current document    
	singleDocLibreOfficeOutputTableRow ($item, $local_doc_db_description, 
	                                    $log_buf, $lo_export); 	    
	$lo_export->End_Of_Table_Row();
		
}

$log->lwrite($log_buf);

// And now write everything into a file and offer it for download
SendLibreofficeCalcDocOverHttp($log, $lo_export);

// DONE!

// Instead of returning at this point, we exit so no additional
// content is sent.
exit();

/**
 * CheckAndOutputSingleYearLimitForCalc()
 *
 * Checks HTTP POST variable if the output is to be limited to a single
 * year. Return the year or 0 if no limit is to be imposed.
 *
 */

function CheckAndOutputSingleYearLimitForCalc($log, &$lo_export) {
      
    $limit_to_year = 0;
    
    // Check if the user wants to limit the display to a certain year
    // (document search by decade/category)
    if (isset($_POST['limit_to_year'])) {
        $limit_to_year = (int) $_POST['limit_to_year'];
        
        if ($limit_to_year != 0) {
            
            $lo_export->New_Table_Row();
            
            $log->lwrite('List limited to ' . $limit_to_year);
            
            $str = '<p><strong>Limiting </strong>list to ' .
                   DatabaseConfigStorage::getCfgParam('doc-name-ui-plural'). ' of ' . $limit_to_year .
                   ' (and ' . DatabaseConfigStorage::getCfgParam('doc-name-ui-plural') .
                   ' without a date immediately after a ' .
                   DatabaseConfigStorage::getCfgParam('doc-name-ui') .' that matches the year)</p><p></p>';
                               
            $log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($str);
            $lo_export->End_Of_Table_Row();
                
            $log->lwrite($log_buf);
        }
    }
    else {
        $limit_to_year = 0;
    }
    
    return $limit_to_year;
}



