<?php
/**
 * bs_test.php
 *
 * For experimental stuff, not linked from anywhere.
 *
 * @version    1.0 2017-02-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-17 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */



$log = new Logging();
$log->lwrite('Test page accessed');

echo $_SERVER['REMOTE_ADDR'] . '<br>';


if (UserPermissions::hasAccess('admin')) {    
    $log->lwrite("ACCESS GRANTED");
} else {
    $log->lwrite("ACCESS NOT GRANTED");
    return;
}
   

return;

?>