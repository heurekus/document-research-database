<?php

/**
 * bs_record_list_view.php
 * 
 * Takes URL parameters to decide what kind of record list to generate,
 * calls the appropriate method in the database class (which contains the 
 * SQL statement for the query) and displays the resulting document list.
 * 
 *
 * @version    1.3 2017-02-05
 * @package    DRDB
 * @copyright  Copyright (c) 2014-17 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'includes/html-tools.php';
require_once 'includes/field-empty-and-trim-functions.php';

define ('NEW_TAB', true);
define ('SAME_TAB', false);

global $list_view_header_small_text_size;

?>
<style>
td {
    padding: 5px;
}

</style>

<script src="js/close_tab.js"></script>
<script src="js/record_list_view_move.js"></script>
<script src="js/record_list_view_export.js"></script>

<div class="container-fluid">

  <div class="row content">
    <div class="col-sm-2 sidenav">      
      <br>

      <ul id="menu_area" class="nav nav-pills nav-stacked custom">
        <li class="active close-tab">
          <a href="#a1" id="menu_text"><span class="glyphicon glyphicon-asterisk"></span> Close tab</a>
        </li>

<?php 
if (UserPermissions::hasAccess('edit')) { 
    echo ('        
        <li class="active">
          <a href="#a2" id="show-move-buttons"> 
             <span class="glyphicon glyphicon-sort"></span> Move 
             a ' . DatabaseConfigStorage::getCfgParam('doc-name-ui') . '</a>                 
        </li>
        ');
}
 
if (UserPermissions::hasAccess('edit')) { 
    echo ('        
                    
        <li class="active">
          <a href="index.php?content=bs_record_update&id=" id="menu_text" 
             target="_blank"><span class="glyphicon glyphicon-plus-sign"></span> 
             Add new ' . DatabaseConfigStorage::getCfgParam('doc-name-ui') . '</a>                 
        </li>
        ');
}

if (UserPermissions::hasAccess('export')) { 
    echo ('
            <li class="active">
              <a href="#a2" class="show-print-export"> 
                 <span class="glyphicon glyphicon-cog"></span> Export</a>
            </li>
        ');
}
       
if (UserPermissions::hasAccess('edit') && UserPermissions::hasAccess('help')) {
    echo ('
        <br>
        <li class="active">
          <a href="#a3" id="list-view-help"
             id="menu_text">
          <span class="glyphicon glyphicon-question-sign"></span> Help on Move</a>
        </li>
    ');
}
?>
                                                 
       </ul>
    </div>
   
   
    <div class="col-sm-10"> 

<?php


$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

$log = new Logging();

$banner_text = '';
$doc_list_type = 'empty';
$extra_fields = false;

echo '<h1>' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . ' Overview</h1>';

// Check if the user has requested a move/insert-after document operation
// and do this before anything else to retrieve a list of documents
// from the database that already reflects this.
if (isset($_GET['move']) 
	&& isset($_POST['doc-move']) 
	&& isset($_POST['doc-after'])) {

	if (UserPermissions::hasAccess('edit')) moveDocRecord($log);
}

// Find out what kind of list the calling page desires and act accordingly
if (isset($_GET['doc_list_type'])) {
	$doc_list_type = $_GET['doc_list_type'];	
}

// Find out if the user wants extra db fields shown for each record found
if (isset($_POST['extra_fields'])) {
	$extra_fields = true;
	$log->lwrite('User wants to see extra fields');
}

// Find out if a case sensitive search is requested
if (isset($_POST['case_sensitive'])) {
	$case_sensitive_search = true;
} else {
	$case_sensitive_search = false;
}

// Use the getListOfRecords() to get a list of documents and to set the 
// banner text and return URL depending on the list type extracted from 
// the GET request. Also, if it was a search, the search term and, in 
// case it was a search/replace a replacement term.
list ($items, 
	  $banner_text, 
	  $footer_text, 
	  $replacement_term,
	  $fields_and_search_terms) = 
      getListOfRecords($doc_list_type, 
      		           $local_doc_db_description,
      		           $case_sensitive_search);

// Debug output of field and search terms to the log file
if (count($fields_and_search_terms) > 0) {
	$log->lwrite('bs_record_list_view.php() - Search Term(s) and Field(s): ');	
	
	foreach ($fields_and_search_terms as $field_num => $cur_field):	
		$field_name = $cur_field[0];
		$field_print_name = $cur_field[1];
		$search_term = $cur_field[2];
		
		$log->lwrite('bs_record_list_view.php() - ' . $field_name . ', ' .
				     $field_print_name . ', ' . $search_term);		
	endforeach;	
}

if (strlen($replacement_term) > 0) {
	$log->lwrite('bs_record_list_view.php() - Replacement Term: ' .
			$replacement_term);
}

$remove_style = trim(filter_input(INPUT_POST,
		'style_change', FILTER_SANITIZE_STRING));

// Check if search/replace should also add italics during replacement and add
// to the replacment term.
if (strcmp($remove_style, 'add_italics') == 0) {
	
	$replacement_term = '<em>' . $replacement_term . '</em>';
	$log->lwrite('bs_record_list_view.php() - Adding italics to the ' .
			'replacement term');	
}

// Check if search/replace should also be made bold during replacement and add
// to the replacment term.
if (strcmp($remove_style, 'add_bold') == 0) {
	$replacement_term = '<strong>' . $replacement_term . '</strong>';
	$log->lwrite('bs_record_list_view.php() - Adding bold to the ' .
			'replacement term');
	
}

// Note: Removal of bold / italcis is treated further below because removal
// not only affects the replacement term but the surrounding text in the
// overall string!

// Check if this is a search/replace call and the replace was commited
// by the user
$replace_commit = '';
if (isset($_GET['replace_commit'])) {
	$replace_commit = $_GET['replace_commit'];
}

// Prevent replacement if user has no 'edit' rights
if (!UserPermissions::hasAccess('edit')){
    $log->lwrite('User has no editing rights, search/replace commit aborted');
    $replace_commit = '';
}

if (strlen($replace_commit) > 0) {
	$log->lwrite('bs_record_list_view.php() - Replace Commit: ' .
			$replace_commit);
}

if ($items === false) {
	
	echo '<div class="alert alert-danger">';
	echo '<strong>'. $banner_text . ':</strong> ' . ' Database Query Error! ' . 
	     'Please contact your administrator!';
	echo '</div>';
	exit;
		
} else if (!$items){

	echo '<div class="alert alert-warning">';
	echo '<strong>'. $banner_text . ':</strong> ' . ' No matches found!';
	echo '</div>';
	echo '</div>'; // end of the panel
	return;
		
} else {
	
	echo '<div class="alert alert-success">';
	echo '<strong>'. $banner_text . ':</strong> ' . count($items). ' entries found.';
	echo '</div>';
}

echo PHP_EOL;

// Put document token used by Javascript on the client side into an invisible
// page element
$token = createOrReuseSecurityToken();
echo '<span id="doc_token" style="display:none">'. $token .
'</span>' . PHP_EOL;

// Check if the user wants to limit the display to a certain year
// (document search by decade/category)
//
// IMPORTANT: This option must NOT be combined with search/replace
// because the replace action is done before limiting the view with this
// variable and hence, database records not shown would be modified!

if (isset($_POST['limit_to_year'])) {
	$limit_to_year = (int) $_POST['limit_to_year'];
	
	if ($limit_to_year != 0) {
		$log->lwrite('bs_record_list_view.php() - List limited to ' .
				$limit_to_year);
		
		echo '<div class="alert alert-success">';
		echo '<strong>Limiting </strong> list to ' . 
		     DatabaseConfigStorage::getCfgParam('doc-name-ui-plural'). ' of ' . $limit_to_year .
		     ' (and ' . DatabaseConfigStorage::getCfgParam('doc-name-ui-plural') . 
		     ' without a date immediately after a ' . 
		     DatabaseConfigStorage::getCfgParam('doc-name-ui') .' that matches the year)';
		echo '</div>';		
	}
} 
else {
	$limit_to_year = 0;
}

$found_first_doc_of_year = false;
	 

// Now output each database record found
foreach ($items as $i=>$item):
 	
	//Get the content of the first db field and remove all HTML paragraph tags
    $content = generateHeaderText($item);
	$content = str_ireplace('<P>', '', $content);
 	$content = str_ireplace('</P>', '', $content);
 	$content = strip_tags($content);	

 	// If output of found documents is limited to a certain year
 	if ($limit_to_year != 0) {
 	    	
    	//Is the current document of this year?
    	if (strpos($content, (string) $limit_to_year) !== false) {
 	    		
    		// Mark that a document has been found. This is necessary to
    		// later also count documents without a year in the first db field
    		// as belonging to the year
			$found_first_doc_of_year = true;
		}
 	    elseif (($content == "" || $content == "----") &&
 	    		$found_first_doc_of_year == true) {
 	    		
 	    	// The first field is empty but follows a previous document of 
 	    	// the year to limit the output to
 	    	$found_first_doc_of_year = true;
 	    }
 	    else {
 	    		
 	    	// The document does not belong to the year, don't count
 	    	// subsequent empty ones to this year.
 	    	$found_first_doc_of_year = false;
 	    		
 	    	// Don't show this document, continue with next loop cycle
 	    	continue;
 	    } 	    		
 	} // end of limit to a certain year
 	    	    
	if (strlen($content) < 3) $content = "---";
	    
	echo '<div class="panel-group">';
	echo '<div class="panel panel-info">';
	echo '<div class="panel-heading">';	    
	    
	if ($list_view_header_small_text_size != true) echo '<h3>';  

  	// Create invisible "Move" and "Insert After" buttons
  	echo '<span class="ms-move" style="display:none" ' . 
         'id="' . $item->getField('id') . '" ' . 
         'docName="' . strip_tags($item->getField(getDbFirstFieldName())) . '">' .
         '<button type="button" class="btn btn-warning">' .
  	     '<span class="glyphicon glyphicon-scissors"></span>&nbsp;' . 
  	     'Move</button>&nbsp;</span>';

  	echo '<span class="ms-insert-after" style="display:none" ' . 
  	     'id="' . $item->getField('id') . '" ' . 
  	     'docName="' . strip_tags($item->getField(getDbFirstFieldName())) . '">' .
  	     '<button type="button" class="btn btn-success">' .
  	     '<span class="glyphicon glyphicon-paste"></span>&nbsp;' . 
  	     'Insert After This</button>&nbsp;</span>';
  	     	  	 
  	// Create the "Details" button + Nummerotation in large font  	      	 
	echo '<a class="btn btn-primary" role="button" ' . 
	     'href="index.php?content=bs_record_details&id=' . 	          
	     $item->getField('id') . 
  	     '" target="_blank">Details <span class="glyphicon '. 
  	     'glyphicon-chevron-right"></span></a> '. $content;

	if ($list_view_header_small_text_size != true) echo '</h3>';
  	      	    
  	echo '  </div>'; //end of panel-heading
  	echo '<div class="panel-body">';  	     
 	
  	// If the current user (session) has an adapted database record field
  	// sort order load it. Otherwise use the default from database-structure.php
  	if (isset($_SESSION['db_field_order_for_list_output'])){
  		$fieldArrayWithOptions = $_SESSION['db_field_order_for_list_output'];
  		//$log->lwrite('bs_record_list_view.php: Using user specific field order');
  	}
  	else {
  		$fieldArrayWithOptions = $local_doc_db_description;
  		
  		// Initialize all fields as NOT shown!
  		// Note: $field options are by reference due to the '&'
  		foreach ($fieldArrayWithOptions as $field_print_name=>&$field_options):
  			$field_options[SHOW_FIELD] = 0;
  		endforeach;
  		// the following unset() is required update the reference of the
  		// last array element. This is not a bug, it's a feature.
  		//--> see "warning" in:
  		// http://php.net/manual/en/control-structures.foreach.php
  		unset($field_options);  		
  		
  		$_SESSION['db_field_order_for_list_output'] = $fieldArrayWithOptions;
  		$log->lwrite('bs_record_list_view.php: Using DEFAULT field order');
  	}
  	
  	// For each field of the database record
  	foreach ($fieldArrayWithOptions as $field_print_name=>$field_options):
  	
  	    // Special output if the current field is part of a search and 
  	    // potential replace action
  		$isSpecialField = checkAndOutputFieldWithSearchTerm ($item,
  		                                   	 $field_options,
  			                                 $fields_and_search_terms,
  			                                 $replace_commit,
  				                             $replacement_term,
  				                             $case_sensitive_search,
  			                                 $log);
  	
  		// Don't output other db record fields if already done above
  		// or if this is a search / replace
 		if (
 			($isSpecialField == true) or 
 			(strlen($replacement_term) > 0) or 
 			($extra_fields == false)
 		   ) {
 			continue;
 		}

  	    // A normal field, no special search (and potential replace)
 		outputCurrentField ($field_options, $field_print_name, $item);
		
  	
  	endforeach; // end of loop through all fields of a database record
  	 	  	
  	     	    
  	echo '</div>'; //panel-body  	    
  	echo '</div>'; //end of pannel
  	echo '</div>'; //end of pannel group

  	echo PHP_EOL;
  	    
endforeach; // end of output of database records

echo PHP_EOL;
echo '<br>';
echo $footer_text;
echo '<br>';

// If the user can trigger a replacement commit, create a button
if (strlen($replacement_term) > 0 && strlen($replace_commit) == 0) {
	
	createHTTPPostButton(
			'COMMIT Replacement Text NOW!',
			SAME_TAB, 
			'index.php?content=bs_record_list_view&replace_commit=yes' . 
			'&doc_list_type=' . $doc_list_type);	
	echo '<br>';
}
else // not a search commit page request
{
	// Output a button to EXPORT the list to Libreoffice
	
    if (UserPermissions::hasAccess('export')) {
        echo '&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary show-print-export">' .
            '<span class="glyphicon glyphicon-cog"></span> ' .
            'Print or Export to LibreOffice</button>';        
    }
	
	echo "<br><br>";
}


/**
 * outputCurrentField()
 *
 * Output the current field if the user has not disabled the field for output. 
 *
 */

function outputCurrentField ($field_options,
		                     $field_print_name,
		                     $item) {

	// If the field is configured not to be shown, advance to next field
	if ($field_options[SHOW_FIELD] != true) return;
	 
	// Get the current field content with the database field name
	// stored in $field_options[DB_FIELD_NAME]
	$output_string = $item->getField($field_options[DB_FIELD_NAME]);
	
	// if the field is empty (but the user still wants to see it,
	// display a few dashes instead so the user can click on them to edit
	// the field.
	if (isFieldEmpty($output_string) == true) {
		$output_string = "<p>-----</p>";
	}
	
	// ===> Move this to html-tools.php!!!!! (also in bs_record_details.php)
	// Replace CR + LF with a HTML <BR>
	$output_string = preg_replace("/\r\n?/", "\n", $output_string);
	$output_string = str_replace("\n", '<BR>', $output_string);
	
	// Make URLs clickable
	//
	// Note: This just changes the output in the web browser, the text
	// of the field is unchanged in the database. If the user edits the field
	// by double clicking on it, ckeditor removes the clickable URL again
	// as the "link" plug-in is not activated and hence URLs can't be
	// embedded. This is intentional as the URL should be shown as plain text
	// because the ordinary user would change the text of the URL but not
	// the URL itself which would lead to inconsistency.
	
	// ===> Move this to html-tools.php!!!!! (also in bs_record_details.php)
	$output_string = preg_replace('!(http)(s)?:\/\/[a-zA-Z0-9\-.?:%=&_/#\+\',_();~\\\\]+!',
	  			"<a target=\"_blank\" href=\"\\0\">\\0</a>",
	  			$output_string);
		
	echo PHP_EOL;
	
	echo '<div class="panel-group">';
	echo '<div class="panel panel-info">';
	echo '<div class="panel-heading">';
	
	/*
	echo '<div dir="ltr" id="dir_' . $field_options[DB_FIELD_NAME] . '"></div>';
	echo PHP_EOL;
	 */
	
	echo '  <strong>' . $field_print_name . ': </strong>';

	echo '  </div>'; //end of panel-heading
	echo PHP_EOL;
	echo '<div class="panel-body">';
	
	echo $output_string;
	
	echo '</div>'; //panel-body
	echo '</div>'; //end of pannel
	echo '</div>'; //end of pannel group
	
}

/**
 * checkAndOutputFieldWithSearchTerm()
 *
 * This function checks if the current field of a database record was
 * searched in.
 * 
 * If not, the function returns false to let the calling function know that
 * no output for this field was done.
 * 
 * If the field of the database record was searched in, make an output of 
 * where the search term is marked in that field. If in addition a replacement 
 * term was also given, output this as well. At the end return true to let
 * the calling function now and output was made for this field.
 *
 */

function checkAndOutputFieldWithSearchTerm ($item,
		                                    $field_options,
		                                    $fields_and_search_terms, 
		                                    $replace_commit,
		                                    $replacement_term,
		                                    $case_sensitive_search,
		                                    $log){

	$output_made = false;
	
	// Return straight away if there are no searches
	if (count($fields_and_search_terms) < 1) {
		return $output_made;
	}
	
	// $log->lwrite('Checking if current field is searched/replaced...');
	
	// get the current database field name of the current database record
	$db_field_name = $field_options[DB_FIELD_NAME];
	
	// Loop over all search-term / field combinations
	foreach ($fields_and_search_terms as $field_num => $cur_field):

		$search_field_name = $cur_field[0];
		$field_print_name = $cur_field[1];
		$search_term = $cur_field[2];
		
		// $log->lwrite(' DB Field Name: ' . $db_field_name . 'Search Fieldname: ' . $search_field_name);

		// If current search filed name is not the the current database field 
		if ($search_field_name != $db_field_name) {
			continue;
		}		
		
		// Get the content of the current field
		$output_string = $item->getField($search_field_name);
		
		// $log->lwrite('This field is searched/replaced');
		 
		// If the search term is in the current field
		// Search can be case sensitive or case in-sensitive		
		if ($case_sensitive_search === false) {
			// case in-sensitive
			$is_search_term_in_cur_field = stripos($output_string, $search_term);
		} else {
			// case sensitive
			$is_search_term_in_cur_field = strpos($output_string, $search_term);				
		}	
		
		if ($is_search_term_in_cur_field !== false) {
	
			// If not yet commited, show current field content with the
			// search term(s) highlighted. If a replacement term (single!) is
			// given the function also shows how the field would look
			// after the replacement.
			if (strcmp($replace_commit, "yes") != 0) {
				markAndOutputText($output_string, $field_print_name,
						          $search_term, $replacement_term, 
						          $case_sensitive_search, $log);
			}
			else {
				// We are in the replacement comit phase now!
				commitReplaceText($item, $output_string,
						          $search_field_name, $search_term,
						          $replacement_term, $case_sensitive_search,
						          $log);
			} // end of replacment phase
			
			$output_made = true;
			
		} // end of search term is in the current field

	endforeach; // End of loop over all search-term / field combinations

	return($output_made);
}

/**
 * markAndOutputText()
 *
 * Encapsulates local functionality to mark the search term in a text and
 * output it. 
 * 
 * In addition, if a replacement term (only one!) is given, the function does a 
 * search/replace with the replacement term show it as well. If the user has 
 * selected removing bold or italics formatting this function removes those 
 * styles around the search term as well.
 *
 */
function markAndOutputText ($output_string, $field_print_name, $search_term, 
		           $replacement_term, $case_sensitive_search, $log)
{
	echo '<div class="panel-group">';
	echo '<div class="panel panel-success">';
	echo '<div class="panel-heading">';
			
	echo '<p><strong>Search term found in \'' .
			$field_print_name . '\':</strong> ';
		
	// Replace CR + LF with a HTML <BR>
	$output_string = preg_replace("/\r\n?/", "\n",
			$output_string);
	$output_string = str_replace("\n", '<BR>',
			$output_string);
	

	echo '</div>'; //end of panel-heading
	echo PHP_EOL;
	echo '<div class="panel-body">';
	
	
  	// Mark all instances of the search term in the field in blue	    	    		
  	// CASE IN-SENSITIVE POSITION SEARCH
  	//
  	// Note: A do/while loop is required here as a single str_ireplace() call
  	// that could change all occurences at once + add the color html markup
  	// would not keep the uppler/lower case character use of the original 
  	// because the search is case insensitive!
  	$search_offset = 0;  
  	$pre_text = '<span id="none" style="color:#3385ff">';
  	$post_text = '</span>';
  	do {
  		
  		// Case sensitive or in-sensitive search
  		if ($case_sensitive_search == false) {
  			$mark_pos = stripos ($output_string, $search_term, $search_offset);
  		}
  		else {
  			$mark_pos = strpos ($output_string, $search_term, $search_offset);
  		}  		
  		
  		if ($mark_pos !== false){
  				
  			//$log = new Logging();
  			//$log->lwrite('BEFORE MARKING: ' . $output_string);
  				
  			// Insert a color span, start with the </span>
  			// element at the end of the string
  			$output_string = substr_replace($output_string,
  					$post_text, $mark_pos + strlen($search_term), 0);
  				
  			//$log->lwrite('AFTER MARKING 1: ' . $output_string);
  		
  			// Now insert the front part
  			$output_string = substr_replace($output_string, $pre_text,
  					                        $mark_pos, 0);
  				
  			//$log->lwrite('AFTER MARKING 2: ' . $output_string);
  		
  			// In the next iteration search after the found position
  			$search_offset = $mark_pos + strlen($pre_text) + 1;
  		}
  	} while ($mark_pos !== false);
	
	echo $output_string;
	
	echo PHP_EOL;
	echo '</div>'; //panel-body
	echo '</div>'; //end of pannel
	echo '</div>'; //end of pannel group

	
	// If a replacement term is given and we are not in the
	// commit phase, show how the field would look like
	// after the replacement
	if (strlen($replacement_term) > 0) {
		
		echo '<div class="panel-group">';
		echo '<div class="panel panel-warning">';
		echo '<div class="panel-heading">';
				
		echo "<p><strong>Draft with replacement " .
				"(marked in blue):</strong></p>";
	
		echo '</div>'; //end of panel-heading

		echo '<div class="panel-body">';
		
		// Replace the search term with the replacement_term
		// The blue marking has already been done above
		// by marking the search term in blue.
		// Search can be case senstive or in-senstive (str_ireplace)!	
		if ($case_sensitive_search == false) {
			$output_string = str_ireplace($search_term,
					$replacement_term,
					$output_string);		
		}
		else {
			$output_string = str_replace($search_term,
					$replacement_term,
					$output_string);			
		}

		//Remove style around the replacement term if required
		$remove_style = trim(filter_input(INPUT_POST,
				'style_change', FILTER_SANITIZE_STRING));
			
		if (strcmp($remove_style, 'remove_italics') == 0) {
		
			$log->lwrite('Removing italics style around the search term');
			$output_string = htmlRemoveAllInstancesOfEncapsTag($output_string,
					$replacement_term, 'em', $case_sensitive_search);
		}
			
		if (strcmp($remove_style, 'remove_bold') == 0) {
			$log->lwrite('Removing bold style around the search term');
			$output_string = htmlRemoveAllInstancesOfEncapsTag($output_string,
					$replacement_term, 'strong', $case_sensitive_search);
		}
		
	
		echo $output_string;
		
		echo '</div>'; //panel-body
		echo '</div>'; //end of pannel
		echo '</div>'; //end of pannel group
		
	} // End of show field with replaced text
	
}

/**
 * commitReplaceText
 *
 * Encapsulates functionality to change text in a database field in
 * the database field
 *
 */
function commitReplaceText($item, $output_string, $field_name, 
		                   $search_term, $replacement_term, 
		                   $case_sensitive_search, $log)
{
	// Run web security checks
	if (checkAntiCsrfProtection() == false) {
		echo "<h1>There was a security error, commit aborted!</h1>";
		exit;
	}
			 
    // Do a search and replace case sensitive or in-sensitive
	if ($case_sensitive_search == false) {
		$output_string = str_ireplace($search_term, $replacement_term, 
			                          $output_string);			
		//$log->lwrite('CASE IN-sensitive commit!');	
	}
	else {
		$output_string = str_replace($search_term, $replacement_term,
				$output_string);			
		//$log->lwrite('CASE SENSITIVE commit!');				
	}	
	
	$remove_style = trim(filter_input(INPUT_POST,
			'style_change', FILTER_SANITIZE_STRING));
	
	if (strcmp($remove_style, 'remove_italics') == 0) {
	
		$log->lwrite('Removing italics style around the search term');
		$output_string = htmlRemoveAllInstancesOfEncapsTag($output_string,
				$replacement_term, 'em', $case_sensitive_search);
	}
	 
	if (strcmp($remove_style, 'remove_bold') == 0) {
		$log->lwrite('Removing bold style around the search term');
		$output_string = htmlRemoveAllInstancesOfEncapsTag($output_string,
				$replacement_term, 'strong', $case_sensitive_search);
	}	

	$log->lwrite("Committing Search/Replace in " . $item->getId());
	
	// Lock the database record
	$lock_result = lockDatabaseRecord($item);
	
	if ($lock_result[0] != true){
		$log->lwrite("bs_record_list_view.php: Unable to lock the record");
		$log->lwrite('Reason: ' . $lock_result[1]);
		
		echo '<div class="alert alert-danger">';
		echo 'Field not changed! ' . $lock_result[1];
		echo '</div>';
		
	    return;
	}
	
	// commit the values
	$write_result = $item->editSingleFieldInRecord($field_name, $output_string, 
			                                       true);
	
	// Unlock the database record
	unlockDatabaseRecord($item->getId());
	
	// Also write the updated field content into the modification
	// history table
	ModHistoryStorage::saveFieldContent($item->getId(), $field_name, $output_string);

	
	$log->lwrite("Field: " . $field_name);
	$log->lwrite("Content: ". $output_string);
	$log->lwrite("Result: " . $write_result[1]);
	
	echo '<div class="alert alert-success">';
	echo $write_result[1];
	echo '</div>';
}

/**
 * moveDocRecord
 *
 * Changes the ID of a document in the database (move/insert-after)
 * operation execution.
 *
 */
function moveDocRecord($log) {
	
	$b_id_to_move = (int) $_POST['doc-move'];
	$b_id_to_insert_after = (int) $_POST['doc-after'];
	
	// Unset POST variables to prevent them being present twice if the
	// user does another move/insert-after
	unset($_POST['doc-move']);
	unset($_POST['doc-after']);
		
	$result = moveRecordAfterID($b_id_to_move, $b_id_to_insert_after);
	
	echo '<br>';
	if ($result[0] === true ){
		echo '<div class="alert alert-success">';
	}else {
		echo '<div class="alert alert-danger">';		 
	}
		
	echo '<strong>' . $result[1] . '</strong>';
	echo '</div>';
}

/**
 * createHTTPPostButton
 *
 * Creates a button on the web page that will load a new
 * page with all POST variables that were used to load the
 * current page. To do so all POST arguments from the previoius iteration 
 * will be put in invisible elements.
 * 
 * @param string  $button_text Text for the button
 * @param boolean $new_tab Load next page in the same or a new tab
 * @param string  $get_arguments The URL (e.g. starting with index.php + get arguments)
 * @param array   $extra_post_arguments Optional extra HTTP POST arguments as a
 *        named array.
 *
 * @return null
 */

function createHTTPPostButton($button_text,
						 	  $new_tab,
		                      $get_arguments,
		                      $extra_post_arguments = NULL) {

	// create token if not already done in a previous iteration or somewhere
	// else by another action. The token is sent when the "save" button is
	// pressed and then analyzed in the next iteration of the bs_doc_list_
	// view invocation during the commit phase! This is done to prevent
	// Cross Site Request Forgery (CSRF) attacks.
	$token = createOrReuseSecurityToken();
	
	// Create option string to open a new tab
	if ($new_tab == NEW_TAB) {
		$tab_option = 'target="_blank"';
	}
	else {
		$tab_option = '';		
	}

	echo '<form action="' . $get_arguments . '" ' . 
			'method="post" name="doc_list_view" id="doc_list_view" '.
			$tab_option . 
			'>';

	// Add exisiting POST arguments
	foreach($_POST as $key => $value) {
		
		// Convert apostrophe (') chars to html escaped character
		// as these chars break HTML output by ending the string...
		$value = fixApostrophe($value); 
		
		echo "    <input type='hidden' name='" . $key . "' value='" .
				$value . "' />";
	}

	// Add extra POST arguments
	if (is_array($extra_post_arguments)) {
		foreach($extra_post_arguments as $key => $value) {
			echo "    <input type='hidden' name='" . $key . "' value='" .
					$value . "' />";
		}
	}

	// Insert hidden security token
	echo "<input type='hidden' name='token' value='" . $token. "'/>";

	
	/*
	// Visible submit button with text. Traditional output button, 
	// no bootstrap glyphs as part of the button text possible
	echo "<input class='btn btn-primary' role='button' type='submit' " .
		 "value='" . $button_text . "' />";
    */
	
	// Visible submit button with text. This version allows bootstraph glyphs 
	// as part of the button text.
	echo '<button type="submit" class="btn btn-primary">' .
	     $button_text . '</button>';
	
	echo "</form>";
}

/**
 * generateHeaderText
 *
 * Generates the header text for each entry to show in the list. The
 * default is to use the content of the first field of a record. If
 * $list_view_header_fields is defined in database-structure.php, the 
 * content of several fields are used.
 *
 * @param database record to use
 *
 * @return string text for the header
 */

function generateHeaderText($item) {
    
    GLOBAL $list_view_header_fields;
    
    // Use first field of a database record if nothing else
    // is defined in the database-structure.php config file.
    if (!isset($list_view_header_fields) or
        count($list_view_header_fields) < 1) {
        return $item->getField(getDbFirstFieldName());
    }
    
    // Assemble content of the header from all fields described
    // in the configuration array    
    $content = "";   
    foreach ($list_view_header_fields as $field_name):
        $content = $content . $item->getField($field_name) .  " --- ";
    endforeach;
    
    $content = rtrim($content, " --- ");

    return $content;
}



?>

    </div> <!-- end of right column -->
  </div> <!-- end of row -->
</div> <!-- end of container -->


<!-- Modal dialog box for 'move document' functionality -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please Confirm Move/Insert Command</h4>
      </div>
      <div class="modal-body">
        <p id="modal-replace-text">x</p>
      </div>
      <div class="modal-footer">      
        <button type="button" class="btn btn-default" data-dismiss="modal" 
                style="float:left">Abort</button>      
        <?php
        // Post button for the modal dialog box with place holder POST
        // variables 'move' and 'target' which will be filled by the JS
        // code below!      		
        createHTTPPostButton(
        	'Confirm',
        	SAME_TAB,
        	'index.php?content=bs_record_list_view&move=yes&doc_list_type=' . 
        	$doc_list_type,
        	array('doc-move' => 'x',
        		  'doc-after' => 'y'));        
        ?>   
      </div>
    </div>
    
  </div>
</div>

<!-- Modal dialog box for the 'Export configuration' functionality -->
<div class="modal fade" id="myExportModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Libreoffice Export Configuration</h4>
      </div>
      <div class="modal-body">
        <p><strong>Note:</strong> To configure the order and visibility of 
        fields, go back to the search request page and click on the "Select fields" button 
        at the end of the search dialog. If this button is not present in the original
        search request page, open another search dialog and modify it there!</p> 
        <form>
        <label class="checkbox-inline">
           <input type="checkbox" class="show-id ms-save-export-settings" value="">Show ID
        </label><br><br>
        <label class="checkbox-inline">
           <input type="checkbox" class="include-link ms-save-export-settings" value="">
           <?php echo ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui'));?> ID with link
        </label><br><br>
        <label class="checkbox-inline">
           <input type="checkbox" class="page-break ms-save-export-settings" value="">
           Page break between <?php echo DatabaseConfigStorage::getCfgParam('doc-name-ui-plural');?>
        </label>
        </form>
        
      </div>
      <div class="modal-footer">      
                       
      <table><tr>
      
      <td>              
      <button type="button" class="btn btn-default" data-dismiss="modal" 
                style="float:right">Cancel</button>        
      </td>
<?php                  
	// Generate Libreoffice export buttons, one for the quick print 
	// functionality without extra formatting, a list export to 
	// Writer, and one for HTML table export for Calc.
	// Put all POST variables from this page into the POST request for the 
	// link the button leads to the printout page.
	//
	// In addition, a third button is generated for the "quick print" html-only
	// output so the list can be printed directly from the browser with limited
	// formatting.
	
    echo '<td>';

    // TODO: Change link to a index-raw variant so LO file downloads correctly
    createHTTPPostButton(
    		'<span class="glyphicon glyphicon-export"></span>' .
    		' Quick Print',
    		NEW_TAB,
    		'index-print-raw.php?content=record_list_view_print' .
    		'&doc_list_type=' . $doc_list_type . '&quick_print=1');
    echo '</td><td>';	
	
    // TODO: Change link to a index-raw variant so LO file downloads correctly
	createHTTPPostButton(
			'<span class="glyphicon glyphicon-export"></span>' . 
			' Writer',
			false,
			'index-print-raw.php?content=record_list_view_print' .
			'&doc_list_type=' . $doc_list_type );
	echo '</td><td>';
	
	createHTTPPostButton(
			'<span class="glyphicon glyphicon-export"></span>' .
			' Calc',
			NEW_TAB,
			'index-print.php?content=record_list_view_print-table' .
			'&doc_list_type=' . $doc_list_type );
	echo '</td>';	
	
	if (UserPermissions::hasAccess('help')) {
    	echo ('        
            <td>
              <a id="export-help" href="#" 
                 class="btn btn-primary" role="button">
                 <span class="glyphicon glyphicon-question-sign"></span>
                 Help
              </a>    	    	
        	</td>
             ');
	}
?>        

      </tr></table>
                
      </div>
    </div>
    
  </div>
</div>
