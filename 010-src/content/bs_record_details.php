<?php
/**
 * bs_record_details.php
 *
 * Show all fields of a database record. Javascript code is used to
 * act on a double click on field text and replaces the text with a 
 * CKEDIT text box and save button.
 *
 * @version    2.0 2017 02 05
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

?>
<style>
.tooltip-inner {
 text-align:left;
}
</style>

<!-- Decrease padding in panels to reduce vertical size requirement of fields -->
<style>
.panel .panel-heading {
    padding:3px;
}

.panel-body {
    padding-top: 5px;
    padding-bottom: 1px;
}

</style>

<script type="text/javascript" src="/jquery.touch.swipe/jquery.touchSwipe.min.js"></script>
<script src="/ckeditor/ckeditor.js"></script>

<script src="js/record_details_help.js"></script>
<script src="js/record_details_edit_field.js"></script>
<script src="js/record_details_del.js"></script>
<script src="js/record_details_edit_all.js"></script>
<script src="js/record_details_keys_and_swipe.js"></script>
<script src="js/record_details_load_unload.js"></script>
<script src="js/record_details_permalink.js"></script>
<script src="js/change-doc-id.js"></script>
<script src="js/init_tooltips.js"></script>

<?php  

require_once 'includes/field-empty-and-trim-functions.php';

$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

// Create a new object for displaying and handling conventions info icons
$conventions_info = new ConventionsInfo();

$log = new Logging();

echo PHP_EOL;
echo PHP_EOL;

// Get the $id and the corresponding $item from the database
// requested via the URL.
$id =0; $item= null;
if (!initRecordVariables($id, $item, $log)) return;

// Check if there was an error writing this record to the database before
// this module was called
if (!checkDatabaseWriteSuccess($log)) return;

// Find out if the previous or next document was requested in the URL
// and load the corresponding record into an object
$resRetrieval = loadPreviousOrNextRecord($id, $item);

writeAccessTimeToDbRecord($item);

echo '<div class="container-fluid">';
echo '<div class="row content">';

echo createSideNavigationPanel($id);  
      
echo '<div class="col-sm-10">';

// Get a security token that can be inserted into this web page for POST
// actions to prevent Cross Site Request Forgeries.
$token = createOrReuseSecurityToken();

// Put a number of variables used by Javascript on the client side into 
// invisible page elements
echo '<span id="doc_token" style="display:none">'. $token . 
     '</span>' . PHP_EOL;
echo '<span id="doc_db_id" style="display:none">' . $id . 
     '</span>' . PHP_EOL;
echo '<span id="doc_load_time" style="display:none">' . time() .
'</span>' . PHP_EOL;

$is_convention_record = ($id == CONVENTIONS_RECORD_ID)? 'true' : 'false';
echo '<span id="is_convention_record" style="display:none">'. $is_convention_record;
echo '</span>' . PHP_EOL;

// The content of the first field in the database is used for the document 
// title shown in the text that is changed with a Javascript instruction once 
// the page is ready. If this is the 'convention' record, show a default
// text instead.
$title_text = strip_tags($item->getField(getDbFirstFieldName()));
if (strlen($title_text) == 0) {
	$title_text = '---';
}

if ($id == CONVENTIONS_RECORD_ID) {
	$title_text = 'CONVENTIONS';
}

echo '<span id="title-content" style="display:none">' . 
     $title_text . '</span>' . PHP_EOL;

echo PHP_EOL;
echo PHP_EOL;

echo '<div id="start_after_swipe"></div>';

if ($id != CONVENTIONS_RECORD_ID) {
	echo "<h3>" . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . " Details";
}
else {
	echo "<h3>CONVENTIONS";	
}
echo '</h3>';

echo '<span class="label label-default">';
echo "ID: " . $id;
echo '</span>';
echo '<p></p>';

if ($resRetrieval != '') {
	//Note alert-info alerts automatically fade out after a few seconds
	//The required jquery function (on document ready) can be found above!
	echo '<div class="alert alert-danger ms-fade" role="alert">';
	echo '<strong>Alert: </strong>' . $resRetrieval;
	echo '</div>';	
}

echo '<div class="alert alert-info" role="alert" id="info-text">';
if (UserPermissions::hasAccess('edit')) {
    echo 'Note: Double click on text to edit. ';
}
if ($id != CONVENTIONS_RECORD_ID) {
	echo 'Use left and right arrow keys to move between ' .
	   	 DatabaseConfigStorage::getCfgParam('doc-name-ui-plural') . '.';
}
echo '</div>';	

// Check whether to show or hide empty fields
// The presence of the HTTP GET parameter 'hide_empty' toggles
// the state of the session session variable with the same name
// between true and false.  

if (!isset($_SESSION['blt_user_hide_empty'])) {
	$_SESSION['blt_user_hide_empty'] = false;
}

if (isset($_GET['hide_empty'])){
	if ($_SESSION['blt_user_hide_empty'] == false) {
		$_SESSION['blt_user_hide_empty'] = true;
	}
	else {
		$_SESSION['blt_user_hide_empty'] = false;
	}
	
	// Session variable content has changed, save it!
	UserConfigStorage::saveUserConfig();	
}

// Check whether to override the settings to show only selected fields
// and instead show all fields. This is done by checking if the URL
// contains the parameter 'temp_toggle_view_all' and if so
// toggles the value of session 'session_temp_toggle_view_all'. If the
// parameter is not present, disable the feature.

if (!isset($_SESSION['session_temp_toggle_view_all'])) {
	$_SESSION['session_temp_toggle_view_all'] = false;
}

if (isset($_GET['temp_toggle_view_all'])){
	if ($_SESSION['session_temp_toggle_view_all'] == false) {
		$_SESSION['session_temp_toggle_view_all'] = true;
	}
	else {
		$_SESSION['session_temp_toggle_view_all'] = false;
	}

} else {	
	// If no parameter in URL is given, disable the feature if it is active
	if ($_SESSION['session_temp_toggle_view_all'] == true) {
		$_SESSION['session_temp_toggle_view_all'] = false;
		
	}
}

// If the current user (session) has an addapted database record field
// sort order load it. Otherwise use the default from database-structure.php
if (isset($_SESSION['db_field_order'])){
	$fieldArrayWithOptions = $_SESSION['db_field_order'];
	$log->lwrite('bs_record_details.php: Using customized field display order');
}
else {
	$fieldArrayWithOptions = $local_doc_db_description;
}

echo '<div id="swipe">';

foreach ($fieldArrayWithOptions as $field_print_name=>$field_options):

    $field_name = $field_options[DB_FIELD_NAME];

	// Get the content of the current database field of the current record
    $output_string = $item->getField($field_name);

	// Decide whether to show the field or not
	if (showCurrentField ($id, $output_string, $field_options) == false) continue;	

	$output_string = prepareFieldTextForOutput($output_string);
	
	///////////////////////////////////////////////////////////////	
	// START: Output field name and content
	///////////////////////////////////////////////////////////////
	
    echo PHP_EOL;

    echo '<div class="panel-group">';
    
    if ($id != CONVENTIONS_RECORD_ID) {
    	echo '<div class="panel panel-info">';
    } else {
    	echo '<div class="panel panel-default">';
    }
    
    echo '<div class="panel-heading">';
           
    echo '<div dir="ltr" id="dir_' . $field_name . '"></div>';
    echo PHP_EOL;    
    
    if ($id != CONVENTIONS_RECORD_ID){
        echo $conventions_info->insertInfoButton($field_name);
    }

    if (UserPermissions::hasAccess('history')) {
        insertDiffInfoButton($id, $field_options);
    }
    
    echo '<strong> ';   
    if ($id == CONVENTIONS_RECORD_ID) echo ' CONVENTIONS for ';
    echo $field_print_name . ': ';    
    echo '</strong>';

    echo '</div>'; //end of panel-heading    
    echo PHP_EOL;
    
    echo '<div class="panel-body">';
    
    // Only set the field as editable for the JS code if the user has edit rights
    $edit_id = ">";
    if (UserPermissions::hasAccess('edit')) { 
        $edit_id = ' class="editfield">';
    }
    
    echo '<span id="field_' . $field_name . '"' .
         $edit_id .
         $output_string . 
         '</span>'; 

    echo '</div>'; //panel-body   
    echo '</div>'; //end of pannel
    echo '</div>'; //end of pannel group

    ///////////////////////////////////////////////////////////////
    // END: Output field name and content
    ///////////////////////////////////////////////////////////////    
    
endforeach;      

if (UserPermissions::hasAccess('history')) {
    InsertLastModificationTimeAndListOfChangesLink($id, $item);
} 

echo '</div>'; // end of swipe area
	
$log->lwrite('bs_record_details.php: document details for id ' . $id . ' viewed');


/**
 * 
 * local helper functions
 *
 */

function showCurrentField ($id, $output_string, $field_options) {
	
	if ($id == CONVENTIONS_RECORD_ID) return true;
	
	// Don't show the field if
	// - it has been configured by the user as invisible
	// - unless override to temporarily show all is active
	if ($field_options[SHOW_FIELD] == 0 &&
		$_SESSION['session_temp_toggle_view_all'] == false){

		return false;
	}
		
	// Don't show the field if it is empty and the user has disabled
	// showing empty fields unless it is the first database field which
	// has to be shown even if it is empty.
	if ((isFieldEmpty($output_string) == true) &&
		$field_options[DB_FIELD_NAME] != getDbFirstFieldName() &&
		isset($_SESSION['blt_user_hide_empty']) &&
		$_SESSION['blt_user_hide_empty'] == true) {

		return false;
	}	
	
	return true;
}

function insertDiffInfoButton($id, $field_options) {
    
    // Insert a clickable glyphicon to go to the diff page for the field
    // Include a tooltip to let users know what the icon is good for.
    echo '&nbsp;<a href="index.php?' .
        'content=bs_diff_view&id=' . $id . '&field_name=' .
        $field_options[DB_FIELD_NAME]. '"' .
            ' target="_blank" id="menu_text">' .
            '<span class="glyphicon glyphicon-time" ' .
            'data-toggle="tooltip" title="Show difference to previous versions">' .
            '</span></a>';
}

function createSideNavigationPanel($id) {
    
    $nav_config_visibility = true;
    $export_visibility = true;
    $edit_visibility = true;
    $help_visibility = true;
    
    // Don't show most of the menu on the left side navigation panel
    // if the 'conventions' record is shown.
    if($id == CONVENTIONS_RECORD_ID) {
        $nav_config_visibility = false;
        $export_visibility = false;
        $edit_visibility = false;
    }
    
    // Also don't show some buttons depending on user permissions
    if (!UserPermissions::hasAccess('export')) $export_visibility = false;
    if (!UserPermissions::hasAccess('edit')) $edit_visibility = false;
    if (!UserPermissions::hasAccess('help')) $help_visibility = false;
    
    $str = '
    <div class="col-sm-2 sidenav">
    <br>
    
    <ul id="menu_area" class="nav nav-pills nav-stacked custom">';
    
    if ($nav_config_visibility) $str .=  '    
    <li class="active">
          <a href="index.php?content=bs_record_details&id=' . $id . '&next_record=1" 
          id="menu_text"><span class="glyphicon glyphicon-forward"></span> Next</a>
    </li>        
                               
    <li class="active">
          <a href="index.php?content=bs_record_details&id=' . $id . '&previous_record=1" 
          id="menu_text"><span class="glyphicon glyphicon-backward"></span> Previous</a>     
    </li>
    <br>
        
    <li class="active">         
          <a href="index.php?content=bs_change_display_order&id=' . $id . '" 
 		  id="menu_text"><span class="glyphicon glyphicon-cog"></span> Configure View</a>
    </li>        

    <li class="active">         
          <a href="index.php?content=bs_record_details&id=' . $id . '&temp_toggle_view_all=1" 
 		  id="menu_text"><span class="glyphicon glyphicon-list-alt"></span> Toggle View All</a>
    </li>        
        
    <li class="active">         
          <a href="index.php?content=bs_record_details&id=' . $id . '&hide_empty=1" 
 		  id="menu_text"><span class="glyphicon glyphicon-eye-close"></span> Hide/Show Empty</a>
    </li>
        
    <br>';
    
    if ($edit_visibility) $str .= '        
    <li class="active" id="check-modify-before-edit">          
          <a href="#a1">
 		  <span class="glyphicon glyphicon-pencil"></span> Edit</a>
    </li>
        
    <br>

    <li class="active" id="move-to-other-category">
        <a href="#a1"> 
        <span class="glyphicon glyphicon-transfer"></span> Move</a>            
    </li>
    <br>         

    <li class="active">
          <a href="index.php?content=bs_record_update&id=0&after_id=' . $id . '" id="menu_text" 
             target="_blank"><span class="glyphicon glyphicon-plus-sign">
             </span> Add new AFTER </a>            
    </li>
    <br>';
    
    if ($export_visibility) $str .= '       
    <li class="active">
          <a href="index-print.php?content=record_details_print&quick_print=1&id=' . $id . '" 
 		  target="_blank"><span class="glyphicon glyphicon-export">
 		  </span> Quick Export</a>
    </li>     
     
    <li class="active">
          <a href="index-print.php?content=record_details_print&id=' . $id . '" 
 		  target="_blank"><span class="glyphicon glyphicon-export">
 		  </span> Formatted Export</a>
    </li>                      
    <br><br>';
    
    $str .= '
    <li class="active" id="permalink">
        <a href="#a1"> 
        <span class="glyphicon glyphicon-book"></span> Permalink</a>            
    </li>                  
    <br><br>';

    if ($edit_visibility) $str .= '
    <li class="active" id="deldoc">          
          <a href="#" id="' . $id . '">
 		  <span class="glyphicon glyphicon-trash"></span> Delete</a>
    </li>';     
        
    if ($help_visibility) $str .= '
    <br><br>
    <li class="active" id="details-help">          
          <a href="#a3">
 		  <span class="glyphicon glyphicon-question-sign"></span> Help</a>
    </li>';   

    $str .= '    
    </ul>
    </div>   
    <!-- end of menu side panel, start with the right panel -->';
    
    return $str;
    
}

function InsertLastModificationTimeAndListOfChangesLink($id, $item){
    // Show last modification time and user and link to modification list
    if ($item->getField('last_update_time') != 0) {
        
        echo '<div align="right"><p><small>Last edited by ' .
            ucfirst($item->getField('last_update_user')).
            ' on '. date("Y-m-d H:i:s", $item->getField('last_update_time')) .
            '</small></p>';
            
        echo '<p><small>'.
             '<a href="index.php?content=bs_show_all_diff_events_for_record&id=' .
             $id . '" target="_blank">Show list of all changes</a></small></p>';
                
        echo '</div>';
    }
}

function initRecordVariables(&$id, &$item, $log) {
    
    if (!isset($_GET['id'])) {
        echo '<h1> ERROR: Parameter "ID" missing! </h1>';
        $log->lwrite('bs_record_details.php: Error, ID parameter missing');
        return false;
    }
    
    $id = (int) $_GET['id'];
    
    // Get the current document record
    $item = DocRecord::getRecord($id);
    
    if (!$item) {
        
        // This is a hard fail, nothing to left to do here...
        echo "<h1>" . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . " with ID " .
            $id . " not found!</h1>";
        return false;
    }
    
    // The item has been retrieved with $id. Re-assign $id from the item
    // just to make sure the rest of the code of this file works on the
    // correct data.    
    $id = $item->getId();    
    
    return true;
}

function checkDatabaseWriteSuccess($log) {

    if (isset($_SESSION['result-status']) &&
    $_SESSION['result-status'] == '0') {
        
        $log->lwrite('bs_record_details.php: PROBLEM: document data was not saved, ' .
            'showing error message and exit: ' . $_SESSION['result-msg']);
        
        echo '<div class="alert alert-danger" role="alert">';
        echo '<h2>Data was not saved!</h2> ' .
            '<h3>' . $_SESSION['result-msg'] . '</h3>';
        echo '</div>';
        
        $_SESSION['result-status'] = '';
        $_SESSION['result-msg'] = '';
        
        //hard-fail, nothing left to do here...
        return false;
    }
    
    return true;
}


function loadPreviousOrNextRecord(&$id, &$item) {

    $resRetrieval = '';

    if (isset($_GET['previous_record'])){
        $item_temp = DocRecord::getPreviousDoc($id);
        
        if (!$item_temp){
            $resRetrieval = 'There is no previous ' . DatabaseConfigStorage::getCfgParam('doc-name-ui') . '!';
        }
        else {
            $item = $item_temp;
            $id = $item->getId();
        }
    }
    else if (isset($_GET['next_record'])) {
        $item_temp = DocRecord::getNextDoc($id);
        
        if (!$item_temp){
            $resRetrieval = 'There is no next ' . DatabaseConfigStorage::getCfgParam('doc-name-ui') . '!';
        }
        else {
            $item = $item_temp;
            $id = $item->getId();
        }
    }
    
    return $resRetrieval;
}

function generateHashIdLink($item) {

    $str = '<p>' .
    GetHrefForHashId($item) . 
    'RIGHT click and select \'copy link location\' to get the permalink' .
    '</a></p>';

    return $str;
}

?>

    </div> <!-- end of right column -->
  </div> <!-- end of row -->
</div> <!-- end of container -->


<!-- Modal dialog box for the 'delete document' functionality -->
<div class="modal fade" id="deleteDocModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Please Confirm!</h4>
      </div>
      <div class="modal-body">
        <p>Do you really want to delete this 
        <?php echo DatabaseConfigStorage::getCfgParam('doc-name-ui');?>?</p>
      </div>
      <div class="modal-footer">            
        <button type="button" id="del-button" class="btn btn-default" data-dismiss="modal" 
                style="float:right">Delete</button>      
        <button type="button" class="btn btn-default" data-dismiss="modal" 
                style="float:left">Cancel</button>      
                
      </div>
    </div>
    
  </div>
</div>

<!-- Modal dialog box for the 'delete document result' functionality -->
<div class="modal fade" id="deleteDocExecModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui'));?> 
        Deletion Result</h4>
      </div>
      <div class="modal-body">
        <p id="delete-final-text">x</p>
      </div>
      <div class="modal-footer">                  
        <button type="button" class="btn btn-primary closetab" data-dismiss="modal" 
                style="float:right">OK</button>                      
      </div>
    </div>
    
  </div>
</div>


<!-- Modal dialog box for the 'lock document record failure scenario -->
<div class="modal fade" id="lockFailDocExecModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">This <?php echo ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui'));?> 
        Can't Be Locked For Editing!</h4>
      </div>
      <div class="modal-body">
        <p id="lock-failure-text">This <?php echo DatabaseConfigStorage::getCfgParam('doc-name-ui');?> 
        is currently edited by someone else</p>
        <p>This tab will <strong>close</strong> after clicking OK, please reload
           the <?php echo DatabaseConfigStorage::getCfgParam('doc-name-ui');?> details to get an updated version!</p>
      </div>
      <div class="modal-footer">                  
        <button type="button" class="btn btn-primary closetab" data-dismiss="modal" 
                style="float:right">OK</button>      
                
      </div>
    </div>
    
  </div>
</div>


<!-- Modal dialog box for the 'Move To Other Category' functionality -->
<div class="modal fade" id="moveToOtherCategoryModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Move Record To A New Category</h4>
      </div>
      <div class="modal-body">
        <p id="other-category-text">Select a category to move the record to. 
                                    After pressing the "move" button, the 
                                    current record will be put at the end of 
                                    the selected category or become the first  
                                    record in an empty category. 
                                    Once done, the page will reload and the 
                                    record will have a different ID at the top.</p>
        
		<form id="MoveToOtherCategoryForm" method="post" action="index.php" target="_self">
		<input type="hidden" name="id" id="current_id" value="<?php echo $id ?>" />
		<input type="hidden" name="task" value="doc.change.id" />
		<?php insertCategorySelection('Limit Search to Decade/Category');?>
		</form>        
        
      </div>
      <div class="modal-footer">                  
        <button type="button" class="btn btn-default" data-dismiss="modal" 
                style="float:left">Cancel</button>      
        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                id = "move-commit" style="float:right">Move</button>                
      </div>
    </div>
    
  </div>
</div>

<!-- Modal dialog box for showing the permalink for this record -->
<div class="modal fade" id="permalinkModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Permalink for this <?php echo DatabaseConfigStorage::getCfgParam('doc-name-ui');?>:</h4>
      </div>
      <div class="modal-body">
        <p id="permalink-text">
  		    <?php echo generateHashIdLink($item)?>
        </p>
      </div>
      <div class="modal-footer">                  
        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                style="float:right">OK</button>      
                
      </div>
    </div>
    
  </div>
</div>
