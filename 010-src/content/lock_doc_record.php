<?php
/**
 * lock_doc_record.php
 *
 * This page locks a database record. The page is called after the user
 * has double clicked on a database field on the details page.
 * It locks the corresponding record in the database and returns success
 * or failure.
 *
 * @version    1.0 2017 03 16
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();

$id = (int) $_GET['id'];

if (!$id) {
	
	echo '<p id="doc-lock-result">Error locking document! ID ' .
			'was not present in request</p>';
	$log->lwrite('lock_doc_record.php: Error locking document, ' . 
			     'id was not present in request');
	
} else {

	// Web security check
	if (checkAntiCsrfProtection() == false) {
				
		echo '<p id="doc-lock-result">There was a security error, ' .
		     'locking aborted!</p>';				
		$log->lwrite('lock_doc_record.php: Security error, ' . 
				     'locking aborted ');
		exit;
	}	

	// Lock the database record
	if ($id != 0){
		
		$log->lwrite('lock_doc_record.php: Attempting to lock record');
		
		// TODO: Check if the GET variable exists
		$load_time = (int) $_GET['doc_load_time'];
		$log->lwrite('lock_doc_record.php: document was loaded at: ' . 
				      $load_time);
		
		// Get the current document record and lock the record
		// Note: The $load_time extracted from the HTTP GET Ajax request
		// is given to the lock function to check if the database record
		// was modified after the user's page was initially loaded! In this
		// case the document can't be locked even though nobody is currently 
		// editing it to prevent user B from overwriting text that user A
		// has previously modified!
		$item = DocRecord::getRecord($id);
		
		if (!$item) {
			$res_text = '<p id="doc-lock-result">Record ID does not exist, ' . 
			            'perhaps renumbered?</p>';
			 
			echo $res_text;
			
			$log->lwrite('lock_doc_record.php: Record ID does not exist, ' . 
					     'perhaps renumbered? ID: ' . $id); 
			
			return;
		}
		
		$lock_result = lockDatabaseRecord($item, $load_time);
		$log->lwrite('Back from lockDB');
		
	
		if ($lock_result[0] != true){
			
			$res_text = '<p id="doc-lock-result">' . 
			            $lock_result[1] . 
			            '</p>';
			            
			echo $res_text;		
			$log->lwrite('lock_doc_record.php: Unable to lock record for id ' .
					      $id . ', reason: ' . $lock_result[1]);

		}
		else {
			// IMPORTANT: DO NOT CHANGE THE TEXT in the echo instruction below
			// it is used by the JS function in bs_record_details.php
			// to check if locking the record was successfull.
			echo '<p id="doc-lock-result">Document successfully LOCKED!</p>';
			$log->lwrite('lock_doc_record.php: document ' . $id . 
					     ' successfully LOCKED!');			
		}
	}
	 	
}

?>
