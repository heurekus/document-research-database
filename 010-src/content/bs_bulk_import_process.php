<?php
/**
 * bs_bulk_import_process.php
 *
 * This module takes JSON formated bulk data, checks the data for syntax
 * and consistency and then imports the bulk data into the database.
 * 
 * See the documentation for details on the JSON structure, conditions and 
 * checks performed before the bulk data is imported.
 *
 * @version    1.0 2018-08-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('bs_bulk_import_process.php accessed.');

// Bulk import can take a bit of time for large datasets. Let's increase
// the max execution time. It only applies while the script is running.
ini_set('max_execution_time', 600);

echo '<div class="alert alert-info">';
echo 'Starting bulk import of records...';
echo '</div>';

if (checkAntiCsrfProtection() == false) {
	outputBulkImportMsg(false, 'Security error: Anti-CSRF token missing, ' . 
			                   'aborting...');
	$log->lwrite("set_print_config.php: security error, exiting");
	exit;
}

if (!UserPermissions::hasAccess('admin')) {    
    $log->lwrite("User doesn't have admin rights, bulk upload aborted");
    return;
}

$bulk_data = getBulkData($log);

if (!is_array($bulk_data)) {
	$err_msgs = array();
	array_push($err_msgs, '<b>Getting bulk data failed, reason:</b>');
	array_push($err_msgs, $bulk_data);
	array_push($err_msgs, 'Aborting....');
	outputBulkImportMsg(false, $err_msgs);
	return;
}

// The field names in the bulk input could contain HTML tags. Remove them!
stripHtmlFromFieldNames($bulk_data);

//
// Check that field names in the bulk data match those in the db and that
// the number of fields match as well
//
$result = validateFieldNames ($bulk_data, $log);

if ($result !== true) {
	array_unshift($result, '<b>Aborting import due to field name or count issue!</b>');
	outputBulkImportMsg(false, $result);
	return;	
} else {
	outputBulkImportMsg(true, 'Field names in bulk import match database '. 
			                  'fields, proceeding!');
}

//
// Check if record ids in the bulk import do not yet exist in the db.
//
$result = checkForExistingIds ($bulk_data, $log);

if ($result !== true) {
	array_unshift($result, '<b>Error: One or more IDs already exist in the database:</b>');
	array_push($result, 'Aborting!');
	outputBulkImportMsg(false, $result);
	return;
} else {
	outputBulkImportMsg(true, 'Record ID check succesfull, IDs do not yet '. 
			                  'exist in the database!');
}

//
// Now attempt to import the records
//
$result = importBulkDataToDb ($bulk_data, $log);

if ($result !== true) {
	array_unshift($result, '<b>Error: Importing records failed. Reason(s):</b>');
	array_push($result, 'Aborting!');
	outputBulkImportMsg(false, $result);
	return;
} else {
	outputBulkImportMsg(true, 'All went well, bulk import completed!');
}

return;



/**
 * getBulkData()
 *
 * Gets the bulk data from HTTP Post variable in JSON format and attempts
 * to decode it into a PHP array
 *
 * @param log object to write status information into the log file
 *
 * @return mixed: an array if conversion has worked or an error string giving
 *         details what went wrong.
 *
 */

function getBulkData($log) {

	// If the settings are not included in the POST request, exit
	if (!isset($_POST['bulk'])) {
		$err_msg = 'bulk import not present in POST';
		$log->lwrite($err_msg);
		return $err_msg;
	}
	
	// Get the bulk data from the POST variable and convert from JSON
	// format to an associative array
	$bulk_data_JSON = $_POST['bulk'];
	$bulk_data = json_decode($bulk_data_JSON, true);
	
	if (!is_array($bulk_data)) {
		$err_msg = 'Data conversion error: ' . json_last_error_msg();
		$log->lwrite($err_msg);
		return $err_msg;
	}
	
	if (count($bulk_data) < 2) {
		$err_msg = 'Not enough bulk data to import, exiting';
		$log->lwrite($err_msg);
		return $err_msg;
	}
	
	$log->lwrite('Number of records to import: ' . (count($bulk_data) - 1));
	return $bulk_data;	
}


/**
 * stripHtmlFromFieldNames()
 *
 * Strips HMTL code from the field names which are given in the first
 * array in $bulk_data. In addition spaces are removed at the beginning
 * and end of each string.
 *
 * Note: Field names are contained in the first array of $bulk_data
 *
 * @param $bulk_data array, will be modified and returned
 *
 * @return none
 *
 */

function stripHtmlFromFieldNames(&$bulk_data) {
	
	foreach($bulk_data[0] as $bulk_field_num=>&$bulk_field_name) {
		$bulk_field_name = strip_tags($bulk_field_name);
		$bulk_field_name = trim($bulk_field_name);
		$bulk_field_name = rtrim($bulk_field_name);
	}
	
}

/**
 * validateFieldNames()
 *
 * Checks if the number of fields in the bulk import and the database 
 * is the same. If it is, the function then compares the field names.
 * 
 * Note: Field names are contained in the first array of $bulk_data
 *
 * @param $bulk_data array and $log
 *
 * @return mixed, true if everything is o.k. or an array of strings that
 *         contain stings that can be used to inform the users which fields
 *         do not match or a string that informs the user that the number
 *         of fields does not match.
 *
 */

function validateFieldNames ($bulk_data, $log) {

	// Get the field names as configured for the database
	$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
	$db_field_names = array_keys($local_doc_db_description);	
	
	$fields_with_issues = array();
		
	// Check if number of fields in the bulk import and the database match
	if ((count($bulk_data[0]) - 1) != count($db_field_names)) {
		$err_msg = 'Number of fields in bulk import and database do not match: '  .
				   count($bulk_data[0]) . ' fields in bulk data, ' . 
				   count($db_field_names) . ' fields in the database';
		
		array_push($fields_with_issues, $err_msg);
		$log->lwrite($err_msg);
		return ($fields_with_issues);
	}
	
	// Now validate the field names of the bulk input by comparing them
	// with the field names of the database
	$x = 0;
	foreach($bulk_data[0] as $bulk_field_num=>$bulk_field_name) {
	
		// The first field in the bulk import is the ID which is not
		// a configurable database field, so it has to be ignored
		if ($x == 0) {
			$x++;
			continue;
		}
	
		if (strcmp($bulk_field_name, $db_field_names[$x-1]) != 0) {

			$err_msg = 'Field name error: ' . $bulk_field_name . ' <-> ' . 
			            $db_field_names[$x-1];
			
			array_push($fields_with_issues, $err_msg);
			$log->lwrite($err_msg);
		}
	
		$x++;
	}
	
	if (count($fields_with_issues) != 0) return $fields_with_issues;
	
	return true;
} 


/**
 * validateFieldNames()
 *
 * Checks if the number of fields in the bulk import and the database
 * is the same. If it is, the function then compares the field names.
 *
 * Note: Field names are contained in the first array of $bulk_data
 *
 * @param $bulk_data array and $log
 *
 * @return mixed, true if everything is o.k. or an array of strings that
 *         contain text that can be used to inform the users which fields
 *         do not match or a string that informs the user that the number
 *         of fields does not match.
 *
 */

function importBulkDataToDb ($bulk_data, $log) {
	
	$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
		
	// loop over each record
	$x = 0;
	foreach($bulk_data as $record_id=>$record_content) {
				
		// Ignore the first record, it contains the field names
		if ($x == 0) {
			$x++;
			continue;
		}
		
		$log->lwrite('---------- Bulk importing next record');		
		
		$db_id = 0;
		$item = array();		
		// Create new database entry and loop over all fields
		// to copy the content into it
		foreach ($record_content as $field_num=>$field_content) {
			
			// The first field is the database ID, that doesn't have a name
			// in $bulk_data[0]. Thus, treat it differently
			if ($field_num == 0) {
				//echo '<br>ID = ' . $field_content;
				//echo '<br>- - - - - - - - - -';
				$db_id = (int) $field_content;
				continue;
			}
			
			$field_print_name = $bulk_data[0][$field_num];
			$field_db_name = $local_doc_db_description[$field_print_name][0];
			
			//echo '<br>' . $field_num;
			//echo '<br>' . $field_print_name;				
			//echo '<br>' . $field_db_name;
			//echo '<br> ----------';
						
			$field_content = fixFrenchTextHtmlLinks($field_content);
			$field_content = convertToSafeUtf8Html($field_content);
			$item[$field_db_name] = $field_content;
		}
		
		
		//var_dump($item);
		//echo '-------';
		
		//If the record is empty, don't put it into the database
		if (isRecordEmpty($item, $log)){
			$log->lwrite('INFO: Emtpy line with id ' . $db_id . ', skipping over it!');
			continue;
		}
		
		$doc = new DocRecord($item);	
		$create_record_results = $doc->addRecord($db_id);
		
		// Exit if a record could not be created
		if ($create_record_results[0] != 1) {
			$log->lwrite('ERROR creating new database record with id: ', $db_id);
			$log->lwrite($create_record_results[1]);
			$log->lwrite(print_r($item, true));
			return ($create_record_results[1]);
		}
		
		// New record written. Even though the record is new and has
		// has not been locked, call the unlock function anyway so the
		// write time is noted in the database as well.
		unlockDatabaseRecord($db_id);
		
		// Now write each field to the diff table as well
		foreach ($item as $field_name => $content){
		
			if ($field_name != "id") {
				ModHistoryStorage::saveFieldContent($db_id, $field_name,
						$content);
			}
		}
		
		$x++;
	}
	
	return(true);
}


/**
 * isRecordEmpty()
 *
 * @return true or false
 */
function isRecordEmpty($item, $log) {
	
	$isEmpty = true;
	foreach ($item as $field=>$content) {		
		if (isFieldEmpty($content) == false) {
			$isEmpty = false;
			break;
		}
	}
	
	return $isEmpty;
}

/**
 * checkForExistingIds()
 *
 * Checks if the number of fields in the bulk import and the database
 * is the same. If it is, the function then compares the field names.
 *
 * Note: Field names are contained in the first array of $bulk_data
 *
 * @param $bulk_data array and $log
 *
 * @return mixed, true if everything is o.k. or an array of strings that
 *         contain text that can be used to inform the users which fields
 *         do not match or a string that informs the user that the number
 *         of fields does not match.
 *
 */
function checkForExistingIds ($bulk_data, $log) {

	$existing_ids = array();
	
	// loop over each record
	$x = 0;
	foreach($bulk_data as $record_id=>$record_content) {
	
		// Ignore the first record, it contains the field names
		if ($x == 0) {
			$x++;
			continue;
		}
		
		// echo '<br>' . $record_content[0];
		
		// If the Record already exists add it to the list of errors
		if (DocRecord::getRecord($record_content[0])) {
			$err_text = 'Record ID ' . $record_content[0] . ' already exists';
			$log->lwrite('ERROR: ' . $err_text);
			array_push($existing_ids, $err_text);
		}
	}
	
	// If existing IDs were found return them 
	if (count($existing_ids) > 0) return $existing_ids;
	
	// No existing IDs were found, all is well!
	return true;
}


/**
 * outputBulkImportMsg()
 * 
 * Outputs a Bootstrap Alert message on the page with text supplied
 * to the function.
 * 
 * @param boolean, true for a 'success' alert (green), 
 *                 false for a 'danger' alert (red)
 * @param mixed, either a string or an array of strings for more complex
 *               messages
 *
 * @return none
 *
 */

function outputBulkImportMsg($success, $msg) {
	
	if ($success == true) {
		echo '<div class="alert alert-success">';		
	} else {
		echo '<div class="alert alert-danger">';
	}
		
	if (is_array($msg)) {
		foreach ($msg as $entry) {
			echo '<p>' . $entry . '</p>';				
		}
	} else {
		echo '<p>' . $msg . '</p>';
	}
		
	echo '</div>';
}

?>