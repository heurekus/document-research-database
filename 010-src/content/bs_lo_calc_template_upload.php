<?php
/**
 * bs_lo_writer_template_upload.php
 *
 * This page implements a form for uploading a Libreoffice Calc template
 * document
 *
 * @version    1.0 2019-03-17
 * @package    DRDB
 * @copyright  Copyright (c) 2014-19 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('Libreoffice calc template document upload page accessed');

echo '<center><form action="index.php?content=bs_lo_calc_template_ul_handle" method="post" enctype="multipart/form-data">';
echo '<h2>Select Libreoffice Calc template file to upload</h2><br><br>';
echo '<input type="file" name="fileToUpload" id="fileToUpload"><br>';
echo '<input type="submit" value="Upload file" name="submit">';
echo '</form></center><br>';

?>
