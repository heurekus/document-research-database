<?php
/**
 * bs_manage_vars.php
 *
 * GUI frontend for admin users to modify project variables.
 * The default value of the variables are located in /config/config.php.
 * Once the admin overrides those variables they are put into the
 * database (table drdb_config)
 * 
 * 
 * Note: Originally bs_change_manage_db_fields.php was taken as a template
 * for this page.
 *
 * @version    1.0 2021-07-20
 * @package    DRDB
 * @copyright  Copyright (c) 2014-21 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */
?>

<style type="text/css">
    /* show the move cursor as the user moves the mouse over the panel header.*/
    #draggablePanelList .panel-heading {
        cursor: move;
    }
</style>

<script data-cfasync="false" src="/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

<!--The following small library modifies jquery-ui to also handle-->
<!--touch events, i.e. panels are movable as well with a touch interface-->
<script src="/jquery.ui.touch/jquery.ui.touch-punch.min.js"></script>

<?php 
$log = new Logging();
$log->lwrite('\'Manage config variables\' page accessed');


// If the user has no admin rights, abort
if (!UserPermissions::hasAccess('admin')) {
    return;
}


// Check if a modification opertion is to be run before creating the output
// (e.g. modify/add/delete a field). If there was a problem abort.
if (isset($_POST['param_name'])) {
    if (!executePostOperation($log)) return;
}


// Create the bootstrap page layout container, first row for the side
// navigation menu bar.
echo '<div class="container-fluid">' . PHP_EOL;
echo '<div class="row content">' . PHP_EOL;
echo '<div class="col-sm-2 sidenav">'. PHP_EOL;      

// Create the menu area
echo '<br>';
echo '<ul id="menu_area" class="nav nav-pills nav-stacked custom">'. PHP_EOL;

// Create buttons 
echo '<li class="active">';
echo '<a href="#" id="closetab">'. 
'<span class="glyphicon glyphicon-asterisk"></span> Done</a>';
echo '</li>' . PHP_EOL;

if (UserPermissions::hasAccess('help')) {
    echo '<br>';
    echo '<li class="active">';
    echo '<a href=/drdb-manual/ManageConfigParams target="_blank" ';
    echo 'id="menu_text">';
    echo '<span class="glyphicon glyphicon-question-sign"></span> Help</a>';
    echo '</li>';
}

echo '</ul>';

echo '</div> <!-- end of sidenav column --> '; 
echo '<div class="col-sm-4"> <!-- start of middle column -->'; 
echo '<br>';

// Create token if not already done in a previous iteration or somewhere else
// by another action. The token is sent when the save button is pressed
// and then analyzed in createOrModifiyRecord() to prevent Cross Site
// Request Forgery (CSRF) attacks.
$token = createOrReuseSecurityToken();

// Now create input fields for each variable that can be modified.

$title_value = DatabaseConfigStorage::getCfgParam('title'); 
if ($title_value !== false) {
    echo createHtmlForInputField($token, "Title", "title", $title_value);
}

$footer_value = DatabaseConfigStorage::getCfgParam('footer');
if ($footer_value !== false) {
    echo createHtmlForInputField($token, "Footer", "footer", $footer_value);
}

$record_name_value = DatabaseConfigStorage::getCfgParam('doc-name-ui');
if ($record_name_value !== false) {
    echo createHtmlForInputField($token, "Database record name", "doc-name-ui", $record_name_value);
}

$record_name_plural_value = DatabaseConfigStorage::getCfgParam('doc-name-ui-plural');
if ($record_name_plural_value !== false) {
    echo createHtmlForInputField($token, "Database record name, plural", "doc-name-ui-plural", 
                                 $record_name_plural_value);
}

$def_inc_value = DatabaseConfigStorage::getCfgParam('default_increment_new_record');
if ($def_inc_value !== false) {
    echo createHtmlForInputField($token, "Default increment for a record", 
                                 "default_increment_new_record", $def_inc_value);
}

?>
  

    </div> <!-- end of middle column -->        
    <div class="col-sm-6"></div>    
  </div> <!-- end of row -->
</div> <!-- end of container -->


<?php 


/**
 * createHtmlForInputField()
 *
 * Create the HTML code for a form with a Bootstrap styled input field,
 * and submit button. 
 *
 *
 * @param string, secret security token (will be a hidden form parameter)
 * @param string, header text for the input field panel
 * @param string, name of the parameter
 * @param string, current value of the parameter 
 *
 * @return string, HTML code for a form with a bootstrap input box + hidden
 *         security token
 *
 */

function createHtmlForInputField($token, $param_header, $param_name, $param_value) {

    // Escape double quotes in the parameter value, it messes up the
    // HTML tags.
    $param_value = str_replace('"', "&quot;", $param_value);
    
    $panel_form_input_html_template = '
    <div class="panel panel-info">
     
     <div class="panel-heading"><p>' . $param_header . '</p></div>
     <div class="panel-body">
     
        <form action="index.php?content=bs_manage_vars" method="post">
            <div class="input-group">
                <input type="hidden" name="token" value="' . $token . '">
                <input type="hidden" name="param_name" value="' . $param_name . '">
                <input type="text" class="form-control" name="value" value="' . $param_value . '">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">Save</button>
                </span>
            </div>
        </form>
     
     </div>
    </div>
    ';
    
    return $panel_form_input_html_template;
}


/**
 * executePostOperation()
 *
 * Do a security check with the token and if everything checks out,
 * have a look which operation the user wants to be performed
 *
 * @param object, logging object
 *
 * @return boolean, false if there was an error that should trigger
 *         an abort.
 *
 */

function executePostOperation($log) {
    
    // Run web security checks
    if (checkAntiCsrfProtection() == false) {
        $log->lwrite("ERROR: Anti CSRF check failed, abort!");
        return false;
    }
    
    // Abort if the parameter name or value is missing
    if (!isset($_POST['param_name']) || !isset($_POST['value'])){
        $log->lwrite("ERROR: POST parameter missing");
        return false;
    }
    
    // Abort if the parameter name given does not match a parameter
    // name that exists
    $p = $_POST['param_name'];    
    if (!$p == "title" && 
        !$p == "footer" &&
        !$p == "doc-name-ui" &&
        !$p == "doc-name-ui-plural" &&
        !$p == "default_increment_new_record") {
     
        $log->lwrite("ERROR: parameter name does not exist, abort: " . $p);
        return false;
    }

    // sanitize POST input
    $sanitized_val = convertToSafeUtf8Html($_POST['value'], true);
    $sanitized_val = strip_tags($sanitized_val);
    
    // Write the value for the given parameter to the database
    return DatabaseConfigStorage::setCfgParam($p, $sanitized_val);

}

?>

