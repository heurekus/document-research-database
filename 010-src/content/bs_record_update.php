<?php
/**
 * bs_record_update.php
 *
 * Code for updating a database record or to create a new one.
 *
 * @version    2.0 2017-02-05
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Instantiate a new object for displaying and handling conventions info icons
$conventions_info = new ConventionsInfo();

?>

<style>
.tooltip-inner {
    text-align:left;
}
</style>

<script src="js/close_tab.js"></script>
<script src="js/init_tooltips.js"></script>

<!-- Modal dialog box for the 'lock document record' failure scenario -->
<div class="modal fade" id="lockFailDocExecModal" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">This <?php echo ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui'));?> 
        Can't Be Locked For Editing!</h4>
      </div>
      <div class="modal-body">
        <p id="lock-failure-text">This <?php echo DatabaseConfigStorage::getCfgParam('doc-name-ui');?> 
        is currently edited by someone else</p>
        <p>This tab will <strong>close</strong> after clicking OK!</p>
      </div>
      <div class="modal-footer">                  
        <button type="button" class="btn btn-primary close-tab" data-dismiss="modal" 
                style="float:right">OK</button>      
                
      </div>
    </div>
    
  </div>
</div>

<!-- HTML for the standard page layout -->

<div class="container-fluid">

  <div class="row content">
    <div class="col-sm-2 sidenav">      
      <br>

      <ul id="menu_area" class="nav nav-pills nav-stacked custom">
        <li class="active close-tab">
          <a href="#a1" id="menu_text"><span class="glyphicon glyphicon-asterisk"></span> Close tab</a>
        </li>                
       </ul>
    </div>
    
    <div class="col-sm-10">

<?php 

$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

$log = new Logging();

if (!UserPermissions::hasAccess('edit')) {
    $log->lwrite('User has no edit rights, aborting');
    return;
}
     

// Get the existing information for an existing item or create a new one
$id = 0;
if (isset($_GET['id'])) {
	$id = (int) $_GET['id'];
}

if ($id != 0) {
	$item = DocRecord::getRecord($id);
} else {

	$id = 0;
	$item = new DocRecord;
}

// Get the optional id after which document id to insert the new one
$after_id = 0;
if (isset($_GET['after_id'])) {
	$after_id = (int) $_GET['after_id'];
}

// Lock the database record
if ($id != 0){
	$lock_result = lockDatabaseRecord($item);

	if ($lock_result[0] != true){
		$log->lwrite("bs_record_update.php: Unable to lock the record for editing");
		
		// Pop-up a modal dialog box to inform the user that the record
		// could not be locked.		
		echo PHP_EOL;

		// Put info text why locking the record has failed into the DOM so
		// the JS code can pick it up and put it into the dialog box.
		echo '<div id="lock_result" hidden>' . $lock_result[1] . '</div>';
		
		// Include the JS code that makes the modal error dialog box visible.
		echo '<script src="js/record_update_lock_error.js"></script>';
        echo '</html>';
		
		exit;
	}
}

// Only include the JS files for the main functionality on the page 
// at this point as the unload code should not be present if locking has 
// failed as there must be no check if the ballet is currently edited 
// (which would lead to another dialog box).
?>
<script src="js/record_update_exit.js"></script>
<script src="js/record_update_ckeditor.js"></script>
<?php 

// create token if not already done in a previous iteration or somewhere else
// by another action. The token is sent when the "save" button is pressed
// and then analyzed in createOrModifiyRecord() to prevent Cross Site 
// Request Forgery (CSRF) attacks.
$token = createOrReuseSecurityToken();

// Put a number of variables used by Javascript on the client side into
// invisible page elements
echo '<span id="doc_token" style="display:none">'. $token .
     '</span>' . PHP_EOL;
echo '<span id="doc_db_id" style="display:none">' . $id .
     '</span>' . PHP_EOL;

echo '<h3>Edit ' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . ' Information</h3>';

echo '<span class="label label-default">';
echo "ID: " . $id;
echo '</span>';
echo '<p></p>';

echo '<form action="index.php?content=bs_record_details&id=' . $id . 
     '&after_id=' . $after_id . '" method="post" name="doc_update" id="doc_update">';

echo '<fieldset class="maintform">';     
echo '<script src="../../ckeditor/ckeditor.js"></script>';

// If this is a new record and the user does not want to insert it after a
// specific other record, offer a drop down menu to choose the category
// in which to create it.
if ($id == 0 && $after_id == 0) {
	insertCategorySelection("Select category or leave at '-' to insert " . 
			                "at the end of the database");
}

/* 
 * this loop generates the input boxes for all changeable database fields 
 * of the record to be edited.
 */

// If the current user (session) has an addapted database record field
// sort order load it. Otherwise use the default from database-structure.php
if (isset($_SESSION['db_field_order'])){
    $fieldArrayWithOptions = $_SESSION['db_field_order'];
    $log->lwrite('bs_record_details.php: Using customized field display order');
}
else {
    $fieldArrayWithOptions = $local_doc_db_description;
}

$config_file = 'config.js';
if ($id == CONVENTIONS_RECORD_ID) $config_file = 'config-for-tooltips.js'; 

foreach ($fieldArrayWithOptions as $field_print_name=>$field_options){ 
    
    $field_name = $field_options[DB_FIELD_NAME];
    
    // Decide whether to show the field or not
    if (showCurrentFieldForEdit($id, $field_options) == false) {        
        echo '<div style="display:none"><br>';
    } else {        
        echo '<div><br>';
    }
    
    // If we don't edit the conventions info, insert the convention info
    // icon that shows the convention info on mouse-over
    if ($id != CONVENTIONS_RECORD_ID){
        
        echo $conventions_info->insertInfoButton($field_name) . ' ';
    }
    
	// CKEditor doesn't handle HTML encoded < and > (&lt; and &lt;) correctly
	// This can be fixed by encoding the & to ampersand.
    $field_text = $item->getField($field_name);
	$field_text = str_replace("&lt;", "&amp;lt;", $field_text);
	$field_text = str_replace("&gt;", "&amp;gt;", $field_text);
	
	echo '<label for="'. $field_name . '" class="required">' . 
         $field_print_name . '</label><br>' .
         '<textarea name="' . $field_name .  '" ' . 
	 	 'class="drdb_input"' .  '" ' .	 	  
	 	 'id="' . $field_name . '" ' . 
	 	 'editor="' . $config_file . '"'.
	 	 '>' . $field_text . '</textarea>';
	
    // Note: The standard textarea is replaced with CKEDITOR once the
    // page has fully loaded via the Javascript code contained in  
    // record_update_ckedit.js
	   	
  	echo '<br>';
  	echo '</div>';
   	echo "\r\n"; 

	   
}
    
?>
    
    <input type="hidden" name="id" id="id" value="<?php echo $item->getId(); ?>" />
    <input type="hidden" name="task" id="task" value="doc.update" />
    <input type='hidden' name='token' value='<?php echo $token; ?>'/>
    <input id='mark-data-saved' class="btn btn-primary" type="submit" name="save" value="Save"/>
  </fieldset>
  <br><br>
</form>


    </div> <!-- end of right column -->
  </div> <!-- end of row -->
</div> <!-- end of container -->

<?php 
function showCurrentFieldForEdit ($id, $field_options) {
	
	if ($id == CONVENTIONS_RECORD_ID) return true;
	
	// Don't show the field if
	// - it has been configured by the user as invisible
	// - unless override to temporarily show all is active
	if ($field_options[SHOW_FIELD] == 0 &&
		$_SESSION['session_temp_toggle_view_all'] == false){

		return false;
	}
	
	return true;
}
?>