<?php
/**
 * bs_bulk_import_get.php
 *
 * TBD
 *
 * @version    1.0 2018-08-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('bs_bulk_import_get.php accessed');

// create token if not already done in a previous iteration or somewhere else
// by another action. The token is sent when the "save" button is pressed
// and then analyzed in the createOrModifiyRecord() function in 
// create-or-modify-record.ph to prevent Cross Site Request Forgery (CSRF) 
// attacks.
$token = createOrReuseSecurityToken();

?>
<center>
<form action="index.php?content=bs_bulk_import_process" method="post">
<br>
<h2>Bulk Data Import</h2>
Copy/paste bulk data into the text box and then press the <b>Submit button</b>.
<br>
Note: Depending on the amount of data, the import process 
can take <b>several minutes!</b> 
<br><b>Remain on the page</b> until a success or error message is shown!
<br>
<br>
<textarea name="bulk" rows="10" cols="150"></textarea>
<br>
<input type='hidden' name='token' value='<?php echo $token; ?>'/>
<br>
<input type="submit" value="Submit">
</form>
</center>
<br>
<?php 

return;

?>