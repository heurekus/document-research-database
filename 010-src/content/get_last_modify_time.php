<?php
/**
 * get_last_modify_time.php
 *
 * This page returns the modify time of the database record that is identified
 * with 'id' in the http get request
 *
 * @version    1.0 2018 04 03
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();

$id = 0;

if (isset($_GET['id'])) {
	
	$id = (int) $_GET['id'];
}

if ($id <= 0) {
	
	$res_text = 'Error, invalid id in the request: ';
	
} else if (checkAntiCsrfProtection() == false) {

	$res_text = "There was a security error, aborting request";

} else if (isRecordLocked($id) == false) {
	
	// Assemble result text with two pieces of information:
	// 1. the last update time in Unix time format
	// 2. A text that is parsed in the web browser, do NOT change!
	$res_text = getLastUpdateTime($id) . ", record is not locked";
	
} else {
	
	$res_text = "record is locked";
}

// paragraph with id the javascript will search in for the response
echo '<p id="doc-mod-time-result">' . $res_text . '</p>';
$log->lwrite('get_last_modify_time.php: ' . $res_text . ' id: ' . $id);


?>
