<?php
/**
 * bs_about.php
 *
 * Content for About page
 *
 * @version    1.0 2017-02-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('About page accessed');

if (!UserPermissions::hasAccess('admin')) {
    $log->lwrite("User has no admin rights, aborting");
    return;
}

?>
<script src="js/about.js"></script>

<div class="container-fluid">

  <div class="row content">
    <div class="col-sm-2 sidenav">      
      <br>

      <ul id="menu_area" class="nav nav-pills nav-stacked custom">
        <li class="active" id="closetab">
          <a href="#a1" id="menu_text">
             <span class="glyphicon glyphicon-asterisk"></span> Close tab</a>
        </li>
        <br>
        
<?php 
if (UserPermissions::hasAccess('history')) {

    echo ('
    <li class="active">
      <a href="index.php?content=bs_list_mod_or_viewed&show_modified=1" id="menu_text" target="_blank">
        <span class="glyphicon glyphicon-edit"></span> Modif. Info</a>
    </li>
    
    <li class="active">
      <a href="index.php?content=bs_list_mod_or_viewed" id="menu_text" target="_blank">
        <span class="glyphicon glyphicon-search"></span> Access Info</a>
    </li>  
    ');
} 
?>        

<?php 

if (UserPermissions::hasAccess('export')) {

    echo ('
        <br>

        <li class="active">
          <a href="index.php?content=bs_lo_writer_template_upload" id="menu_text">
             <span class="glyphicon glyphicon-upload"></span> Upload Writer Template</a>            
        </li>
        
        <li class="active">
          <a href="index.php?content=bs_lo_calc_template_upload" id="menu_text">
             <span class="glyphicon glyphicon-upload"></span> Upload Calc Template</a>            
        </li>        
    ');
}

if (UserPermissions::hasAccess('admin')) {

    echo ('        
        <br>
        <li class="active">
          <a href="index.php?content=bs_bulk_import_get" id="menu_text">
             <span class="glyphicon glyphicon-list"></span> Bulk Import</a>            
        </li>
    ');
}

if (UserPermissions::hasAccess('admin')) {
    
    echo ('
        <br>
        <li class="active">
          <a href="index.php?content=bs_user_management" id="menu_text">
             <span class="glyphicon glyphicon-user"></span> User Management</a>
        </li>
    ');
}


if (UserPermissions::hasAccess('admin')) {
    
    echo ('
        <br>
        <li class="active">
          <a href="index.php?content=bs_manage_db_fields" id="menu_text">
             <span class="glyphicon glyphicon-hdd"></span> DB Management</a>
        </li>
    ');
}

if (UserPermissions::hasAccess('admin')) {
    
    echo ('
        <li class="active">
          <a href="index.php?content=bs_manage_vars" id="menu_text">
             <span class="glyphicon glyphicon-pencil"></span> Modify Config</a>
        </li>
    ');
}



if (UserPermissions::hasAccess('help')) {
    echo ('         
        <br>
        <li class="active">
          <a href="/drdb-manual/index.html" target="_blank"
             id="menu_text">
          <span class="glyphicon glyphicon-question-sign"></span> Help</a>
        </li>
        ');
}
        
?>        
        
        
       </ul>
    </div>
   
    <div class="col-sm-10"> 

<br>
<h3>Copyright, License and Warranty Information</h3>
<br>

<div class="alert alert-info">
The Document Research Database is (c) 2014-2021 by Martin Sauter and Gerrit Berenike Heiter. 
It is distributed as free software and the source code of this 
project is freely available and licensed under the <b>GNU General Public 
License v3</b>.
<br>
<br>
For details see <a href="https://www.gnu.org/licenses/gpl-3.0.en.html" target="_blank">
www.gnu.org/licenses/gpl-3.0.en.html</a>
<br>
<br>
This software comes with ABSOLUTELY NO WARRANTY, to the extent permitted by 
applicable law and as laid out in the GPLv3.
</div>

<br>
<h3>Version History</h3>
<br>

<div class="alert alert-success"><h4>v22.3, 5. September 2021</h4></div>
<ul>
<li>New: Move source and update URLs from Gitlab to Codeberg</li>
</ul>

<div class="alert alert-success"><h4>v22.2, 22 August 2021</h4></div>
<ul>
<li>New: Checkbox added to configure inline print behavior of field data during export.</li>
</ul>

<div class="alert alert-success"><h4>v22.1, 23 July 2021</h4></div>
<ul>
<li>New: Config variables can now also be changed via the web browser.</li>
</ul>

<div class="alert alert-success"><h4>v22.0, 24 June 2021</h4></div>
<ul>
<li>New: Main table management in the browser.</li>
</ul>

<div class="alert alert-success"><h4>v21.1, 4. May 2021</h4></div>
<ul>
<li>Fix: Updated default installation username/password combination in 
the documentation.</li>
</ul>

<div class="alert alert-success"><h4>v21.0, 3. May 2021</h4></div>
<ul>
<li>New: User and permission management in the web GUI.</li>
</ul>

<div class="alert alert-success"><h4>v20.9, 10. April 2021</h4></div>
<ul>
<li>Fix: More memory given to database export code to enable the 
export of large databases.</li>
<li>New: Dockerfile now has a single line to install all required 
packages. Also does an apt update before so the version index is 
updated and points to the latest package versions.</li>
<li>New: Docker: PHP debug output now made to /var/log/apache2/error.log</li>
<li>New: Documentation updated, description of how logs can be viewed added.</li>
</ul>

<div class="alert alert-success"><h4>v20.8, 23. March 2021</h4></div>
<ul>
<li>New: The first entry after creating the main table now uses the first 
'real' id in all cases and not a low number such as 90. Only affects  
new project installations. </li>
</ul>

<div class="alert alert-success"><h4>v20.7, 19. March 2021</h4></div>
<ul>
<li>Fix: mysqlMainTable variable in default config file now contains a 
good default main table name.</li>
</ul>

<div class="alert alert-success"><h4>v20.6, 02. March 2021</h4></div>
<ul>
<li>Fix: Duplicate styles (e.g. text background color) now handled 
correctly during Libreoffice Calc or Writer export. Only the last found 
duplicate will be used in the document. This fully preserves the 
formatting as shown in the browser.</li>
</ul>

<div class="alert alert-success"><h4>v20.5, 28. February 2021</h4></div>
<ul>
<li>Fix: Temporarily disabled multi-style formating in calc and writer output 
as duplicate styles that can be present leads to an error in Libreoffice 
when opening the documents.</li>
<li>Fix: Writer/Calc export: Extra bytes no longer sent which lead to 
broken file transfers.</li>
</ul>

<div class="alert alert-success"><h4>v20.4, 27. February 2021</h4></div>
<ul>
<li>New: Sample database and import macro added to the project.</li>
</ul>

<div class="alert alert-success"><h4>v20.3, 25. February 2021</h4></div>
<ul>
<li>New: Docker update script now uses the latest released version or the 
version given on the command line.</li>
</ul>

<div class="alert alert-success"><h4>v19.3, 25. February 2021</h4></div>
<ul>
<li>Version number bump to test Docker container update with version number 
tag. No code changes</li>
</ul>

<div class="alert alert-success"><h4>v19.2, 22. February 2021</h4></div>
<ul>
<li>Fix: When using the arrow keys to go from one record to another, the access 
time was written to the previous record, i.e. the wrong record.</li>
</ul>

<div class="alert alert-success"><h4>v19.1.1, 21. February 2021</h4></div>
<ul>
<li>New: Docker pull and update script added which tags image with date 
to allow a rollback.</li>
</ul>

<div class="alert alert-success"><h4>v19.1, 20. February 2021</h4></div>
<ul>
<li>Updated: Copyright statement</li>
</ul>

<div class="alert alert-success"><h4>v19.0.2, 12. February 2021</h4></div>
<ul>
<li>New: Log output to the Docker Logging system in run.sh</li>
<li>New: Calc/Writer templates now also exposed</li>
<li>New: Source moved to new directory</li>
</ul>

<div class="alert alert-success"><h4>v19.0, 20. January 2021</h4></div>
<ul>
<li>New: First version with Docker support.</li>
</ul>


<div class="alert alert-success"><h4>v18.7, 3. December 2020</h4></div>
<ul>
<li>New: In list view the header text of each record can now be one or more  
fields instead of only the first field of a record.</li>
<li> New: The size of the header text is configurable to make a long header  
text more pleasing to the eye.</li>
</ul>

<div class="alert alert-success"><h4>v18.6, 11. November 2020</h4></div>
<ul>
<li>New: Permalink button in details view</li>
</ul>

<div class="alert alert-success"><h4>v18.5, 7. November 2020</h4></div>
<ul>
<li>Fix: Close button removed from details page as a script can no longer 
close a tab opened by another script/page.</li>
<li>Fix: Add and Move buttons now show variable content that contains "document" 
or other descriptor again.</li>
</ul>

<div class="alert alert-success"><h4>v18.4, 18. July 2020</h4></div>
<ul>
<li>New: Reduction of padding around fields in detail view to save vertical 
space
</ul>

<div class="alert alert-success"><h4>v18.3, 18. July 2020</h4></div>
<ul>
<li>New: The 'diff' button in details view has moved up into the header of 
each filed to save vertical space.</li>
</ul>

<div class="alert alert-success"><h4>v18.2, 12. July 2020</h4></div>
<ul>
<li>Fix: When the database-structure.php and version variable in config.php is 
changed, the list view additional fields config is now proprly reset.
</ul>

<div class="alert alert-success"><h4>v18.1, 10. June 2020</h4></div>
<ul>
<li>Fix: In searches, e and è (w/wo diacritis) are no longer considered the 
same character.</li>
</ul>

<div class="alert alert-success"><h4>v18.0, 31. May 2020</h4></div>
<ul>
<li>New: First code batch for individual user permissions added.</li>
</ul>

<div class="alert alert-success"><h4>v17.17, 16. May 2020</h4></div>
<ul>
<li>New: Improved detection of clickable URLs.</li>
</ul>

<div class="alert alert-success"><h4>v17.16, 16. May 2020</h4></div>
<ul>
<li>New: Single record quick and formatted export to Libreoffice Writer now 
also contains a clickable URL.</li>
</ul>

<div class="alert alert-success"><h4>v17.15, 19. October 2019</h4></div>
<ul>
<li>Fix: URLs with parameters that are separated by ? and & characters are 
now handled correctly. The clickable link function can now handle the html 
code of an ampersand correctly and the fix french punctuation part now removes 
a non-breakable space that is inserted before a ? character.</li>
</ul>

<div class="alert alert-success"><h4>v17.14, 5. August 2019</h4></div>
<ul>
<li>Fix: Print/Export improved, empty lines removed and ---- added for 
empty and not inline fields.
</ul>

<div class="alert alert-success"><h4>v17.13, 31. July 2019</h4></div>
<ul>
<li>New: Performance optimization in the export data code.</li>
</ul>

<div class="alert alert-success"><h4>v17.12, 31. July 2019</h4></div>
<ul>
<li>New: Emtpy lines at the end of the string are now also removed when exporting 
to a Libreoffice Calc document.</li>
</ul>

<div class="alert alert-success"><h4>v17.11, 30. July 2019</h4></div>
<ul>
<li>New: Emtpy lines at the end of the string are removed when exporting 
to a Libreoffice Writer document.</li>
</ul>

<div class="alert alert-success"><h4>v17.10, 26. July 2019</h4></div>
<ul>
<li>Fix: Searching for empty fields now also includes entries in which 
fields are set to NULL.</li>
<li>Fix: ----- now also treated as empty in database searches and in show/
hide empty fields in the details view.</li>
</ul>

<div class="alert alert-success"><h4>v17.9, 23. July 2019</h4></div>
<ul>
<li>New: When editing 'all fields', only show fields that were selected 
in the details view as well. Furthermore, show them in the order selected by the 
user.</li>
</ul>

<div class="alert alert-success"><h4>v17.8, 30. May 2019</h4></div>
<ul>
<li>New: Option added to 'List all records' selection to select fields 
to show in list result.</li>
</ul>

<div class="alert alert-success"><h4>v17.7, 29. May 2019</h4></div>
<ul>
<li>New: When exporiting to calc or office and no fields have been 
selected, all fields will now be automatically selected.</li>
</ul>

<div class="alert alert-success"><h4>v17.6, 26. May 2019</h4></div>
<ul>
<li>Fix: When creating a new record or during the bulk import process, the 
creation time of a record is now added as the modification date 
of the record.</li>
</ul>

<div class="alert alert-success"><h4>v17.5, 11. May 2019</h4></div>
<ul>
<li>New: Small conventions info buttons / tooltips now also available 
on the 'edit all' page.</li>
</ul>

<div class="alert alert-success"><h4>v17.4, 24. April 2019</h4></div>
<ul>
<li>New: Help pages and buttons added</li>
</ul>

<div class="alert alert-success"><h4>v17.3, 19. March 2019</h4></div>
<ul>
<li>New: Upload of Libreoffice template files added.</li>
</ul>

<div class="alert alert-success"><h4>v17.2, 4. March 2019</h4></div>
<ul>
<li>New: Table output to Calc is now also limited to a certain year if the 
user has selected this.</li>
</ul>

<div class="alert alert-success"><h4>v17.1, 3. March 2019</h4></div>
<ul>
<li>New: Table output now also directly exported to Libreoffice Calc instead 
of to HTML and then via Writer to Calc.</li>
</ul>

<div class="alert alert-success"><h4>v17.0, 15. February 2019</h4></div>
<ul>
<li>New: Libreoffice Export now generates a Libreoffice document directly 
instead of copy/pasting HTML text to Libreoffice and then using a macro 
to apply paragraph styles.</li>
</ul>

<div class="alert alert-success"><h4>v16.6, 25. November 2018</h4></div>
<ul>
<li>New: When the log file exceeds 10 MB it is now archived in a tar.gz 
file in the log directory.</li>
</ul>

<div class="alert alert-success"><h4>v16.5, 22. November 2018</h4></div>
<ul>
<li>Refactoring: Inline invocation of JS code from 'onclick' events 
now also put into JS modules. HTTP Security Headers updated to prevent 
inline JS code to be executed. </li>
</ul>

<div class="alert alert-success"><h4>v16.4, 28. October 2018</h4></div>
<ul>
<li>Refactoring: All Javascript code put into modules separate from php code</li>
<li>New: Show all changes of a record now opens in a new tab</li>
</ul>

<div class="alert alert-success"><h4>v16.3, 22. October 2018</h4></div>
<ul>
<li>New: A record can now be moved to a different category</li>
<li>Some further code refactoring, new js subdrirectory for Javascript code 
to be moved from php modules.</li> 
</ul>

<div class="alert alert-success"><h4>v16.2.1, v16.2.2, 02. and 03. October 2018</h4></div>
<ul>
<li>Refactored some code for maintainability</li> 
</ul>

<div class="alert alert-success"><h4>v16.2, 28. September 2018</h4></div>
<ul>
<li>Bug fix: Pointed braces now also treated correctly in edit boxes.</li>
<li>Bug fix: Title in tab: HTML encoded chars now converted to plain characters</li> 
</ul>

<div class="alert alert-success"><h4>v16.1.1, 27. September 2018</h4></div>
<ul>
<li>Internal function cleanup</li>
</ul>

<div class="alert alert-success"><h4>v16.1, 7. September 2018</h4></div>
<ul>
<li>New: Hostname now checked every time to indicate config errors early.</li>
<li>New: Salt for security token now in config.php</li>
</ul>

<div class="alert alert-success"><h4>v16.0.2, 29. August 2018</h4></div>
<ul>
<li>Bug fix: A page with an empty search result now also has a correct 
footer including the calculated time that was spent on the query.</li>
</ul>

<div class="alert alert-success"><h4>v16.0.1, 13. August 2018</h4></div>
<ul>
<li>Bug fix: List print output function now longer includes formatting string for 
the ID field when quick print is selected and the id is to be printed 
without an HTTP link.</li>
</ul>

<div class="alert alert-success"><h4>v16.0.0, 9. August 2018</h4></div>
<ul>
<li>New: JSON bulk import</li>
</ul>

<div class="alert alert-success"><h4>v15.0.0, 21. July 2018</h4></div>
<ul>
<li>New: Export calc and writer are now referencing the new hash id 
and thus the link to a record still works even if the record was put 
in a different location and therefore has received a new id.</li>
<li>New: New version table introduced. In the future, changes to the data 
scheme is automatically performed when a new software version is 
deployed.</li>
</ul>

<div class="alert alert-success"><h4>v14.6.0, 6. July 2018</h4></div>
<ul>
<li>New: Link to a new page that shows all instances when changes where 
made to any field of a record.</li>
<li>New: Execution time for a query now shown at the bottom of the page</li>
</ul>

<div class="alert alert-success"><h4>v14.5.0, 4. July 2018</h4></div>
<ul>
<li>New: A record is now locked before it is moved. Move is aborted if record 
is already locked by someone else. In addition, moving a record now also 
updates the 'last edited date/time/user' fields.</li>
</ul>

<div class="alert alert-success"><h4>v14.4.0, 2. July 2018</h4></div>
<ul>
<li>New: Security headers introduced in init.php to counter XSS and other
potential attacks.</li>
</ul>

<div class="alert alert-success"><h4>v14.3.1, 24. June 2018</h4></div>
<ul>
<li>Fix: Initial creation of the mod_history table did not create a 
primary key and auto increment and thus storing data into the table did not 
work.</li>
</ul>

<div class="alert alert-success"><h4>v14.3.0, 3. June 2018</h4></div>
<ul>
<li>New: Functionality added for creating the database tables when a new 
project is started.</li>
</ul>

<div class="alert alert-success"><h4>v14.2.0, 21. May 2018</h4></div>
<ul>
<li>New: Add a new document record at the end of a selectable category.</li>
</ul>

<div class="alert alert-success"><h4>v14.1.1, 20. May 2018</h4></div>
<ul>
<li>New: String for category 'FR - Ballets-fragments poetiques' updated</li>
<li>New: String for category '1641-1651' updated</li>
</ul>

<div class="alert alert-success"><h4>v14.1.0, 12. May 2018</h4></div>
<ul>
<li>New: Added CONVENTIONS for fields that can be viewed and edited by 
cicking on an info button next to a field of a record. A tooltip shows 
the text for quick access.</li>
<li>New: In detail view, the diff icon now also has a tooltip</li>
</ul>

<div class="alert alert-success"><h4>v14.0.0, 20. April 2018</h4></div>
<ul>
<li>New: PHP7 compatability</li>
<li>Bug fix: Full edit + save always jumped to the last doc record but 
should have returned to the same.</li>
</ul>

<div class="alert alert-success"><h4>v13.1.1, 6. April 2018</h4></div>
<ul>
<li>Bug fix: Full edit after single field edit of same record was blocked due 
to document load time being the same as the edit/save time. Time check modified 
from 'smaller or equal' to 'equal'</li>
</ul>

<div class="alert alert-success"><h4>v13.1.0, 5. April 2018</h4></div>
<ul>
<li>New: 'Detail view' now checks it the record was modified before 
opening the 'edit all' page. If modified an error message is given.</li>
</ul>

<div class="alert alert-success"><h4>v13.0.0, 3. April 2018</h4></div>
<ul>
<li>New: After moving/inserting a document record it is now checked if 
there is enough space after each database record to insert another one. If 
not the ID of records is incremented until there is one record ID in
between each existing record. If re-ordering is not (fully) possible due to 
locked records by the same or another user the (remaining) operation is 
deferred until the locked record is released (or the 3h lock timeout 
expires).</li>
</ul>

<div class="alert alert-success"><h4>v12.2.0, 18. March 2018</h4></div>
<ul>
<li>New: Check introduced that stops execution if no http user is given in  
the request.</li>
<li>New: .htaccess files now prevent execution of php files in subdirs.</li>
</ul>

<div class="alert alert-success"><h4>v12.1.0, 14. March 2018</h4></div>
<ul>
<li>New: A field is now only saved in the diff db table if the content 
is different from the previous field content.</li>
</ul>

<div class="alert alert-success"><h4>v12.0.1, 11. March 2018</h4></div>
<ul>
<li>New: Additional signatures for empty fields added</li>
</ul>

<div class="alert alert-success"><h4>v12.0, 11. March 2018</h4></div>
<ul>
<li>New: Use of HTMLPurifier to defend against XSS.</li>
</ul>

<div class="alert alert-success"><h4>v11.1, 4. March 2018</h4></div>
<ul>
<li>Bug fix: search/replace + removing bold/italics with a different 
search term was not working in the final replacement due to use of a 
wrong variable in a function call.</li>
</ul>

<div class="alert alert-success"><h4>v11.0, 4. March 2018</h4></div>
<ul>
<li>Database queries now use SQL prepared statements when user supplied data 
is embedded in an SQL query to better protect against SQL injection 
attacks.</li>
</ul>

<div class="alert alert-success"><h4>v10.14, 2. March 2018</h4></div>
<ul>
<li>Keyboard and swipe events in diff page added.</li>
</ul>

<div class="alert alert-success"><h4>v10.13, 1. March 2018</h4></div>
<ul>
<li>Moving a record after the last record of a category now sets the id 
of the record to +100 instead of a much higher number.</li>
</ul>

<div class="alert alert-success"><h4>v10.12, 28. February 2018</h4></div>
<ul>
<li>Init diff table now has its own page instead of using bs_test.php</li>
</ul>

<div class="alert alert-success"><h4>v10.11, 27. February 2018</h4></div>
<ul>
<li>Additional definitions for empty fields added</li>
</ul>

<div class="alert alert-success"><h4>v10.10, 26. February 2018</h4></div>
<ul>
<li>userconfigstorage.php now uses 'version' info from config.php instead 
of a local definition to decide whether the configuration in config.php 
and/or database-structure.php has changed. When the version number has changed 
the new database field names and order is written to the user configuration in
the database</li>
</ul>

<div class="alert alert-success"><h4>v10.9, 20. February 2018</h4></div>
<ul>
<li>Insert at end of db now increases ID by a configurable value (before: 
always only by 1)</li>
</ul>

<div class="alert alert-success"><h4>v10.8, 20. February 2018</h4></div>
<ul>
<li>Insert after a record functionality added</li>
</ul>

<div class="alert alert-success"><h4>v10.7.1, 16. February 2018</h4></div>
<ul>
<li>Cleanup, moved function out of config file, no functionality change</li>
</ul>

<div class="alert alert-success"><h4>v10.7, 27. January 2018</h4></div>
<ul>
<li>A modal dialog box is now shown if no diff is found</li>
</ul>

<div class="alert alert-success"><h4>v10.6, 22. January 2018</h4></div>
<ul>
<li>Refactoring: Fix french punctuation can now be switched on/off 
via config.php</li>
<li>.htaccess removed from main directory, now has to be done via web server 
config file or the file has to be added for a specific project setup.</li>
</ul>

<div class="alert alert-success"><h4>v10.5, 20. January 2018</h4></div>
<ul>
<li>Refactoring: id-structure now also generalised</li>
</ul>

<div class="alert alert-success"><h4>v10.4, 11. January 2018</h4></div>
<ul>
<li>Bug fix: 'not like empty' searches in multi-search fixed</li>
<li>Error notification introduced if there is a problem with the SQL query 
instead of just returning an empty result set</li> 
<li>Refactoring: additional generalisations made</li>
</ul>

<div class="alert alert-success"><h4>v10.3, 7. January 2018</h4></div>
<ul>
<li>Refactoring: table name now in config variable. No new functionality 
added.</li>
</ul>

<div class="alert alert-success"><h4>v10.2, 7. January 2018</h4></div>
<ul>
<li>Refactoring: DocRecord now uses loops instead of variable lists. No 
new functionality added.</li>
</ul>

<div class="alert alert-success"><h4>v10.1, 6. January 2018</h4></div>
<ul>
<li>Refactoring: Places in which ballet/document is mentioned now contain
a variable for the name which can be set in config.php. No new  
functionality.</li>
</ul>

<div class="alert alert-success"><h4>v10.0, 4. January 2018</h4></div>
<ul>
<li>Refactoring: Major variable name and comment changes in the code, 
no functionality change</li>
</ul>

<div class="alert alert-success"><h4>v9.1, 2. January 2018</h4></div>
<ul>
<li>Refactoring: Ballet wording removed from all php filenames.</li>
</ul>

<div class="alert alert-success"><h4>v9.0, 25. December 2017</h4></div>
<ul>
<li>New: Diff functionality for individual record fields introduced.</li>
</ul>

<div class="alert alert-success"><h4>v8.3, 21. November 2017</h4></div>
<ul>
<li>New: A case sensitive search and search/replace functionality has been 
introduced.</li>
</ul>

<div class="alert alert-success"><h4>v8.2, 7. November 2017</h4></div>
<ul>
<li>New: On search pages the checkbox for showing extra fields gets 
ticked automatically when the field selection button is pressed.</li>
</ul>

<div class="alert alert-success"><h4>v8.1.2, 7. November 2017</h4></div>
<ul>
<li>Updated: Text in print dialog box in list view</li>
<li>Bug fix: Further unset()s added in other parts of the code where
foreach is used with references instead of values.</li>
</ul>

<div class="alert alert-success"><h4>v8.1.1, 6. November 2017</h4></div>
<ul>
<li>Bug fix: unset() to make modification in foreach loop work for the 
last element as well in bs_change_display_order.php.</li>
</ul>

<div class="alert alert-success"><h4>v8.1, 6. November 2017</h4></div>
<ul>
<li>New: Configuration of extra fields in the search result list now 
independent from extra fields configured for single ballets detail view.</li>
<li>New: Print functions extended to show extra fields of list view or 
detail view depending on where they are called from.</li>
</ul>

<div class="alert alert-success"><h4>v8.0, 2. November 2017</h4></div>
<ul>
<li>New: Show extra fields in the search result list and enable the user to 
configure which extra fields to show.</li>
</ul>


<div class="alert alert-success"><h4>v7.31, 20. October 2017</h4></div>
<ul>
<li>Bug fix: When print variables are initialized for the first time 
create the variables on the server as well and save them to the 
database so that the first printout with or without IDs works correctly.</li>
</ul>

<div class="alert alert-success"><h4>v7.30, 6. October 2017</h4></div>
<ul>
<li>Multisearch results now also show fields in which one of the search terms 
was found.</li>
</ul>

<div class="alert alert-success"><h4>v7.29, 23. Sept 2017</h4></div>
<ul>
<li>Internal code cleanup, no functionality change.</li>
</ul>

<div class="alert alert-success"><h4>v7.28, 19. Sept 2017</h4></div>
<ul>
<li>Print function enhanced to replace HTML breaks with HTML paragraph to 
have proper line endings for justify formatting in Libreoffice. 
year.</li>
</ul>

<div class="alert alert-success"><h4>v7.27, 19. Sept 2017</h4></div>
<ul>
<li>Print function enhanced to also understand searches limited to a given 
year.</li>
</ul>

<div class="alert alert-success"><h4>v7.26, 19. Sept 2017</h4></div>
<ul>
<li>Print function can now also print empty fields if the user does not 
hide them in web view.</li>
<li>Info text clarification that when a group search is limited to a certain 
year than the list will also include ballets with an empty numerotation that 
follow directly behind a ballet that matches the selected year.</li>
</ul>

<div class="alert alert-success"><h4>v7.25.1, 2. Sept 2017</h4></div>
<ul>
<li>HTML clean-up in bs_home.php, no functionality change</li>
</ul>

<div class="alert alert-success"><h4>v7.25, 2. Sept 2017</h4></div>
<ul>
<li>Javascript code added in bs_home.php to expand the multi field search to  
more fields with a '+' button instead of allways showing 
all fields.</li>
</ul>

<div class="alert alert-success"><h4>v7.24, 25. August 2017</h4></div>
<ul>
<li>Search in title option removed. This was an early search option that  
is no longer used.</li>
</ul>

<div class="alert alert-success"><h4>v7.23, 25. August 2017</h4></div>
<ul>
<li>Extended 'multi-search value checking' feature. Several error scenarios 
are now checked and a Bootstrap modal dialog box is used to interact with 
the user.</li>
</ul>

<div class="alert alert-success"><h4>v7.22, 23. August 2017</h4></div>
<ul>
<li>First implementation of multi-search value checking before the 
request is sent to the server.</li>
</ul>

<div class="alert alert-success"><h4>v7.21, 22. August 2017</h4></div>
<ul>
<li>field-empty-functions refactored, empty HTML string representations 
put into an array and used for both display and SQL search query assembly 
functions.</li>
</ul>


<div class="alert alert-success"><h4>v7.20.1, 18. August 2017</h4></div>
<ul>
<li>Field now also recognized as empty if only paragraph+emphasis inside.</li>
</ul>

<div class="alert alert-success"><h4>v7.20, 18. August 2017</h4></div>
<ul>
<li>Emtpy field detection function moved to field-empty-functions.php.</li>
<li>Bug fix emtpy/non-empty was giving false positve due to use of SQL 
LIKE instead of equal/not equal parameters.</li>
</ul>

<div class="alert alert-success"><h4>v7.19, 12. August 2017</h4></div>
<ul>
<li>Quick print functionality introduced to enable printing without a lot of 
formatting directly from the browser.</li>
</ul>

<div class="alert alert-success"><h4>v7.18.1, 12. August 2017</h4></div>
<ul>
<li>Paragraph + bold and otherwise empty now also treated as empty field</li>
</ul>

<div class="alert alert-success"><h4>v7.18, 12. August 2017</h4></div>
<ul>
<li>New categories added: "Autres Aires Geographiques" for ballets in other 
countries than FR and AT.</li>
</ul>

<div class="alert alert-success"><h4>v7.17, 11. August 2017</h4></div>
<ul>
<li>Fix in multi-search: Search for empty / not-empty field now also takes
the string length into account (i.e. more or less than 3 chars) and thus 
also finds empty fields without any HTML formatting.</li>
</ul>

<div class="alert alert-success"><h4>v7.16, 11. August 2017</h4></div>
<ul>
<li>Fix in print function: In case numerotation is empty it was not 
printed at all thus not showing the beginning of a new ballet in the 
printout.</li>
</ul>

<div class="alert alert-success"><h4>v7.15, 9. August 2017</h4></div>
<ul>
<li>bs_record_details.php: the definition of 'empty' field as modified  
in v.7.14 is now also applied for the logic whether to show the field  
or not and also in ballet.php in the 'search for empty field' 
method.</li>
</ul>

<div class="alert alert-success"><h4>v7.14, 9. August 2017</h4></div>
<ul>
<li>bs_record_details.php: Added further strings that can be included in  
a field to be treated as empty strings and thus replaced by -----.</li>
</ul>

<div class="alert alert-success"><h4>v7.13, 3. August 2017</h4></div>
<ul>
<li>Search/replace in all fields can now remove italics and bold formatting 
around search terms</li>
</ul>

<div class="alert alert-success"><h4>v7.12, 3. August 2017</h4></div>
<ul>
<li>Bug fix: Aprostorophe in search replace string has led to errors
as the strings are embedeed in the html pages for the commit button and
the aprostrophe terminates the string early. Also the GUI field editor 
replaces the aprostrophe with the HTML escaped sequence so searches
were not successful. This update now replaces the aprostroph charachters in 
the search and replace strings with the HTML escape sequence (39).</li>
</ul>

<div class="alert alert-success"><h4>v7.11.1, 1. August 2017</h4></div>
<ul>
<li>Multi search extended from 5 to 7 fields with simple parameter 
modification</li>
</ul>

<div class="alert alert-success"><h4>v7.11, 1. August 2017</h4></div>
<ul>
<li>Additional changes in field names</li>
<li>Changed default field order for viewing and printing</li>
<li>Minor comments cleanup, no functionality change</li>
</ul>

<div class="alert alert-success"><h4>v7.10.1, 24. July 2017</h4></div>
<ul>
<li>First preparations for function to remove surrounding italics/bold 
formatting in a field in the search/replace functionality</li>
<li>Minor code cleanup, no functionality change</li>
</ul>


<div class="alert alert-success"><h4>v7.10, 22. July 2017</h4></div>
<ul>
<li>Print names of a number of columns changed (to plural)</li>
</ul>

<div class="alert alert-success"><h4>v7.9, 9. July 2017</h4></div>
<ul>
<li>Switched from InnoDB to myISAM database engine as rows with 
large entries could not be edited anymore. No code change necessary, this 
is a mySQL internal change.</li>
<li>mySQL error messages now written to database log (but NOT yet shown in the 
browser.</li>
<li>Fixed uninitialized log object in French gramar adaption 1 that prevented 
an updated of a field or record in case the function made changes </li>
<li>Changed 'Genre' has be an inline field.</li>
</ul>

<div class="alert alert-success"><h4>v7.8, 9. June 2017</h4></div>
<ul>
<li>New function in details view: Toggle temporary display of all fields 
instead of only the selected ones</li>
</ul>

<div class="alert alert-success"><h4>v7.7, 6. June 2017</h4></div>
<ul>
<li>Print field name 'Nom d'éditeur Livret' changed to 'Nom d'éditeur'. 
Note: Personal field order settings have to be reset for the change to become 
visible for individual users.</li>
</ul>

<div class="alert alert-success"><h4>v7.6, 21. May 2017</h4></div>
<ul>
<li>HTTP and HTTPS URLs are now clickable and open a new tab.
<li>Non-breakable spaces are no longer inserted in https:// and http://.</li>
<li>Fix text functions moved from functions.php to fix-text-functions.php.</li> 
</ul>

<div class="alert alert-success"><h4>v7.5, 19. May 2017</h4></div>
<ul>
<li>Refined 'ballet multi-search': Empty field search now also invoked via 
checkbox.</li>
<li>Refined 'ballet multi-search': Non-empty field search implemented. </li> 
</ul>

<div class="alert alert-success"><h4>v7.4, 18. May 2017</h4></div>
<ul>
<li>Refined 'ballet multi-search' with a check box to be able to exclude 
search results that contain a given text in a field. 
</ul>

<div class="alert alert-success"><h4>v7.3, 8. May 2017</h4></div>
<ul>
<li>Refined 'search for all ballets with colored text' to exclude ballets 
with text that is colored in black (#00000), which is the same as not colored 
at all but still encapsulated in an html span element. This was done using
a REGEX SQL query that searches for the following term in all fields: 
color:#[^000000] 
</ul>

<div class="alert alert-success"><h4>v7.2, 23. April 2017</h4></div>
<ul>
<li>New: Table New HTML Table Export for Libreoffice Calc.
</ul>

<div class="alert alert-success"><h4>v7.1, 20. April 2017</h4></div>
<ul>
<li>List view Export to Lireoffice can now be configured (Ballet ID on/off, 
include link yes/no, page break yes/no). Settings are saved on the server side 
in session variables and in a database table.
</ul>

<div class="alert alert-success"><h4>v7.0, 16. April 2017</h4></div>
<ul>
<li>Session variables are now saved to a new table (user_config) in the 
database.
</ul>

<div class="alert alert-success"><h4>v6.9.2, 11. April 2017</h4></div>
<ul>
<li>Include of database-structure.php in several other files replaced 
by including the file in init.php and then declaring the ballet database 
structure variable as global in the source code files that require it. Moved 
database-structure.php to config directory.
<li>id-structure.php moved to config directory.
</ul>

<div class="alert alert-success"><h4>v6.9.2, 12. April 2017</h4></div>
<ul>
<li>Moved database access parameters to config.php.
<li>Moved database-structure.php and id-structure.php to /config. Code change 
to access those read only variables as 'global' instead of importing them in 
every code file.
</ul>

<div class="alert alert-success"><h4>v6.9.1, 09. April 2017</h4></div>
<ul>
<li>Page header now wrapped in bootstrap container-fluid div to exactly adjust 
to the page width 
</ul>

<div class="alert alert-success"><h4>v6.9, 09. April 2017</h4></div>
<ul>
<li>List ballets by decade/category can now be limitied to a certain year.
</ul>

<div class="alert alert-success"><h4>v6.8.2, 07. April 2017</h4></div>
<ul>
<li>Records now locked before being modified during search/replace.
</ul>

<div class="alert alert-success"><h4>v6.8.1, 05. April 2017</h4></div>
<ul>
<li>Extended anti-CSRF functionality and moved to sec-functions.php.
<li>config.php introduced. First parameter are the allowed hostnames from 
which change requests can originate (anti-CSRF stage 2 function!).
<li>bs_record_list_view.php POST button generation generalized.
<li>Bug fix: Single ballet field edit error is now shown to the user.
</ul>

<div class="alert alert-success"><h4>v6.8, 31. March 2017</h4></div>
<ul>
<li>Libreoffice List Output now contains database links as part of the id of 
each exported ballet.
</ul>

<div class="alert alert-success"><h4>v6.7, 31. March 2017</h4></div>
<ul>
<li>List of the latest modified and accessed database record now available 
via the about page.
<li> Info text in detail view and change field order pages now changes from 
desktop to touch if a touch device is detected.
</ul>

<div class="alert alert-success"><h4>v6.6.1, 30. March 2017</h4></div>
<ul>
<li>Bug fix: Reset view now fully resets the view and not only the fields 
shown in bs_change_display_order.php.
</ul>

<div class="alert alert-success"><h4>v6.6, 29. March 2017</h4></div>
<ul>
<li>Swipe left/right implemented for detail view on touch devices to go 
to the previous/next ballet
<li>New paragraph style ab-0..40 for in-line ballet database fields. 
aa-0 and ah-0 formats are no longer used. This way, changes to all heads 
and paragraph childern can be better propagated.
</ul>

<div class="alert alert-success"><h4>v6.5.1, 28. March 2017</h4></div>
<ul>
<li>Code clean-up: bs_change_display_order.php now loops over '.ballet-field' 
instead of 'li' list elements.
</ul>

<div class="alert alert-success"><h4>v6.5, 27. March 2017</h4></div>
<ul>
<li>Select field order for the details view now also works with a touch 
interface.
</ul>

<div class="alert alert-success"><h4>v6.4.1, 23. March 2017</h4></div>
<ul>
<li>Some code cleanup.
</ul>

<div class="alert alert-success"><h4>v6.4, 22. March 2017</h4></div>
<ul>
<li>First implementation of 'Select Order' in which a ballet record's 
fields are shown and exported to Libreoffice
</ul>

<div class="alert alert-success"><h4>v6.3, 18. March 2017</h4></div>
<ul>
<li>Detail view now shows when and by whom the entry was last modified in 
small print at the end of the list
<li>Empty fields can now be hidden in the detail view. The server stores 
whether to show or hide empty fields for subsequent requests while the session
is not destroyed.
</ul>

<div class="alert alert-success"><h4>v6.2, 18. March 2017</h4></div>
<ul>
<li>In the detail view, the left and right arrow keys can now be used to go to
the next or the previous ballet
<li>Additional quote types (used in Austrian descriptons) are now detected and
automatically changed to French quotes
</ul>

<div class="alert alert-success"><h4>v6.1.1, 18. March 2017</h4></div>
<ul>
<li>Unlocking mechanism extended to also unlock a record if the web page
is left without saving changes
</ul>

<div class="alert alert-success"><h4>v6.1, 17. March 2017</h4></div>
<ul>
<li>Database records are now locked/released before/after editing to 
prevent users from inadvertedly overwriting data by changing the same record
simultaneously.
</ul>

<div class="alert alert-success"><h4>v6.0.1, 13. March 2017</h4></div>
<ul>
<li>'Leave' warning deactivated after ballet was deleted to prevent dialog
box to pop-up warning that some data was not saved
<li>Moved all Javascript functions in same 'script' area at the start of the file
<li>bs_record_details.php code cleanup
</ul>

<div class="alert alert-success"><h4>v6.0, 12. March 2017</h4></div>
<ul>
<li>Ballet 'delete' function introduced again
</ul>

<div class="alert alert-success"><h4>v5.9.2, 09. March 2017</h4></div>
<ul>
<li>index_bs.php removed to have a single entry page. Links adapted.
</ul>

<div class="alert alert-success"><h4>v5.9.1, 08. March 2017</h4></div>
<ul>
<li>Now using aa instead of ah style for single line of a field 
Libreoffice export to prevent headers of consecurity single line fields
sticking together and forcing a page break to early
</ul>

<div class="alert alert-success"><h4>v5.9, 07. March 2017</h4></div>
<ul>
<li>DB field name and content can now be put on a single line in LO export.
Flag in database-structure.php added to make this flexible
<li>Moved common code of Libreoffice Export functionality to a single place
<li>database-structure.php cleaned up
</ul>

<div class="alert alert-success"><h4>v5.8, 06. March 2017</h4></div>
<ul>
<li>Austrian decades/categories created
<li>Searches can now not only inlcude a single decade/category but all
French or all Austrian categories ('FR - all' and 'AT - all' selectors)
<li>List view print now hides fields if they are empty.
</ul>


<div class="alert alert-success"><h4>v5.7.1, 05. March 2017</h4></div>
<ul>
<li>Bug fix: Move/Insert - stripped HTML tags to prevent color HTML markings
to interfere with the on-screen button layout
</ul>

<div class="alert alert-success"><h4>v5.7, 05. March 2017</h4></div>
<ul>
<li>Further French punctuation corrections introduced
<li>Text in all search variants is now adapted to French punctuation style
before used for a database search. 
</ul>

<div class="alert alert-success"><h4>v5.6, 03. March 2017</h4></div>
<ul>
<li>Multi search (several fields) not supports searching for empty fields
</ul>

<div class="alert alert-success"><h4>v5.5, 02. March 2017</h4></div>
<ul>
<li>Additional French punctuation correction introduced
</ul>

<div class="alert alert-success"><h4>v5.4.1, 01. March 2017</h4></div>
<ul>
<li>Code to create a drop down list in bs_home.php to limit a search
to a decade/category concentrated in a single place
<li>Multi-search output of the search terms in ballet list result page
deactivated.
</ul>

<div class="alert alert-success"><h4>v5.4, 28. Feburary 2017</h4></div>
<ul>
<li>Most searches can now be limited to an id-range (decade/category)
</ul>

<div class="alert alert-success"><h4>v5.3, 28. Feburary 2017</h4></div>
<ul>
<li>Search for ballets with empty fields added
</ul>

<div class="alert alert-success"><h4>v5.2, 27. Feburary 2017</h4></div>
<ul>
<li>Search in several data base fields now with AND or OR boolean operation possible
<li>Text can now be put into superscript when editing a field 
</ul>

<div class="alert alert-success"><h4>v5.1, 27. Feburary 2017</h4></div>
<ul>
<li>Ballet move/insert-after functionality added in list view
</ul>

<div class="alert alert-success"><h4>v5.0, 26. Feburary 2017</h4></div>
<ul>
<li>Mark ALL occurences of a search term in blue in a field.
<li>Renamed "ballet sans datation" to "ballet sans date" in bs_home.php
</ul>

<div class="alert alert-success"><h4>v4.9.1, 24. Feburary 2017</h4></div>
<ul>
<li>Bug fix: List all ballets with colors
</ul>

<div class="alert alert-success"><h4>v4.9, 24. Feburary 2017</h4></div>
<ul>
<li>Search/Replace now with the option to put the replacement text in bold 
    and italics.
</ul>

<div class="alert alert-success"><h4>v4.8.1, 23. Feburary 2017</h4></div>
<ul>Bug fix: Ballet List View - Libre Office Export button fixed
</ul>

<div class="alert alert-success"><h4>v4.8, 22. Feburary 2017</h4></div>
<ul>
<li>First part of search/replace introduced, non-style sensitivie, case in-sensitive
<li>Search all fields: The search term is now colored in blue!
</ul>

<div class="alert alert-success"><h4>v4.7, 20. Feburary 2017</h4></div>
<ul>
<li>When searching all fields, the list of ballets in which the search term
was found now contains the field name(s) + field content(s) where the search
term was found.
</ul>

<div class="alert alert-success"><h4>v4.6, 19. Feburary 2017</h4></div>
<ul>
<li>Logging module now outputs calling function name
<li>SQL statements are now logged
<li>Code scrub
</ul>

<div class="alert alert-success"><h4>v4.5, 17. Feburary 2017</h4></div>
<ul>
<li>Removal of unused files.
</ul>

<div class="alert alert-success"><h4>v4.4, 17. Feburary 2017</h4></div>
<ul>
<li>Neutral quotes in fields are now converted to French left and right quotes
    when the save button is pressed.
</ul>

<div class="alert alert-success"><h4>v4.3.2, 11. Feburary 2017</h4></div>
<ul>
<li>Some code clean-up
<li>Ballet record or field update result text now logged.
<li>Invalid session token event now logged.
</ul>

<div class="alert alert-success"><h4>v4.3.1, 10. Feburary 2017</h4></div>
<ul>
<li>Some code clean-up
<li>Create new ballet button on main page now working
<li>Create new ballet button on list page now pointing to correct bs URL
</ul>

<div class="alert alert-success"><h4>v4.3, 10. Feburary 2017</h4></div>
<ul>
<li>UI improvement: Single click to edit text in ballet details on devices 
    without a mouse (mobile, tablet) introduced.
</ul>

<div class="alert alert-success"><h4>v4.2, 08. Feburary 2017</h4></div>
<ul>
<li>UI improvement: Sidebar now always goes to the bottom and works on mobile.
<li>UI improvement: On mobile a small picture is shown in the header
<li>UI improvement: CKEditor resizes when the browser window width changes. 
</ul>

<div class="alert alert-success"><h4>v4.1, 07. Feburary 2017</h4></div>
<ul>
<li>Added fade-out functionality for the alert box that is shown 
    when no next or previous record is found
<li>Welcome message shown on the home page with fadeout effect
<li>HTTP username added to logging class
</ul>

<br>
<div class="alert alert-success"><h4>v4.0, 05. Feburary 2017</h4></div>
<ul>
<li>Bootstrap Web Frontend Framework introduced
</ul>

<br>
<div class="alert alert-success"><h4>v3.2, 03. Feburary 2017</h4></div>
<ul>
<li>Numerotation of a Ballet entry is now shown in the browser tab
</ul>

<br>
<div class="alert alert-success"><h4>v3.1, 02. Feburary 2017</h4></div>
<ul>
<li>Goto previous and next ballet details function and buttons added
<li>Larger Buttons
<li>Header image size reduced
<li>Log file format optimized
</ul>

<br>
<div class="alert alert-success"><h4>v3.0, 31. January 2017</h4></div>
<ul>
<li>Add feature to list all ballets of a decade
    (previously a "tab" in the Calc document)
<li>Ballet ID added on details page with 80% font size
<li>Back button on pages now only shown if back URL is given
<li>Code clean-up
</ul>

<br>
<div class="alert alert-success"><h4>v2.9, 29. January 2017</h4></div>
<ul>
<li>Search for ballets with colored text implemented
<li>Search for text in all fields implemented
</ul>

<br>
<div class="alert alert-success"><h4>v2.8, 28. January 2017</h4></div>
<ul>
<li>Ballet Output to Libreoffice for searches implemented
</ul>

<br>
<div class="alert alert-success"><h4>v2.7, 27. January 2017</h4></div>
<ul>
<li>Ballet Output to Libreoffice "single" + "all" now contain formatting 
    information so a Libreoffice Macro can assign a different style to
    each header and each field of a ballet when exported from the database.
</ul>

<br>
<div class="alert alert-success"><h4>v2.6, 21. January 2017</h4></div>
<ul>
<li>When viewing a ballet's details a double click on any field 
    text opens an editor field. Editor expands automatically by using
    a separate ckeditor config file (/ckeditor/config-auto-grow.js)
<li>Update all ballet details page now presents a warning when the user wants 
    to leave without saving
<li>Bigger font size in edit window (/ckeditor/content.css)
<li>Multisearch can now also handle French special characters (ballet.php)
    updated getRecordsMultiFieldSearch to convert search text into HTML 
    with htmlentities()
<li>Multisearch page now makes clear it ANDs the fields for seraching    
</ul>

<br>
<div class="alert alert-success"><h4>v2.5, 17. January 2017</h4></div>
<ul>
<li>Further field updates done in database-structure.php
<li>Several ballets can now be modified in different tabs simultaneously 
    (CSRF 'token' reused)
<li>Changed session lifetime token (session.gc_maxlifetime) in 
    /etc/php5/apache2/php.ini from 30 minutes to 2 days
</ul>

<br>
<div class="alert alert-success"><h4>v2.4.1, 15. January 2017</h4></div>
<ul>
<li>Fixed create new ballet function after adding 3 fields
<li>Libreoffice output of single ballet enhanced to properly output names of 
    empty fields
</ul>

<br>
<div class="alert alert-success"><h4>v2.4.0, 15. January 2017</h4></div>
<ul>
<li>Three new database fields added: Contenu, Auteurs, Structure du ballet
<li>Presentation and print order of fields changed
</ul>

<br>
<div class="alert alert-success"><h4>v2.3.1, 10. January 2017</h4></div>
<ul>
<li>Button functionality at end of single ballet display changed to lead to 
    Libreoffice output
</ul>

<br>
<div class="alert alert-success"><h4>v2.3.0, 8. January 2017</h4></div>
<ul>
<li>Text color now important and usable
<li>search in multiple fields sort order changed to ID
<li>Output of all fields of all entries for Libreoffice added
</ul>

<br>
<div class="alert alert-success"><h4>v2.2.0, 4. January 2017</h4></div>
<ul>
<li>Database and code updated again to reflect current columns in Excel charts
<li>ID database field is a 6/7 digit number that identifies the sheet and each 
ballet line from the original Excel file. 100 numbers between the ballets ensures
that ballets can still be inserted in the future.
<li>'Livret Titre (Originalversion) field is auto generated from 'Livret' field
in basic before database import
<li>French lanugage correction in ckeditor input fields added
<li>General List by ID not by Year anymore
<li>General list updated to show numerotation, presentation date, title, 
formatting improved (paragraph at beginning and end removed)
<li>Conversion of double quotes to French sideways double quotes with space
</ul>


<br>
<div class="alert alert-success"><h4>v2.1.0, 16. December 2016</h4></div>
<ul>
<li>Database and code updated to reflect current columns in Excel charts
<li>Update and create ballets now works with HTML, all fields updated
<li>Search now possible with french characters, string is HTML converted before 
    the search
<li>Old options no longer used (e.g. RTF export/templates) removed. Code still 
present
</ul>

<br>
<div class="alert alert-success"><h4>v2.0.0, 28. June 2016</h4></div>
<ul>
<li>Database changed, new files added, old fields removed
<li>Input fields can now be configured to allow formatting (bold, italics, etc.)
<li>Database fields changed from varchar to text to remove field length limits.
</ul>

<br>
<div class="alert alert-success"><h4>v1.2.0, 29. May 2014</h4></div>
<ul>
<li>Multi-field search added
<li>Input box length adapted for Firefox and IE on Windows systems (100 chars 
    instead of 110)
<li>Download of single + list rtf templates, current + original version
</ul>

<br>
<div class="alert alert-success"><h4>v1.1.0, 16. March 2014 </h4></div>
<ul>
<li>RTF Template Upload added 
<li>Activity logging added to log/ballet-logfile.log
</ul>

<br>
<div class="alert alert-success"><h4>v1.0.1, 13. March 2014</h4></div>
<ul>
<li>About page extended to contain version history
<li>Legacy PHP24 code removed
</ul>

<br>
<div class="alert alert-success"><h4>v1.0.0, 12. March 2014, Initial Release
</h4></div>
<ul>
<li>Initial design of frontend, database and processing logic
<li>Introduced limit for input fields to ensure nothing is cut without the user 
    noticing
<li>Search functionality for a ballet added
<li>Functionality to delete a ballet added
<li>Complete import function to read all fields from csv to database
<li>Result list counter of the number of ballets found introduced
<li>Enable return from ballet details to search result list
<li>Presentation date database field extended as it was too short for original 
    information
<li>Search: all ballets with a wrong year format
<li>Search: all ballets with a presentation date
<li>Export functionality of single ballets and ballet lists to Office Writer 
    in RTF format
</ul>

    </div> <!-- end of right column -->
  </div> <!-- end of row -->
</div> <!-- end of container -->
