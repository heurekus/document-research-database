<?php
/**
 * bs_list_mod_or_viewed.php
 *
 * Shows a list of database records that were recently modified or viewed
 * depending on the presence of the HTTP GET 'show_modified' parameters.
 *
 * @version    1.0 2017-03-31
 * @package    DRDB
 * @copyright  Copyright (c) 2014-17 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('Show list of modified records');

if (!UserPermissions::hasAccess('history')) {
    return;
} 

?>
<script src="js/list_mod_or_viewed.js"></script>

<div class="container-fluid">

  <div class="row content">
    <div class="col-sm-2 sidenav">      
      <br>

      <ul id="menu_area" class="nav nav-pills nav-stacked custom">
        <li class="active" id="closetab">
          <a href="#a1" id="menu_text">
             <span class="glyphicon glyphicon-asterisk"></span> Close tab</a>
        </li>              
       </ul>
    </div>
   
    <div class="col-sm-10"> 

<?php 

// Get list of modified or access document records
if (isset($_GET['show_modified'])) {
	echo '<h3>List of the last 100 modified database records</h3><br><br>';
	$items = DocRecord::getSortedListOfModifiedDocs(100);
}
else {
	echo '<h3>List of the last 100 viewed database records</h3><br><br>';
	$items = DocRecord::getSortedListOfViewedDocs(100);
}

// If document entries were found, display them
if ($items) {
	
	foreach ($items as $i=>$item) :
	 	
 		echo '<div class="alert alert-info">';
 		echo '<span class="label label-default">';
	 	
	 	//index.php?content=bs_record_details&id=1001000450
	 	echo '<a href="index.php?content=bs_record_details&id=' .
	 			$item->getField('id') . 
	 			'" style="color:white" target="_blank">ID: ' . 
	 			$item->getId() . '</a>';
	 	
	 	echo '</span>';
	 	 
	 	$first_db_field_name = getDbFirstFieldName();
	 	$content = $item->getField($first_db_field_name);
	 	$content = str_ireplace('<P>','',$content);
	 	$content = str_ireplace('</P>','',$content);
	 	$content = strip_tags($content);
	 	if (strlen($content) < 3) $content = "---";	 	 
	 	echo ' ' . $content;
	 	
	 	// Show last modification or view time and user
	 	if (isset($_GET['show_modified'])) {
	 		if ($item->getField('last_update_time') != 0) {
	 			 
	 			echo ', last edited by ' .
	 					ucfirst($item->getField('last_update_user')).
	 					' on '. date("Y-m-d H:i:s", 
	 							     $item->getField('last_update_time'));
	 		}	 		
	 	}
	 	else {
	 		if ($item->getField('detail_view_time') != 0) {
	 		
	 			echo ', last viewed by ' .
	 					ucfirst($item->getField('detail_view_user')).
	 					' on '. date("Y-m-d H:i:s", 
	 							     $item->getField('detail_view_time'));
	 		}
	 		
	 	}
	 	
	 	 
 		echo '</div>';
 	
	endforeach;
}
else {
	$log->lwrite('No items...!?');
}

?>


    </div> <!-- end of right column -->
  </div> <!-- end of row -->
</div> <!-- end of container -->