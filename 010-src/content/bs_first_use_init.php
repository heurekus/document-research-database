<?php
/**
 * bs_first_use_init.php.php
 *
 * To be called after modifying the configuration files when starting a new
 * project. Creates the required database tables. Quite a number of
 * configuration variables need to be set correctly to make this work.
 * In case there is a problem, the createInitialTable() functions output
 * SQL error messages to the log file. 
 *
 * @version    1.0 2018-06-03
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('bs_first_use_init.php: Attempting to create the database ' . 
		     'tables for a new project');

echo '<p></p>';

echo '<div class="alert alert-info">';
echo 'Trying to create database tables...';
echo '</div>';

if (!DocRecord::createInitialTable()) {
	outputTableCreateFail('main');	
	return;
} else {
	outputTableCreateSuccess('main');	
}

if (!ModHistoryStorage::createInitialTable()) {
	outputTableCreateFail('change history');	
	return;
} else {
	outputTableCreateSuccess('change history');
}

if (!UserConfigStorage::createInitialTable()) {
	outputTableCreateFail('user config');
	return;
} else {
	outputTableCreateSuccess('user config');
}


if (!UserConfigStorage::populateInitialUsersAndPermissions()) {
    outputTableCreateFail('initial user permissions');
    return;
} else {
    outputTableCreateSuccess('initial user permissions');
}

if (!ProjectVersion::createInitialTable()) {
	outputTableCreateFail('project version config');
	return;
} else {
	outputTableCreateSuccess('project version config');
}

if (!DatabaseConfigStorage::createInitialTable()) {
    outputTableCreateFail('database config');
    return;
} else {
    outputTableCreateSuccess('database config');
}


echo '<div class="alert alert-success">';
echo 'All tables created successfully!';
echo '</div>';

return;


function outputTableCreateSuccess($table_name) {
	echo '<div class="alert alert-success">';
	echo $table_name . ' table/entries created successfully...';
	echo '</div>';
}

function outputTableCreateFail($table_name) {
	echo '<div class="alert alert-danger">';
	echo '<p>Database Error! Unable to create the ' . $table_name . 
	     ' table/entries, aborting...</p>';
	echo '<p>Have a look at the log file for details!</p>';
	echo '</div>';	
}

?>