<?php
/**
 * bs_change_field_display_order.php
 *
 * This page offers movable panels for the user to select and change the 
 * order of how database fields of a record are presented.
 *
 * @version    1.0 2017-03-17
 * @package    DRDB
 * @copyright  Copyright (c) 2014-17 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */
?>

<style type="text/css">
    /* show the move cursor as the user moves the mouse over the panel header.*/
    #draggablePanelList .panel-heading {
        cursor: move;
    }
</style>

<script data-cfasync="false" src="/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

<!--The following small library modifies jquery-ui to also handle-->
<!--touch events, i.e. panels are movable as well with a touch interface-->
<script src="/jquery.ui.touch/jquery.ui.touch-punch.min.js"></script>

<script src="js/change_display_order.js"></script>

<?php 
$log = new Logging();
$log->lwrite('\'Change field display order\' page accessed');

// Get the ID of the database record to which to return and embedd it in the
// DOM so the Javascript part of this page can pick it up!
if (!isset($_GET['id'])) {
	echo '<h1> ERROR: Parameter "ID" missing! </h1>';
	$log->lwrite('bs_change_display_order.php: Error, ID parameter missing');
	exit;
}
$id = (int) $_GET['id'];

// Put the ID of the doc record to return to into the web page for Javascript 
// functions. ID=0 means this page has been called from a search page and is 
// there to select fields to display in the result list and NOT in the detail 
// view. This has to be signaled to the server when a field is changed as a 
// different data structure has to be updated.
echo '<span id="doc_db_id" style="display:none">' . $id . '</span>' . PHP_EOL;

// Create token if not already done in a previous iteration or somewhere else
// by another action. The token is sent when the save button is pressed
// and then analyzed in createOrModifiyRecord() to prevent Cross Site 
// Request Forgery (CSRF) attacks.
$token = createOrReuseSecurityToken();

// Put document token used by Javascript on the client side into an invisible 
// page element
echo '<span id="doc_token" style="display:none">'. $token .
'</span>' . PHP_EOL;


// Create the bootstrap page layout container, first row for the side
// navigation menu bar.
echo '<div class="container-fluid">' . PHP_EOL;
echo '<div class="row content">' . PHP_EOL;
echo '<div class="col-sm-2 sidenav">'. PHP_EOL;      

// Create the menu area
echo '<br>';
echo '<ul id="menu_area" class="nav nav-pills nav-stacked custom">'. PHP_EOL;

// If called from document details, go back to the document when the 'Done' button
// is pressed.
if ($id > 0) {

	echo '<li class="active">';
	echo '<a href="index.php?content=bs_record_details&id=' . $id .
	'" id="menu_text"><span class="glyphicon glyphicon-asterisk"></span> Done</a>';
	echo '</li><br>' . PHP_EOL;
	
} else {

	// If not called from detailed view just close this window when the 
	// done button is pressed
	echo '<li class="active">';
	echo '<a href="#" id="closetab">'. 
	'<span class="glyphicon glyphicon-asterisk"></span> Done</a>';
	echo '</li>' . PHP_EOL;
		
}

echo '<li class="active">';           
echo '<a href="#" id="hideall">' .  
     '<span class="glyphicon glyphicon-unchecked"></span> Hide all</a>';
echo '</li>' . PHP_EOL;

echo '<li class="active">';
echo '<a href="#" id="showall">' . 
     '<span class="glyphicon glyphicon-expand showall"></span> Show all</a>';
echo '</li><br>' . PHP_EOL;

echo '<li class="active">';
echo '<a href="index.php?content=bs_change_display_order&id=' . $id . 
     '&reset=1' . '" id="menu_text">' . 
     '<span class="glyphicon glyphicon-erase"></span> Reset View</a>';
echo '</li>' . PHP_EOL;


if (UserPermissions::hasAccess('help')) {
    echo '<br>';
    echo '<li class="active">';
    echo '<a href="/drdb-manual/ListAndSearch/#configure-which-fields-will-be-shown" target="_blank"';
    echo 'id="menu_text">';
    echo '<span class="glyphicon glyphicon-question-sign"></span> Help</a>';
    echo '</li>';
}

echo '</ul>';

echo '</div> <!-- end of sidenav column --> '; 
echo '<div class="col-sm-4"> <!-- start of middle column -->'; 
echo '<br>';

echo '<div class="alert alert-info" role="alert" id="info-text">';
echo 'Usage:<br>1. Drag/drop the fields into the desired order.<br>' . 
     '2. Double click on field name to show (blue) or hide (yellow) the field.';
echo '</div>';
    
echo '<!-- Bootstrap 3 panel list. -->';
echo '<ul id="draggablePanelList" class="list-unstyled">';
 
// get access to the global $doc_db_description array (read-only)
$doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

// Load the current field order. There are two cases:
//
// doc id <= 0: The field order is to be modified for the search result list
// doc id > 0: The field order is to be modified for document details pages
//
if ($id <= 0) {
	
	// If the field order for the list output already exist use it
	if (isset($_SESSION['db_field_order_for_list_output'])) {

		$fieldArrayWithOptions = $_SESSION['db_field_order_for_list_output'];
		$log->lwrite('bs_change_display_order.php: Using CUSTOMIZED field display ' .
				     'order for LIST output');		
	} else { // use the default
		
		$fieldArrayWithOptions = $doc_db_description;

		// Initialize all fields as NOT shown!
		// Note: $field options are by reference due to the '&'
		foreach ($fieldArrayWithOptions as $field_print_name=>&$field_options):
			$field_options[SHOW_FIELD] = 0;		
		endforeach;
		// the following unset() is required update the reference of the
		// last array element. This is not a bug, it's a feature. 
		//--> see "warning" in:
		// http://php.net/manual/en/control-structures.foreach.php
		unset($field_options);
				
		$_SESSION['db_field_order_for_list_output'] = $fieldArrayWithOptions;
		
		$log->lwrite('bs_change_display_order.php: Using DEFAULT field display '.
				     'order for LIST output');		
	}
	
} else { // field order for document details output is to be modified

	// If the field order for the details output already exist use it	
	if (isset($_SESSION['db_field_order'])){
	
		$fieldArrayWithOptions = $_SESSION['db_field_order'];
		$log->lwrite('bs_change_display_order.php: Using CUSTOMIZED field display ' .
					 'order for DETAILS output');	
			
	} else { // use the default for the details output
		
		$fieldArrayWithOptions = $doc_db_description;
		$log->lwrite('bs_change_display_order.php: Using DEFAULT field display '.
		 		     'order for DETAILS output');		
	}
}

// If the user has pressed the "reset" button, go back to the default
// order of database fields and display all
if (isset($_GET['reset'])) {

	if ($id <= 0) {
		$fieldArrayWithOptions = $doc_db_description;
		
		// Initialize all fields as NOT shown!
		// Note: $field options are by reference due to the '&'
		foreach ($fieldArrayWithOptions as $field_print_name=>&$field_options):
			$field_options[SHOW_FIELD] = 0;
		endforeach;
		// the following unset() is required update the reference of the
		// last array element. This is not a bug, it's a feature.
		//--> see "warning" in:
		// http://php.net/manual/en/control-structures.foreach.php
		unset($field_options);
				
		$_SESSION['db_field_order_for_list_output'] = $fieldArrayWithOptions;
				
	} else {
		$fieldArrayWithOptions = $doc_db_description;
		$_SESSION['db_field_order'] = $doc_db_description;		
	}
	
	// Save changed user configuration
	$log->lwrite('bs_change_display_order(): Saving reset field configuration');
	UserConfigStorage::saveUserConfig();
}

// Assemble a list of all record fields described in database-structure.php
foreach ($fieldArrayWithOptions as $field_print_name=>$field_options):

    // Adapt panel class according whether the field is visible or hidden.
    if ($field_options[SHOW_FIELD] == 1) {
    	$classContent = 'panel panel-primary doc-field'; 
    }
    else {
    	$classContent = 'panel panel-warning doc-field';    	
    }

	echo '<li class="' . $classContent . '" id="' . $field_print_name . '">';
	echo '<div class="panel-heading">' . $field_print_name;
	echo '<span id="panel-id-print-name" style="display:none">' . 
	     $field_print_name . '</span>';	
	echo '<span id="panel-id-field-name" style="display:none">' . 
	     $field_options[DB_FIELD_NAME] . '</span>';
	echo '<span id="panel-id-f-inline-print" style="display:none">' . 
	     $field_options[PRINT_INLINE] . '</span>';		
	echo '</div>';
	echo '</li>';
	echo PHP_EOL;
endforeach;
?>
   
</ul> <!-- end of panel list -->

    </div> <!-- end of middle column -->        
    <div class="col-sm-6"></div>    
  </div> <!-- end of row -->
</div> <!-- end of container -->
