<?php
/**
 * update_main_table_field_order.php
 *
 * Receives a POST request from a client with a new sorting order for the
 * fields of main database table. If the request is valid it:
 * 
 *  * stores the new field order in the database.
 *  * increases the version number of the database config the individual
 *    display order of other uses gets invalidated. Next time they access
 *    the page they will thus see the changes made. 
 *
 * @version    1.0 2021 06 04
 * @package    DRDB
 * @copyright  Copyright (c) 2017-2021 Martin sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

    $log = new Logging();
    $log->lwrite('Updating record field order of the main db table');

	//Check if the anti-CSRF token is present and valid
	if (checkAntiCsrfProtection() == false) {
				
		echo '<p id="doc-lock-result">There was a security error, ' .
		     'locking aborted!</p>';				
		$log->lwrite('lock_doc_record.php: Security error, ' . 
				     'locking aborted ');
		exit;
	}	
  
    if (!isset($_POST['fieldDataJson'])) {
    	$log->lwrite("update_field_display_order.php: 'fieldDataJson' not " .
    			     "present");
    	exit;
    }

    if (!UserPermissions::hasAccess('admin')) {
        $log->lwrite("User has no admin rights, aborting");
        return;
    }
       
    // Get JSON encoded field order and configuration from the POST request
    $fieldDataJSON = $_POST['fieldDataJson'];
    $log->lwrite("JSON: " . $fieldDataJSON);
    
    // Convert from JSON string to a native PHP associative array
    $newFieldConfig = json_decode($fieldDataJSON, true);
       
    // Sanitize the input data and output result in log
    $log->lwrite('New field order for main db table with sanitized values:');   
    $newFieldConfigSanitized = array();    
    foreach ($newFieldConfig as $field_name => $value_array) {
        
        /*
        $log->lwrite('Field Print Name: ' . $field_name . ' - Field DB Name: ' .
            $value[0] . ' - Visibility: ' . $value[1] .
            ' Print Inline: ' . $value[2]);
        */

        $field_name_sanitized = convertToSafeUtf8Html($field_name, true);
        $field_name_sanitized = strip_tags($field_name_sanitized);
        
        $values_sanitized = array();
        foreach ($value_array as $single_value) {
            
            $single_value_sanitized = convertToSafeUtf8Html($single_value, true);
            $single_value_sanitized = strip_tags($single_value_sanitized);
            array_push ($values_sanitized, $single_value_sanitized);
        }
        
        $newFieldConfigSanitized[$field_name_sanitized] = $values_sanitized;
        
        $log->lwrite('Sanitized:' . $field_name_sanitized . ", " . 
                     $newFieldConfigSanitized[$field_name_sanitized][0] . ", " .
                     $newFieldConfigSanitized[$field_name_sanitized][1] . ", " .
                     $newFieldConfigSanitized[$field_name_sanitized][2]);
    }
    
    // Write new sort order to the main table config info in the database
    if (DatabaseConfigStorage::updateMainDbFieldConfig($newFieldConfigSanitized) === false) {
        $log->lwrite("ERROR: Updating the main table field order failed, aborting");
        return;        
    }
    
    // Update the table config version
    $config_version = DatabaseConfigStorage::getCfgParam('version'); 
    $config_version = strval((int) $config_version + 1);
    $log->lwrite("New Version     : " . $config_version);
    if (!DatabaseConfigStorage::setCfgParam('version', $config_version)) {
        $log->lwrite("ERROR: Unable to save updated config version number");
    }
    
?>
