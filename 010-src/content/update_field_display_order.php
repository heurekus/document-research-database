<?php
/**
 * update_field_display_order.php
 *
 * Receives a POST request from a client with a new sorting order for the
 * fields of a database record. It retrieves the information from the request
 * and stores it in a session variable.
 *
 * @version    1.0 2017 03 21
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

    $log = new Logging();
    $log->lwrite('Updating database record field display order');

	//Check if the anti-CSRF token is present and valid
	if (checkAntiCsrfProtection() == false) {
				
		echo '<p id="doc-lock-result">There was a security error, ' .
		     'locking aborted!</p>';				
		$log->lwrite('lock_doc_record.php: Security error, ' . 
				     'locking aborted ');
		exit;
	}	
  
    if (!isset($_POST['field_name'])) {
    	$log->lwrite("update_field_display_order.php: 'field_order' not " .
    			     "present");
    	exit;
    }
    
    // Get the document id from which the refrerring web page was called from.
    // If the balled id is 0 it means it wasn't called from a document details
    // page but from a search web page. As a consequence a different data
    // structure on the server side is altered.
    if (!isset($_POST['db_doc_id'])) {
    	$log->lwrite("update_field_display_order.php: ERROR: 'db_doc_id' not " .
    			"present");
    	exit;
    }
    
    $id = trim(filter_input(INPUT_POST, 'db_doc_id', FILTER_SANITIZE_STRING));
    $log->lwrite('document ID given in search request: ' . $id);
    
    // Get the POST variables that contain the field names, field print
    // names, visibility etc. in the new sort order. Input strings are 
    // sanitized before being split into an array
    $in_temp = trim(filter_input(INPUT_POST, 'field_name', 
    FILTER_SANITIZE_STRING));
    $dbFieldName = explode('&&&', $in_temp);

    $in_temp = trim(filter_input(INPUT_POST, 'field_print_name',
    FILTER_SANITIZE_STRING));
    $dbFieldPrintName = explode('&&&', $in_temp);
        
    $in_temp = trim(filter_input(INPUT_POST, 'field_visibility',
    FILTER_SANITIZE_STRING));
    $dbFieldVisibility = explode ('&&&', $in_temp);
    
    $in_temp = trim(filter_input(INPUT_POST, 'field_inline_print',
    FILTER_SANITIZE_STRING));
    $dbFieldInlinePrint = explode ('&&&', $in_temp);

    // Let's create and fill a multidimensional named array that looks as follows:
    //
    // $dbFieldPrintName => {$dbFieldName, $dbFieldVisibility}
    // $dbFieldPrintName => {$dbFieldName, $dbFieldVisibility}    
    // $dbFieldPrintName => {$dbFieldName, $dbFieldVisibility}
    // ...
    // The named index ($dbFieldPrintName) and the first value of the array of
    // each entry corresponds to the format of the document database structure
    // variable in database-structure.php (from which those fields were
    // taken and then reordered before being sent back to the server!
    //
    $dbFieldsWithOptions = array();

    $array_length = count($dbFieldName);
    for ($x = 0; $x < $array_length; $x++){
    	if ($dbFieldName[$x]!=''){
    		$dbFieldWithOptions[$dbFieldPrintName[$x]]= array ($dbFieldName[$x],
    				                                     $dbFieldVisibility[$x],
    													$dbFieldInlinePrint[$x]);    		
    	}
    }
    
    // Output result in log
    $log->lwrite('update_field_display_order.php: New field order:');
    foreach ($dbFieldWithOptions as $field_name=>$value):
        $log->lwrite('Field Print Name: ' . $field_name . ' - Field DB Name: ' . 
        		     $value[0] . ' - Visibility: ' . $value[1] .
        		     ' Print Inline: ' . $value[2]);
    endforeach;
    
    // Assign new field display order in one of the two possible PHP session 
    // variables so it is used from now on in other parts of the code
    if ($id > 0){
    	// Set variable used for the pages that shows the fields of a single
    	// record
    	$log->lwrite('Updating DETAILS field order');
    	$_SESSION['db_field_order'] = $dbFieldWithOptions;    	
    	// Save the changed configuration
    	UserConfigStorage::saveUserConfig();    	    	
    } else {
    	// Set the varaible used for outputing fields in search result lists
    	$log->lwrite('Updating LIST field order');
    	$_SESSION['db_field_order_for_list_output'] = $dbFieldWithOptions;
    }
    

    
?>
