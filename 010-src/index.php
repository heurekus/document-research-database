<?php
/**
 * index.php
 *
 * Main file
 *
 * @version    1.0 2014-02-01
 * @package    DRDB
 * @copyright  Copyright (c) 2014 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Before going any further check if the web server has authenticated a user.
if (!isset($_SERVER['PHP_AUTH_USER'])) {
	echo '<p>ERROR: No user authentication configured e.g. in apache2.conf ' . 
	     'or .htaccess. Aborting...</p>';
	exit;
}

require_once 'includes/init.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo DatabaseConfigStorage::getCfgParam('title');?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <script src="/jquery/jquery-3.1.1.min.js"></script>
  <script src="/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  
  <script src="js/index.js"></script>
  
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 800px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
    
    /* This following <div> id is used to show or hide
       some parts of a page */
    #ms-display-state {
    display: none;
    }
    
    /* Glyphicons to the left in alert box */
    .alert .glyphicon{
        float:left;
        margin-right:9px;
    }

    .alert div{
        overflow:hidden;
    }
    
  </style> 
</head>

<body>

<div class="container-fluid">
  <div class="row hidden-xs hidden-sm">
    <div class="col-sm-12" style="background-color:#f2f2f2;">
     <header>
       <br><a href="index.php">
         <img src="images/db-front-pic.jpg" 
              class="img-rounded center-block" 
              style="border:2px solid grey;" alt="Document Research Database"/>
       </a><br>
      </header> 
    </div>
  </div> 
  
  <div class="row visible-xs visible-sm">
    <div class="col-sm-12" style="background-color:#f2f2f2;">
     <header>
       <br><a href="index.php">
         <img src="images/db-front-pic-xs.jpg" 
              class="img-rounded center-block" 
              style="border:2px solid grey;" alt="Document Research Database"/>
       <br></a>
      </header> 
    </div>
  </div>
</div> 

<div class="content">
<?php

$executionStartTime = microtime(true);
loadContent('content', 'bs_home'); 
$executionEndTime = microtime(true);


$log = new Logging();
$executionTime = round($executionEndTime - $executionStartTime, 4);
$log->lwrite("Execution Time: " . $executionTime . ' seconds');
?>
</div><!-- end content -->
  

<footer class="footer">
<!-- <div class="container"> -->
<p class="text-muted"><?php echo DatabaseConfigStorage::getCfgParam('footer');?></p>
<p class="text-muted"><small>Execution time: 
<?php echo $executionTime;?> 
seconds </small></p>
<!--  </div> -->
</footer>  

</body>
</html>
