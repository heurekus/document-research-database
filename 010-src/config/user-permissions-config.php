<?php

/**
 *
 * user-permissions-config.php
 * 
 * This is a LEGACY config file.
 * 
 * In the past, this config file contained the permissions assigned
 * to different users. In the meantime, however, user permissions
 * have been moved into the database. This config file thus only
 * contains the permissions of the initial admin user which is copied
 * to the database during the initial database creation operation. 
 * It has been left in place to make migration of already existing 
 * installations seamless.
 * 
 * Note: Permissions of the admin account are changed in the database
 * and hence, those given in this config file are completely ignored after
 * initial installation.
 * 
 * For details on user permissions and management see userpermissions.php
 *
 * @version    1.1 2021-04-24
 * @package    DRDB
 * @copyright  Copyright (c) 2020 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Only used during the initial creation of the database.
// Ignored otherwise.
$user_permissions = array(
		    
    'admin' => array('history', 'export', 'edit', 'help', 'admin'),
);




?>
