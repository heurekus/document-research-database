<?php

/**
 * database-structure.php
 *
 * Variables and constants that describe the database structure / fields
 * that are used in many places of this web application to iterate over
 * database fields, to define input box sizes, etc.
 *
 * @version    2.0 2017-03-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

/*
*
* The following array contains
*
* - The fields to be displayed + edited and their order
*
* Note:
*
* - The content of the array (e.g. 'F0K', 'FOA', 'F0B) are the field names in 
*   the database. BEFORE the database is initially created they can be 
*   changes as required. Don't use spaces and special characters that MYSQL
*   might not support. '_' is ok to replace spaces!
*   
* - The index names (e.g. 'Demo Field 01', 'Demo Field 02', etc.) are strings 
*   for output and can be changed as required even AFTER the database has
*   already been created!
*   
* - By default all fields are visible (=1) but the user can override this 
*   by setting the SHOW_FIELD sub-array content to 0. 
*   
* - The content of a field can be printed in a separate row (default) or, 
*   if it is short it can also be printed in-line (together with the
*   fieldname) when exporting to Libreoffice. Can be 'true = 1' for inline
*   printing or 'false = 0' for a separate line.
*
* Possible modifications:
*
* - Order of the array can be changed
* - Entries can be added and deleted to change which fields to present on 
*   the page.
* - The index names (see above) can be changed to modify what's printed on 
*   the page
*
* IMPORTANT: When the array is changed, increase 'version' in config.php
* so users will get the latest version of the field configuration when 
* they load a page. Also, their current field order configuration that
* is stored in the database is overwritten as names and the number of
* fields might have changed.
*
* DO NOT CHANGE:
*
* - Once a database is in use don't change the the database field names
*   (first string in the sub arrays) Change of these requires a change 
*   in the database as well!
*
*/

// field constants for the sub-array contents
define ('DB_FIELD_NAME', 0);
define ('SHOW_FIELD', 1);
define ('PRINT_INLINE', 2);

// Array that describes which fields to display. Have a look at the constants 
// directly above for information on what the numbers in the sub-array 
// represent.
$doc_db_description = array(
                'Doc ID'                => array('F0K', 1, 0),
                'Title'                 => array('F0A', 1, 0),
                'Title Notes'           => array('F0B', 1, 0),
                'Author'                => array('F0C', 1, 0),
                'Cross Reference'       => array('F0D', 1, 0),
                'Format - Paramétrage'  => array('F0E', 1, 0),
                'Notes on Content'      => array('F0F', 1, 0),
                'Relates to'            => array('F0G', 1, 0),
                'Number of Systems'     => array('F0H', 1, 0),
);


// Optional configuration. If not present, default values will be used
// =====================================================================

// Per default, the first field of the record described in $doc_db_description
// will be used in the list view as header text. The array below can be used
// to override this behavior. If empty or missing, the first entry of
// $doc_db_description will be used. Several fields can be used as header.
// IMPORTANT: The array contains the DATABASE FIELD NAME(s) and NOT the names presented
// to the user!!!

/*
$list_view_header_fields = array (
    'Doc ID',
    'Title',
);
*/

// If in list view the header text for each record is expected to be very
// long, set this variable to true to use small text. Otherwise, text
// is formated as <h3> to appear larger. This is also the default in case
// the variable is false or not declared at all.

/*
$list_view_header_small_text_size = true;
*/


?>
