<?php

/**
 * id-structure.php
 *
 * This file contains the structure of the database id field which has a
 * number of ranges that represent different pieces of data. The structure
 * can be used in various places for things such as, for example, 
 * generating a list of 'decade / categories' to limit database searches 
 * to subsets of the database content.
 * 
 * In addition all functions that work on the structure and might have
 * to be adapted in case the rules of the structure are changed are contained
 * here as well.
 *
 * @version    1.0 2017-02-28
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


/*

In the database, the ID has 10 digits, i.e. up to 1.999.999.999.
As an example, the ID could be split as follows for data that was
imported from an Excel file:

1.    100.     000.100

Excel File   : 1 Digit  (up to 9)     --> Always 1. Was initially thought to 
                                          be used to to separate documents of 
                                          different countries. However, PHP
                                          uses 32 bit integers and resorts to
                                          floats for higher numbers. Therefore
                                          this is a future option. 
                                          
                                      --> DANGER! DANGER! MYSQL also 
                                          stores the ID as 32-bit integer limit
                                          here with 2.147.483.647. BIGINT data
                                          type is available that should fix
                                          things. Not implemented so far!

Sheet Number : 3 digits (up to 999)   --> to distinguish decades and counties 
										  
										  Decades + Country 1 is assinged 001-030
										  Decades + Country 2 is assigned 031-050
										  
Doc. Number: 6 digits (up to 99999)   --> 99 document ids in between each
                                          document to have room to insert 
                                          documents later on

Special numbers:

In addition, all numbers in the array 0 <= 1000 are special numbers used to
group several categories together for search and display purposes. A
multi-category group then includes all groups from the first special number
to the next special number (or the end of the array if non follows). 

Note: The special numbers are handled in the getAndSetSearchLimit() function that
is also contained in this file.

*/

$id_categories = array(
		'Demo - All'                         => 1,
		'Demo - set 1'                       => 1001000000,
		'Demo - set 2'                       => 1002000000,
		
		'--------'                           => 2,
		'More Demo - All'                    => 2,		
		'More demo - set a'                  => 1031000000,
		'More demo - set b'                  => 1032000000,
);


/**
 *
 * getAndSetSearchLimit()
 *
 * Checks HTTP Post variable 'start_id' and if it is > 0 adapts the
 * search_limit_start and _end variables to limit a database search to
 * ONE category or a set of categories (= multi-category) in case of
 * values <= 1000.
 *
 * The end limit of a category or a multi-category is caluclated by adding
 * 999.999 to the start limit (single category) or by adding 999.999 to the
 * last category number found for a multi-category.
 *
 * For category details have a look at $id_categorie in id-structure.php.
 *
 * The variables are passed by reference and can thus modified!
 *
 * @log
 * @$search_limit_start
 * @$search_limit_end
 */

function getAndSetSearchLimit($log, &$search_limit_start, &$search_limit_end){

	$search_limit_start = intval(trim(filter_input(INPUT_POST,
			'start_id', FILTER_SANITIZE_STRING)));

	// Handle single category case if selected by the user
	if ($search_limit_start > 1000) {

		// A single category is selected
		$search_limit_end = $search_limit_start + 999999;
		$log->lwrite('Single cateogry search from ' . $search_limit_start .
				' to ' . $search_limit_end);
		return;
	}

	// Multi-cateogry selected, thus we need to search for beginning and
	// the end of the multi-category in the $id_categories array.

	global $id_categories;
	$local_id_categories = $id_categories;

	$multi_cat_num = $search_limit_start;
	$set_search_limit_start = false;
	$search_limit_temp = 0;

	foreach ($local_id_categories as $cat_name => $cat_num) {

		// previous loop iteration found the start value, now set it
		if ($set_search_limit_start == true) {
			$search_limit_start = $cat_num;
			$set_search_limit_start = false;
		}

		// If the current array entry contains the multi-category number
		// the start number of the multi-category will be contained in the
		// next array entry.
		if ($cat_num == $multi_cat_num) {
			$set_search_limit_start = true;
			continue;
		}

		// If we have reached the next multi-category number in the
		// array we can now calculate the end of the multi-category
		// and end the loop.
		if ($cat_num == $multi_cat_num + 1) {
			$search_limit_end = $search_limit_temp + 999999;
			break;
		}

		$search_limit_temp = $cat_num;
		$search_limit_end = $cat_num + 999999;

	} // end foreach

	$log->lwrite('Multi-category limit search from ' . $search_limit_start .
			' to ' . $search_limit_end);
	return;
}

/**
 * getFirstDataBaseId()
 *
 * Required in case the databae is empty and an id for the first record
 * has to be found.
 *
 * Looks for the first id > 1000 in $id_categories and returns this as the
 * first database id. Ids below 1000 are special numbers and must not 
 * be  used as identifiers.
 * 
 */

function getFirstDataBaseId($log) {
    
    global $id_categories;
    $local_id_categories = $id_categories;
    
    foreach ($local_id_categories as $cat_num) {
        if ($cat_num >= 1000) return $cat_num;        
    }
    
    $log->lwrite('ERROR: Unable to find a first id, returning 0');
    return 0;
}

?>

