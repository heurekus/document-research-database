<?php

/**
 *
 * Contains a number of security functions against web attacks that
 * are required in several places.
 * 
 * @version    1.0 2017-04-03
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

/**
 * checkAntiCsrfProtection()
 *
 * Check that the request is not a Cross Site Scripting (CSRF) attack.
 * The function does this by:
 * 
 * - Checking if a correct session token is present
 * - Checking if the HTTP referer matches (to counter javascripts attacks
 *   that could previously get a token but come from a different website!
 *
 * @param  none
 * @return boolean true if ok, false if there is a security error
 * 
 */

function checkAntiCsrfProtection () {
	
	$log = new Logging();
	$log->lwrite('anti-CSRF part 1: Checking if the token is present and valid');

	// The security check fais if the token is not present, if no session token
	// was previously generated, if it is empty or if it is not equal to the
	// token saved in the session variable.
	if (   !isset($_POST['token'])
		|| !isset($_SESSION['token'])
		|| empty($_POST['token'])
		|| $_POST['token'] !== $_SESSION['token']) {

		$log->lwrite('Security token error!');
		return false;
	}

	// Now check if the hostname in the URL matches the hostname of the server.
	// A list of allowed hostnames is given in config.php. 
	// Manual configuration of the hostname is required because there is no 
	// direct way to query this in runtime variable.	
	$log->lwrite('anti-CSRF part 2: Checking referer');
	
	if (!isset($_SERVER['HTTP_REFERER'])) {
	    $log->lwrite("anti-CSRF error: HTTP_REFERER not set, aborting");
	    return false;
	}
	
	// First option: (For Docker environment) 
	// Use the DRDB_VIRTUAL_HOST environment variable if defined
	$env_hostname = getenv('DRDB_VIRTUAL_HOST');
	if ($env_hostname !== false) {

	    $log->lwrite("Comparing hostname with env. variable: " . $env_hostname);
	    
	    if ($env_hostname != parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST)) {
	        $log->lwrite("anti-CSRF error: Hostname and env. " . 
	                     "variable DRDB_VIRTUAL_HOST do not match, aborting: " .
	                     $env_hostname . " vs. " . $_SERVER['HTTP_REFERER']);
	        return false;
	    }
	    else {
	        $log->lwrite("Hostname matched environment variable, ok");
	        return true;
	    }
	}

	// Second option: Use the configured hostnames
	if(DatabaseConfigStorage::getCfgParam('trusted_domains')) {
		
		if(in_array(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), 
		    DatabaseConfigStorage::getCfgParam('trusted_domains')) === false) {
		   	
			$log->lwrite('anti-CSRF error: REFERER "' .  
					     $_SERVER['HTTP_REFERER']);
			$log->lwrite('ERROR: Add DOMAIN NAME to trusted domains ' . 
					     'in config.php!');
		   	return false;		   	
		}
	}
	else {
		$log->lwrite('anti-CSRF error: trusted_domains ' . 
				     '(config.php) not set!');
		return false;		
	}
	
	$log->lwrite('HTTP_REFERER: ' . parse_url($_SERVER['HTTP_REFERER'], 
			                                  PHP_URL_HOST));

	return true;	
}

/**
 * createOrReuseSecurityToken()
 *
 * Create a security token if not already done in a previous iteration or 
 * somewhere else by another action. The token is sent when a POST "save" 
 * button is pressed and is analyzed in the target page to prevent 
 * Cross Site Request Forgery (CSRF) attacks.
 *
 * @param null
 * @return string token
 */

function createOrReuseSecurityToken () {
	
	$log = new Logging();
	
	if (!isset($_SESSION['token'])) {
		
		$log->lwrite('Creating NEW security token');
		
		// Get the salt from the config file.
		$salt = '';
		if (DatabaseConfigStorage::getCfgParam('salt')) {
		    $salt = DatabaseConfigStorage::getCfgParam('salt');
		} else {
			$log->lwrite("ERROR: No 'salt' in the config file. This is " . 
					     "a security weakness!");
			$log->lwrite('IMPORTANT: Add this variable to config.php!!!');
		}
			
		$token = sha1(mt_rand(1,1000000) . $salt);
		$_SESSION['token'] = $token;
	}
	else {
		
		$log->lwrite('Re-using existing security token');
		
		$token = $_SESSION['token'];
	}
	
	return $token;
}

/**
 * checkHostName()
 *
 * This function checks if the server name given in the http
 * 'host' header matches a trusted_domain in the config
 * array.
 *
 * Note: This is NOT a security feature itself but just ensures
 * that a later write activity will not fail because a call to
 * checkAntiCsrfProtection() fails because the domain in the
 * HTTP referer field is not present in the trusted domain list.
 * In other words, calling this function at every server request catches
 * a configuration issue early.
 *
 * @param  null
 * @return boolean, true if the host name matches a configured hostname,
 *         otherwise false
 *
 */
function checkHostName() {

	$log = new Logging();
	$log->lwrite('Checking if hostname is in list of trusted domains');

	// If the HTTP request does not contain HOST, fail straight away
	if (!isset($_SERVER['HTTP_HOST'])){
		$log->lwrite('ERROR: HOST not set in http request');
		return false;
	}

	$hostname = $_SERVER['HTTP_HOST'];
	$log->lwrite($hostname);

	// Remove tcp port number if present
	if (strpos($hostname, ":") !== false) {
		$log->lwrite('Removing tcp port number from hostname');
		$hostname = strstr($hostname, ":", true);
	}

	// Fail if the hostname is empty
	if ($hostname == ''){
		$log->lwrite('ERROR: Hostname is empty');
		return false;
	}

	
	// First option for hostname comparison: (For Docker environment)
	// Use the DRDB_VIRTUAL_HOST environment variable if defined
	$env_hostname = getenv('DRDB_VIRTUAL_HOST');
	if ($env_hostname !== false) {
	    
	    $log->lwrite("Comparing hostname with env. variable: " . $env_hostname);
	    
	    if ($env_hostname != $hostname) {
	        $log->lwrite("ERROR: Hostname and env. " .
	            "variable DRDB_VIRTUAL_HOST do not match, aborting: " .
	            $env_hostname . " vs. " . $hostname);
	        return false;
	    }
	    else {
	        $log->lwrite("Hostname matched environment variable, ok");
	        return true;
	    }
	}
	
	// Second option: hostname array in config file:
	//
	// If the config variable is not present, exit with negative
	// result straight away
	if (DatabaseConfigStorage::getCfgParam('trusted_domains') === false){
	    $log->lwrite('ERROR: trusted_domains not present in config.php');
	    return false;
	}
	
	if(in_array($hostname, DatabaseConfigStorage::getCfgParam('trusted_domains')) == false) {

		$log->lwrite('ERROR: HTTP_HOST "' .
				$_SERVER['HTTP_HOST'] . '" is not a trusted domain!');
		$log->lwrite('ERROR: Add DOMAIN NAME to trusted domains ' .
				'in config.php!');
		return false;
	}

	return true;
}

?>

