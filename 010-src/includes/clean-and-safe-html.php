<?php 
/**
 * clean-and-safe-html.php
 *
 * This file contains a wrapper for HTMPPurify functionality.
 *
 * @version    1.0 2018-03-10
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


/**
 * convertToSafeUtf8Html()
 *
 * This is a wrapper function to HTMLPurify that removes malicious content
 * from HTML input strings. This is done to protect against 
 * Cross-Site Scripting (XSS) attacks which try to inject Javascript code 
 * into user supplied input to the backend.
 * 
 * As a side effect, the function also converts all HTML-encoded characters to
 * UTF-8 characters and thus potentially significantly alters the input string.
 * As a consequence, functionality can be disabled by setting the 
 * 'protect_against_xss' parameter in config.php to false. As this function
 * should only be disabled for legacy projects the function is used if the
 * parameter is not present in the config file.
 *
 * @param string with HTML inside 
 * @param boolean, optional, true=override config.php deactivation
 *
 * @return string with cleaned up HTML formatting and malicious content removed
 * 
 */

function convertToSafeUtf8Html($input_str, $forced = false){
	
	require_once 'x-3rd-party-includes/htmlpurifier-4.10.0/library/HTMLPurifier.auto.php';
	
	$log = new Logging();
	
	if ($forced == false) {
	    if (DatabaseConfigStorage::getCfgParam('protect_against_xss') == false) {
	            $log->lwrite('HTMLPurify disabled!');
	            return($input_str);
	        }	        
	}
	
		
	$log->lwrite('Before HTMLPurify: ' . $input_str);
	
	$pure_config = HTMLPurifier_Config::createDefault();
	$purifier = new HTMLPurifier($pure_config);
	$input_str = $purifier->purify($input_str);
	
	return($input_str);
}






