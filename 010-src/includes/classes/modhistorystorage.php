<?php
/**
 * modhistorystorage.php
*
* This class takes care of storing and retrieving previous content of
* database record fields that have been modified. This information can then
* be used, for example, to make a diff with the current field content.
*
* @version    1.0 2017-12-21
* @package    DRDB
* @copyright  Copyright (c) 2017 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
*/

class ModHistoryStorage
{
	 
	/**
	 * saveFieldContent()
	 *
	 * This method saves the current content of a field in the modification
	 * history database.
	 * 
	 * @param int, id
	 * @param string, field_name
	 * @param string, content
	 * @param string, optional, name of user (to override the username of the current
	 * user which is helpful for intial population of the diff database
	 *
	 * @return bool, true if successful, false if there was a table access problem.
	 *
	 */

	public function saveFieldContent($id, $field_name, $content, $user_name = "") {

		$log = new Logging();
		$cur_time = time();
		
		if (!isset($_SERVER['PHP_AUTH_USER'])) {
			$log->lwrite('Error - No authenticated user...');
			return(false);
		}
		
		if ($user_name == "") {
			$user_name = $_SERVER['PHP_AUTH_USER'];
		}
		
		$prev_versions = ModHistoryStorage::getPreviousFieldContents($id, $field_name);
		
		// If there is a previous version of the field check if it is different
		// from the new content. If not, do not save the new content to the
		// diff database.
		if ($prev_versions !== false && (sizeof($prev_versions) > 0)) {
			
			if ($content == $prev_versions[0]['content']) {

				$log->lwrite('New field content unchanged, not saving a diff');
				return true;
			}
		} else {
			
			$log->lwrite ('No previous version of the field in diff db');
		}
		
		$connection = drdb::getConnection();
		
		$query = "INSERT INTO mod_history (id, field_name, mod_time, " .
				 "mod_user, content) VALUES (?, ?, ?, ?, ?)";
		$prep_query = $connection->prepare($query);		
		$prep_query->bind_param("sssss", $id, $field_name, $cur_time, 
				                $user_name, $content);
		
		$log->lwrite('SQL: ' . $query);
		$log->lwrite("ID: " . $id);
		$log->lwrite("Field name: " . $field_name);
		$log->lwrite("Time: " . $cur_time);
		$log->lwrite("User name: " . $user_name);
		$log->lwrite("Content: " . substr($content, 0, 30) . " [...]");		
		 
		if ($prep_query->execute()) {
			$log->lwrite('Modification history record successfully saved.');
			$prep_query->close();
			return true;
		}
		else {
			$log->lwrite('ERROR: Unable to save the modification history record');
			$prep_query->close();
			return false;
		}
	} // end of saveFieldContent()


	/**
	 * getPreviousFieldContents()
	 *
	 * Returns an array that contains all previous versions of field of a
     * particular record.
	 *
	 * @return mixed, array of strings or bool false if there are no records
	 * for this field or if there was a db problem.
	 */

	public static function getPreviousFieldContents($id, $field_name) {	 

		$log = new Logging();
						 
		// Get the database connection and get the user's configuration
		$connection = drdb::getConnection();
		 
		$query = 'SELECT * FROM mod_history WHERE id=?  AND ' .
		         'field_name=? ORDER BY mod_time DESC';
		$prep_query = $connection->prepare($query);
		$prep_query->bind_param("ss", $id, $field_name);
		
		$log->lwrite('SQL: ' . $query);		
		$log->lwrite("ID: " . $id);
		$log->lwrite("Field name: " . $field_name);
		
		// Make the database query
		$result = $prep_query->execute();
		 
		if (!$result) {
			$log->lwrite('Database access error, this should not have happened!');
			$prep_query->close();
			return(false);
		}
		
		$result_obj = $prep_query->get_result();
		
		if (!$result_obj) {
			$log->lwrite('Database access error 2, this should not have happened!');
			$prep_query->close();
			return(false);
		}		
		
		// Get all retrieved records out of the database data structure into
		// an array that contains an associative array for each record found.
		// Example:
		//
		//    $result_array[2]['content']
		//
		// points to the string contained in the 'content' field 
		// of the 3rd record that was retrieved.
		
		$result_array = array();		
		while ($next_result = $result_obj->fetch_assoc()) {
			array_push($result_array, $next_result);			
		}
		
		return ($result_array);

	} // end of getPreviousFieldContents()

	/**
	 * changeIdInRecords()
	 *
	 * Changes all modification records with a given current id to a new id. 
	 * This is necessary when a record in the main table is given a different 
	 * id which is done for reordering.
	 *
	 * @param $current_id, $new_id
	 *
	 * @return array, true/false indicating success, string with success or
	 * failure message.
	 */
	
	public static function changeIdInRecords($current_id, $new_id) {
	
		// Get the Database connection
		$connection = drdb::getConnection();
	
		$query = "UPDATE `mod_history` SET id='". (int) $new_id .
		"' WHERE id='" . (int) $current_id . "'";
	
		$log = new Logging();
		$log->lwrite('SQL: ' . $query);
	
		// Run the MySQL statement
		if ($connection->query($query)) {
			
			$success_text = 'Modification history entries successfully ' . 
					'changed! Old id: ' . $current_id . ', new id: ' . $new_id;
			$return = array(true, $success_text);
			return $return;
				
		} else {
			$error_text = 'ERROR: Modification history entries change FAILED! ' .
					'Old id: ' . $current_id . ', new id: ' . $new_id;				
			$return = array(false, $error_text);
			return $return;
		}
	}
	
	
	/**
	 * deleteRecords()
	 *
	 * Deletes all modification history records for the given id.
	 *
	 * @param int, document record id
	 *
	 * @return bool, true if deletion was successful or false if there
	 * was a problem communicating with the database.
	 */
	
	public static function deleteRecords($id) {
		// Get the database connection
		$connection = drdb::getConnection();
	
		//sanitize the id
		$id = (int) $id;
	
		$query = '';
	
		// Set up the query
		if ($id != 0){
			// get the last entry from the database
			$query = 'DELETE FROM `mod_history` WHERE id="' . (int) $id . '"';
		}
		 
		$log = new Logging();
		$log->lwrite('SQL: ' . $query);
	
		// Run the MySQL command
		$result_obj = '';
		try
		{
			$result_obj = $connection->query($query);
			if (!$result_obj)
			{
				throw new Exception($connection->error);
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	/**
	 * getNumberOfDiffEntries()
	 *
	 * Get the nubmer of entries in the diff database.
	 *
	 * @return int, number of entries or -1 in case the table could not 
	 * be accessed
	 */
	public static function getNumberOfDiffEntries() {
		
		
		// Get the Database connection
		$connection = drdb::getConnection();
		
		$query = 'SELECT COUNT(*) from mod_history';
	
		$log = new Logging();
		$log->lwrite('SQL: ' . $query);
		
		$result_obj = $connection->query($query);
		// Run the MySQL statement
		if ($result_obj) {
				
			$numRecords = 0;
			
			$result = $result_obj->fetch_assoc();
			$numRecords = $result['COUNT(*)'];		
		
			$log->lwrite('Number of Diff Records Found: ' . $numRecords);
			return $numRecords;
		
		} else {
			$log->lwrite('ERROR: Unable to get number of diff records');
			return -1;
		}
		
	}

	
	/**
	 * createInitialTable()
	 *
	 * Creates the initial mod history database table.
	 * 
	 * The table will only be created if it does not exist already so nothing
	 * is overwritten in case this method is called accidentally.
	 *
	 * @param none
	 *
	 * @return boolean, true = succes, false = fail, database table was not 
	 * created
	 *
	 */
	public static function createInitialTable() {
	
		$log = new Logging();
	
		// Get the Database connection
		$connection = DRDB::getConnection();
	
		$query = PHP_EOL . 'CREATE TABLE `mod_history` (' . PHP_EOL;
		
		$query .= '`mod-id` int(11) NOT NULL AUTO_INCREMENT,' . PHP_EOL;
		$query .= '`id` int(11) NOT NULL,' . PHP_EOL;
		$query .= '`field_name` text NOT NULL,' . PHP_EOL;
		$query .= '`mod_time` bigint(20) NOT NULL,' . PHP_EOL;
		$query .= '`mod_user` text NOT NULL,' . PHP_EOL;
		$query .= '`content` text NOT NULL,' . PHP_EOL;
		$query .= 'PRIMARY KEY (`mod-id`)' . PHP_EOL;
		$query .= ') ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;' . PHP_EOL;		
			 
		$log->lwrite('SQL: ' . $query);
	
		try {
			// Get the connection and run the query
			$connection = DRDB::getConnection();
			$result_obj = $connection->query($query);
				
			if (!$result_obj) {
				throw new Exception($connection->error);
			}
	
			$log->lwrite('Creation of the mod_history table successful');
	
		}
	
		catch(Exception $e) {
	
			if (!$result_obj) {
				$log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);
	
			} else {
				$log->lwrite('ERROR: Exception was raised');
			}
	
			return false;
		}
	
		return true;
	}	
	
	/**
	 * getAllDiffsForId()
	 *
	 * Returns the list of all modifications events for the given
	 * record id. The list is structured as an associative two dimensional
	 * array that is structured as follows:
	 * 
	 * 	$mod_history_records[x]['field_name']
	 *	$mod_history_records[x]['mod_time']
	 *	$mod_history_records[x]['mod_user']
	 *
	 * Note: 'field_name' is the field name in the document record table and
	 * NOT the name of the field shown to the user. If the calling function
	 * requires the user presentable field name, it has to use the field name 
	 * to search for the user field name inn $doc_db_description.
	 *
	 * @param int $id of the record
	 *
	 * @return mixed, false if there was an error, or array as described
	 * above. If no records were found the count() of the array will be 0.
	 * Therefore, false only indicates an error and is NOT sent if no 
	 * records have been found (which is count=0).
	 *
	 */
	public static function getAllDiffsForId($id) {
		
		$log = new Logging();
			
		// Get the database connection
		$connection = drdb::getConnection();
			
		$query = 'SELECT field_name, mod_time, mod_user FROM ' .
				 'mod_history WHERE id=? ORDER BY mod_time DESC';
		$prep_query = $connection->prepare($query);
		$prep_query->bind_param("s", $id);
	
		$log->lwrite('SQL: ' . $query);
		$log->lwrite("ID: " . $id);
	
		// Make the database query
		$result = $prep_query->execute();
			
		if (!$result) {
			$log->lwrite('Database access error, this should not have happened!');
			return(false);
		}
	
		$result_obj = $prep_query->get_result();
	
		if (!$result_obj) {
			$log->lwrite('Database access error 2, this should not have happened!');
			return(false);
		}

		// Now get the diff records and put them into the result array that
		// is structured as described in the method header.
		$mod_history_records = array();
		while ($next_result = $result_obj->fetch_assoc()) {
			array_push($mod_history_records, $next_result);
		}
				
		return ($mod_history_records);
	}	
	
} // end of class
