<?php
/**
 * docrecord.php
 *
 * docrecord class file
 * 
 * This class contains all variables and methods to access the main database 
 * table that stores document records. Most methods in the class contain
 * an SQL string to query the database and thus shield the outside world 
 * from the use of SQL.
 *
 * @version    2.0 2018-01-07
 * @package    DRDB
 * @copyright  Copyright (c) 2014-2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

/**
 * document class
 *
 * @package  DRDB
 */
class DocRecord
{
  
  // Protected array to hold the content of each field of the database
  // record.  
  protected $fields = array ();
  
  /**
   * Initialize the document and populate variables
   * @param array
   */
  public function __construct($input = false) {
  	
	  $log = new Logging();  	
  	
	  // Initialize administrative database fields as well as they 
	  // might not be given to the constructor from the outside.
	  $fields['lock_time'] = '';
	  $fields['lock_user'] = '';
	  $fields['detail_view_time'] = '';
	  $fields['detail_view_user'] = '';
	  $fields['last_update_time'] = '';
	  $fields['last_update_user'] = '';
	  $fields['hash_id']          = '';
	  
	  // Now all the variables given from the outside
	  if (is_array($input)) {
	    foreach ($input as $key => $val) {

		    $this->fields[$key] = $val;		    
	    }
	    
	    // Some debug output
	    /*
	    foreach ($this->fields as $key => $val){
	    	$log->lwrite('Constructor: ' . $key . ' : ' . $val);    	 
	    }
	    */
	    
	  }
	  
  } // end of __construct
  
  /**
   * Return ID
   * @return int
   */
  public function getId() {

  	if (array_key_exists ('id' , $this->fields)){
  		return $this->fields['id'];
  	}
  	else {
  		return "";
  	}  	
  	
  }
  

  /**
   * Read access to protected variables
   * @return string
   */
  public function getField($field_name) {

  	if (array_key_exists ($field_name , $this->fields)){
  		return $this->fields[$field_name];
  	}
  	else {
  		return "";
  	}
  	
  }
  
  
  /**
   * Before running and SQL query, check if parameters make sense. Currently
   * the following checks are done:
   * 
   *  - Are all key names correct, i.e. do they exist in the config file? 
   * 
   * @return true if everything is o.k. or false if there was a problem
   */ 
  protected function _verifyInput() {
 	
    $doc_db_description = DatabaseConfigStorage::getDbFieldConfig();	
  
    $log = new Logging();
      	    
    // Check that all key names are correct
  	foreach ($this->fields as $key => $value) {
  		
  		// key = 'id' is an exception, it is not part of the
  		// doc_db_description array!
  		if ($key = "id") continue; 
  		
	  	if (!array_key_exists($key, $doc_db_description)){
	  		$log->lwrite("Key " . $key . " does not exist!");
	  		return false; 
	  	}
  	}
  
 	return true;
  }

  /**
   * generateHashId() 
   *  
   * Generates a hash_id that will never change while the record exists.
   * The hash_id is useful, for example, to create a unique URL to a
   * record.
   *
   * To generate a unique hash_id for the new record, the ID is converted
   * to a string and the current date and time is added. Should a record 
   * lateron get the same ID, the creation time is different and thus, 
   * the hash_id is different. 
   * 
   * Note: The hash_id is not used for cryptographic purposes
   * but just to have a unique handle to generate a link from!
   *
   * The only way two records could be the same is if two records with the
   * same ID are created the same second. This can't happen as the ID
   * must be unique when a record is created or changed in the database!
   *
   * If the ID is 0 (i.e. new record appended at the end), use a random
   * number instead. Again with the time it two records would have to
   * be created in the same second AND the random number generator
   * would have to generate the same number.
   * 
   */
  
  public function generateHashId() {
 	 
  	$log = new Logging();
  	
  	$hash_input = (string) $this->fields['id'];
  	if ($this->fields['id'] == 0) {
  		$hash_input = (string) mt_rand();
  	}
  	$hash_input = $hash_input . ' ' . date(DATE_RFC822);
  	$this->fields['hash_id'] = hash ('sha1', $hash_input);
  	
  	$log->lwrite ('Hash Input: ' . $hash_input);
  	$log->lwrite('Hash: ' . $this->fields['hash_id']);

  	return;
  }

  /**
   * addRecord()
   * 
   * The function can be called two create a new record in the database from
   * the protected variables that have previously been filled via the 
   * '__construct()' method that is called when the object is newly created.
   * 
   * If no parameter is given the database will give the new record an id 
   * number after the highest id in the database (append at the end).
   * 
   * If an id number is given the new record will be created with this id
   * number. If the id number is already present in the database the creation
   * of the record will fail.
   * 
   * @param id number of the new record (optional)
   * 
   * @return an array with the first element being and integer indicating 
   *         success (1) or failure (0). The second array element is a string 
   *         that indicates success or gives the failure reason in human 
   *         readable format.
   */
  
  public function addRecord($id_num = 0) {
  	  
	  $log = new Logging();
	  	
	  $log->lwrite('Adding a new record');
	  
	  // Verify the fields
	  if (!$this->_verifyInput()) {
	  	  // send fail message and return
	  	  $return = array(0, 'ERROR: Input could not be verified, no doc record added.');
	  	  return $return;
	  }

	  // id_num = 0 --> insert after last record, id_num !=0 the new recrod's id
	  $this->fields['id'] = $id_num;
	  
	  
	  // Generate a hash id for the new record that does never change
	  // while the record exists.
      $this->generateHashId();
      	  
	  // Get the Database connection
	  $connection = DRDB::getConnection();
	  
	  //Assemble SQL statement to create a new database record	  
	  $query = "INSERT INTO " . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . "(";
	  
	  $values = array();
	  $param_types = "";	  
	  
	  foreach ($this->fields as $key => $value) {	  	
	  	  $query = $query . DRDB::prep($key) . ', '; 	  	
	  }
	  
	  // remove last comma and space
	  $query = substr($query, 0, -2);
	  $query = $query . ") VALUES (";

	  foreach ($this->fields as $key => $value) {
	  	  		
	  	  // Three things are needed for the prepared SQL query:
	  	  //
	  	  // 1. The variable content placeholder
	  	  $query = $query . "?,";
	  		  		
	  	  // 2. The content in an array
	  	  array_push($values, $value);
	  	
	  	  // 3. The type of variable. Always s = string
	  	  $param_types = $param_types . "s";
	  }
	    
	  // remove last comma, then add a closing bracket
	  $query = substr($query, 0, -1) . ')';
	  
	  $log->lwrite("SQL: " . $query);
	  
	  $prep_query = $connection->prepare ($query);
	  
	  // The SQL prepared statment bind_param() doesn't take a variable
	  // number of parameters natively. Therefore call_user_func_array is used	  
	  // to call bind_param() indirectly:
	  $input_arry = array();
	  $input_array[] = &$param_types; // first var in the array is "ssss..."
	  $j = count($values);
	  for($i = 0;$i < $j; $i++){
	      $input_array[] = &$values[$i];
	  }	  
	  
	  $log->lwrite("Values for bind: " . print_r($input_array, true));
	  
	  // Now bind the parameter values to the SQL query string ?-marks
	  call_user_func_array(array($prep_query, 'bind_param'), $input_array);	  
	  
	  // Run the MySQL statement 
	  if (!$prep_query->execute()) {	
	  	  // send fail message and return
	  	  $log->lwrite('ERROR: Unable to create record.');
	  	  $log->lwrite('ERROR msg: ' . $prep_query->error);
	  	  $return = array(0, 'No Document Record added. Unable to create record.');
	  	  $prep_query->close();
	  	  return $return;
	  }	 
		  
	  $return = array(1, 'Document Record successfully added.');
	  // add success message
	  $prep_query->close();
	  return $return;
  
  } // end of addRecord()

  
  /**
   * editRecord()
   * 
   * This function can be called to write the changes made to the variables in
   * a document object back to the database. The protected 'id' variable of the
   * object must be set to an existing record in the database for the operation
   * to be successful
   * 
   * @return an array with the first element being and integer indicating 
   *         success (1) or failure (0). The second array element is a string 
   *         that indicates success or gives the failure reason in human 
   *         readable format.
   */
  
  public function editRecord() {
   	
  	$log = new Logging();
  	
  	// Make sure all parameters for the SQL query are o.k.
  	if (!$this->_verifyInput()) {
  		$return = array(0, 'Document NOT Updated. Input params could not be verified');
  		return $return;  		
  	}
  
  	// Get the Database connection
  	$connection = DRDB::getConnection();
 		  		
  	//Assemble SQL statement to update an exisiting database record
  	$query = "UPDATE  `" . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . "` SET ";
  		 
  	$values = array();
  	$param_types = "";  		
  	
  	foreach ($this->fields as $key => $value) {

  		if ($key == 'id') continue;
  				
  		// Three things are needed for the prepared SQL query:
  		//
  		// 1. The variable, and a placeholder (?) for the content
  		$query .= $key . "=?,";
  			
  		// 2. The content in an array
  		array_push($values, $value);
  		
  		// 3. The type of variable. Always s = string
  		$param_types = $param_types . "s";
  	}
  		 
  	// remove last comma
  	$query = substr($query, 0, -1);
  	$query = $query . " WHERE id='". (int) $this->fields['id'] . "'";
 		 
  	$log->lwrite("SQL: " . $query);  		

  	$prep_query = $connection->prepare ($query);

  	// The SQL prepared statment bind_param() doesn't take a variable
  	// number of parameters natively. Therefore call_user_func_array is used
  	// to call bind_param() indirectly:
  	$input_array = array();
  	$input_array[] = &$param_types; // first var in the array is "ssss..."
  	$j = count($values);
  	for($i = 0;$i < $j; $i++){
  		$input_array[] = &$values[$i];
  	}
  	 
  	$log->lwrite("Values for bind: " . print_r($input_array, true));
  	
  	// Now bind the parameter values to the SQL query string ?-marks
  	call_user_func_array(array($prep_query, 'bind_param'), $input_array);
    	
  	// Run the MySQL statement
  	if (!$prep_query->execute()) {	
	  	  // send fail message and return
	  	  $log->lwrite('ERROR: Unable to create record.');
	  	  $log->lwrite($connection->error);	  	  
	  	  $return = array(0, 'No Document Record added. Unable to create record.');
	  	  $prep_query->close();
	  	  return $return;
  	}   
	    
  	$return = array(1, 'Document successfully edited');
  	$prep_query->close();
  	return $return;
  
  } // end of editRecord()

  /**
   * editSingleFieldInRecord()
   *
   * This function can be called to write the changes made to a SINGLE FIELD 
   * in a document object back to the database. The protected 'id' variable of the 
   * object must be set to an existing record in the database for the operation 
   * to be successful
   *
   * The first input parameter has to contain the name of the database field
   * that is to be modified and the second parameter the content. 
   * 
   * The third variable can be set to FALSE to prohibit logging and checking
   * the validity of the field name. Thisis done so updates of administrative 
   * data does not leave a line in the log file and because the administrative
   * database fields are not part of the $doc_db_description array!
   * 
   * @param string the name of the field that is to be edited
   * @param string the content to be written into the field
   * @param boolean: TRUE to show logging and validate the field name 
   *        (to protect against SQL injection) or FALSE not to log and 
   *        not to validate (internal use only, see above).
   *
   * @return an array with the first element being and integer indicating 
   *         success (1) or failure (0). The second array element is a string 
   *         that indicates success or gives the failure reason in human 
   *         readable format.
   */
  
  public function editSingleFieldInRecord($dbFieldName, $dbFieldContent, 
  		                                  $write_log_and_validate_field_name) {
  
  	$doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
  	
  	$log = new Logging();  	 
  	 
  	// If the database field name does not match one of the field
  	// names in $doc_db_description then abort this search, there is
  	// something really wrong here. Anyone trying an SQL injection attack??
  	$found = false;
  	foreach ($doc_db_description as $field_name => $properties) {
  		if ($dbFieldName == $properties[DB_FIELD_NAME]) {
  			$found = true;
  			break;
  		}
  	}
  	
  	// 'id' is also a valid field name!
  	if ($dbFieldName == 'id') $found = true;
  	
  	if (!$found && $write_log_and_validate_field_name == true){
  		$err_txt = 'ERROR: Field ' . $dbFieldName . ' does not exist!';
  		$log->lwrite($err_txt);
  		$return = array(0, $err_txt);
  		return $return;
  	}  		
  
  	// Get the Database connection
  	$connection = DRDB::getConnection();
  
  	$query = "UPDATE `" . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . "` SET " .
  			$dbFieldName . "=? WHERE id=?";  	
  	 		
	if ($write_log_and_validate_field_name == true) {
		$log->lwrite('SQL: ' . $query);
		$log->lwrite('Values for bind: ' . $dbFieldContent . ', ' . 
				     $this->fields['id']);			
	}
	
	$prep_query = $connection->prepare ($query);	
	$prep_query->bind_param("ss", $dbFieldContent, $this->fields['id']);
  			
  	// Run the MySQL statement
  	if ($prep_query->execute()) {

  		$prep_query->close();
  		$return = array(1, 'Single field of record successfully edited.');
   		return $return;
  			
  	} else {
  		// send fail message and return
  		$prep_query->close();
  		$return = array(0, 'Single field of docrecord NOT Updated. Unable to modify the record.');  			
  		$log->lwrite('ERROR: ' . $connection->error);
  		return $return;
  	}
  	  
  } // end of editSingleFieldInRecord()
    

  /**
   * getRecord
   * 
   * Get a SINGLE docrecord from the database. If id = 0 then the last entry
   * (i.e. the latest docrecord that was (just) added is selected) is returned
   *
   * @param $id
   * @returns object a single Document object or NULL if the requested database
   *          entry does not exist.
   */
  
  public static function getRecord($id) {
  	
  	// Get the database connection
  	$connection = DRDB::getConnection();
  	// Set up the query
  	if ($id == 0){
  		// get the last entry from the database
  		$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` ORDER BY id DESC LIMIT 1';
  	}
  	else {
  		// get the docrecord with the given ID
  		$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` WHERE id="'. (int) $id.'"';
  	}

  	$log = new Logging();
  	$log->lwrite('SQL: ' . $query);
  	
  	// Run the MySQL command
  	$result_obj = '';
  	try
  	{
  		$result_obj = $connection->query($query);
  		if (!$result_obj)
  		{
  			throw new Exception($connection->error);
  		}
  		else
  		{  			
  			// Get fields of the record into an associative array
  			// (field name) -> (field content) 
  			$result_array = $result_obj->fetch_assoc();
  	  			
  			if (!$result_array)
  			{
  				throw new Exception($connection->error);
  			}
  			else
  			{  				
  				// Create a new DocRecord Object. This calls __construct which
  				// will copy the associative array into the class's 
  				// $fields array
  				$item = new DocRecord($result_array);
  				return($item);
  			}
  		}
  	}
  	catch(Exception $e)
  	{
  		echo $e->getMessage();
  		$log->lwrite('ERROR during getRecord() for id: ' . $id .
  				     ' with msg: ' . $e->getMessage());
  	}  
  
  } // end of getRecord()
  
  
  /**
   * getIdFromHashId()
   * 
   * The function finds the record with the given hash_id and returns its id
   *
   * @param $hash_id
   * @returns int, hash_id or 0 if not found.
   * 
   */
  
  public static function getIdFromHashId($hash_id) {
  	 
  	$log = new Logging();

  	$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` WHERE hash_id=?';

  	$log->lwrite('SQL: ' . $query);
  	$log->lwrite('hash_id: ' . $hash_id);
  	
  	$connection = DRDB::getConnection();
  	$prep_query = $connection->prepare ($query);
  	$prep_query->bind_param("s", $hash_id);
  	 
  	// Run the MySQL command
  	$result_obj = '';
  	try
  	{
  		$result = $prep_query->execute();
  		if (!$result) throw new Exception($connection->error);

  		$result_obj = $prep_query->get_result();
  		if (!$result_obj) throw new Exception('Could not get prep query result');
  		
  		$result_array = $result_obj->fetch_assoc();  
  		if (!$result_array) throw new Exception('Could no read result array');
		
  	}
  	catch(Exception $e)
  	{
  		$log->lwrite('ERROR for hash_id: ' . $hash_id .
  				     ' with msg: ' . $e->getMessage());
  		return 0;
  	}
  	
  	$item = new DocRecord($result_array);
  	return($item->getId());
  	 
  } // end of getIdFromHashId()  
  
  
  /**
   * deleteDoc
   *
   * Delete docrecord with the given id. If successful TRUE is returned,
   * otherwise FALSE
   *
   * @param $id
   */
  public static function deleteDoc($id) {
  	
  	// Get the database connection
  	$connection = DRDB::getConnection();
  	 
  	//sanitize the id
  	$id = (int) $id;
  	 
  	$query = '';
  	 
  	// Set up the query
  	if ($id != 0){
  		// get the entry that corresponds the given id from the database
  		$query = 'DELETE FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` WHERE id="' . 
  		         (int) $id . '" LIMIT 1';
  	}
  	
  	$log = new Logging();
  	$log->lwrite('SQL: ' . $query);  	
  
  	// Run the MySQL command
  	$result_obj = '';
  	try
  	{
  		$result_obj = $connection->query($query);
  		if (!$result_obj) throw new Exception($connection->error);  		
  	}
  	catch(Exception $e)
  	{
  		echo $e->getMessage();
  		return false;
  	}
  	
  	return true;
  	
  } // end of deleteDoc()
  
  
  /**
   * getRecords()
   * 
   * Protected internal function that is called by public functions of the 
   * class that have assembled an SQL query string that is the first parameter
   * to the method.
   * 
   * It is used to get A LIST OF ONE OR MORE documents
   * 
   * CAUTION: Using this function does not protect against SQL injection 
   * attacks if column names and values are user supplied. In this case 
   * use getRecordsWithPrepStatement()!!!
   * 
   * @param  string with SQL statement
   * @return array of document objects 
   *         NULL if the query was successful but no results were found 
   *         FALSE if there was a problem with the query, e.g. due to an 
   *         SQL syntax error.
   */
  
  protected static function getRecords($query) {

    $items = array();
    $result_obj = '';
        
    try {
	    // Get the connection and run the query  
	    $connection = DRDB::getConnection();
	    $result_obj = $connection->query($query);
	    
	    if (!$result_obj) {
			throw new Exception($connection->error);
	    }    
	    
	    // Loop through the results, 
	    // passing them to a new object of this class, 
	    // and making a regular array of the objects
	  
	    while($result_array = $result_obj->fetch_assoc()) {
	      	
	      	// assigning the current row to a new DocRecord object invokes 
	      	// the __construct() function above which copies the fields 
	      	// of the current row into the new object's variables
	        $items[] = new DocRecord($result_array); 
	    }
    }
    
    catch(Exception $e) {

    	if (!$result_obj) {
    		$log = new Logging();
    		$log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);

    	} else {
    		$log->lwrite('ERROR: Exception was raised');    	 
    	}
    	 
      return false;
    }

    return($items);
          
  }

  
  /**
   * getRecordsWithPrepStatement()
   *
   * This private function is used to get A LIST OF ONE OR MORE documents
   * from the database. Instead of an SQL query that includes field names
   * and parameters, this function takes the $query with placeholders (=?)
   * the parameter types of the placeholders and the values for them
   * as separate variables to perform ann SQL PREPARED STATEMENT query
   * that is safe against SQL injection attacks.
   *
   * @param string with SQL statement that includes placeholers (=?)
   * @param string list of param types for SQL prepared statments
   * @param an array of values for the prepared statment.
   * 
   * @return array of document objects
   *         NULL if the query was successful but no results were found
   *         FALSE if there was a problem with the query, e.g. due to an
   *         SQL syntax error.
   */
  
  protected static function getRecordsWithPrepStmt($query, $param_types, $values) {
  
  	$log = new Logging();  	 
  	
  	$items = array();
  	$result_obj = '';
  
  	try {
  		// Get the connection and run the query
  		$connection = DRDB::getConnection();
  		
  		$prep_query = $connection->prepare($query);
  		
  		// The SQL prepared statment bind_param() doesn't take a variable
  		// number of parameters natively. Therefore call_user_func_array is used
  		// to call bind_param() indirectly:
  		$input_array = array();
  		$input_array[] = &$param_types; // first var in the array is "ssss..."
  		$j = count($values);
  		for($i = 0; $i < $j; $i++){
  			$input_array[] = &$values[$i];
  		}
  		 
  		$log->lwrite('SQL: ' . $query);
  		$log->lwrite("Values for bind: " . print_r($input_array, true));
  		 
  		// Now bind the parameter values to the SQL query string ?-marks
  		call_user_func_array(array($prep_query, 'bind_param'), $input_array);
  		
  		$result_obj = $prep_query->execute();
  		
  		if (!$result_obj) {
  			throw new Exception($connection->error);
  		}
  		
  		$result = $prep_query->get_result();
  		
  		// Loop through the results,
  		// passing them to a new object of this class,
  		// and making a regular array of the objects
  		 
  		while($result_array = $result->fetch_assoc()) {
  
  			// Assigning the current row to a new DocRecord object invokes
  			// the __construct() function above which copies the fields
  			// of the current row into the new object's variables
  			$items[] = new DocRecord($result_array);
  		}  		
  		
  	}
  
  	catch(Exception $e) {
  
  		if (!$result_obj) {
  			$log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);
  
  		} else {
  			$log->lwrite('ERROR: Exception was raised');
  		}
  
  		$prep_query->close();  		
  		return false;
  	}
  
  	$prep_query->close();
  	return($items);
  
  }
  

  /**
   * getRecordsOrderByID()
   * 
   * This method can be called externally to get an array of docrecord objects that
   * includes all entries of the database ordered by the ID (key of the database)
   * It assembles the SQL query for the operation and then calls the internal 
   * protected function generic getRecords() method to do the actual job.
   * 
   * Note: The conventions record with id = -10 is NOT included! 
   * 
   * @return array of docrecord objects or false
   */
  public static function getRecordsOrderByID() {
  	
  	$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` WHERE id > 0 ' . 
  	         'ORDER BY id';

  	// call the general function to issue the SQL command and return the result 
  	// directly to the caller
  	return (DocRecord::getRecords($query));
  }
  
  
  /**
   * getIdOfAllRecordsOrderedById()
   *
   * This method can be called externally to get an array of docrecord objects 
   * that only contains the id of all document records.
   * 
   * The conventions record (id = -10) is excluded.
   *
   * @return array of docrecord objects or false
   */
  
  public static function getIdOfAllRecordsOrderedById() {
  	 
  	$query = 'SELECT id FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` WHERE id > 0 ' . 
  	         'ORDER BY id';
  
  	// call the general function to issue the SQL command and return the result
  	// directly to the caller
  	return (DocRecord::getRecords($query));
  }
  
  
  /**
   * getRecordsBetweenIds()
   *
   * Get a list of all documents between two database IDs (inclusive)
   * Input parameters are to be given as strings.
   *
   * @return array of docrecord objects or false
   */
  public static function getRecordsBetweenIds($start_id, $stop_id) {
  
  	$query = "SELECT * FROM `" . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . "` WHERE `id` >= " . 
  	         (int) $start_id . 
  	         " AND `id` <= " . (int) $stop_id . " ORDER BY id";
  
  	$log = new Logging();
  	$log->lwrite('SQL: ' . $query);  	
  	
  	// call the general function to issue the SQL command and return the result
  	// directly to the caller
  	return (DocRecord::getRecords($query));
  }
    
  
  
  /**
   * getRecordsMultiFieldSearch($search_parameters, $case_sensitive_search)
   *
   * Get all bellet entries from the database that match a multiple field
   * query as defined in the $search_parameters. 
   * 
   * @ param $search_parameters is formatted as follows
   * 
   * search_parameters {
   *   (db field name, search text, not_like (bool), empty (bool)),
   *   (db field name, search text, not_like (bool), empty (bool)),
   *   (db field name, search text, not_like (bool), empty (bool)),
   *   ...
   * }
   * 
   * Access to variables in the 2nd dimension arry is done via constants
   * defined in init.php such as MULTI_FIELD_NAME, MULTI_SEARCH_TERM, etc.
   * 
   * @param bool: $case_sensitive_search true or false
   *
   * @return array of docrecord objects or false
   */
  public static function getRecordsMultiFieldSearch($search_parameters, 
  		                                            $boolean_operation,
  				                                    $search_limit_start, 
		                                            $search_limit_end,
  		                                            $is_case_sensitive_search) {
 	
  	$doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
  	
  	$log = new Logging();
  	
  	$values = array();
  	$param_types = "";
  	
  	// if there are no search parameters, return straight away
    if (count($search_parameters) <1) return '';
    
  	$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` WHERE (';
  	
  	$num_search_fields = 0;
  	
  	// assemble the SQL string with field names and search strings
  	for ($i = 0; $i < count($search_parameters); $i++){
  		
  		$cur_field_name = $search_parameters[$i][MULTI_FIELD_NAME]; 
  		$cur_search_term = $search_parameters[$i][MULTI_SEARCH_TERM];
  		
  		// If no field name is selected don't append anything to the 
  		// SQL search string
  		if ($cur_field_name == "-") {
  			continue;
  		}
  		
  		// If the current field name does not match one of the field 
  		// names in $doc_db_description then abort this search, there is
  		// something really wrong here. Anyone trying an SQL injection attack??
  		$found = false;
  		foreach ($doc_db_description as $field_name => $properties) {
	  		if ($cur_field_name == $properties[DB_FIELD_NAME]) {
	  			$found = true;
	  			break;			
	  		}
  		}
  		
  		if (!$found) {
  			$log->lwrite('ERROR: Field ' . $cur_field_name . ' does not exist!');
  			return NULL;
  		}
  			  			
  		// If it is not the first term, we need an AND/OR in the query string
  		if ($i != 0){
  			if (strcmp($boolean_operation, "and") == 0) {
  				$query .= 'AND ';  					
  			}
  			else {
  				$query .= 'OR ';  					
  			}
  		}
  		
  		// If the search term is NOT EQUAL to "EMPTY" it's a normal search
  		if ($search_parameters[$i][MULTI_EMPTY] != true){
  		    
  		    // A note on how the search term query is assembled below as
  		    // this is not straight forward from the code below:
  		    //
  		    // Case senstivive search is done with collate utf8_bin in the
  		    // SQL search term assembled below. This way the search also
  		    // does not trip of diacritis collation, i.e. Pére and Pere will
  		    // be treated as different words and not as the same as would
  		    // be the case with the standard SQL collation for the colum in 
  		    // the datbase + the use of the SQL LIKE term below
  		    //
  		    // Cased IN-senstive search is done by converting all chars to
  		    // lower case in the SQL statement for both the column searched
  		    // and the search term. In addition the utf8_bin collation is used
  		    // in the search to ensure 'e' and 'è' are treated as different
  		    // characters.
  		    //
  		    // The use of LOWER and utf8_bin collation that is different from
  		    // the default collation in the datbase means that no index
  		    // can be used by the database. However, as we search through 
  		    // many different fields, I'm not sure if that makes a difference
  		    // and results in a speed penalty. In practice, even with 300
  		    // database entries, this does not seem to make a difference.
  		    if ($is_case_sensitive_search === true) {
  		        $query .= ' (`';
  		    }
  		    else {
  		        $query .= ' LOWER (`';
  		    } 
			
  			// Search for term or exclude it?
  			if ($search_parameters[$i][MULTI_NOT_LIKE] == false){  				
	  			
  				// Three things are needed for the prepared SQL query:
  				//
  				// 1. The variable content placeholder
	  			$query .= $cur_field_name . '`) collate utf8_bin ';

	  			if ($is_case_sensitive_search === true) {
	  			    $query .= 'LIKE(?) ';
	  			}
	  			else {
	  			    $query .= 'LIKE LOWER(?) ';
	  			}
	  			
	  			// 2. The content in an array
	  			array_push($values, '%'. $cur_search_term . '%');

	  			// 3. The type of variable. Always s = string
	  			$param_types .=  "s";	  			
	  			
	  			
  			} else {
  			    
  				$query .= $cur_field_name . '`) collate utf8_bin ';
  				
  				// $query .= 'NOT LIKE ? ';
  				if ($is_case_sensitive_search === true) {
  				    $query .= 'NOT LIKE(?) ';
  				}
  				else {
  				    $query .= 'NOT LIKE LOWER(?) ';
  				}
  				
  				array_push($values, '%'. $cur_search_term . '%');
  				
  				$param_types .= "s";
  			}  				
  		}
  		else {

  			// search for empty/non-empty fields
  			$query .= assembleSqlQuerryForEmptyOrNotEmptyField(
  					  $cur_field_name,
  					  $search_parameters[$i][MULTI_NOT_LIKE]);
  			
  		}
  			
  		$num_search_fields++;
  		
  	} // end of for each term loop
  	
  	$query .= ') ';
  	
  	// If limits are given, limit the search to a subset of the database
  	if ($search_limit_start > 0) {
  	
  		$query .= " AND `id` >= " . (int) $search_limit_start .
  		" AND `id` <= " . (int) $search_limit_end . " ";
  	}
  	else {
  		// start the search at the beginning and omit the conventions records
  		// with id = -10
  		$query .= " AND `id` > 0 ";
  	}	
  	
  	$query .= 'ORDER BY id';
	
  	if ($num_search_fields > 0) {
  		// call the general function to issue the SQL command and return the 
  		// result directly to the caller
  		return (DocRecord::getRecordsWithPrepStmt($query, $param_types, $values));
  	}
  	else 
  	{
  		return NULL;
  	}
  	
  }
  
/**
 * getRecordsSearchAllFields()
 *
 * This method can be called externally to get an array of docrecord objects that
 * includes all entries of the database ordered by the ID (key of the database)
 * that contain the search string in at least ONE of the database fields.
 * It assembles the SQL query for the operation and then calls the internal
 * protected function generic getRecords() method to do the actual job.
 * 
 * The last (optional) parameter is set to true in case a regular expression
 * is given to the function in $search string.
 *
 * @return array of docrecord objects or false
 */
public static function getRecordsSearchAllFields($doc_db_description, 
		                                         $search_string,
		                                         $search_limit_start, 
		                                         $search_limit_end,
		                                         $is_case_sensitive_search,
												 $is_regex = false) {
	
	$log = new Logging();
	
	$values = array();
	$param_types = "";
	
	$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` WHERE (';
	 
	$num_search_fields = 0;
	$i = 0;
	 
	// Assemble the SQL string with field names and search string to look
	// for fields with HTML color attributes inside
	foreach ($doc_db_description as $field_print_name=>$field_options):		

	    //if it is not the first term, we need an OR in the query string
		if ($i != 0) $query .= 'OR ';
		$i++;

		// Have a look in getRecordsMultiFieldSearch() for how 
		// the SQL statemens below work for case sensitive and case IN-sensistive
		// search that is NOT diacritis agnostic.
		if ($is_case_sensitive_search === true) {
			$query .= ' (`';
		}
		else {
			$query .= ' LOWER (`';
		}

		if ($is_regex === true) {
			
			// Three things are needed for the prepared SQL query:
			//
			// 1. The variable content placeholder
			$query .= $field_options[DB_FIELD_NAME] . '`) RLIKE ? ';			
				
		}
		else {
			// 1. The variable content placeholder
			$query .= $field_options[DB_FIELD_NAME] . '`) collate utf8_bin ';
			
			if ($is_case_sensitive_search === true) {
			    $query .= 'LIKE(?) ';
			}
			else {
			    $query .= 'LIKE LOWER(?) ';
			}
			 				
		}
			
		// 2. The content in an array
		array_push($values, $search_string);
			
		// 3. The type of variable. Always s = string
		$param_types = $param_types . "s";
		
	endforeach; 

	$query .= ') ';
	
	// If limits are given, limit the search to a subset of the database
	if ($search_limit_start > 0) {
	
		$query .= " AND `id` >= " . (int) $search_limit_start .
		" AND `id` <= " . (int) $search_limit_end . " ";
	}
	else {
		// start the search at the beginning and omit the conventions records
		// with id = -10
		$query .= " AND `id` > 0 ";
	}
	
	$query .= 'ORDER BY id';
	
    return (DocRecord::getRecordsWithPrepStmt($query, $param_types, $values));
	
}


/**
 * getPreviousDoc
 *
 * Get the ID of the docrecord record that precedes the ID given 
 * to this method.
 *
 * @param $id
 * 
 * @return $item (database record object)
 */

public static function getPreviousDoc($id) {
	
	// Get the database connection
	$connection = DRDB::getConnection();

	if (!$id){
		exit;
	}
		
	$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . 
			 '` WHERE `id` > 0 AND `id` < ' .  (int) $id . 
	         ' ORDER by `id` DESC LIMIT 1';

	$log = new Logging();
	$log->lwrite('SQL: ' . $query);	
	
	// Run the MySQL command
	$result_obj = '';
	try
	{
		$result_obj = $connection->query($query);
		if (!$result_obj)
		{
			throw new Exception($connection->error);
		}
		else
		{			
			$result_array = $result_obj->fetch_assoc();

			// If there is a result in the array
			if (!$result_array)
			{
				throw new Exception($connection->error);
			}
			else
			{
				// pass back the results
				$item = new DocRecord($result_array);
				return($item);
			}
		}
	}
	catch(Exception $e)
	{
		echo $e->getMessage();
	}

} 

/**
 * getNextDoc
 *
 * Get the ID of the docrecord record that after the ID given
 * to this method.
 *
 * @param $id
 *
 * @return $item (database record object)
 */

public static function getNextDoc($id) {
	
	// Get the database connection
	$connection = DRDB::getConnection();

	if (!$id){
		exit;
	}

	$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` WHERE `id` > ' . 
	          (int) $id . ' ORDER by `id` LIMIT 1';		

	$log = new Logging();
	$log->lwrite('SQL: ' . $query);	
	
	// Run the MySQL command
	$result_obj = '';
	try
	{
		$result_obj = $connection->query($query);
		if (!$result_obj)
		{
			throw new Exception($connection->error);
		}
		else
		{
			$result_array = $result_obj->fetch_assoc();

			// If there is a result in the array
			if (!$result_array)
			{
				throw new Exception($connection->error);
			}
			else
			{
				// pass back the results
				$item = new DocRecord($result_array);
				return($item);
			}
		}
	}
	catch(Exception $e)
	{
		echo $e->getMessage();
	}

}

/**
 * changeRecordId
 *
 * Changes the id of a docrecord from the given current one to the new one.
 * Such an operation is necessary if the docrecord sorting order is changed.
 *
 * @param $current_id, $new_id
 *
 * @return array: (true/false indicating success, string with success or 
 * failure message)
 */
public static function changeRecordId($current_id, $new_id) {

	// Get the Database connection
	$connection = DRDB::getConnection();

	$query = "UPDATE `" . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . "` SET id='". (int) $new_id . 
	         "' WHERE id='" . (int) $current_id . "'";

	$log = new Logging();
	$log->lwrite('SQL: ' . $query);
		
	// Run the MySQL statement
	if ($connection->query($query)) {
		$return = array(true, 'Database record successfully moved/inserted!');
		return $return;
			
	} else {
		$return = array(false, 'Database record move/insert failed during SQL execution!');
		return $return;
	}
}


/**
 * getRecordsWithEmtpyField
 *
 * Get docrecord records in which the field given
 * is empty
 *
 * @param $field_name_empty
 *
 * @return array: (true/false indicating success, string with success or
 * failure message)
 */
public static function getRecordsWithEmtpyField($field_name_empty,
		               $search_limit_start, $search_limit_end) {

	$doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
	
	$log = new Logging();
		
	// Get the Database connection
	$connection = DRDB::getConnection();
	
  	// If the database field name does not match one of the field
  	// names in $doc_db_description then abort this search, there is
  	// something really wrong here. Anyone trying an SQL injection attack??
  	$found = false;
  	foreach ($doc_db_description as $field_name => $properties) {
  		if ($field_name_empty == $properties[DB_FIELD_NAME]) {
  			$found = true;
  			break;
  		}
  	}
  	
  	if (!$found){
  		$err_txt = 'ERROR: Field ' . $field_name_empty . ' does not exist!';
  		$log->lwrite($err_txt);
  		$return = array(0, $err_txt);
  		return $return;
  	}  	
	
	
	// Assemble SQL query for empty field search
	$query = "SELECT * FROM `" . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . "` WHERE ";
	$query .= assembleSqlQuerryForEmptyOrNotEmptyField($field_name_empty, false);
	
	// If limits are given, limit the search to a subset of the database
	if ($search_limit_start > 0) {
		
		$query .= " AND `id` >= " . (int) $search_limit_start .
		          " AND `id` <= " . (int) $search_limit_end;			
	}
	else {
		// start the search at the beginning and omit the conventions records
		// with id = -10
		$query .= " AND `id` > 0 ";
	}

	$query .= " ORDER BY id";
         		
	$log->lwrite('SQL: ' . $query);

  	// call the general function to issue the SQL command and return the result 
  	// directly to the caller
  	return (DocRecord::getRecords($query));
}

/**
 * getSortedListOfModifiedDocs($max_num)
 *
 * This function returns $max_num of docrecord database entries with the latest
 * modification (write) date. This way a web page can be populated with 
 * information about the database entries that were recently modified.
 *
 * @param $max_num
 *
 * @return array: (true/false indicating success, string with success or
 * failure message)
 */
public static function getSortedListOfModifiedDocs($max_num) {

	// Get the Database connection
	$connection = DRDB::getConnection();
	
	// Remove slashes to make variable SQL save
	$field_name_empty = DRDB::prep($max_num);
	
	$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . 
			 '` WHERE last_update_time > 0 AND id > 0 ' .
			 'order by last_update_time DESC LIMIT ' . (int) $max_num;
		
	$log = new Logging();
	$log->lwrite('SQL: ' . $query);
	
	// call the general function to issue the SQL command and return the result
	// directly to the caller
	return (DocRecord::getRecords($query));

	
}


/**
 * getSortedListOfViewedDocs($max_num)
 *
 * This function returns $max_num of docrecord database entries with the latest
 * view date.
 *
 * @param $max_num
 *
 * @return array: (true/false indicating success, string with success or
 * failure message)
 */
public static function getSortedListOfViewedDocs($max_num) {

	// Get the Database connection
	$connection = DRDB::getConnection();

	// Remove slashes to make variable SQL save
	$field_name_empty = DRDB::prep($max_num);

	$query = 'SELECT * FROM `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . 
			 '` WHERE detail_view_time > 0 and id > 0 ' .
			 'order by detail_view_time DESC LIMIT ' . (int) $max_num;

	$log = new Logging();
	$log->lwrite('SQL: ' . $query);

	// call the general function to issue the SQL command and return the result
	// directly to the caller
	return (DocRecord::getRecords($query));


}

/**
 * createInitialTable()
 *
 * Creates the initial docrecord database table based on the data the
 * user has entered in the configuration files in the '/config' directory.
 * The table will only be created if it does not exist already so nothing
 * is overwritten in case this method is called accidentally.
 * 
 * @param none
 *
 * @return boolean, true = succes, false = fail, database table was not created
 * 
 */
public static function createInitialTable() {

	$fieldArrayWithOptions = DatabaseConfigStorage::getDbFieldConfig();

	$log = new Logging();
	
	// Get the Database connection
	$connection = DRDB::getConnection();

	$query = PHP_EOL . 'CREATE TABLE `' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') . '` (' . PHP_EOL;
	$query .= '`id` int(11) NOT NULL AUTO_INCREMENT, ' . PHP_EOL;   

	// Include all database fields the user has configured
	foreach ($fieldArrayWithOptions as $field_name => $properties) {
		$query .= '`' . $properties[DB_FIELD_NAME] . '` text, ' . PHP_EOL;
	}
	
	// Include database fields that are not user configurable.
	$query .= '`lock_time` bigint(20) DEFAULT NULL,' . PHP_EOL;
	$query .= '`lock_user` text,' . PHP_EOL;
	$query .= '`detail_view_time` bigint(20) DEFAULT NULL,' . PHP_EOL;
	$query .= '`detail_view_user` text,' . PHP_EOL;
	$query .= '`last_update_time` bigint(20) DEFAULT NULL,' . PHP_EOL;
	$query .= '`last_update_user` text,	' . PHP_EOL;
	$query .= '`hash_id` text,	' . PHP_EOL;
 
	// And now finish the SQL string
  	$query .= 'PRIMARY KEY (`id`)' . PHP_EOL;
    $query .= ') ENGINE=MyISAM AUTO_INCREMENT=1000000000 ' . PHP_EOL . 
  			  'DEFAULT CHARSET=utf8;';
   
	$log->lwrite('SQL: ' . $query);

	try {
		// Get the connection and run the query
		$connection = DRDB::getConnection();
		$result_obj = $connection->query($query);
		 
		if (!$result_obj) {
			throw new Exception($connection->error);
		}
		
		$log->lwrite('Creation of the main table successful');

	}
	
	catch(Exception $e) {
	
		if (!$result_obj) {
			$log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);
	
		} else {
			$log->lwrite('ERROR: Exception was raised');
		}
	
		return false;
	}	
	
	return true;
}

/**
 * delFieldInTable()
 *
 * Deletes a field (column) to the existing table for user input
 *
 * @param string, the SQL colum name (must already be sanitized!)
 *
 * @return boolean, true or false if there was a problem deleting the column
 *
 */

public static function delFieldInTable($field_name) {
    
    $log = new Logging();
    
    $log->lwrite('Deleting field ' . $field_name . ' in the main DB table');
    
    $query = 'ALTER TABLE `'. DatabaseConfigStorage::getCfgParam('mysqlMainTable') .
    '` DROP `' . $field_name . '`;';
    
    try {
        // Get the connection and run the query
        $connection = DRDB::getConnection();
        $result_obj = $connection->query($query);
        
        if (!$result_obj) {
            throw new Exception($connection->error);
        }
        
        $log->lwrite('Deleting the field/column was successful');
        
    }
    
    catch(Exception $e) {
        
        if (!$result_obj) {
            $log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);
            
        } else {
            $log->lwrite('ERROR: Exception was raised');
        }
        
        return false;
    }
    
    return true;
    
}


/**
 * addFieldToTable()
 *
 * Adds a field (column) to the existing table for user input
 *
 * @param string, the SQL colum name (must already be sanitized!)
 *
 * @return boolean, true or false if there was a problem creating the column
 *
 */

public static function addFieldToTable($field_name) {

    $log = new Logging();
    
    $log->lwrite('Adding field ' . $field_name . ' to the main DB table');
    
    $query = 'ALTER TABLE `'. DatabaseConfigStorage::getCfgParam('mysqlMainTable') .
             '` ADD `' . $field_name . '` TEXT NULL;';
    
     try {
         // Get the connection and run the query
         $connection = DRDB::getConnection();
         $result_obj = $connection->query($query);
         
         if (!$result_obj) {
             throw new Exception($connection->error);
         }
         
         $log->lwrite('Adding the new field/column was successful');
                     
    }

    catch(Exception $e) {
        
        if (!$result_obj) {
            $log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);
            
        } else {
            $log->lwrite('ERROR: Exception was raised');
        }
        
        return false;
    }
    
    return true;
    
}

/**
 * addColumnHashId()
 *
 * This function is called when updating from database structure v3 to v4
 * and adds the new column hash_id to the table.
 *
 * @param none
 *
 * @return boolean, true = success, false = failure
 *
 */
public static function addColumnHashId() {

	$log = new Logging();

	$query = 'alter table ' . DatabaseConfigStorage::getCfgParam('mysqlMainTable') .
	' add hash_id text';

	$log->lwrite('SQL: ' . $query);

	try {
		$connection = DRDB::getConnection();
		$result_obj = $connection->query($query);
			
		if (!$result_obj) {
			throw new Exception($connection->error);
		}

		$log->lwrite('Adding hash_id column was successful!');
	}

	catch(Exception $e) {

		if (!$result_obj) {
			$log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);

		} else {
			$log->lwrite('ERROR: Exception was raised');
		}

		return false;
	}

	return true;
}

} // end of class