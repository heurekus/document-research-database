<?php
/**
* maintablecfg.php
*
* Called by frontend modules such as bs_manage_db_fields.php
* to modify the configuration of the main database table. 
* Modifications are, for example, changing field names and
* creating / deleting fields.
*
* @version    1.0 2021-06-19
* @package    DRDB
* @copyright  Copyright (c) 2021 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
*/


class MainTableCfg
{

/**
 * changeFieldName()
 * 
 *
 * @param none, parameters taken from http post variables
 *
 * @return boolean, true = succes, false = fail, field name unchanged
 *
 */
    
public static function changeFieldName() {
    
    $log = new Logging();
    
    if (!isset($_POST['original-field-name']) or !isset($_POST['new-field-name'])) {
        $log->lwrite('ERROR: Paramter missing');
        return false;
    }
    
    $original_name = $_POST['original-field-name'];
    $original_name = convertToSafeUtf8Html($original_name, true);
    $original_name = strip_tags($original_name);
    
    $new_name = $_POST['new-field-name'];
    $new_name = convertToSafeUtf8Html($new_name, true);
    $new_name = strip_tags($new_name);
    
    $log->lwrite('Renaming field from: ' . $original_name . " to : " . $new_name);
    
    $fields = DatabaseConfigStorage::getDbFieldConfig();
    $new_fields = array();

    // Check if the new field name given by the user already exists.
    // If so, don't add the field.
    if (self::fieldNameExists($fields, $new_name)) {
        $log->lwrite("ERROR: Field name '". $new_name . "' already exists, aborting");
        return false;
    } 
    
    // Output current field names to the debug log
    foreach ($fields as $field_print_name=>$field_options) {
        $log->lwrite($field_print_name);
    }
     
    // Go through the field name array, find the entry to be
    // changed and change it. All others remain as before.
    foreach ($fields as $field_print_name=>$field_options) {
        
        if ($field_print_name == $original_name) {
            $new_fields[$new_name] = $field_options;
        } else {
            $new_fields[$field_print_name] = $field_options;
        }
    }

    $log->lwrite('');    
    $log->lwrite('Changed Fields:');
    $log->lwrite('');
    
    // Output new field names to the debug log
    foreach ($new_fields as $field_print_name=>$field_options) {
        $log->lwrite($field_print_name);
        $log->lwrite($field_options[0] . ", " . $field_options[1] . ", " . $field_options[2]);

    }
    
    // Now update the field description in the main table config in the database   
    DatabaseConfigStorage::updateMainDbFieldConfig($new_fields);
    
    self::increaseConfigVersion($log);    
    
    return true;
}


/**
 * changeFieldOptions()
 *
 * Change the options of a field. Currently, only the inline print option
 * is changed by this method.
 *
 * @param none, parameters taken from http post variables
 *
 * @return boolean, true = succes, false = fail
 *
 */
    
public static function changeFieldOptions() {
    
    $log = new Logging();
    
    if (!isset($_POST['original-field-name'])) {
        $log->lwrite('ERROR: Paramter missing');
        return false;
    }
    
    $field_name = $_POST['original-field-name'];
    $field_name = convertToSafeUtf8Html($field_name, true);
    $field_name = strip_tags($field_name);

    // On export, shall the field be printed inline or not?
    $inline_print = "0";
    if (isset($_POST['print-field'])) {
        $inline_print = "1";
    }  
    
    $log->lwrite ('Modifying options for field: ' . $field_name);
    $log->lwrite('Setting field option for inline printing to: ' . $inline_print);
    
    $fields = DatabaseConfigStorage::getDbFieldConfig();
    $new_fields = array();
   
    // Output current field names to the debug log
    foreach ($fields as $field_print_name=>$field_options) {
        $log->lwrite($field_print_name);
    }
     
    // Go through the field name array, find the entry to be
    // changed and change it. All others remain as before.
    foreach ($fields as $field_print_name=>$field_options) {
        
        if ($field_print_name == $field_name) {
            // Set the inline print parameter to the value set in the GUI
            $field_options[2] = $inline_print;
            $new_fields[$field_print_name] = $field_options;
        } else {
            // User the original field options
            $new_fields[$field_print_name] = $field_options;
        }
    }
   
    // Now update the field description in the main table config in the database   
    DatabaseConfigStorage::updateMainDbFieldConfig($new_fields);
    
    self::increaseConfigVersion($log);    
    
    return true;
}



/**
 * createNewField()
 *
 *
 * @param none, parameters taken from http post variables
 *
 * @return boolean, true = succes, false = fail
 *
 */

public static function createNewField() {

    $log = new Logging();
    
    if (!isset($_POST['new-field-name'])) {
        $log->lwrite('ERROR: Paramter missing');
        return false;
    }    
   
    $new_field = $_POST['new-field-name'];
    $new_field = convertToSafeUtf8Html($new_field, true);
    $new_field = strip_tags($new_field);
    
    // On export, shall the field be printed inline or not?
    $inline_print = "0";
    if (isset($_POST['print-field'])) {
        $inline_print = "1";
    }  

    // The new field needs an SQL internal field name. Fields
    // generated when the user creates a new field in the GUI
    // internally start with 'FLD_' and a number. Now go though
    // the existing fields, search for all 'FLD_' entries, and
    // get the highest number. For the new field the number
    // is then increased by 1
    $field_suffix = 0;
    $fields = DatabaseConfigStorage::getDbFieldConfig();
    foreach ($fields as $field_print_name=>$field_options) {
        if (strpos($field_options[0], "FLD_") === 0) {
            $x = (int) substr($field_options[0], 4);
            if ($x > $field_suffix) $field_suffix = $x;
        }
    }
    $field_suffix += 1;    
    $sql_column_name = "FLD_" . $field_suffix;
    $log->lwrite("New field name: " . $sql_column_name);
    
    // Check if the new field name given by the user already exists. 
    // If so, don't add the field.
    if (self::fieldNameExists($fields, $new_field)) {
        $log->lwrite("ERROR: Field name '". $new_field . "' already exists, aborting");
        return false;
    }    
    
    if (!DocRecord::addFieldToTable($sql_column_name)) {
        $log->lwrite('Unable to create new field in the database, aborting');
        return false;
    }
        
    // now add it to the config array and write it to the config table
    $fields[$new_field] = [$sql_column_name, "1", $inline_print];
    DatabaseConfigStorage::updateMainDbFieldConfig($fields);
    
    self::increaseConfigVersion($log);
    
    return true;
}

/**
 * deleteField()
 *
 *
 * @param none, parameters taken from http post variables
 *
 * @return boolean, true = succes, false = fail
 *
 */

public static function deleteField() {
    
    $log = new Logging();
    
    if (!isset($_POST['name-of-field-to-delete'])) {
        $log->lwrite('ERROR: Paramter missing');
        return false;
    }
    
    $name_of_field_to_del = $_POST['name-of-field-to-delete'];
    $name_of_field_to_del = convertToSafeUtf8Html($name_of_field_to_del, true);
    $name_of_field_to_del = strip_tags($name_of_field_to_del);
    
    
    // Check if the new field name given by the user exists.
    // If not, there is nothing to do
    $fields = DatabaseConfigStorage::getDbFieldConfig();
    
    if (!self::fieldNameExists($fields, $name_of_field_to_del)) {
        $log->lwrite("ERROR: Field name to delete '". $name_of_field_to_del . 
                     "' does not exist!");
        return false;
    }
    
    $sql_column_name = $fields[$name_of_field_to_del][0];
    $log->lwrite('DB column name to delete: ' . $sql_column_name);
    
    if (!DocRecord::delFieldInTable($sql_column_name)) {
        $log->lwrite('Unable to delete the field in the database, aborting');
        return false;
    }
    
    // Delete the entry from the config and update config in the database
    unset($fields[$name_of_field_to_del]);
    DatabaseConfigStorage::updateMainDbFieldConfig($fields);
    
    self::increaseConfigVersion($log);
    
    return true;
}



/**
 * fieldNameExists()
 *
 * Private function to update the config version and invalidate
 * the config cache. This is necessary so the database config
 * info is reloaded during the current run on the server.
 *
 * @parm array, fields object
 * @param object of the logger
 *
 * @return boolean, false if field does not yet exist otherwise true
 *
 */

private function fieldNameExists($fields, $given_field_name) {

    foreach ($fields as $field_print_name => $field_options) {
        
        if ($given_field_name == $field_print_name) {
            return true;
        }
    }
    
    return false;
}

/**
 * increaseConfigVersion()
 *
 * Private function to update the config version and invalidate
 * the config cache. This is necessary so the database config
 * info is reloaded during the current run on the server.
 *
 * @param object of the logger
 *
 * @return null
 *
 */

private static function increaseConfigVersion($log) {
    
    // Increase config version number so database config info is reloaded
    // Update the table config version
    $config_version = DatabaseConfigStorage::getCfgParam('version');
    $config_version = strval((int) $config_version + 1);
    $log->lwrite("New Version: " . $config_version);
    if (!DatabaseConfigStorage::setCfgParam('version', $config_version)) {
        $log->lwrite("ERROR: Unable to save updated config version number");
    }
    
    // Invalidate the temporary config cache as the config in the DB has changed
    DatabaseConfigStorage::clearConfigCache();
}

}
// end of class

?>
