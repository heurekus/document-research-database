<?php
/**
 * userconfigstorage.php
 *
 * This class takes care of:
 * 
 *  * storing and retrieving the user's configuration from database table 
 *    user_config.
 *    
 *  * Storing and retrieving permissions
 *  
 *  * Creating the initial table and populating it with the admin user
 *    data and permissions.
 *    
 * Notes:
 * 
 * The getUserConfig() method is called once for every HTTP request in init.php. 
 * 
 * The saveUserConfig() method has to be called whenever a user config
 * parameter is changed.
 * 
 * Save Misc user session variables:
 * ==================================
 * 
 * All $_SESSION variables that start with "blt_user_" are saved and
 * restored when saveUserConfig() and getUserConfig() are called!
 * 
 *
 * @version    1.0 2017-04-13
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

class UserConfigStorage
{
   
/**
 * saveUserConfig()
 * 
 * This method saves the user's configuration data from PHP SESSION variables
 * to the user_config database table. 
 * 
 * Details:
 * 
 * - If a session variable doesn't exist, default values will be saved.
 *  
 * - If no record exists for the user a new one is created.
 * 
 * - In addition to the document field configuration (database-structure.php)
 *   all SESSION variables that start with 'blt_' are saved as well
 * 
 * @return bool true if successful, false if there was a table access problem
 * 
 */
  
public function saveUserConfig() {

	$config_version = DatabaseConfigStorage::getCfgParam('version');
	
	$log = new Logging();
	
	if (!isset($_SERVER['PHP_AUTH_USER'])) {
		$log->lwrite('Error - No authenticated user...');
		return(false);
	}
	
	$user_name = $_SERVER['PHP_AUTH_USER'];
	
	$doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
	
	if (isset($_SESSION['db_field_order'])){
		$log->lwrite('Saving field order SESSION variable');
		$doc_db_fieldsJSON = json_encode($_SESSION['db_field_order']);
	}
	else {
		$log->lwrite('Saving default field order');
		$doc_db_fieldsJSON = json_encode($doc_db_description);		
	}
	
	$_SESSION['blt_user_cfg_version'] = $config_version;
	$misc_user_config['blt_user_cfg_version'] = $config_version;
	
	// Collect other user config session variables in an array
	// All session variables to save start with 'blt_'
	foreach ($_SESSION as $name=>$value) {
			
		if (strpos($name, 'blt_') !== false) {
			$log->lwrite('saving "' . $name . '" - "' . $value . '"');
			$misc_user_config[$name] = $_SESSION[$name];
				
		}
	} // end of find session variables to save
	
	$misc_user_configJSON = json_encode($misc_user_config);
	   
  	// Get the Database connection
	$connection = drdb::getConnection();

    // Update existing configuration record for the user in the database
    // or create new record if it does not exist.	
	$query = "UPDATE user_config SET config_fields=?, config_misc=? " . 
	         "WHERE user_name=?";

	$prep_query = $connection->prepare ($query);
	$prep_query->bind_param("sss", $doc_db_fieldsJSON, $misc_user_configJSON,
	                        $user_name);
		
    $log->lwrite('SQL: ' . $query);  
    $log->lwrite('user_name: ' . $user_name);
    $log->lwrite('doc_db_fieldsJSON: ' . $doc_db_fieldsJSON);
    $log->lwrite('misc_user_configJSON: ' . $misc_user_configJSON);
    	
  	// Run the MySQL statement
  	if ($prep_query->execute()) {
  		$log->lwrite('User config record successfully saved.');
  		$prep_query->close();
  		return(true);   	    
  	} 
  	else { 
	    $log->lwrite('User config NOT updated. Unable to save record');
	    $prep_query->close();
	    return(false);	    	  	    
  	}
} // end of saveUserConfig()


/**
 * getUserConfig
 * 
 * Get the current user's configuration record from the database tabe and
 * store the values in the corresponding PHP SESSION variables. If the
 * user does not yet have a user config record, put the database entry 
 * default values into the session variables.
 * 
 * Special case:
 * 
 * If the main database table configuration has changed, the method
 * loads the default configuration from database-structure.php and stores
 * it in table user_config.
 *
 * @returns bool true of successful, false if there was a db problem
 */
  
public static function getUserConfig() {
  	
	$config_version = DatabaseConfigStorage::getCfgParam('version');

	$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
  	
  	$log = new Logging();
  	
  	$log->lwrite ('Current config version: ' . $config_version);
  	
  	$user_name = $_SERVER['PHP_AUTH_USER'];
  	$log->lwrite('Getting user configuration for user from db: ' . $user_name);
  	
  	if (!isset($_SERVER['PHP_AUTH_USER'])) {
  		$log->lwrite('Error - No authenticated user...');
  		return(false);
  	}
  	
  	// Exit if configuration is already present in session variables and
  	// the config.php version has not changed.
  	if (isset($_SESSION['db_field_order']) &&
  		isset($_SESSION['blt_user_cfg_version']) &&
  		$_SESSION['blt_user_cfg_version'] == $config_version) {
  			
  		$log->lwrite('Configuration already loaded and valid');
  		return(true);
  	}
  	
  	// The config was not yet loaded or the config.php version variable
  	// has changed. So load the config from the database into the session
  	// variables.
 	  	
  	// Get the database connection and get the user's configuration
  	$connection = drdb::getConnection();
  	
  	$query = 'SELECT * FROM user_config WHERE user_name=?';
  	$prep_query = $connection->prepare ($query);
  	$prep_query->bind_param("s", $user_name);
  	
  	$log->lwrite('SQL: ' . $query);
  	$log->lwrite('user_name: ' . $user_name);
  	 	
  	$result_obj = $prep_query->execute();
  	
  	if (!$result_obj) {
  		$log->lwrite('Database access error, this should not have happened!');
  		$prep_query->close();
  		return(false);
  	}
  		
  	// Move the user's configuration variables from the database into an 
  	// associtative array. Each field of the database entry is put into one 
  	// associative array entry. 
  	//
  	// Example:
  	//
  	// user_name => martin-test
  	//
  	// user_name = database field name
  	// martin-test = content of the field
  	//
  	$result = $prep_query->get_result();
  	$user_cfg_db_entry = $result->fetch_assoc();
  	$prep_query->close();
  	
  	if (!$user_cfg_db_entry)
  	{  		
  		$log->lwrite('User does not exist in database, ' .
  				     'using default values');

  		// Save the default config.
  		$_SESSION['db_field_order'] = $local_doc_db_description;  		
  		UserConfigStorage::saveUserConfig();
  		
  		// Also reset the field order for list output
  		// Note: The field order for list output is NOT saved in the database
  		// so only the session variable has to be reset!
  		if (isset($_SESSION['db_field_order_for_list_output']))
  		    unset($_SESSION['db_field_order_for_list_output']);  		    
  		
		return(true);
   	}
   	
   	// ----------------------------------------------------------------------
   	// Part 1: Convert data from the config_misc field from JSON to an 
   	// array and then put them into individual SESSION variables.
   	// ----------------------------------------------------------------------   	
   	
   	$misc_user_configJSON = $user_cfg_db_entry['config_misc'];
   	$misc_user_config = json_decode($misc_user_configJSON, true);
   	
   	foreach ($misc_user_config as $field_name=>$field_value) {
   		
   		$_SESSION[$field_name] = $field_value;
   		$log->lwrite('restoring session variable "' . $field_name . '" ' .
   				     'with value: "'. $field_value . '"');
   		   		
   	}

    // If the config.php version variable is different from the value in the
    // database record of the user something has changed in 
    // config.php or databasestructure.php. Thus:
    //
    // * Use the default values for the session variables
    // * Save the overwrite the values in the user config in the database 
    //   with the default values.

  	if ($_SESSION['blt_user_cfg_version'] != $config_version) {
  		$log->lwrite('Note: User config version is different, ' .
  				'putting default values into session variable');  		
  		$_SESSION['db_field_order'] = $local_doc_db_description;

  		// Save the default config.
  		UserConfigStorage::saveUserConfig();
  		
  		// Also reset the field order for list output
  		// Note: The field order for list output is NOT saved in the database
  		// so only the session variable has to be reset!
  		if (isset($_SESSION['db_field_order_for_list_output'])) 
  		    unset($_SESSION['db_field_order_for_list_output']);
  		
  		return(true);  		  		
  	}
  	
  	// ---------------------------------------------------------------------
  	// Part 2: Get the configuration of the database record fields in JSON
  	// format, convert it back into an associative array and put it into
  	// a SESSION variable.
  	// ---------------------------------------------------------------------
  	
  	$fieldConfigOfUserJSON = $user_cfg_db_entry['config_fields'];
  	$fieldsConfigOfUser = json_decode($fieldConfigOfUserJSON, true);

  	// Final sanity check
  	if (count($fieldsConfigOfUser) != count($local_doc_db_description)){

  		$log->lwrite('Error: Number of fields do not match, ' .
  				     'putting default values into session variable');
  		$_SESSION['db_field_order'] = $local_doc_db_description;
  		
  		// Save the default config.
  		UserConfigStorage::saveUserConfig();
  		  		
  		return(true);  		
  	}

  	$log->lwrite('Putting document field configuration from database ' . 
  			     'to session variable');
  	$_SESSION['db_field_order'] = $fieldsConfigOfUser;
  		  	
  	return (true);
  
} // end of getUserConfig()


/**
 * createInitialTable()
 *
 * Creates the initial user_config database table.
 *
 * The table will only be created if it does not exist already so nothing
 * is overwritten in case this method is called accidentally.
 *
 * @param none
 *
 * @return boolean, true = succes, false = fail, database table was not created
 *
 */
public static function createInitialTable() {

	$log = new Logging();

	// Get the Database connection
	$connection = DRDB::getConnection();

	$query = PHP_EOL . 'CREATE TABLE `user_config` (' . PHP_EOL;

	$query .= '`user_name` varchar(100) NOT NULL,' . PHP_EOL;
	$query .= '`config_fields` text NOT NULL,' . PHP_EOL;
	$query .= '`config_misc` text NOT NULL,' . PHP_EOL;
	$query .= '`permissions` text,' . PHP_EOL;
	$query .= 'PRIMARY KEY (`user_name`)' . PHP_EOL;
	$query .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;' . PHP_EOL;

	$log->lwrite('SQL: ' . $query);

	try {
		// Get the connection and run the query
		$connection = DRDB::getConnection();
		$result_obj = $connection->query($query);

		if (!$result_obj) {
			throw new Exception($connection->error);
		}

		$log->lwrite('Creation of the mod_history table successful');

	}

	catch(Exception $e) {

		if (!$result_obj) {
			$log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);

		} else {
			$log->lwrite('ERROR: Exception was raised');
		}

		return false;
	}

	return true;
}

/**
 * populateInitialUsersAndPermissions()
 *
 * Takes the permissions of the users configured in user-permissions.php
 * and writes them into the database. The permissions in user-permissions.php
 * are then no longer used.
 *  
 * @param none
 *
 * @return boolean, true = success, otherwise false
 *
 */
public static function populateInitialUsersAndPermissions() {
    
    $log = new Logging();
    
    global $user_permissions;
    
    if (count($user_permissions) < 1) {
        $log->lwrite('ERROR: No user permissions in user-permissions-config.php');
        return false;
    }
    
    foreach ($user_permissions as $user_name => $permissions) {
        $log->lwrite('Initializing user ' . $user_name);   
        self::createNewUser($user_name, $permissions, $log);
    }
    
    return true;
}


/**
 * createNewUser()
 *
 * Creates a new user in the database table and populates the permissions field.
 * Other fields are left empty. 
 *
 * @param string, username
 * @param array, permissions
 * @parm object, logging object
 *
 * @return boolean, true = success, otherwise false
 *
 */

public static function createNewUser($user_name, $permissions, $log){

    $permissionsJSON = json_encode($permissions);
    $log->lwrite('JSON permissions: ' . $permissionsJSON);
    
    // Get the Database connection
    $connection = drdb::getConnection();
    
    // Now create the new record in the database and store the permissions
    $query = "REPLACE INTO user_config(user_name, permissions, config_fields, config_misc) " .
             "VALUES (?, ?, ?, ?)";
    $prep_query = $connection->prepare ($query);
    $empty="";
    $prep_query->bind_param("ssss", $user_name, $permissionsJSON, $empty, $empty);
    
    $log->lwrite('SQL: ' . $query);
    $log->lwrite('user_name: ' . $user_name);
    $log->lwrite('permissionsJSON: ' . $permissionsJSON);
    
    // Run the MySQL statement
    if (!$prep_query->execute()) {
        
        $log->lwrite('User permissions NOT updated. Unable to create record');
        $log->lwrite('SQL error: ' . mysqli_error($connection));
        $prep_query->close();
        // Even failure of adding one user requires an abort.
        return false;
    }
    
    $log->lwrite('User permissions successfully stored in record.');
    $prep_query->close();
    return true;
  
}


/**
 * addPermissionsField()
 *
 * This method is called when updating from database structure v3 to v4
 * and adds the new field for storing permissions to the table.
 *
 * @param none
 *
 * @return boolean, true = success, false = failure
 *
 */
public static function addPermissionsField(){
   
    $log = new Logging();
    
    $query = 'alter table user_config' .
    ' add permissions text';
    
    $log->lwrite('SQL: ' . $query);
    
    try {
        $connection = DRDB::getConnection();
        $result_obj = $connection->query($query);
        
        if (!$result_obj) {
            throw new Exception($connection->error);
        }
        
        $log->lwrite('Adding permissions field to the user_config table was successful!');
    }
    
    catch(Exception $e) {
        
        if (!$result_obj) {
            $log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);
            
        } else {
            $log->lwrite('ERROR: Exception was raised');
        }
        
        return false;
    }
    
    return true;
    
}


/**
 * saveUserPermissions()
 *
 * Updates the permissions of a user in the user config database
 *
 * @param string, username
 * @param array of permission strings
 *
 * @return boolean, true = success, false = failure
 *
 */
public static function saveUserPermissions($user_name, $permissions){
    
    $log = new Logging();
    
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        $log->lwrite('Error - No authenticated user...');
        return(false);
    }    
    
    // Convert the php array that stores the permissions into a JSON encoded
    // single string that can be stored in database.
    $permissionsJSON = json_encode($permissions);
    
    $log->lwrite('JSON permissions: ' . $permissionsJSON);

    // Get the Database connection
    $connection = drdb::getConnection();
    
    // Update existing configuration record for the user in the database
    $query = "UPDATE `user_config` SET permissions=? WHERE user_name=?";  
    
    $prep_query = $connection->prepare ($query);
    $prep_query->bind_param("ss", $permissionsJSON, $user_name);
    
    $log->lwrite('SQL: ' . $query);
    $log->lwrite('user_name: ' . $user_name);
    $log->lwrite('permissionsJSON: ' . $permissionsJSON);
    
    // Run the MySQL statement
    if ($prep_query->execute()) {        
        $log->lwrite('Permissions in user config record successfully saved.');
        
        if ($prep_query->affected_rows != 1) {
            $log->lwrite('Rows changed: ' . $prep_query->affected_rows);
            $log->lwrite('Note: 0 could mean unaltered (same permissions) or error');            
        }
        $prep_query->close();
    }
    else {
        $log->lwrite('User permissions NOT updated. Unable to save record');
        $log->lwrite('SQL ERROR: ' . $prep_query->error);
        $prep_query->close();
        return false;
    }
    
    return true;

}

/**
 * getUserPermissions()
 *
 * Gets the permissions of a user from the user config database
 *
 * @param string, username
 *
 * @return mixed, array if found or false if user was not found
 *
 */
public static function getUserPermissions($user_name) {

    $log = new Logging();
    
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        $log->lwrite('Error - No authenticated user...');
        return(false);
    }

    // Get the database connection and get the user's configuration
    $connection = drdb::getConnection();
    
    $query = 'SELECT permissions FROM user_config WHERE user_name=?';
    $prep_query = $connection->prepare ($query);
    $prep_query->bind_param("s", $user_name);
    
    //$log->lwrite('SQL: ' . $query);
    //$log->lwrite('user_name: ' . $user_name);
    
    $result_obj = $prep_query->execute();
    
    if (!$result_obj) {
        $log->lwrite('Database access error, this should not have happened!');
        $prep_query->close();
        return(false);
    }

    // Move the user's permission info from the database into an
    // associtative array. Each field of the database entry is put into one
    // associative array entry.
 
    $result = $prep_query->get_result();
    $user_cfg_db_entry = $result->fetch_assoc();
    $prep_query->close();
    
    
    if (!$user_cfg_db_entry) {
        $log->lwrite('User ' . $user_name . ' does not exist in the DB');
        return false;
    }
    
    if (!array_key_exists('permissions', $user_cfg_db_entry)) {
        $log->lwrite('ERROR: Permissions for user ' . $user_name . ' do not exist in the DB');
        return false;
    }
    
    $user_permissions_configJSON = $user_cfg_db_entry['permissions'];
    $user_permissions_config = json_decode($user_permissions_configJSON, true);
    
    //$log->lwrite('Read user permissions from db: ' . $user_permissions_configJSON);
    
    return $user_permissions_config;
}




/**
 * getPermissionsOfAllUsers()
 *
 * Returns an array of all users and their permissions. 
 * The key of each entry of the associative array is the
 * username and the value of the array entry is an array
 * with the permission names.
 *
 * @param none
 *
 * @return mixed, the associative array containing
 *         usernames/permissions or fales on error.
 *
 */
public static function getPermissionsOfAllUsers() {
    
    $log = new Logging();
    $log->lwrite('Retrieving permissions of all users from the db');
    
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        $log->lwrite('Error - No authenticated user...');
        return(false);
    }
    
    // Get the database connection and get the user's configuration
    $connection = drdb::getConnection();
    
    $query = 'SELECT user_name, permissions FROM user_config';
    $prep_query = $connection->prepare ($query);
       
    $result_obj = $prep_query->execute();
    
    if (!$result_obj) {
        $log->lwrite('Database access error, this should not have happened!');
        $prep_query->close();
        return(false);
    }
    
    $result = $prep_query->get_result();
    $db_result = $result->fetch_all();
    $prep_query->close();
    
    $user_permissions = array();
    
    // Convert the restul returned from the database to an
    // associative array. The key of each entry is the username
    // which points to an array of permissions
    foreach ($db_result as $db_entry) {       
        $user_name = $db_entry[0];        
        $permissions_array = json_decode($db_entry[1], true);        
        $user_permissions[$user_name]=$permissions_array;
    }
    
    return $user_permissions;
}

/**
 * deleteUser()
 *
 * Deletes the config record of a user in the database
 *
 * @param string, the user name
 *
 * @return boolean, true on success, else false
 *
 */
public static function deleteUser($user_name) {
    
    $log = new Logging();
    $log->lwrite('Deleting user ' . $user_name . ' from the database');
    
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        $log->lwrite('Error - No authenticated user...');
        return(false);
    }
    
    // Get the database connection and get the user's configuration
    $connection = drdb::getConnection();
    
    // Update existing configuration record for the user in the database
    $query = 'DELETE FROM `user_config` WHERE user_name=? LIMIT 1';
    
    $prep_query = $connection->prepare ($query);
    $prep_query->bind_param("s", $user_name);
    
    $log->lwrite('SQL: ' . $query);
    $log->lwrite('user_name: ' . $user_name);

    
    // Run the MySQL statement
    if (!$prep_query->execute()) {
        $log->lwrite('ERROR: User was not deleted');
        $log->lwrite('SQL ERROR: ' . $prep_query->error);
        $prep_query->close();
        return false;       
    }

    $log->lwrite('User record successfully deleted');
    return true;    
    
}

} // end of class

