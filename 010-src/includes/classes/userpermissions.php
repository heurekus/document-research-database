<?php
/**
 * userpermissions.php
 *
 * This module contains a class that contains functions to handle
 * users and permissions. There are three places where users and
 * permissions are stored:
 * 
 *  - in config/user-permissions-config.php 
 *    (legacy, only used during init)
 *  - in the database user records (for users not statically
 *    configured in the php config file.
 *  - passwords in the Apache web server pwd.txt file
 *  
 *  In addition, this class contains a global array of all user
 *  permission categories.
 *  
 *  Currently, the following permissions exist:
 * 
 *  * 'history': The user is allowed to see history of changes made to
 *               a database entry.
 *             
 *  * 'export' : The user is allowed to export the database, parts of it or
 *               individual records.
 *               
 *  * 'edit'   : The user is allowed to edit, move and delete records.
 *  
 *  * 'help'   : Help buttons are shown to the user.
 *
 *  * 'admin'  : The user can see the admin page and perform bulk uploads.
 *               Note: export + admin rights are required to upload export 
 *               templates.
 *
 * @version    1.1 2021-04-09
 * @package    DRDB
 * @copyright  Copyright (c) 2018-2021 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


class UserPermissions
{

    // Global array that contains permission categories that can be assigned
    private static $permission_categories = array(
        'history', 'export', 'edit', 'help', 'admin'
    );
    
    
    public static function getPermissionCategories() {
        return self::$permission_categories;
    }
    
    /**
     *
     * hasAccess()
     *
     * Check if a given user has the given access right
     *
     * @param string user name
     * @param string access right name
     *
     * @return true or false
     *
     */
    
    public static function hasAccess($requested_permission) {

        $log = new Logging();
        
        $user_name = $_SERVER['PHP_AUTH_USER'];
        $user_permissions = UserConfigStorage::getUserPermissions($user_name);
        
        if ($user_permissions === false) {
            $log->lwrite('ERROR: User ' . $user_name . 
                         ' has no permissions in the db');
            return false;
        }

        if (!in_array($requested_permission, $user_permissions)) {
            //$log->lwrite('User ' . $user_name . 
            //             ' does not have permission: ' . $requested_permission);
            return false;
        }
        
        return true;
    }

    
    /**
     *
     * getUsersAndPermissions()
     *
     * Return all users and their permissions
     *
     * @param void
     * 
     * @return mixed, array of usernames, each with an array of permissions
     *         same as in user-permissions-config.php. False if there was
     *         a problem
     *
     */
    
    public static function getUsersAndPermissions() {
        
        return UserConfigStorage::getPermissionsOfAllUsers();
                       
    }
    
    
    /**
     *
     * updatePermissionsOfSingleUser()
     *
     * Updates the permissions of a single user in the database.
     * The username and permissions are taken from HTTP $_POST variables.
     * 
     * IMPORTANT: The check if the current user is allowed to change
     * the permissions of another user needs to be checked elsewhere.
     *
     * @param none, HTTP $_POST variables
     *
     * @return boolean, true if operation has succeeded, false if there
     *         was a problem.
     *
     */
    
    public static function updatePermissionsOfSingleUser($log) {
        
        if (!(isset($_POST['operation']) && $_POST['operation'] == "change_permissions")) {
            $log->lwrite('ERROR: operation variable not present or wrong value');
            return false;
        }
        
        if (!(isset($_POST['user']))) {
            $log->lwrite('ERROR: user NOT given in HTTP POST operation to update permissions');
            return false;
        }
        
        $user = $_POST['user'];
        $log->lwrite('New permissions for user: ' . $user);
        
        $permissions_of_this_user = self::getPermissionsFromHttpPostVars();

        // If the current (admin) user changes his own permissions, make sure
        // that the admin permissions remain in place.
        if ($user == $_SERVER['PHP_AUTH_USER']) {
            $log->lwrite("Admin uses changes his own permission, making sure " . 
                         "admin rights remain");
            array_push($permissions_of_this_user, "admin");
        }
        
        if (!UserConfigStorage::saveUserPermissions($user, $permissions_of_this_user)){
            $log->lwrite('ERROR: Saving updated user permissions to the database failed');
            return false;
        }
        
        $log->lwrite('updatePermissionsOfSingleUser() succeeded.');
        return true;
        
    }

    
    /**
     *
     * addUser()
     *
     * Adds a new user to the database and the webserver's password file.
     *
     * @param none, all parameters are taken from HTTP POST variables
     *
     * @return boolean true for success, else false
     *
     */
    public static function addUser($log) {
        
        if (!(isset($_POST['operation']) && $_POST['operation'] == "add_user")) {
            $log->lwrite('ERROR: operation variable not present or not set to add_user');
            return false;
        }
        
        if (!(isset($_POST['user']))) {
            $log->lwrite('ERROR: username NOT given in HTTP POST operation to add a new user');
            return false;
        }
        
        if (!(isset($_POST['password']))) {
            $log->lwrite('ERROR: password NOT given in HTTP POST operation to add a new user');
            return false;
        }
        
        $username = convertToSafeUtf8Html($_POST['user'], true);
        // remove double quotes in username. This is malicious
        $username = str_replace('"', "", $username);
        
        $password = convertToSafeUtf8Html($_POST['password'], true);
        
        if ($password == "") {
            $log->lwrite('Password empty, user not created');
            return false;
        }
        
        $log->lwrite('New user: ' . $username);
        
        $permissions_of_this_user = self::getPermissionsFromHttpPostVars();        
        
        if (!UserConfigStorage::createNewUser($username, $permissions_of_this_user, $log)){
            $log->lwrite('ERROR: Creating a new user in the database failed');
            return false;
        }
        
        // Now store the password in the web server password file
        self::storePassword($username, $password, $log);
        
        return true;
        
    }
    
    
    /**
     *
     * getPermissionsFromHttpPostVars()
     *
     * Gets the permissions for add or modify user operations from HTTP
     * Post variables. If no permissions for the user are set a single
     * 'empty' permission will be put into the array.
     *
     * @param none, all parameters are taken from HTTP POST variables
     *
     * @return array with permissions
     *
     */
    
    static function getPermissionsFromHttpPostVars() {
       
        // Get the permissions given in the HTTP POST operation and put
        // them into an array
        $permissions_of_this_user = array();
        foreach (self::$permission_categories as $permission) {
            
            if (isset($_POST[$permission])) {
                array_push($permissions_of_this_user, $permission);
            }
        }
        
        // If the user has no permissions, put an 'empty' permission in the array
        // to prevent php warnings when itterating over the array later on.
        if (count($permissions_of_this_user) < 1) {
            array_push($permissions_of_this_user, "empty");
        }
        
        return $permissions_of_this_user;
        
    }
    
    
    /**
     *
     * changePasswordOfUser()
     *
     * Changes a user's password
     *
     * @param none, all parameters are taken from HTTP POST variables
     *
     * @return boolean true for success, else false
     *
     */
    
    public static function changePasswordOfUser($log) {
        
        if (!isset($_POST['username'])) {
            $log->lwrite('Username not given, password not changed');
            return false;
        }
        
        if (!isset($_POST['password'])) {
            $log->lwrite('Password not given, password not changed');
            return false;
        }
                
        self::storePassword($_POST['username'], $_POST['password'], $log);
        
        return true;

    }
    
    
    /**
     *
     * storePassword()
     *
     * Create or Update a password in the webserver's pwd file.
     * 
     * Note: shell_exec() does not distinguish in its return value if there was an
     * error or if there was no change. Hence, the function does not return a result.
     * This could be enhanced in the future.
     *
     * @param string, username
     * @param string, password
     * @param object, logging object
     *
     * @return void
     *
     */
    static function storePassword ($username, $password, $log) {

        // Escape the quote character as it is used to encapsulate the username and password
        $username = str_replace('"', '\\"', $username);
        $password = str_replace('"', '\\"', $password);
        
        // Note: htpasswd writes to stderr which is not returned by shell_exec(). Therefore
        // piping stderr to stdout (2>&1)

        $cmd_str = 'htpasswd -b /drdb-pwd-dir/pwd.txt "' . $username . '" "' . $password . '" 2>&1';       
        // $log->lwrite('CMD Str: ' . $cmd_str);
        
        $exec_result = shell_exec ($cmd_str);
        $log->lwrite('Password creation result: ' . $exec_result);
        
    }
    
    /**
     *
     * deleteUser()
     *
     * Delete user config from the database and the password
     *
     * @param none, all parameters are taken from HTTP POST variables
     *
     * @return boolean true for success, else false
     *
     */
    public static function deleteUser($log) {

        if (!isset($_POST['username'])) {
            $log->lwrite('Username not given, password not changed');
            return false;
        }
        
        $username = $_POST['username'];
        
        // Prevent accidental deletion of 'self'
        if ($username == $_SERVER['PHP_AUTH_USER']) {
            $log->lwrite('ERROR: User wanted to delete himself, aborting request');
            return false;
        }
        
        // Escape the quote character as it is used to encapsulate the username
        $username = str_replace('"', '\\"', $username);
        
        // Delete the user password
        $cmd_str = 'htpasswd -D /drdb-pwd-dir/pwd.txt "' . $username . '" 2>&1';
        $log->lwrite("Delete user pwd string: " . $cmd_str);
        $exec_result = shell_exec ($cmd_str);
        $log->lwrite('Password deletion result: ' . $exec_result);
        
        // Delete the user record in the database
        UserConfigStorage::deleteUser($username);
        
        return true;
    }
    
} // end of class
