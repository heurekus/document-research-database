<?php
/**
 *
 * Database class
 * For one point of database access to the tables in the DRDB
 * (Document Research Database) database
 * 
 */

class DRDB
{
  /**
   * User name to connect to database
   * @var string $_mysqlUser
   */
  private static $_mysqlUser = NULL;
  /**
   * Password to connect to database
   * @var string $_mysqlPass
   */
  private static $_mysqlPass = NULL;
  /**
   * Database name
   * @var string $_mysqlDb
   */
  private static $_mysqlDb = NULL;
  /**
   * Hostname for the server
   * @var string $_hostName
   */
  private static $_hostName = NULL;
  /**
   * Database connection
   * @var Mysqli $connection
   */
  private static $_connection = NULL;

  /**
   * Constructor
   */
  private function __construct(){
  }
  
  /**
   * Get the Database Connection
   * 
   * @return Mysqli
   */
  public static function getConnection() {
  	 	
    if (!self::$_connection) {
        
        $log = new Logging();
        $log->lwrite('Creating new database connection');
        
    	// Get the following variables for the database server either from the
    	// environment variable (DOCKER scenario) or from the projects
    	// config file.
 
        self::$_hostName = getenv('DRDB_DB_HOST');
        if (self::$_hostName === false) {
            $log->lwrite('NO ENV VARIABLE FOR MYSQL HOST NAME, using config');
            self::$_hostName  = DatabaseConfigStorage::getCfgParam('mysqlHostName');
        }
        
        self::$_mysqlDb = getenv('DRDB_DATABASE');
    	if (self::$_mysqlDb === false) {
    	    $log->lwrite('NO ENV VARIABLE FOR MYSQL DATABASE NAME, using config');
    	    self::$_mysqlDb   = DatabaseConfigStorage::getCfgParam('mysqlDb');
    	}

    	self::$_mysqlUser = getenv('DRDB_USER');
    	if (self::$_mysqlUser === false) {
    	    $log->lwrite('NO ENV VARIABLE FOR MYSQL USER, using config');
    	    self::$_mysqlUser = DatabaseConfigStorage::getCfgParam('mysqlUser');
    	}
    	
    	self::$_mysqlPass = getenv('DRDB_PASSWORD');
    	if (self::$_mysqlPass === false) {
    	    $log->lwrite('NO ENV VARIABLE FOR MYSQL PWD, using config');
    	    self::$_mysqlPass = DatabaseConfigStorage::getCfgParam('mysqlPass');
    	}
    	
    	$log->lwrite('USING SQL: DB HOSTNAME: ' . self::$_hostName);
    	$log->lwrite('USING SQL: DB NAME: ' . self::$_mysqlDb);
    	$log->lwrite('USING SQL: DB USER: ' . self::$_mysqlUser);
    	//$log->lwrite('USING SQL: DB PWD: ' . self::$_mysqlPass);

      	self::$_connection = @new mysqli(self::$_hostName, self::$_mysqlUser, 
      			                         self::$_mysqlPass, self::$_mysqlDb);
      	if (self::$_connection->connect_error) {
        	die('Connect Error: ' . self::$_connection->connect_error);
      	} 

      	if (DatabaseConfigStorage::getCfgParam('protect_against_xss') == true) { 

      		$log->lwrite('Switching to mysql UTF-8 communication');
      			
      		// Change character set to utf8
      		mysqli_set_charset(self::$_connection,"utf8");
      	}
      	
    }
    return self::$_connection;
  }
  
  public static function prep($value) { 
    if (MAGIC_QUOTES_ACTIVE) {
      // If magic quotes is active, remove the slashes
      $value = stripslashes($value);
    }
    // Escape special characters to avoid SQL injections
    $value = self::$_connection->real_escape_string($value);
    return $value;
  }

}
