<?php

/**
 * reusable-gui-elements.php
 *
 * This file contains reusable functions called from several GUI modules
 * to generate Web GUI output.
 *
 * @version    1.3 2017-01-21
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 *
 */

/**
 * insertCategorySelection()
 *
 * Inserts a category drop down selection menu directly into the web GUI
 * 
 * @param string, text to be shown above the category drop down menu.
 *
 * @return none.
 *
 */

function insertCategorySelection ($category_text){

	global $id_categories;
	$local_id_categoies = $id_categories;

	echo '<div class="well">';
	echo '<p><strong>' . $category_text . '</strong></p>';
	echo '<select name="start_id" id="category_select">';
	echo '<option value="-">-</option>';
	// put all possible decades/categories into the drop-down list
	foreach ($local_id_categoies as $print_name=>$start_id) {
		echo '<option value="' . $start_id . '">' . $print_name . '</option>';
		echo PHP_EOL;		
	}
	echo '</select>';
	echo '</div>'; //end of well
}



