<?php
/**
 * record-search.php
 * 
 * This file contains functionality to search records in the 
 * database based on HTTP GET/POST parameters
 *
 * @version    1.0 2017-02-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

/**
 * getListOfRecords()
 * 
 * Get a list of records from the database depending on search parameters 
 * given in the GET request, creates a banner text depending on the search and
 * a return URL, also depending on the search.
 *
 * @param string, what kind of list to generate (doc_list_type) 
 * @param structure that describes a document datbase entry 
 *        (see database-structure.php)
 * @param boolean, true for case sensitive search, false for case IN-sensitive
 *        search
 *        
 * @return variable list that contains all database entries that match the
 *         search, the banner text for the web page, a footer text, the fields
 *         and search terms if the search is not generic (i.e. all documents
 *         of a decade and a replacement term if this is a search/replace.
 */

function getListOfRecords ($doc_list_type, 
		                   $doc_db_description, 
		                   $case_sensitive_search)
{
	$log = new Logging();

	$search_term = ""; // local, not returned to calling function!
	
	// variables returned by this function
	$footer_text = "";
	$search_log_string = "";
	$replacement_term = "";

	/* 
	 * Create an empty array to hold information in which fields a search
	 * term was found. This is then used e.g. in the bs_record_list_view.php
	 * logic to show all fields in which the search term(s) were found.
	 *
	 * The array is organized as follows:
	 *
	 * Key: 0,1,2,...
	 * Value: An array with:
	 *   Key = 0,1,2,...
	 *   Value = array(database field name, field print name, search term)
	 *
	 * Example:
	 *	
	 *(
	 *[0] => Array
	 *  (
	 *		[0] => date
	 *      [1] => Date of the document
	 *		[2] => the quick brown fox
	 *  )
	 *
	 *[1] => Array
	 *  (
	 *		[0] => title
	 *      [1] => Document title
	 *		[2] => the quick brown fox
	 *  )
	 *)
	 */

	$fields_and_search_terms = array();
	
	/*
	 * The switch statement below assembles array $items, which is a list of
	 * documents depending on the search query in each case construct.
	 *
	 * The following variables need to be populated as they are used in the
	 * code below:
	 *
	 * $items
	 * $banner_text
	 *
	 * optional: $footer_text
	 * optional: $fields_and_search_terms, $replacement_term
	 */
		
	// Decade/Category limit search by setting the following variables
	// to a non-zero value
	$search_limit_start = 0;
	$search_limit_end = 0;
	
	switch ($doc_list_type){
			
		case 'search_docs_multi_select':
			
            list($items, $fields_and_search_terms) = 
                 searchDocMultiSelect($case_sensitive_search);

            $search_log_string = "Document Multi Search";
			$banner_text = 'Multi Search Result';			
			         
			break;
			
		case 'search_docs_with_colored_text':			

			// Search for text in all fields that contains a color span.
			// Exclude color '000000' as this is black and doesn't count as
			// a color marking. The last parameter in the search all fiels
			// function is set to true to indicate a regular expression search!
			$items = DocRecord::getRecordsSearchAllFields($doc_db_description, 
					 "color:#[^000000]", 0, 0, $case_sensitive_search, true);
						
			$search_log_string = 'Search for colored text';
			$banner_text = 'All ' . DatabaseConfigStorage::getCfgParam('doc-name-ui-plural') . 
			               ' with colored text';
			
			break;
			
		case 'search_in_all_fields':

			list(
			       $items, 
			       $fields_and_search_terms, 
			       $replacement_term, 
			       $search_log_string,
			       $banner_text
			     ) =
			     searchInAllFields($case_sensitive_search, $doc_db_description,
			     				   $fields_and_search_terms);
							
			break;
			
		case 'search_docs_category':

			// Limit the search to one decade or category if included in
			// HTTP POST
			getAndSetSearchLimit($log, $search_limit_start, $search_limit_end);
				
			$items = DocRecord::getRecordsBetweenIds($search_limit_start, 
					                                 $search_limit_end);
			
			$search_log_string = "All records of a category/date";
			$banner_text = ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . ' Selection by Category';
			
			break;
	
		case 'search_docs_empty_field':

			$field_name_empty = trim(filter_input(INPUT_POST,'db_field_empty',
					FILTER_SANITIZE_STRING));
						
			$log->lwrite('Searching for documents in which "'. $field_name_empty .
					     '" is empty');			
			
			// Limit the search to one decade or category if included in
			// HTTP POST
			getAndSetSearchLimit($log, $search_limit_start, $search_limit_end);
					
			$items = DocRecord::getRecordsWithEmtpyField($field_name_empty,
					 $search_limit_start, $search_limit_end);
			
			$banner_text = 'Empty Field Search Result';
			$search_log_string = "Empty Field Search - " . $field_name_empty;

			break;
			
		default:
			$banner_text = 'All ' . DatabaseConfigStorage::getCfgParam('doc-name-ui-plural') . ' ordered by ID';
			$items = DocRecord::getRecordsOrderByID();			
			$search_log_string = "Default - All documents";
	}
	
	$log->lwrite('Document List Search "' . $search_log_string . '" performed');
	
	return array ($items, $banner_text, $footer_text, 
			      $replacement_term, $fields_and_search_terms);
}


/**
 * searchDocMultiSelect()
 * 
 * This local function is called from the case construct in getListOfDocs() 
 * if the user has selected the search option to search for different search
 * terms in different database fields. The function doesn't take any parameters
 * as all information necessary is taken from the HTTP POST variables. It
 * gets the search terms, additional search options such as "NOT like" and
 * "EMPTY" conditions per field and global search options such as "limit
 * to decade/category" + AND/OR combination of fields from HTTP POST variables,
 * triggers the database search and returns the resulting database records
 * ($items) to the calling function.
 *
 * @return array (array result of the database search, array of field and
 *                search term info)
 */

function searchDocMultiSelect($case_sensitive_search){
	
	$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
	
	$log = new Logging();
	
	$search_parameters = array();
	$i = 0;
	$i_selected = 0;
	
	$fields_and_search_terms = array();
	$print_name_cur_field = "";

	// Loop over all search fields
	do {	
		// Read and sanitize the next multi field from the HTTP POST
		$search_field_name = trim(filter_input(INPUT_POST, 'db_multi_field_' .
				                  $i, FILTER_SANITIZE_STRING));
	
		// Also get the text to search in this field from the HTTP
		// POST parameters.
		$in_str = trim(filter_input(INPUT_POST, 'db_field_string_' . $i));
	
		$in_str = htmlentities($in_str);
		$in_str = fixFrenchTextHtmlLinks($in_str);
		$in_str = convertToSafeUtf8Html($in_str);
	
		//  Check if the "NOT like" condition is present for the current
		//  field
		$not_like = false;
		if (isset($_POST['not_like_' . $i])) {
			$not_like = true;
		}
	
		//  Check if the "EMPTY" condition is present for the current
		//  field
		$empty = false;
		if (isset($_POST['empty_' . $i])) {
			$empty = true;
		}
	
		// The current field shall be used for a search if either:
		//
		// * The field name and search term are present
		//   or
		// * The field name is present and a search for empty/not empty is done
		//	
		if ((!empty($search_field_name) && !empty($in_str)) ||
			(!empty($search_field_name) && $empty == true)) {
			// Put search field and search text into an array.
			// Note: an array in an array is used instead of a more
			// simple associative array as the same search field can
			// occur several times to search for different terms in
			// the same database field!
			$search_parameters[$i_selected] = array($search_field_name,
					                                $in_str,
					                                $not_like,
					                                $empty);
			 		 
			$log->lwrite('Search field: "' . $search_parameters[$i_selected][MULTI_FIELD_NAME] .
			'" search term: "' . $search_parameters[$i_selected][MULTI_SEARCH_TERM] .
			'", NOT Like: ' . ($search_parameters[$i_selected][MULTI_NOT_LIKE] ? 'true' : 'false') .
			', EMPTY: ' . ($search_parameters[$i_selected][MULTI_EMPTY] ? 'true' : 'false'));
	
			// Remember the database field and the search term if a 
			// 'normal' search was done in this field without 'empty'
			// and/or 'not like'. This is then used to show the database
			// fields in the result list, e.g. in bs_record_list_view.php
			// Note: This array is NOT used for the SQL search!!!
			if (($not_like == false) and ($empty == false))
			{
				
				// based on the database field name we need to find
				// the print name of that field
				foreach ($local_doc_db_description as $field_print_name=>$field_options):
				
					if ($field_options[DB_FIELD_NAME] == $search_field_name) {
						$print_name_cur_field = $field_print_name;
						break;
					}
				
				endforeach;
				
				// Now write all info to the fields_and_search_terms array
				array_push($fields_and_search_terms, array($search_field_name,
						$print_name_cur_field,
						$in_str));			
			}
			
			$i_selected++;
		}
	
		$i++;
	
	} while (!empty($search_field_name));
			
	// Check if the user wants and 'AND' or 'OR' combine
	$boolean_operation = 'and';
	if (isset($_POST['bool-combine'])) {
		if (strcmp($_POST['bool-combine'], 'or') == 0) {
			$boolean_operation = 'or';
		}
	}
	$log->lwrite($boolean_operation . ' combination selected');
	
	// Limit the search to one decade or category if included in
	// HTTP POST
	getAndSetSearchLimit($log, $search_limit_start, $search_limit_end);
		
	$items = DocRecord::getRecordsMultiFieldSearch ($search_parameters,
		                                            $boolean_operation,
		                                            $search_limit_start,
		                                            $search_limit_end,
			                                        $case_sensitive_search);	
	
	return array($items, $fields_and_search_terms);
}


/**
 * searchInAllFields()
 *
 * This local function contains the functionality for to implement the 
 * following to search options of the GUI: 
 *  
 *  - 'search in all fields',
 *  - 'search/replace' in all fields'
 * 
 */

function searchInAllFields ($case_sensitive_search, $doc_db_description, 
							$fields_and_search_terms) {
	$log = new Logging();
	
	// Html'ify the input variables and then check and fix
	// French punctuation
	$search_term = htmlentities(trim(filter_input(INPUT_POST,'Text')));
	$search_term = fixFrenchTextHtmlLinks($search_term);
	$search_term = convertToSafeUtf8Html($search_term);
		
	// Put the search_term between % signs for the SQL query.
	$text_to_search = '%' . $search_term . '%';
	
	$log->lwrite('Search in all fields, search_term: "' .
			$search_term . '"');
	$log->lwrite('Search in all fields, text_to_search: ' .
			$text_to_search);
		
	// Limit the search to one decade or category if included in
	// HTTP POST
	getAndSetSearchLimit($log, $search_limit_start, $search_limit_end);
	
	$items = DocRecord::getRecordsSearchAllFields($doc_db_description,
			 $text_to_search, $search_limit_start, $search_limit_end,
			 $case_sensitive_search);
		
	/*
	 * Assemble the array that the calling function can use to display
	 * all database fields in which the search term could be present.
	 * As this is 'search in all fields', the array contains ALL
	 * database fields as potential fields with a match.	The calling
	 * function then needs to do its own search in these database
	 * fields (and mark the serach terms in blue).
	*/
	foreach ($doc_db_description as $field_print_name=>$field_options):
		
		array_push($fields_and_search_terms, array($field_options[DB_FIELD_NAME],
				   $field_print_name,
				   $search_term));
	endforeach;

	/* 
	 * Get the replacement term and return it. The string can then be used
	 * by calling functions to show how the search string would be modified
	 * if there is a termin the HTTP POST varialbe or would actually be
	 * replaced if the user has commited the change. Nothing is done with
	 * the variable here.
	 */	
	$replacement_term = htmlentities(trim(filter_input(INPUT_POST, 'Replacement_Text')));
	$replacement_term = fixFrenchTextHtmlLinks($replacement_term);
	$replacement_term = convertToSafeUtf8Html($replacement_term);
	
	$search_log_string = "Search in all fields for: " . $text_to_search;	
	$banner_text = 'All ' . DatabaseConfigStorage::getCfgParam('doc-name-ui-plural') . ' with text in any field';
	
	return array($items, $fields_and_search_terms, $replacement_term, 
			     $search_log_string, $banner_text);
}
