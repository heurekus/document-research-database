<?php
/**
* create-or-modify-record.php
*
* This file contains a number of functions for creating or modifying 
* database records that have been changed by the user in the web browser.
* The two main functions
* 
*   createOrModifiyRecord()
*   modifySingleFieldInRecord()
* 
* are called when the user presses the submit button.
*
* @version    1.0 2018-09-22
* @package    DRDB
* @copyright  Copyright (c) 2014-18 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
*/

/**
 * modifySingleFieldInRecord()
 *
 * This function updates a single field in a document
 *
 * @param none, all input comes from from PHP HTTP variables
 *
 * @return List of strings that indicate the result of the action. The first
 *         string of the list can be analyzed by the calling function to
 *         check if the operation was successful.
 */

function modifySingleFieldInRecord() {

	$results = '';

	$log = new Logging();
	$log->lwrite('Editing single field of a database entry');

	if (checkMaintInputParametersAndLockStatus($results) == false) {
		$log->lwrite('ERROR: Input param error, aborting db record modification!');
		return $results;
	}

	$db_field_name =  $_POST['db_field_name'];
	$db_field_content = filter_input(INPUT_POST, $db_field_name);
	$db_field_content = fixFrenchTextHtmlLinks($db_field_content);
	$db_field_content = convertToSafeUtf8Html($db_field_content);

	// Only the document ID goes into the array
	$item  = array ('id' => (int) $_POST['id']);

	// Set up a Contact object based on the HTTP POST and use it to
	// either edit an existing database record.
	$doc = new DocRecord($item);
	if ($doc->getId()) {

		$results = $doc->editSingleFieldInRecord($db_field_name,
												 $db_field_content,
												 true);

		$log->lwrite('document ' . $doc->getId() . ' single field "' .
				     $db_field_name . '" edited');
		$log->lwrite('Result: "' . $results[1] . '"');

		// Existing database record change done, now unlock the
		// database record again so other users can change it again.
		unlockDatabaseRecord((int) $_POST['id']);

		// Also write the updated field content into the modification
		// history table
		ModHistoryStorage::saveFieldContent($doc->getId(), $db_field_name,
											$db_field_content);
				 
	}  // End of database record change

	return $results;
}

/**
 * createOrModifiyRecord()
 *
 * This function updates an existing document if an ID is given in the POST
 * command or creates a new document if the parameter is missing or if it is 0.
 * With the optional paramter 'after_id' a new document can be created after
 * a particular other document.
 *
 * @param none, all input comes from from PHP HTTP variables
 *
 * @return array of strings that indicate the result of the action. The first
 *         string of the list can be analyzed by the calling function to
 *         check if the operation was successful.
 *
 */

function createOrModifiyRecord() {

	$results = '';
	$id_for_new_record = 0;

	$log = new Logging();
	$log->lwrite('Editing the FULL database entry');

	if (checkMaintInputParametersAndLockStatus($results) == false) {
		$log->lwrite('ERROR: Input param error, aborting db record modification!');
		return $results;
	}

	/*
	 * Retrieves the content of all input fields. The ID is not done in the
	 * loop as it is not part of the database structure.
	 */

	$item = array();
	$item['id'] = (int) $_POST['id'];

	$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

	foreach ($local_doc_db_description as $field_print_name=>$field) :
		$item[$field[DB_FIELD_NAME]] = filter_input(INPUT_POST, $field[DB_FIELD_NAME]);
	endforeach;

	// Fix French punctuation stuff
	foreach ($item as $field_name => &$field_text):
		$field_text = fixFrenchTextHtmlLinks($field_text);
		$field_text = convertToSafeUtf8Html($field_text);
	endforeach;
	unset($field_text);

	// Set up a database object and use it to either
	// edit an existing database record or to create a new one.
	$doc = new DocRecord($item);
	if ($doc->getId()) {

		$results = $doc->editRecord();

		$log->lwrite('Document ' . $doc->getId() . ' edited');
		$log->lwrite('Result: "' . $results[1] . '"');

		// Existing database record change done, now unlock the
		// database record again so other users can change it again.
		unlockDatabaseRecord((int) $_POST['id']);
	  
		// Populate new record id variable with the id that already
		// exists to have a starting point for the renumbering check
		// below. Since the id is still the same renumbering is not
		// necessary but let's do it anyway because things might have
		// been renumbered in other places.
		$id_for_new_record = $doc->getId();
	  
	} else {
		// A new record is to be created

		// Check if the record is to be created at the end of a category
		// or after a given other record

		$add_in_category = 0;
		if (isset($_POST['start_id'])) {
			$add_in_category = intval(trim(filter_input(INPUT_POST,'start_id',
			  		                  FILTER_SANITIZE_STRING)));
		}

		if ($add_in_category > 1000) {
			// The record is to be added at the end of a category
			$id_for_new_record = calculateNewRecordIdAtEndOfCategory();

			$log->lwrite('Adding record at end of category with new id: ' .
					     $id_for_new_record);
		} else {
			
			// The record is to be created after a specific other record
			$id_for_new_record = calculateNewRecordInsertLocation();
				
			$log->lwrite('Adding record  after another record, new id: ' .
			             $id_for_new_record);
		}

		$item['id'] = $id_for_new_record;
		$_GET['id'] = $id_for_new_record;

		$results = $doc->addRecord($id_for_new_record);
		$log->lwrite('New document added with id ' . $id_for_new_record);

		// New record written. Even though the record is new and has
		// has not been locked, call the unlock function anyway so the
		// write time is noted in the database as well.
		unlockDatabaseRecord($id_for_new_record);
		
	}

	// If the change was successfully written into the database,
	// add all fields of this record to the diff database table as all
	// of them could have potentially changed.
	if ($results[0] == 1) {

		$log->lwrite("Writing all fields of the current record to the diff db.");
	
		// If this is a new database record we have to find out which ID
		// was given to it
		if ($item['id'] == 0) {
			// getRecord with id=0 gets the last record added
			$temp_record = DocRecord::getRecord(0);
			$item['id'] = $temp_record->getId();
			$log->lwrite('New document, id: ' . $item['id']);
		}
	
		// Now write each field to the diff table
		foreach ($item as $field_name => $content):
			if ($field_name != "id") {
				ModHistoryStorage::saveFieldContent($item['id'], $field_name,
				$content);
			}
		endforeach;
	}

	// Check if renumbering the record ids is required. This could be
	// necessary if after inserting a ballet there is no space before or after
	// it to the next/previous one.
	$renumber_result = checkIdDistanceAndRenumber($id_for_new_record);
	if ($renumber_result[0] == RENUMBER_TEMP_FAIL) {
		$log->lwrite('Not all IDs renumbered due to locking, ' .
		'queueing request for later');
		$_SESSION['continue_renumber_id'] = true;
	}

	// Renumbering could have changed the record's ID, continue processing
	// with the new number.
	$_GET['id'] = $renumber_result[1];

	return $results;
}


/**
 * moveRecordAfterID
 *  
 * Move/Insert-After Functionality: Move a record in the database
 * after a given other record by assigning it a new ID.
 * 
 * Note: It's location in the database remains the same but since the ID 
 * changes, the record is put into a different location in search and list 
 * output.
 * 
 * @param int     the ID of the record to move
 * @param int     ID of record after which to insert the string
 *
 * @return array (true/false, string with reason)
 *
 */

function moveRecordAfterID($b_id_move, $b_id_insert_after) {

	$log = new Logging();

	// Run web security checks
	if (checkAntiCsrfProtection() == false) {
		$err_msg = "There was a security error, unable to move record!";
		$log->lwrite($err_msg);
		return array(false, $err_msg);
	}

	// Get ID of next document
	$next_id = 0;
	$next_item = DocRecord::getNextDoc($b_id_insert_after);
	if (!$next_item){
		// The last database id was selected, there is no next document. Thus,
		// set the id of the next document to the increment defined in config.php
		$next_id = $b_id_insert_after + 2 * DatabaseConfigStorage::getCfgParam('default_increment_new_record');
		$log->lwrite('No next document, using configured value for new id');
	}
	else {
		$next_id = $next_item->getId();
	}

	$log->lwrite('Performing "Move/Insert-After" operation');
	$log->lwrite('Document ID: ' . $b_id_move . ' insert after: ' .
			$b_id_insert_after);
	$log->lwrite('Document ID after the insert document id: ' . $next_id);

	// Sanity check
	if ($b_id_move == $b_id_insert_after) {
		$err_str = ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . ' can\'t be inserted ' .
				'after itself, aborting!';
		$log->lwrite($err_str);
		return array(false, $err_str);
	}

	// Calculate the increment and the new_id for the document that is in
	// between the 'insert after' and the 'next_id'
	$increment = (int) (($next_id - $b_id_insert_after) / 2);
	if ($increment > DatabaseConfigStorage::getCfgParam('default_increment_new_record')) {

		$log->lwrite('New record added after last record in category, ' .
				'setting increment to 100');
		$increment = DatabaseConfigStorage::getCfgParam('default_increment_new_record');
	}

	$new_id = $b_id_insert_after + $increment;

	$log->lwrite('Increment: ' . $increment . ', new ID for database record: ' . $new_id);

	// Sanity check
	if (($new_id == $next_id) || ($new_id == $b_id_insert_after)){
		$err_str = 'Can\'t insert ' . DatabaseConfigStorage::getCfgParam('doc-name-ui') . ' after id ' .
				$b_id_insert_after . '. Reason: There might be no ID ' .
				'available between the two records and one record is ' .
				'currently locked for editing!';
		$log->lwrite($err_str);
		return array(false, $err_str);
	}

	return moveRecord($b_id_move, $new_id);
}

/**
 * moveRecord
 *
 * 
 * @param int, the ID of the record to move
 * @param int, the new ID of the record
 *
 * @return array (true/false, string with reason)
 *
 */

function moveRecord($b_id_move, $new_id) {

	$log = new Logging();

	// Now modify the database record
	$item = DocRecord::getRecord($b_id_move);
	if (!$item) {
		$err_str = "Unable to get record from the db, could not move the record";
		$log->lwrite('ERROR: ' . $err_str);
		return array(false, $err_str);
	}

	$lock_result = lockDatabaseRecord($item);
	if ($lock_result[0] != true) {
		$err_str = "Unable to lock the record. " . $lock_result[1];
		$log->lwrite($err_str);
		return array(false, $err_str);
	}

	$move_result = DocRecord::changeRecordId($b_id_move, $new_id);

	unlockDatabaseRecord($new_id);

	$log->lwrite($move_result[1]);
	if ($move_result[0] == false) {
		$err_str = 'Unable to move the record, reason: ' . $move_result[1];
		$log->lwrite('ERROR: ' . $err_str);
		return array(false, $err_str);
	}

	// Save old ID to modifcation history table
	$result = ModHistoryStorage::saveFieldContent($b_id_move, 'id', $b_id_move);
	if ($result == false) {
		$err_str = 'Unable to save the ID to the mod history table';
		// Only log error but continue anyway, perhaps it is at least possible
		// to change the modification history.
		$log->lwrite('ERROR: ' . $err_str);
	}

	// Now modify the history entries as well
	$diff_move_result = ModHistoryStorage::changeIdInRecords($b_id_move, $new_id);
	$log->lwrite('bs_record_list_view.php() - ' . $diff_move_result[1]);

	// Check if renumbering the record ids is required. This could be
	// necessary if after inserting a ballet there is no space before or after
	// it to the next/previous one.
	$renumber_result = checkIdDistanceAndRenumber();
	if ($renumber_result[0] == RENUMBER_TEMP_FAIL) {
		$log->lwrite('Not all IDs renumbered due to locking, ' .
				'queueing request for later');
		$_SESSION['continue_renumber_id'] = true;
	}

	return array(true, "Database record successfully moved/inserted!");
}



/* 
 * 
 * Private functions that are only used in this module. Note: Perhaps create
 * a class out of this module and hide these functions as private...
 * 
 */


/**
* calculateNewRecordInsertLocation()
*
* This function determines the id number for a new database record.
* This is done by evaluating the HTTP GET parameters 'id' and
* 'after_id'
*
* @param none, all input comes from from PHP HTTP variables
*
* @return int, id for new document record
*/

function calculateNewRecordInsertLocation () {

	$log = new Logging();
	$log->lwrite('Determining ID for new database record');
	
	// calculate id that would be used if inserted at the end of the DB
	$last_record = DocRecord::getRecord(0);
	
	if (!$last_record) {
		$log->lwrite('ERROR: Unable to get id of last document in db!');

		// Calculate and use the first id for a standard database record
		// as defined in id-structure.php.
		$insert_at_end_id = getFirstDataBaseId($log) +
		                    DatabaseConfigStorage::getCfgParam('default_increment_new_record');
		$log->lwrite('No record in the database yet, creating new one with id: ' . 
		             $insert_at_end_id);
		
	} else {
		$insert_at_end_id = $last_record->getId() +
		                    DatabaseConfigStorage::getCfgParam('default_increment_new_record');
		
		// If the database only contains the conventions record with id=-10
		// (e.g. because all previously created records were deleted)
		// we need to get the first real id to be used for a record.
		if ($insert_at_end_id < 1000) {
		    
		    $insert_at_end_id = getFirstDataBaseId($log) +
		    DatabaseConfigStorage::getCfgParam('default_increment_new_record');
		    $log->lwrite('Only conventions record present in db, using first id: ' .
		                 $insert_at_end_id);
		}
	}
		
	// Get the optional id after which document id to insert the new one
	$after_id = 0;
	if (isset($_GET['after_id'])) {
		$after_id = (int) $_GET['after_id'];
	}
	
	// If no insert after id is given, append new record at the end of the db.
	if ($after_id == 0){
		$log->lwrite('New document to be added at the end!');
	    return $insert_at_end_id;
	}
	
	$before_record = DocRecord::getNextDoc($after_id);
	
	if (!$before_record) {
		$log->lwrite('before_record does not exist, new document to be added at the end!');
		return $insert_at_end_id;
	}
	
	$before_id = $before_record->getId();
	
	if ($before_id <= $after_id) {
		$log->lwrite('ERROR: before_id is the same or lower as after id, new document to be added at the end!');
		return $insert_at_end_id;
	}
	
	$increment = (int) (($before_id - $after_id) / 2);
	
	// If the new record is to be inserted after the last record of a category
	// the increment will be very large. In this case, add
	// 'default_increment_new_record' so the new record stays in the
	// category!
	if ($increment > DatabaseConfigStorage::getCfgParam('default_increment_new_record')) {
		$log->lwrite('New record added after last record in category, setting increment to 100');
		$increment = DatabaseConfigStorage::getCfgParam('default_increment_new_record');
	}
	
	$id_for_new_record = (int) ($after_id + $increment);
	
	if ($id_for_new_record <= $after_id) {
		$log->lwrite('No space between the db records, new record needs to go at the end!');
		return $insert_at_end_id;
	}
	
	$log->lwrite('New document to be inserted with id: ' . $id_for_new_record);
	return $id_for_new_record;

}

/**
 * calculateNewRecordIdAtEndOfCategory()
 *
 * This function determines the id number for a new database record that is
 * to be added at the end of a specific category (see id-structure.php for
 * details). This is done by evaluating the HTTP POST parameter 'start_id'.
*
* @param none, all input comes from from PHP HTTP variables
*
* @return id for new document record
*/
function calculateNewRecordIdAtEndOfCategory() {

	$search_limit_start = 0;
	$search_limit_end = 0;
	
	$selected_id = 0;
	
	$log = new Logging();
	$log->lwrite('Determine last record id of the selected category');
	
	// Get the start end end ids of this category
	getAndSetSearchLimit($log, $search_limit_start, $search_limit_end);
	
	$log->lwrite('Category limits are: ' . $search_limit_start . ' and ' .
				     $search_limit_end);
	
	// Now get all database records in this category
	$items = DocRecord::getRecordsBetweenIds($search_limit_start,
	                                         $search_limit_end);
	
	$num_records = count($items);
	
	$log->lwrite('Number of records found in the category: ' . $num_records);
	
	if ($num_records == 0) {
		$selected_id = $search_limit_start + DatabaseConfigStorage::getCfgParam('default_increment_new_record');
		$log->lwrite('No records in the category so far, new id: '. $selected_id);
	} else {
		$selected_id = $items[$num_records-1]->getId();
		$selected_id += DatabaseConfigStorage::getCfgParam('default_increment_new_record');
		$log->lwrite('Creating new record in category with id: '. $selected_id);
	}
	
	return $selected_id;
}


/**
 * checkMaintInputParametersAndLockStatus()
 *
 * This function checks the HTTP generic input parameters that have to be
 * present when the user wants to change one or all fields of a database record
 *
 * @param string array, call by reference, usually given empty, result
 *        strings are put into it.
 *
 * @return true on success, false if a problem was found
 *
 */

function checkMaintInputParametersAndLockStatus (&$results) {

	$log = new Logging();

	// Check input parameters
	if (isset($_POST['save']) == false OR $_POST['save'] != 'Save') {

		$results = array('','Sorry, go back and try again. ' .
						 'save POST variable not present or invalid.');
		$log->lwrite('ERROR: Invalid POST parameter (save), '. 
				     'record not modified!');
			
		return false;	
	}
		
	// Check the token that was set by the edit page to prevent
	// Cross Site Request Forgery (CSRF)
	if (checkAntiCsrfProtection() == false) {
		$results = array('','Sorry, go back and try again. ' . 					        
			                'There was a security issue.');
		$log->lwrite('ERROR: Invalid token, page processig aborted!');

		return false;			
	} 
		
	// Check if the record is still locked for the user and abort
	// if it is not.
	$lock_result = isRecordStillLockedForThisUser((int) $_POST['id']);
	if ($lock_result[0] == false) {
		$results = array('db_update_error', $lock_result[1], '');
		return false;
	}

	$log->lwrite ('Maint. input parameters and db record lock status o.k.');
	return true;
}


