<?php
/**
 * print-functions.php
 * 
 * Helper functions for database content quick print / export
 *
 * @version    2.0 2019-01-26
 * @package    DRDB
 * @copyright  Copyright (c) 2014-2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */
require_once 'includes/field-empty-and-trim-functions.php';
require_once 'x-3rd-party-includes/libreoffice-export/lo-writer-export.php';

// Array that hold different kinds of HTML break constellations in 
// database fields
$HtmlBreakTypes = array(
		"<br />",
		"<br /><br />",
		"<br><br>"
);


/**
 *
 * singleDocLibreOfficeOutput()
 *
 * Exports the content of a document database entry into XML format and stores
 * the result in the first variable given to the function
 *
 * @param $lo_export object of LO_Writer_Export
 * 
 * @param $item object DRDB database item
 * 
 * @param $doc_db_description object Document database description 
 *        (array with record field names)
 * 
 * @param $list_view_fields boolean, false to use the fields of an entry 
 *        defined for DETAIL view. Not given or true: Use fields selected to 
 *        be shown for LIST view.
 * 
 * @param $page_break boolean True to insert a page break before outputing 
 *        the fields of this document.
 * 
 * @param $quick_print boolean True for quick print with standard style,
 *        false to use a separate style for each field.
 *
 * @return null
 */

function singleDocLibreOfficeOutput (&$lo_export,
		                             $item, 
		                             $doc_db_description, 
		                             $list_view_fields,
		                             $page_break,
		                             $quick_print) {

	$log = new Logging();
	
	$log_buf = "";
		
	// Check if session variable that decleares if an empty field is 
	// to be printed or not exist. If not, create it.
	if (!isset($_SESSION['blt_user_hide_empty'])) {
		$_SESSION['blt_user_hide_empty'] = false;
	}
	
	// Start counting styles aa and ah from 2 (increased at the start of
	// each loop). This way aa-0 and ah-0 are (un-used) master-styles and
	// can be changed in Libreoffice to inherit global attributes to all
	// following styles without impacting the output for a particular
	// database field.
	$i = 1;

	
	// Decide to use 'list' or 'details' database structure to select
	// which fields of a database record are to be shown.	
	$fieldArrayWithOptions = selectFieldsToPrint($list_view_fields);
	if ($fieldArrayWithOptions === false) {
		$log->lwrite('ERROR: Unable to load db_field_order_for_list_output!');
		return;
	}
	

	// For each field of the database record
	foreach ($fieldArrayWithOptions as $field_print_name=>$field_options) {
	
		$i++;

		// Don't show the field if it has been configured by the user as
		// invisible
		if ($field_options[SHOW_FIELD] == 0){
		    continue;
		}		
		
		$para_format_name = 'aa-' . $i;
		$para_header_name = 'ah-' . $i;
		if ($quick_print) {
			$para_format_name = "Standard-Indent";
			$para_header_name = "Standard";
		}
	
		// Get the current field content with the database field name
		// stored in $field_options[DB_FIELD_NAME]
		$output_string = $item->getField($field_options[DB_FIELD_NAME]);
	
		// Clean the database field string for printout. Not all formatting
		// that looks good in web view looks good in print/export
		$output_string = prepareStringForPrint($output_string);
		
		// Check if current field is empty or not and only print it if:
		//  - It is the first database field
		//  - If the user wants to see empty fields
		if ((isFieldEmpty($output_string) == true) &&
			$field_options[DB_FIELD_NAME] != getDbFirstFieldName()) {
	
			if ($_SESSION['blt_user_hide_empty'] == true) {
				continue;
			}
		}

		
		// Clean the end of the string
		$output_string = removeEmptyEndLines($output_string);
		
		// If the field is empty, replace with dashes
		if (isFieldEmpty($output_string)) {
			
			$output_string = '<p>-----</p>';
		}
					
		// If field content is NOT to be printed inline
		if ($field_options[PRINT_INLINE] == 0) {

			//$field_print_name = '<p><strong>' . $field_print_name . '&nbsp;: </strong></p>';
			$field_print_name = '<p><strong>' . $field_print_name . ' : </strong></p>';
			
			$log_buf .= $lo_export->Convert_Html_To_LO_XML($field_print_name, 
					                                       $para_header_name);
				
			// Print the field content.			
			if (strlen($output_string) == 0) $output_string = "<p></p>";
			$log_buf .= $lo_export->Convert_Html_To_LO_XML($output_string,
						                                   $para_format_name,
			                                               $page_break);					
				
			// Page break only before the first field
			$page_break = false;
				
		} else {

			// For standard output:
			// Print field content right after the field title (in-line)
			// Use the AB paragraph style (instead of AH header paragraph style)
			// to prevent sticky consecutive AHs breaking to the next page.
			// Use the AB paragraph style (instead of AA) so they are not
			// idented.
			//
			// For quick print use 'Standard' instead of 'Standard-Indent'
			
			if ($quick_print) {
				$para_format_name = "Standard";
			}
			
			//$field_print_name = '<p><br><strong>' . $field_print_name . '&nbsp;: </strong>';
			$field_print_name = '<p><strong>' . $field_print_name . ' : </strong>';
			
			// Remove HTML paragraph tags at the beginning and end
			// of the field content so name and content are on the
			// same output line.
			$output_string = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1',
					$output_string);
						
			// Now put the field name and the field content together in one 
			// string and terminate with an end paragraph html tag.
			$output_string = $field_print_name . $output_string . '</p>';
			
			// Finally, convert to XML
			$log_buf .= $lo_export->Convert_Html_To_LO_XML($output_string,
					                                       $para_format_name, 
					                                       $page_break);
			
			// Page break only before the first field
			$page_break = false;
				
		}

	} //endforeach

	// If there was an error during writing this record into a 
	// Libreoffice file, print some debug information to the log file.
	if ($log_buf){
		$log->lwrite('Libreoffice Output ERROR in record with id: ' . $item->getId());
		$log->lwrite(PHP_EOL . $log_buf);
	}
	
	return;
}

/**
 *
 * singleDocLibreOfficeOutputTableRow()
 * 
 * Outputs the content of a document database entry in HTML table format
 *
 * @param $item object the database record
 * @param $doc_db_description array with record field names
 * @param $log_buf string of log messages saved so far for adding. Called
 *        by reference
 * @param $lo_export object for exporting HTML to Calc
 * 
 */

function singleDocLibreOfficeOutputTableRow ($item, 
                                             $doc_db_description,  
                                             &$log_buf, &$lo_export) {
	$log = new Logging();	
	
	// Decide to use 'list' or 'details' database structure to select
	// which fields of a database record are to be shown.	
	$fieldArrayWithOptions = selectFieldsToPrint(true);
	if ($fieldArrayWithOptions === false) {
		$log->lwrite('ERROR: Unable to load db_field_order_for_list_output!');
		return;
	}	
	
	// For each field of the database record
	foreach ($fieldArrayWithOptions as $field_print_name=>$field_options):
	
    	// Don't show the field if it has been configured by the user as
    	// invisible
    	if ($field_options[SHOW_FIELD] == 0){
    	    continue;
    	}
    	
		// Get the current field content with the database field name
		// stored in $field_options[DB_FIELD_NAME]
		$output_string = $item->getField($field_options[DB_FIELD_NAME]);	
		
		// Clean the database field string for printout. Not all formatting
		// that looks good in web view looks good in print/export
		$output_string = prepareStringForPrint($output_string);

		
		$output_string = removeEmptyEndLines($output_string);
				
		if (isFieldEmpty($output_string)) {
			$output_string = "<p></p>";
		}
		
		$log_buf .= $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($output_string);
		
					
	endforeach;

	// If there was an error during writing this record into a
	// Libreoffice file, print some debug information to the log file.
	if ($log_buf){
	    $log->lwrite('Libreoffice Output ERROR in record with id: ' . $item->getId());
	    $log->lwrite(PHP_EOL . $log_buf);
	}
}

/**
 *
 * selectFieldsToPrint ()
 *
 * The function returns a structure that contains the information which fields
 * are to be printed. Which structure is returned depends on the input
 * parameter which can be true for list view fields or false for details
 * view fields. If there was a problem getting the struct a boolean false
 * is returned.
 *
 * @param boolean, true if list view field selection is to be used or 
 *        false if detail view field selection is to be used
 * @return mixed, false if there was an error or the struct that contains
 *         information on which fields are to be shown
 */

function selectFieldsToPrint($list_view_fields) {

	$log = new Logging();

	if ($list_view_fields == false) {
		// If the current user (session) has an adapted database record field
		// sort order load it. Otherwise use the default from database-structure.php
		if (isset($_SESSION['db_field_order'])){
			$fieldArrayWithOptions = $_SESSION['db_field_order'];
			//$log->lwrite('Using customized field display order');
		}
		else {
		    $fieldArrayWithOptions = DatabaseConfigStorage::getDbFieldConfig();;
		}
	} else {
		// Use database record fields selected in the list view structure

		// If the current user (session) has an adapted database record field
		// sort order load it. Otherwise use the default from database-structure.php
		if (isset($_SESSION['db_field_order_for_list_output'])){
			$fieldArrayWithOptions = $_SESSION['db_field_order_for_list_output'];
			//$log->lwrite('Using user specific field order');
		}
		else {
			$log->lwrite('ERROR: Unable to load db_field_order_for_list_output!');
			return false;
		}
	}
	
	SelectAllFieldsIfNoFieldsSelected($fieldArrayWithOptions);

	return $fieldArrayWithOptions;
}

/**
 *
 * SelectAllFieldsIfNoFieldsSelected()
 *
 * This function checks if at least one field in the given array is set
 * for output. If not it sets them all.
 *
 * @param array, the array with options for each field, given by reference
 * @return N/A
 */

function SelectAllFieldsIfNoFieldsSelected(&$fieldArrayWithOptions) {
    
    foreach ($fieldArrayWithOptions as $field_print_name=>$field_options){
        
        // Exit function without modifying the Options array if at least
        // one field is marked for output.
        if ($field_options[SHOW_FIELD] != 0){
            return;
        }
    }
    
    // No field is selected, let's mark all
    foreach ($fieldArrayWithOptions as $field_print_name=>&$field_options){
        
        // Exit function without modifying the Options Array if at least
        // one field is marked for output
        $field_options[SHOW_FIELD] = 1;
    }
    // the following unset() is required to update the reference of the
    // last array element. This is not a bug, it's a feature.
    //--> see "warning" in:
    // http://php.net/manual/en/control-structures.foreach.php
    unset($field_options);   

    return;
    
}

/**
 *
 * prepareStringForPrint()
 *
 * This function makes the string given to it read for printing. 
 * 
 * It's current function is does two things:
 * 
 *  - Correct carriage return and line feeds
 *  - Replaces different kind of HTML line breaks with HTML paragraph. 
 * 
 * Break background: While HTML line breaks are displayed as CR+LF in web view 
 * it gets in the way of the 'justified' formatting in the LibreOffice output
 * as the last line of a string then extends to the end of the line. This is
 * fixed by replacing HTML breaks with pagragraphs.
 *
 * @param string, HTML formatted
 * 
 * @return string, HTML formatted, ready for printing.
 *
 */

function prepareStringForPrint($HtmlString) {
	
	
	// Replace CR + LF with a HTML <BR>
	$HtmlString = preg_replace("/\r\n?/", "\n", $HtmlString);
	$HtmlString = str_replace("\n", '<BR>', $HtmlString);
		
	global $HtmlBreakTypes;
	
    // Replace all unusual BR instances in the input string. Note: str_replace()
    // takes ALL different BR versions as a string array in the third parameter
    // and applies all of them!
	$HtmlString = str_replace ($HtmlBreakTypes, "</p><p>", $HtmlString);
	
	return ($HtmlString);
}

/**
 * CheckIfDocumentNotFromThisYear()
 *
 * Checks if the current document given in $item is from the year
 * given in $limit_to_year. This function also includes documents in the
 * given year if the first field is empty but a previous document
 * was part of the year. This is done with the $found_first_doc_of_year
 * which is called by reference and is stored in the calling function.
 * 
 * @param $item object containing the current odcument
 * @param $limit_to_year
 * @param $found_first_doc_of_year boolean returns true or false depending 
 *        whether this is the first document of the year.
 *        
 * @return boolean true/false depending on whether the document belongs to
 *         the current year
 *
 */
function IsDocumentNotFromThisYear($item,
                                   $limit_to_year,
                                   &$found_first_doc_of_year) {
        
        //Get the content of the first db field and remove all HTML paragraph tags
        $content = $item->getField(getDbFirstFieldName());
        $content = str_ireplace('<p>', '', $content);
        $content = str_ireplace('</p>', '', $content);
        $content = strip_tags($content);
        
        //Is the current document of this year?
        if (strpos($content, (string) $limit_to_year) !== false) {
            
            // Mark that a document has been found. This is necessary to
            // later also count documents without a year in the first field
            // as belonging to the year
            $found_first_doc_of_year = true;
        }
        elseif (($content == "" || $content == "----") &&
                $found_first_doc_of_year == true) {
                
                // The first field of the record is empty but follows a previous
                // document of the year to limit the output to
                $found_first_doc_of_year = true;
        }
        else {
            
            // The document does not belong to the year, don't count
            // subsequent empty ones to this year.
            $found_first_doc_of_year = false;
            
            // Don't show this document, continue with next loop cycle
            return false;
        }
        
        return true;
}


/**
 * SendLibreofficeWriterDocOverHttp()
 *
 * @param $log object
 * @param $lo_export object Writer output
 *
 */

function SendLibreofficeWriterDocOverHttp ($log, $lo_export) {

    $src = 'templates' . DIRECTORY_SEPARATOR . 'writer-export-template.odt';
    
    // Generate and use a unique temporary filename for the output
    $dst = tempnam(sys_get_temp_dir(), 'LOX');
    $log->lwrite('Temporary export file: ' . $dst);
    
    // Now insert the Libreoffice XML content into the template document
    // and save the result to the temporary export document.
    $log_buf = $lo_export->Save_To_Writer_Document($src, $dst);
    $log->lwrite($log_buf);
    
    sendDocumentOverHTTP ($dst, 'search-result.odt', $log);
    
    // Delete the output file again
    unlink($dst);
}


/**
 * SendLibreofficeCalcDocOverHttp()
 *
 * @param $log object
 * @param $lo_export object Calc output
 *
 */

function SendLibreofficeCalcDocOverHttp ($log, $lo_export) {
    
    $src = 'templates' . DIRECTORY_SEPARATOR . 'calc-export-template.ods';
    
    // Generate and use a unique temporary filename for the output
    $dst = tempnam(sys_get_temp_dir(), 'LOX');
    $log->lwrite('Temporary export file: ' . $dst);
    
    // Now insert the Libreoffice XML content into the template document
    // and save the result to the temporary export document.
    $log_buf = $lo_export->Save_To_Calc_Document ($src, $dst);
    $log->lwrite($log_buf);
    
    sendDocumentOverHTTP ($dst, 'search-result.ods', $log);
    
    // Delete the output file again
    unlink($dst);
}

/**
 * sendDocumentOverHTTP()
 *
 * @param $dst string destination filename
 * @param $filename string path and name of the file announced to the web browser
 * @param $log object 
 *
 */

function sendDocumentOverHTTP ($dst, $filename, $log) {
    
    // This PHP module is called from index-print-raw.php that has started
    // output buffering. ob_end_clean() below discards any output that
    // has previously been made as such output would be part of the file, which
    // is unwanted and results in the file not being readable.
    ob_end_clean();
    
    // Now offer the resulting document as a file download
    if (!file_exists($dst)) {
        $log->lwrite('ERROR: Temporary export file does not exist!: ' . $dst);
        return;
    }
    
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $filename . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($dst));
    readfile($dst);
    
}

/**
 * GenerateClickableDocumentIdURL()
 *
 * @param $item object DRDB database item
 * @return string, clickable URL to the database record
 *
 */

function GenerateClickableDocumentIdURL($item) {
    
    $docId = '<p><b>' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) .
    ' Internal ID&nbsp;: ' . 
    GetHrefForHashId ($item) . 
    $item->getField('id') .
    '</a></b></p>';
    
    return $docId;
}

/**
 * GetHrefForHashId()
 *
 * Generates the <a href...> for a database record with a hash id.
 * This functionality is used in several places in the code (not only
 * in the function above)!
 * 
 * @param $item object DRDB database item
 * @return string, <a href...> part of a clickable URL with 
 * the hash id of a record.
 *
 */

function GetHrefForHashId($item) {

    /*
     * Get server port (e.g. 443) and protocol (http, https) from
     * either the request itself. If used in a Docker container,
     * environment variables can be used to override these two values.
     * This is necessary if the Docker container is used behind a
     * reverse proxy setup as in this case port 80 and http is used
     * which does not reflect what is used at the frontend web server.
     */
    
    $server_port = getenv('DRDB_SERVER_PORT');
    if ($server_port === false) $server_port = $_SERVER['SERVER_PORT'];
    
    $protocol = getenv('DRDB_SERVER_PROTOCOL');
    if ($protocol === false) $protocol = "https";
    
    /*
     *
     * Getting the URL is a bit tricky as the index-print.php of the base
     * URL has to be removed before index.php is added. This is done in the
     * substr/strrpos line by curring everything away from the REQUEST_URI
     * after the last '/' character.
     *
     */    
    
    $href = '<a href="' . $protocol . '://' . $_SERVER['SERVER_NAME'] .
    ':' . $server_port .
    substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/')) .
    '/index.php?content=bs_record_details_with_hash&hash_id=' .
    $item->getField('hash_id') . '">';
    
    return $href;
}


