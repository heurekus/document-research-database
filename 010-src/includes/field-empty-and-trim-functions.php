<?php

/**
 * field-empty-and-trim-functions.php
 *
 * This file contains all functions and variables that are used
 * in various other places of the application to determine if the HMTL
 * content in a variable should be considered as empty, i.e. there is nothing
 * visible on the page if the variable is printed.
 * 
 * This functionality is required e.g. in:
 * 
 *  - The details view
 *  - When printing database record details
 *  - In the database class to search for empty / non-empty fields
 *
 * @version    2.0 2017 08 17
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 * 
 */


// The following array holds all HTML strings that don't show anything
// on the page and are thus considered empty. It is used in the functions
// below to check for 'empty' strings and to assemble SQL queries that search
// for empty fields.
//
// Note: Apart from the string being empty (not part of the array, has to
// be checked separately), the HTML Paragraph + End Paragraph markup is the 
// default "empty" string, therefore it is the first entry in the array.
//
// Note: Functions using the array should use case-insensitive searches 
// to catch all upper/lower case combinations for each array entry.

$emptyHtmlStrg = array(
		"<p></p>",
		"<p> </p>",
		"<p>  </p>",
		'<p><span style="color:#FF0000"></span></p>',
		"<p><b></b></p>",
		"<p><em></em></p>",
		'<P><span style="color:#66CC"></span></P>',
		'<P><span style="color:#FF"></span></P>',
		'<P><span style="color:#66CC;"></span></P>',
		'<P><span style="color:#FF;"></span></P>',		
		'<p><span style="color:#FF0000;"></span></p>',
        '<p>-----</p>',
		'<p><span></span></p>',
        '<p>&nbsp;</p>',
		);


/**
 * isFieldEmpty()
 *
 * Checks whether a given string is empty. As the input string contains HTML
 * code there are quite a number of different non-fully empty strings that
 * still have to be considered as empty.
 *
 * @param string
 *
 * @return boolean: true or false
 */

function isFieldEmpty ($field) {

	// If the input string is too short it is considered empty
	// thus abort the search right away and report "empty" to the calling
	// function.
	if (strlen($field) < 3) return true;
		
	// if the field content matches one of the empty HTML strings
	// defined in the array at the beginning of the file, exit the
	// function and indicate that the field is empty.
	global $emptyHtmlStrg;
	foreach ($emptyHtmlStrg as $strg):
		if (strcasecmp($field, $strg) == 0) return true;		
	endforeach;
	
	// String is not empty
	return false;
}

/**
 * assembleSqlQuerryForEmptyOrNotEmptyField()
 *
 * Used by the database class to assemble the condition part of an SQL 
 * query string to return a set of records in which a given field 
 * ($field_name) is either empty or not empty.
 *
 * NOTE: Due to the database collation option used, database queries
 * are CASE INSENSITIVE (unless searched with binary option). As a consequence
 * it is not necessary to search for upper and lower case variants!
 *
 * @param string, the field name
 * @param boolean, true to search for non-empty and false to search for empty field
 *
 * @return string: the condition part of the SQL query string
 */

function assembleSqlQuerryForEmptyOrNotEmptyField ($field_name,
		                                           $not_like_search) {

	$log = new Logging();
	$log->lwrite('Assembling Like empty / Not Like empty SQL search term');

	/* check whether to search for LIKE or NOT LIKE */
	$like_not_like = '` = \'';
	$bool_concat = 'OR ';
	$length_check = ' < 3 ';
	 
	if ($not_like_search != false){
		$like_not_like = '` <> \'';
		$bool_concat = 'AND ';
		$length_check = ' >= 3 ';
	}

	$query = '(';
	 	
	// include all empty HTML string variants in the search query
	global $emptyHtmlStrg;
	foreach ($emptyHtmlStrg as $strg):	
		$query .= '`' . $field_name . $like_not_like . $strg . '\' ';
		$query .= $bool_concat;	 
	endforeach;
	
	// A short length also qualifies as empty as in the database all fields
	// contain at least a paragraph / end paragraph HTML code.
	$query .= "LENGTH(" . $field_name . ")" . $length_check;

	// And finally, include NULL if this is an empty field search
	if (!$not_like_search) {
	   $query .= " OR " . $field_name . " IS NULL"; 
	}
	
	$query .= ') ';
	
	return $query;
}



/**
 * removeEmptyEndLines()
 *
 * Removes ALL empty end lines in the given string. All HTML sequences
 * that represent empty lines are taken from $emptyHtmlStrg in this module
 *
 * @param string from which empty lines at the end are to be removed
 *
 * @return string with the empty lines removed.
 */

function removeEmptyEndLines($str) {
    
    $log = new Logging();
    
    // Remove standard whitspaces and other chars from the end of the string
    $str = rtrim ($str);
    
    // Now remove all sorts of HTML newlines, paragraphs, etc.
    // Note: Abort after the number of iterations given below. If this number
    // is reached then there is a logic error somewhere.
    $abortCounter = 50;
    while ($abortCounter > 0) {
              
        if (rightTrimAll($str)) {

            $abortCounter--;
            
            if ($abortCounter == 0) {
                $log->lwrite('ERROR: Trim Counter Exceeded, aborting trimming');
            }
        }
        else break;
    }
    
    return $str;
}



/**
 * rightTrimAll()
 *
 * Removes all strings that represent empty lines that are contained
 * in $emptyHtmlStrg from the right side of the given string. 
 * 
 * Note: This function only performs one pass. For removal of several empty 
 * lines at the end of the string, the function has to be called until it 
 * returns false.
 *
 * @param string from which to cut empty lines
 *
 * @return bool true if an empty line was found at the end of the string or
 *              false if nothing was cut.
 */

function rightTrimAll(&$str) {
    
    $empty_line_found = false;

    // search for all empty line variants
    global $emptyHtmlStrg;
    foreach ($emptyHtmlStrg as $needle):
    if (rightTrimSingle ($str, $needle, false)) $empty_line_found = true;    
    endforeach;
    
    return $empty_line_found;
}

/**
 * rightTrimSingle()
 *
 * If the end of the input string matches needle, needle is removed from the
 * end of the string.
 * 
 * @param string  &$str, original string --> called by reference
 * @param string  $needle,  needle to trim from the end of $str
 * @param bool    perform case sensitive matching, default=true
 *
 * @return bool true if string was cut and false if not cut at the end
 * 
 */

function rightTrimSingle(&$str, $needle, $caseSensitive = true)
{

    $strPosFunction = $caseSensitive ? "strpos" : "stripos";
    
    $len_str = strlen($str);
    $len_needle = strlen($needle);
      
    // If the needle is longer than the string return straight aways
    if ($len_str - $len_needle < 0) return false;
    
    if ($strPosFunction($str, $needle, ($len_str - $len_needle)) !== false) {
        
        $str = substr($str, 0, -strlen($needle));
        return true;
    }
    
    return false;
}



?>