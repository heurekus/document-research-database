<?php
/**
 * field-functions.php
 * 
 * This file contains funtions to deal with individual fields of
 * a record.
 *
 * @version    1.0 2017-02-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

/**
 * getDbFirstFieldName()
 *
 * This function returns the name of the FIRST database field as
 * defined in the database structure in database-structure.php.
 *
 * The first field of the database structure array is special as its
 * content is used in many places, e.g. as a header for before other
 * conent is shown. As the name of the database field is flexible it
 * can't be directly addressed.
 *
 * @params
 * @return Name of first database field (NOT the print name!)
 * 
 */

function getDbFirstFieldName() {

	$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

	reset($local_doc_db_description);
	return $local_doc_db_description[key($local_doc_db_description)][0];
}

/**
 *
 * prepareFieldTextForOutput()
 *
 * Returns a modified string that contains dashes in case the input string
 * is empty and makes URLs clickable by adding HTML code in the output
 * string.
 *
 * @param string
 * @return string, modified text
 * 
 */

function prepareFieldTextForOutput($output_string) {

	// If the field is empty (but the user still wants to see it,
	// display a few dashes instead so the user can click on them to edit
	// the field.
	if (isFieldEmpty($output_string) == true) {
		$output_string = "<p>-----</p>";
	}

	// Replace CR + LF with a HTML <BR>
	$output_string = preg_replace("/\r\n?/", "\n", $output_string);
	$output_string = str_replace("\n", '<BR>', $output_string);

	// Make URLs clickable
	//
	// Note: This just changes the output in the web browser, the text
	// of the field is unchanged in the database. If the user edits the field
	// by double clicking on it, ckeditor removes the clickable URL again
	// as the "link" plug-in is not activated and hence URLs can't be
	// embedded. This is intentional as the URL should be shown as plain text
	// because the ordinary user would change the text of the URL but not
	// the URL itself which would lead to inconsistency.
	// Note: In the regular expression the ';' char was added as a valid
	// character to cover the scenario in which the & character is encoded
	// as &amp; as per HTML conventions.
	$output_string = preg_replace('!(http)(s)?:\/\/[a-zA-Z0-9\-.?:%=&_/#\+\',_();~\\\\]+!',
	    "<a target=\"_blank\" href=\"\\0\">\\0</a>",
	    $output_string);
	
	return $output_string;
}


/**
 *
 * Convert_UTF8_To_RTF_Format()
 *
 * The RTF format uses an ancient way of encoding non-ASCII characters.
 * This function converts those characters into RTF usable format.
 * RTF encodes special characters in a special Unicode format:
 *
 * '\uxxxx?' whee xxxx is the unicode number of the character
 * The backslash is the start of special character sign, the ?
 * the alternative character to show in case the program displaying
 * the RTF content is unable to show that unicode character.
 *
 * Note: UTF-8 format can have up to 5 extension bytes. This function
 * only treats up to two extension bytes.
 *
 * @param a UTF8 input string
 * 
 */

function Convert_UTF8_To_RTF_Format ($input_string)
{
	$output_string = "";

	for ($i = 0; $i < strlen($input_string); $i++){

		// If the current character indicates that there is an UTF-8 extension
		// bytei.e. the bits are set to 110xxxxx = 0xC0, compute the resulting
		// RTF special character number
		// For details on UTF-8 see http://en.wikipedia.org/wiki/Utf8
		if ((ord($input_string[$i])&0xE0) == 0xC0) {

			// The RTF special character number consists of the 5 lower bits of
			// the first byte and the 6 lower bits of the second byte. The
			// other bits have to be removed. The first step therefore consists
			// of removing the upper 3 bits of the first byte
			// (i.e. AND 31 operation) + a shift left by 6 bits. The second
			// step then removes the upper 2 bits of the second byte
			// (i.e. AND 63 operation) and then adds the result to the first
			// calulation.
			$char_num = (ord($input_string[$i]) & 31) << 6;
			$char_num += ord($input_string[$i+1]) & 63;

			// The next character can be skipped in the loop as it was part of
			// the 2 byte UTF-8 coding
			$i++;
				
			$output_string .= "\u" . $char_num . "?";
		}
		elseif ((ord($input_string[$i])&0xE0) == 0xE0){
			// The current char has two extension bytes
			// use similar bit magic as above, for details see again the
			// Wikipedia article linked above.
			$char_num = (ord($input_string[$i]) & 15) << 6;
			$char_num += ord($input_string[$i+1]) & 63;
			$char_num = $char_num << 6;
			$char_num += ord($input_string[$i+2]) & 63;

			//the next two characters can be skipped in the loop as it was
			// part of the 3 byte UTF-8 coding
			$i += 2;
				
			$output_string .= "\u" . $char_num . "?";
		}
		else {
			// the current char is a normal char, copy to the output string
			$output_string .= $input_string[$i];
		}
	}

	return ($output_string);
}
