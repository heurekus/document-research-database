<?php
/**
 * db-lock-functions.php
 * 
 * This file contains a number of functions that are needed in several
 * other places for locking and unlocking database records to ensure
 * only a single user can edit a database record at any one time.
 *
 * @version    1.0 2017-02-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-17 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

define ('LOCK_PERIOD', 3 * 60 * 60 ); // lock time is 3h

/**
 *
 * lockDatabaseRecord($item)
 * 
 * This function locks a database record and thus limits editing of a record
 * to a single user when called before editing starts.
 * 
 * The function checks if the record is not already locked by another user.
 * If already locked, it returns false. Otherwise it locks the record for the 
 * user and returns true.
 *
 * @param $item, the database record
 * @param $record_load_date (optional): The "single field edit" has received
 *        its data at an earlier time so a write access might have happened
 *        after the field was loaded into the browser. If this optional
 *        variable is given to the function and is not 0, this function will
 *        first check if the record is still locked. If it is not, it will
 *        take a look at the last_update_time field if the time contained in
 *        there is NOT OLDER then then $record_load_date variable given to this
 *        function.
 * 
 * @return array: true/false, error string
 * 
 */

function lockDatabaseRecord($item, $record_load_date = 0) {

	if (!$item) {
		return(array(false, 'ERROR: item empty!'));
	}
	
	$cur_lock_user = $item->getField("lock_user");
	$cur_lock_time = $item->getfield("lock_time");
	
	$log = new Logging();
	$log->lwrite("Locking database record for editing");
	$log->lwrite("Previous User: " . $cur_lock_user);
	$log->lwrite("Previous Lock Time: " . $cur_lock_time .
			     ' until ' . date("H:i:s", $cur_lock_time));
	
	// If there is still a user in the lock_user field that is NOT the 
	// current user check if we can abort or proceed
	if ($cur_lock_user != "" && $cur_lock_user != $_SERVER['PHP_AUTH_USER']) {
		
		$log->lwrite("Other user locked this record, check if still valid!");
		
		// If there is a lock time in the record and hasn't expired yet, abort!
		if ($cur_lock_time + LOCK_PERIOD > time()) {
			
			$ret_string = "Reason: The record is still locked by " . 
			              ucfirst($cur_lock_user) . 
			              ' until ' . date("H:i:s", $cur_lock_time 
					      + LOCK_PERIOD);
			
			$log->lwrite($ret_string);
			return(array(false, $ret_string));
		}
		else {
			$log->lwrite("Log has expired, we can continue");	
		}
		
	} // end of check if there is still a lock in place
	
	// If a time was given as an input to this function check
	// if the record was modified since (despite being unlocked). If so
	// reject the lock. If no time is given to this function then don't
	// do this check (update full record, record data is loaded at the time
	// the record is locked, so no need to do this!
	if ($record_load_date != 0 &&
		$record_load_date < $item->getField("last_update_time")) {
			
			$prev_write_time = $item->getField("last_update_time");
			$prev_write_user = $item->getField("last_update_user");
			
			$ret_string = "Reason: Record was modified by " . 
			              ucfirst($prev_write_user) .
			              ' at ' . date("H:i:s", 
			              $prev_write_time) . ' or its ID has changed!';
				
			$log->lwrite($ret_string);
			$log->lwrite('Record load date given: ' . $record_load_date);
			return(array(false, $ret_string));			
	}
	
	// Put the lock into the database record
	$item->editSingleFieldInRecord("lock_time", time(), false);
	$item->editSingleFieldInRecord("lock_user", $_SERVER['PHP_AUTH_USER'], 
		   false);
	
	$log->lwrite("DB entry successfully locked for this user!");
	
	return(array(true, ''));
}


/**
 *
 * isRecordLocked($id)
 *
 * This function checks if a record with the given ID is currently locked
 * by ANY user (including the current one) and if the lock time is still 
 * valid. 
 *
 * @param $id - of the database record
 * @return boolean: true if locked, false if not locked / lock time expired
 *
 */

function isRecordLocked($id) {

	$log = new Logging();
	$log->lwrite("Checking if record is locked for ANY user, id: ". (int) $id);
	
	$id = (int) $id;
	
	if ($id <= 0){
		$log->lwrite ('Invalid record ID, exiting!');
		return false;
	}
	
	$item = DocRecord::getRecord($id);
	
	if (!$item) {
		$log->lwrite('Unable to get the record, exiting!');
		return false;
	}
	
	$lock_user = $item->getField("lock_user");
	$lock_time = $item->getfield("lock_time");
	
	$log->lwrite ("Lock User: " . $lock_user);

	if (strlen($lock_user) <= 0) {
		$log->lwrite('No lock user, record is not locked.');
		return false;
	}
	
	// check if database record is locked by a user but lock time has expired
	if ($lock_time + LOCK_PERIOD <= time()) {
		
		$log->lwrite('Record was locked but time has expired, therfore not locked.');
		return false;
	}
	
	$log->lwrite('Record is locked and lock time has not exired');
	return true;
	
}


/**
 *
 * isRecordStillLockedForThisUser($id)
 *
 * Before writing back data into a record in the database, check if the
 * record is still locked for the user. If it is not locked anymore, return
 * false so the calling function can abort its write procedure.
 * 
 * Note: The function does not check if the lock time has already expired. As
 * long as the user that has locked the record is the same as the current 
 * user nobody else has since modified the record and hence one can still
 * update without overwriting somebody else's data.
 * 
 * @param $id - of the database record
 * @param $log_result - optional parameter. If true or omitted, the result is
 *                      written to the log file. If false, the result
 *                      is not written to the log file.
 * @return array: true/false, error string
 *
 */

function isRecordStillLockedForThisUser($id, $log_result = true) {
	
	$log = new Logging();
	$log->lwrite("Checking if record is still locked for id ". (int) $id);
	
	if ((int) $id == 0){
		$ret_text = 'Record id is 0, must be a new document, exiting!';
		$log->lwrite($ret_text);
		return(array(true, $ret_text));
	}
	
	$item = DocRecord::getRecord((int) $id);
	
	if (!$item) {
		$ret_text = ('Unable to get the record, exiting!');
		$log->lwrite($ret_text);
		return(array(false, $ret_text));
	}
	
	$prev_lock_user = $item->getField("lock_user");
	$cur_lock_user = $_SERVER['PHP_AUTH_USER'];
	
	$ret_text = "Lock User: " . $prev_lock_user . 
	            " - Prev User: " . $cur_lock_user;
	$log->lwrite($ret_text);
	
	if ($prev_lock_user != $cur_lock_user) {
		
		$last_update_user = $item->getField("last_update_user");
		$ret_text = 'PROBLEM: The lock has expired due to long inactivity, '. 
		            'or the page was RELOADED in the browser. ' .
		            'The record was last written by ' .
		            ucfirst($last_update_user) . '. ' .
		            'Sorry, can\'t save the modifications!'; 
		if ($log_result) $log->lwrite($ret_text);
		return(array(false, $ret_text));		
	}
	
	if ($log_result) $log->lwrite('DB record still locked for this user, OK!');
	return(array(true, ''));
}

/**
 *
 * getLastUpdateTime($id)
 *
 * Get the time the record was last modified
 *
 * @param  int - of the database record
 * @return int - unix time when the record was last modified or 0 if the 
 *         record was never modified
 *
 */

function getLastUpdateTime($id) {
	
	$log = new Logging();
	
	$id = (int) $id;
	if ($id <= 0) {		
		$log->lwrite('ERROR: Invalid ID:' . $id);
		return 0;
	}
		
	$item = DocRecord::getRecord($id);
	
	$last_update_time = $item->getField("last_update_time");
	
	$log->lwrite('Last Update Time: ' . $last_update_time);
	
	return $last_update_time;
}

/**
 *
 * unlockDatabaseRecord($id)
 *
 * This function UN-locks a database record and thus allows other users
 * to modify the database record again. It also populates the last_update*
 * fields to note when the record was last written back into the database.
 * This information is important for the 'edit single field' UI user
 * interaction as when a lock request is received via AJAX the data that
 * was initially loaded into the page (could have been hours ago) could be
 * outdated due to someone editing the record. By knowing when the record
 * was last written to it can be detected that the data the user wants to
 * modify inline is outdated!
 * 
 * The record can also be unlocked without the record having been changed. 
 * In this case the calling function has to set the optional 
 * $record_unlock_time parameter to false!

 * 
 * IMPORTANT: Call isRecordStillLockedForThisUser() before as this function
 *            DOES NOT CHECK if the record is locked to the user. It just
 *            removes the lock!
 *
 * IMPORANT: Unlike the lock function, this function requires the ID (and 
 *           NOT the full db item). This is because the write procedure
 *           does not update the item object and does thus not update the 
 *           lock/access time/user fields!
 *
 * @param $id, The ID of the database record
 * @param $record_unlock_time: true or false
 *
 */

function unlockDatabaseRecord($id, $record_unlock_time = true) {

	$item = DocRecord::getRecord($id);
	
	$log = new Logging();
	$log->lwrite("UN-locking database record for editing");
	$log->lwrite("Current User: " . $item->getField("lock_user"));
	$log->lwrite("Current Lock Time: " . $item->getfield("lock_time") .
			     ", " . date("H:i:s", $item->getfield("lock_time")));
	
	// Delete the lock user and time
	$item->editSingleFieldInRecord("lock_user", "", false);
	$item->editSingleFieldInRecord("lock_time", "0", false);
	
	// Write the user and time into the last_update_* fields when the lock 
	// was released. This is necessary when locking for a single field edit 
	// as the web page showing the data could have been open for a long time 
	// and in the meantime someone else could have modified the record. By 
	// writing the time the lock was released here, it's possible to compare
	// when the page was loaded vs. the time the record was modified.
	if ($record_unlock_time == true){
		$item->editSingleFieldInRecord("last_update_time", time(), false);
		$item->editSingleFieldInRecord("last_update_user",
				$_SERVER['PHP_AUTH_USER'], false);		
	}
	
	return(true);
}

/**
 *
 * writeAccessTimeToDbRecord($item)
 * 
 * For administrative and statistical purposes this function is called
 * when a user accesses the details in a document record. 
 * 
 * @param $item
 *
 */

function writeAccessTimeToDbRecord($item) {
	
	$log = new Logging();
	$log->lwrite("Noting to the DB which user has accessed the document details");
	
	$item->editSingleFieldInRecord("detail_view_time", time(), false);
	$item->editSingleFieldInRecord("detail_view_user", 
			                       $_SERVER['PHP_AUTH_USER'], 
			                       false);
}


