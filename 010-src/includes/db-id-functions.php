<?php

/**
 * db-id-functions.php
 *
 * This file contains functions to check and modify the id of a document
 * record which controls the order of the database entries (searches usually 
 * sort by the document id)
 *
 * @version    1.0 2018-03-29
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

define ('RENUMBER_SUCCESS', 0);
define ('RENUMBER_TEMP_FAIL', -1);
define ('RENUMBER_PERM_FAIL', -2);

define ('PERIODIC_CHECK_TIME', 60 * 10); // 10 minutes

/**
 * checkIdDistanceAndRenumber()
 *
 * Checks if there is at least one empty ID between two database records. If
 * not it pushes database records forward by renumbering their ids until this
 * is reached again.
 * 
 * If $id is given to the function it is checked during renumbering if this
 * id is impacted. If so, the function notes the new id and returns this at
 * the end of the process.
 *
 * @param none 
 *
 * @return array: int(RENUMBER_SUCCESS, RENUMBER_TEMP_FAIL, RENUMBER_PERM_FAIL)
 *                int(0 if $id not given, otherwise $id or $new_id if the id changed)
 *
 */
function checkIdDistanceAndRenumber($id = 0){
	
	$log = new Logging();
	$log->lwrite('Checking ID distances');

	$result = RENUMBER_PERM_FAIL;
	$sanity_counter = 0;
	
	$new_id = $id;
	
	while (true) {
		$items = $items = DocRecord::getIdOfAllRecordsOrderedById();
		
		$prev_id = 0;
		$num_of_no_spaces_found = 0;
		$last_id_no_space_after = 0;
		
		// loop over all IDs to find the last problematic location
		foreach ($items as $i=>$item) {
		
			$cur_id = (int) $item->getField('id');
			
			// If there is no space between this and the previous one
			if ($cur_id == $prev_id + 1) {
				// record the occurance
				$num_of_no_spaces_found++;
				$last_id_no_space_after = $prev_id;
			}
				
			$prev_id = $cur_id;
		}
				
		$log->lwrite('Num no spaces: ' . $num_of_no_spaces_found);
		
		// if no problematic location was found, reorganization is complete,
		// exit the while loop
		if ($num_of_no_spaces_found <= 0){

			// reorganization complete, exit while loop
			$log->lwrite('Renumbering complete.');
			$result = RENUMBER_SUCCESS;
			break;
		}
				
		// Change the document record
		// ===========================
				
		// If the record is locked, abort with a temporary error code so the 
		// function can be invoked again later.		
		if (isRecordLocked($last_id_no_space_after + 1)) {
			$log->lwrite('Record is currently locked, aborting renumbering ' .
					'for now, id: ' . ($last_id_no_space_after + 1));
			$result = RENUMBER_TEMP_FAIL;
			break;			
		}

		// Get the object for the record
		$item_to_modify = DocRecord::getRecord($last_id_no_space_after + 1);		
						
		// Lock the record for modification. If locking the record was not 
		// successful, abort with a temporary error code so the function can
		// be invoked again later.
		$lock_result = lockDatabaseRecord($item_to_modify);
		
		if ($lock_result != true) {
			$log->lwrite('Record could not be locked, aborting renumbering ' .
					     'for now, id: ' . ($last_id_no_space_after + 1));
			$result = RENUMBER_TEMP_FAIL;
			break;
		}
		
		$write_result = $item_to_modify->editSingleFieldInRecord('id',
				        $last_id_no_space_after + 2, true);
	
		$log->lwrite('ID of record ' . ($last_id_no_space_after + 1) .
				     ' updated in database to ' . ($last_id_no_space_after + 2) .
				     ', result: ' . ($write_result[0] ? 'ok':'error'));
		
		// If document record change was successful change the IDs in all 
		// modification history records for this document record as well
		if ($write_result[0] == true) {

			// Save old ID to modifcation history table
			$result = ModHistoryStorage::saveFieldContent($last_id_no_space_after + 1,
					'id', $last_id_no_space_after + 1, 'INTERNAL RENUMBERING');
			if ($result == false) {
				$err_str = 'Unable to save the ID to the mod history table';
				// Only log error but continue anyway, perhaps it is at least 
				// possible to change the modification history.
				$log->lwrite('ERROR: ' . $err_str);
			}
			
			$diff_move_result = ModHistoryStorage::changeIdInRecords(
					            $last_id_no_space_after + 1, 
					            $last_id_no_space_after + 2);
			$log->lwrite('ModHistoryStorage change id result: - ' . 
					     $diff_move_result[1]);
			
			// Unlock the database record with the new id, set second 
			// parameter to false to indicate not to modify the change
			// record as this was an internal operation.		
			unlockDatabaseRecord($last_id_no_space_after + 2, false);
			
			// If the $id that was initially given to this function is part
			// of the renumbering, remember to what it was changed to so 
			// this can be returned.
			if (($last_id_no_space_after + 1) == $new_id) {
				$new_id = $last_id_no_space_after + 2;
			}
			 					
			
		} else {
			// There was an error changing the ID, unlock the record
			// with the old ID. Set the second parameter to false to indicate 
			// not to modify the change record as this was an internal operation.
			unlockDatabaseRecord($last_id_no_space_after + 1,flase );
		}

		// exit while loop if we had a problem writing to the database
		if ($write_result[0] == false || $diff_move_result[0] == false) {
			$log->lwrite('ERROR: Could not write to the database, aborting!');
			$result = RENUMBER_PERM_FAIL;
			break;
		}
					
		// Increase sanity counter and abort if there were an implausible
		// number of loop iterations
		$sanity_counter++;
		if ($sanity_counter > 5000) {
			$log->lwrite('ERROR: Sanity counter exceeded, aborting!');
			$result = RENUMBER_PERM_FAIL;
			break;
		}
					
	} // end of while loop, run through the database again after fixing a location
	
	$log->lwrite('ID returned: ' . $new_id);
		
	return array ($result, $new_id);
}

/**
 * periodicIdRenumberingCheck()
 *
 * Periodic check if renumbering is necessary accross all active sessions. 
 * 
 * This is necessary as there aren't global variables accross sessions of 
 * different users that could be used to only do this when renumbering is 
 * really required. This could be done by storing global variables in the 
 * database and query them before executing an http request. However, that 
 * would be a bit of an overkill.
 *
 * @param none
 *
 * @return none
 *
 */
function periodicIdRenumberingCheck() {
	
	$log = new Logging();	
	
	// If the function was called in this session context before
	if (isset($_SESSION['periodic_renumber_check'])) {
		
		// has the periodic renumbering check timer expired
		if (($_SESSION['periodic_renumber_check']) < time()) {
			
			$log->lwrite('Periodic id renumbering check timer expired, checking...');				
			checkIdDistanceAndRenumber();
			$_SESSION['periodic_renumber_check'] = time() + PERIODIC_CHECK_TIME;
		}
		
	} else {
		
		// Never called so far, do a renumbering and then initialize the 
		// peridoc id check timer.
		$log->lwrite('First instance of periodic id renumbering check');
		checkIdDistanceAndRenumber();
		$_SESSION['periodic_renumber_check'] = time() + PERIODIC_CHECK_TIME;
	}
	
}