<?php
/**
 * init.php
 *
 * Initialization file
 *
 * @version    1.3 2017-01-21
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 * 
 */

// Database version information. CHANGE if database structure changes!
// Value must be an integer!
define('DRDB_DB_VERSION', 5);

session_start(); // starts new or resumes existing session
define('MAGIC_QUOTES_ACTIVE', get_magic_quotes_gpc());

// Include required configuration, helper functions, global definitions
// and global read-only variables.
require_once 'config/config.php';
require_once 'config/id-structure.php';
require_once 'config/database-structure.php';
require_once 'config/user-permissions-config.php';
require_once 'includes/reusable-gui-elements.php';
require_once 'includes/field-functions.php';
require_once 'includes/record-search.php';
require_once 'includes/record-handling.php';
require_once 'includes/db-lock-functions.php';
require_once 'includes/db-id-functions.php';
require_once 'includes/sec-functions.php';
require_once 'includes/print-functions.php';
require_once 'includes/fix-text-functions.php';
require_once 'includes/clean-and-safe-html.php';

require_once 'includes/check-and-upgrade-db.php';

// Static declarations for the $search_parameter array index
// for document Multi-Field search
define ('MULTI_FIELD_NAME', 0);
define ('MULTI_SEARCH_TERM', 1);
define ('MULTI_NOT_LIKE', 2);
define ('MULTI_EMPTY', 3);


$log = new Logging();
$log->lwrite('------------------------------------------------------');

$log->checkLogFileSizeAndArchive(10);

includeSecurityHeaders();

// If this is a new project and the initialize page is called, don't
// initialize anything, the tables need to be created first...
$content = filter_input(INPUT_GET, 'content', FILTER_SANITIZE_STRING);
if ($content && strpos($content, "bs_first_use_init") !== false) {
	$log->lwrite('Table initialization requested, doing nothing in init.php');
	return;
}

// If ballet details with a hash_id are requested, translate the hash_id into
// the current ID and return a '302 moved temporarily' header with a new link.
if ($content && strpos($content, "bs_record_details_with_hash") !== false) {

	$log->lwrite('Translating hash_id to id');	
	
	if (!isset($_GET['hash_id'])) {
		$log->lwrite('ERROR: No hash_id included');
		return;
	}
	
	$id = DocRecord::getIdFromHashId($_GET['hash_id']);
	
	if ($id == 0) {
		$log->lwrite('ERROR: Hash ID unknown');
		return;
	}
	
	$log->lwrite('INFO: 302 redirecting to document id ' . $id);
	
	header('Location: ' . $_SERVER["SCRIPT_NAME"] . 
		   '?content=bs_record_details&id=' . $id, true, 302);	
	exit;
}

if (!checkHostName()) {
	echo '<br>Hostname in URL does not match trusted domains in ' . 
		 'config.php or the DRDB_VIRTUAL_HOST environment variable. ' . 
         'Request aborted! Have a look at the log for further details and '. 
		 'adapt the list of trusted domains or the environment variable ' . 
	     'accordingly.';
	exit;
}


checkDatabaseVersionAndUpgrade();

UserConfigStorage::getUserConfig();

/***********************************************************************
 *
 * First run tasks that need to be done independently of which page
 * is to be loaded.
 *
 ***********************************************************************/

// Continue ID renumbering if required (could not have been done before
// as one of the records that needs to be renumbered was locked.
if (isset($_SESSION['continue_renumber_id']) &&
  		  $_SESSION['continue_renumber_id'] == true) {

	$renumber_result = checkIdDistanceAndRenumber();
	if ($renumber_result[0] == RENUMBER_TEMP_FAIL) {
		$log->lwrite('init.php() Not all IDs renumbered due to locking, ' .
				     'queueing request for later');
		$_SESSION['continue_renumber_id'] = true;
	} else {
		$_SESSION['continue_renumber_id'] = false;
	}

} else {

	periodicIdRenumberingCheck();
}

$task = filter_input(INPUT_POST,'task', FILTER_SANITIZE_STRING);

// Process based on the task. Default to display
switch ($task) {
      
  case 'doc.update' :
    if (UserPermissions::hasAccess('edit')) {
      	// process new or updated document
      	$results = createOrModifiyRecord();
      	// First return parameter is 0 for an error or 1 if operation was successful
      	$_SESSION['result-status'] = $results[0];
      	// Second results parameter contains a human readable success or error msg  	  
      	$_SESSION['result-msg'] = $results[1];
    } else {
        $log->lwrite("Record not updated, user has no 'edit' rights");
    }
  break;
  
  case 'doc.update.single.field' :
    if (UserPermissions::hasAccess('edit')) {          
      	// process new or updated document
      	$results = modifySingleFieldInRecord();
      	// first return parameter is 0 for an error or 1 if operation was successful
      	$_SESSION['result-status'] = $results[0];
      	// Second results parameter contains a human readable success or error msg
      	$_SESSION['result-msg'] = $results[1];
    } else {
        $log->lwrite("Record not updated, user has no 'edit' rights");
    }
  break;
  
} // End of switch statement


/**
 * 
 * Include a number of HTTP security headers to increase overall security.
 * For details see: https://www.owasp.org/index.php/OWASP_Secure_Headers_Project
 * The following headers include all security related additions that
 * are widely deployed in browsers as per OWASP in 2018!
 *
 */
function includeSecurityHeaders() {
	
	$log = new Logging();
	
	// Some pages require inline JS content, e.g. because they use
	// CKEDITOR 4. For these pages, a different HTTP CSP header is necessary
	// which relaxes security requirements.
	$pages_for_security_downgrade = array ('bs_record_details', 
			                	           'bs_record_update');
	
	// Get the name of the page to load and compare it to the array
	// of pages that require a security downgrade
	$page_to_load = filter_input(INPUT_GET, 'content', FILTER_SANITIZE_STRING);
	
	$js_csp = "script-src 'self' 'unsafe-eval'; ";

	if ($page_to_load && in_array($page_to_load, $pages_for_security_downgrade)) {
			$log->lwrite('Downgrading CSP for this page...');

			$js_csp = "script-src 'self' 'unsafe-inline' 'unsafe-eval'; ";
	}
	
	// For details on the effect of the Content-Security-Policy header
	// have a look at https://content-security-policy.com/
	header("Content-Security-Policy: " .
			"default-src 'self' data:; " .
			"font-src 'self' data:; ".
			"object-src 'none'; " .
			$js_csp .
			"style-src 'self' 'unsafe-inline'; " .
			"connect-src 'self'; " .
			"worker-src 'self';" .
			"frame-src 'self'");
	
	// The website must not be put into a frame. Protects against some forms
	// of click-jacking.
	header("X-Frame-Options: deny");
	
	// Block rendering of the complete page if an XSS attack is detected. Note:
	// It would be better to have the script-src option above not as 
	// "unsafe-inline" for all pages but since CKEditor 4 requires 
	// JS unsafe-inline, it's better than nothing to have this protection.
	header("X-Xss-Protection: 1; mode=block");
	
	// The browser must not try to change the MIME header by sniffing. This
	// reduces the risk of running code embedded in up/downloaded files.
	header("X-Content-Type-Options: nosniff");
	
	// Always send the full referrer to this page unless there is a downgrade
	// from https to http (which HSTS prevents if set by the admin)
	header("Referrer-Policy: no-referrer-when-downgrade");
	
}


/**
 * Auto load the class files
 * @param string $class_name
 */
function __autoload($class_name) {	
  try {
    $class_file = 'includes/classes/' . strtolower($class_name) . '.php';
    if (is_file($class_file)) {
      require_once $class_file;
    } else {
      throw new Exception("Unable to load class $class_name in file $class_file.");
    }
  } catch (Exception $e) {
    echo 'Exception caught: ',  $e->getMessage(), "\n";
  }
}

/**
 * loadContent()
 *
 * Load the PHP file given in the URL in the ?content=xxx part
 *
 * @param $default
 */

function loadContent($where, $default='') {
	// Get the content from the url
	// Sanitize it for security reasons
	$content = filter_input(INPUT_GET, $where, FILTER_SANITIZE_STRING);
	$default = filter_var($default, FILTER_SANITIZE_STRING);

	// If there wasn't anything on the url, then use the default
	$content = (empty($content)) ? $default : $content;

	// If content was found pass it back
	if ($content) {

		$filename = 'content/'.$content.'.php';

		if (!file_exists($filename)) {
			$log = new Logging();
			$log->lwrite('ERROR: Page does not exist: ' . $filename);
			return $default;
		}

		$html = include $filename;
		return $html;
	}
}
