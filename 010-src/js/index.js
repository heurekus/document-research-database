/**
 * index.js
 *
 * Contains Javascript functionality for index.php
 *
 * @version    2.0 2018 21 11
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

    
// Adapt the height of the sidenav to the length of the document
// after loading and whenever the browser is resized IF a PC screen
// is used. When on mobile don't set the height element.
// Without this code the heigt of the sidenav is only 800px after
// which only white is shown. This does not look pretty

function adaptSidebar() {
	cur_width = $(document).width();
	if (cur_width >= 768) {		
	    $('.sidenav').css('height', $(document).height());
	}
	else
	{
		$('.sidenav').css('height','');
	}
}
	
$(document).ready(function(){
    adaptSidebar();
}); 

$(window).resize(function() {
    adaptSidebar();
});