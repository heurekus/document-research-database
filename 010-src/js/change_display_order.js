/**
 * change_display_order.js
 *
 * This module contains client side Javascript code that is included
 * by bs_change_display_order.php to implement moving, activating and 
 * deactivating fields.
 *
 * @version    2.0 2018 10 24
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// ==========================================================================
// The following variable, the if statement and the function handle influence
// if a CLICK or DOUBLECLICK is required to activate / deactivate a field. 
// On Android, iPhones and iPad the single click functionality is enabled
// as those OSes are used on touch based devices where dbl clicks don't work.
// ==========================================================================

isMobile = false;

// Note: /xxx/i is a regular expression, '.test' tests for true or
// false against the variable in the round brakets (userAgent...)
if (/Android|iPhone|iPad|iPod/i.test(navigator.userAgent)) isMobile = true;

function toggleVisibilitySingleClick() {

	if (isMobile == true) {
		
		// On mobile, click acts like double click.
		// NOTE: toggleVisibility is called with '.call(this)' which
		// forwards the calling object into the object space of
		// toggleVisibility. If toggleVisibility() is called,
		// 'this' is undefined and the function can't access 
		// the part of the DOM from which it was called.
		
		toggleVisibility.call(this);
	} 
	// else ignore the single click call, the user agent is not from a 
	// mobile OS	
}


/**
 * This jQuery function construct implements the draggable panels
 *
 * @param none
 *
 * @return none
 */

jQuery(function($) {
    var panelList = $('#draggablePanelList');

    panelList.sortable({
        // Only make the .panel-heading child elements support dragging.
        // Omit this to make then entire <li>...</li> draggable.
        handle: '.panel-heading', 
        update: function(event, ui) {
        	updateFieldOrder();        	
        } // end of update
    }); // end of panelList.sortable
}); // end of jQuery function


/**
 * updateFieldOrder()
 *
 * When some aspect of the field list is changed this function is called
 * It reads the current configuration of the field list from the DOM,
 * assembles POST variables and then sends a request to the server with
 * the updated data.
 *
 * @param none
 *
 * @return none
 */

function updateFieldOrder(){
	console.log('\r\n\r\nReordering!');

	// delete the old field strings
	var fieldNameString = "";
	var fieldPrintNameString = "";
	var fieldVisibilityString = "";
	var fieldInlinePrintString = "";	
	
	// Loop over each panel in the new sort order!
    $('.doc-field').each(function(index, element) {

    	fieldName = $("#panel-id-field-name", element).text();
    	fieldPrintName = $("#panel-id-print-name", element).text();
    	fieldVisibility = $(element).attr("class");
    	fieldInlinePrint = $("#panel-id-f-inline-print", element).text();
    	
        	            	
    	// Add the field if the name is not 0 (list elements in the
    	// sidebar are going through this loop as well but return
    	// a 0-length string as they don't have an id = panel-id!
	    if (fieldName.length != 0) {

	        // Append to string that will be sent to server eventually
	        // Note: Already preparing for show/hide with extra
	        // 0/1 parameter
	        fieldNameString += fieldName + '&&&';
	        fieldPrintNameString += fieldPrintName + '&&&';

	        // Depending on the panel color (panel-primary (blue) vs. 
	        // panel-warning (yellow) set the visibility bit to 1 (true)
	        // or 0 (false)	        
	        if (fieldVisibility == 'panel panel-primary doc-field') {
	        	fieldVisibilityString += '1&&&';
	        }
	        else {
	        	fieldVisibilityString += '0&&&';
	        }

	        // And finally get the fieldInlinePrint status for the current
	        // field. 
	        if (fieldInlinePrint == '1'){
	        	fieldInlinePrintString += '1&&&';
	        }
	        else {
	        	fieldInlinePrintString += '0&&&';
	        }
	        
	    } // end of fieldName.length != 0
    
    }); // end of the 'each' list element loop

	// Get content of the span element in the DOM that contains the doc_db__id
	// into a local variable.
	var dbDocId = document.getElementById("doc_db_id").innerHTML;
           	    
    console.log(fieldNameString);
    console.log(fieldPrintNameString);
    console.log(fieldInlinePrintString);
    console.log(dbDocId);
    
	// HTTP post query to the server to update the field struct, including the 
	// security token as a HTTP POST parameter.
	var dbDocToken = document.getElementById("doc_token").innerHTML
	
	$.post("index.php?content=update_field_display_order",
	       {field_name: fieldNameString,
	        field_print_name: fieldPrintNameString,
	        field_visibility: fieldVisibilityString,
	        field_inline_print: fieldInlinePrintString,
	        db_doc_id: dbDocId,
	        token: dbDocToken}, 
	        updateFieldOrderCallback);    	    
}


/**
 * updateFieldOrderCallback()
 *
 * This function gets called once the AJAX call update the in which order 
 * to display a records's filed returns. It maks a log to the console and
 * otherwise does nothing if the call was successful. If the server could
 * not be reached an error pop-up is presented.
 *
 * @param data, status
 *
 * @return NA
 */

function updateFieldOrderCallback(data, status) {

    var resultText = "";
	
    if (status == "success") {
  
		console.log('Field sort order successfully updated');
    }
    else {
        alert ('problem sending update to server!');
    }

}


/**
 * toggleVisibility()
 *
 * When the user double clicks on a panel to indicate to make the field
 * visible or invisible this function is called.
 *
 * @param element
 *
 * @return none
 */

function toggleVisibility() {

	// Toggle visibility indication (blue/yellow)
	if ($(this).attr("class") == "panel panel-warning doc-field") {
		$(this).attr("class", 'panel panel-primary doc-field');
	}
	else {		 
		$(this).attr("class", 'panel panel-warning doc-field');
	}

	updateFieldOrder();
}


/**
 * hideAll()
 *
 * Handlers called when the "Hide all" or "Show all" buttons are pressed
 *
 * @param none
 *
 * @return NA
 */

function hideAll(){
	// Loop over each panel in the new sort order!
    $('.doc-field').each(function(index, element) {
    	fieldVisibility = $(element).attr("class");

    	if (fieldVisibility == 'panel panel-primary doc-field') {
    		$(element).attr("class", 'panel panel-warning doc-field');
    	}
    });

    updateFieldOrder();
}


/**
 * showAll()
 *
 *
 * @param none
 *
 * @return none
 */

function showAll(){
	// Loop over each panel in the new sort order!
    $('.doc-field').each(function(index, element) {
    	fieldVisibility = $(element).attr("class");

    	if (fieldVisibility == 'panel panel-warning doc-field') {
    		$(element).attr("class", 'panel panel-primary doc-field');
    	}
    });

    updateFieldOrder();
}

/**
 * windowClose()
 *
 *
 * @param none
 *
 * @return none
 */

function windowClose() {
	window.close();
}

/**
 *
 * Misc stuff that needs to be done once the page is fully loaded
 *
 * @param none
 *
 * @return none
 */
	
$(document).ready(function(){

	// In mobile touch browsers, other input options are available. Change
	// info text accordingly.
	if (isMobile == true) {
		$('#info-text').html('Usage:<br>1. Drag/drop the fields into the ' + 
				             'desired order.<br> ' + 
				             '2. Tap on field name to show (blue) ' + 
				             'or hide (yellow) the field.');
	}

	// Add event listeners for click and double clicks of field buttons that
	// are of class 'doc-field'.
    $('.doc-field').click(toggleVisibilitySingleClick);
    $('.doc-field').dblclick(toggleVisibility);
    
    
    // Add event listeners for left column menu buttons
 	$('#showall').click(showAll);
	$('#hideall').click(hideAll);
	$('#closetab').click(windowClose);
	
});

