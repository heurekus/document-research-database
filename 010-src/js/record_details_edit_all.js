/**
 * record_details_edit_all.js
 *
 * The following functions are used when the "edit" button was pressed in
 * bs_record_details.php to see if the record was modified since this page was 
 * loaded. In case it was modified, the edit page must not be loaded and the
 * user must be informed that the record has been modified. Afterward
 * this tab is closed as the information contained is not valid.
 *
 * @version    2.0 2018 10 23
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

function checkModifyBeforeEdit (element) {

	var dbDocId = document.getElementById("doc_db_id").innerHTML;
	var dbDocToken = document.getElementById("doc_token").innerHTML;


	console.log('Checking if the document with ID ' + dbDocId + 
			    ' was modified. Using token ' + dbDocToken);

	// AJAX query to the server to delete the document, including the security
	// token as a HTTP POST parameter.
	$.post("index.php?content=get_last_modify_time&id=" + dbDocId,
	       {token: dbDocToken}, checkModifyCallback);

}

// This function gets called once the AJAX to get the last modification time
// of the document via checkModifyBeforeEdit() returns.
function checkModifyCallback(data, status) {

	var dbDocLoadTime = document.getElementById("doc_load_time").innerHTML;
	var dbDocId = document.getElementById("doc_db_id").innerHTML;
	var lastModifyTime = 0;
  
	if (status != "success") {
	      alert('Unable to contact the server to check for modification of the ' +
  	            'current record, proceed at your own risk!');
          window.open("index.php?content=bs_record_update&id=" + dbDocId, "_self");
          return;
	}

    if (data.search('record is locked') != -1) {
    	// Reset the hasBeenEdited variable to prevent a dialog box poping up
        // that asks the user to save the data (which no longer exists). 
        hasBeenEdited = false;

        // Put the response into the modal dialog box that will be opened
        // next.
        $("#lock-failure-text").html("Document is locked in another window " +
                                     "or by another user, unable to edit!");          
        // Show the dialog box which ultimately closes this tab
      	$('#lockFailDocExecModal').modal('show')

        return;
    }
     
    console.log('DB record is not locked, checking last modify time...');

    lastModifyTime =  parseInt($(data).find("#doc-mod-time-result").text()); 
    console.log('Last modify time: ' + lastModifyTime); 

    if (lastModifyTime > dbDocLoadTime) {
 
    	console.log('Last Modify Time AFTER Doc load, aborting!');
        $("#lock-failure-text").html("Document was modified AFTER this page " + 
                                     "was loaded, unable to edit!");            
    	// Show the dialog box which ultimately closes this tab
        $('#lockFailDocExecModal').modal('show')

        return;
    }
    
	console.log('Last Modify Time before Doc load, proceeding!');
   	window.open("index.php?content=bs_record_update&id=" + dbDocId, "_self");
    
}

//
// Things to do after the web page has fully loaded
//
$(document).ready(function(){
	
	// Edit button handler
	$('#check-modify-before-edit').click(checkModifyBeforeEdit);
});	

