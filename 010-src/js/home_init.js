/**
 * home_init.js
 *
 * Contains Javascript functionality to initialize bs_home.php on the 
 * client side after the page has loaded.
 *
 * @version    2.0 2018 10 23
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


function checkExtraFieldsCheckBox() {

	console.log("Checking extra fields checkbox")
	$('.extra_fields').attr('checked', 'checked');	
}

/**
 * 
 * Once the document has fully loaded, fade-out alerts that have 
 * "ms-fade" in their class!
 *
 * @param NA
 *
 * @return NA
 * 
 */

$(document).ready(function(){
	$(".ms-fade").delay(3000).addClass("in").fadeOut(3000);
	
	// Register the click handler (#) on id, (.) on class.
	$('.check-extra-fields').click(checkExtraFieldsCheckBox);
	
	$('#list-search-help').click(listAndSearchHelpButtonHandler);

	
});


function listAndSearchHelpButtonHandler() {
	
	// Open help in a new window instead of in a tab so the modal
	// print dialog box remains open.
	window.open('/drdb-manual/ListAndSearch/', 
                'newwindow', 
                'left=100,top=100');
	
}

