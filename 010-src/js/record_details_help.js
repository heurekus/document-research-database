/**
 * record_details_help.js
 *
 * Contains Javascript functionality to open a separte window for help
 * information.
 *
 * @version    2.0 2019 04 18
 * @package    DRDB
 * @copyright  Copyright (c) 2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


$(document).ready(function(){

    // Add event listener for opening a help window
	// Note: jquery uses '.' for CLASSES and '#' for IDs.
	$('#details-help').click(detailsHelpButtonHandler);
});

function detailsHelpButtonHandler() {
	
	// Open help in a new window instead of in a tab so the modal
	// print dialog box remains open.
	window.open('/drdb-manual/DetailsView/', 
                'newwindow', 
                'left=100,top=100');
	
}


