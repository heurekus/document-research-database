/**
 * diff_view_show_dbox.js
 *
 * Show the modal dialog box that there are no diffs
 *
 * @version    2.0 2018 10 23
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$('#noDiffFound').modal('show');
