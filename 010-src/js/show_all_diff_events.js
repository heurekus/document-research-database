/**
 * show_all_diff_events.js
 *
 * @version    2.0 2018 11 19
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


/**
 * windowClose()
 *
 * @param none
 *
 * @return none
 */

function windowClose() {
	window.close();
}

/**
 *
 * @param NA
 *
 * @return NA
 * 
 */

$(document).ready(function(){
	$('#closetab').click(windowClose);
});
