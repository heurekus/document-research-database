/**
 * record_details_del.js
 *
 * Contains Javascript functionality to delete a record from the
 * record_details.php page. 
 * 
 * The following functions are used as part of the 'Delete Document'
 * functionality to show dialog boxes, make an AJAX request to the 
 * server to delete the document and to finally close the current tab
 * once the document is deleted.
 * 
 *
 * @version    2.0 2018 10 23
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

function deleteDoc (element) {
	$('#deleteDocModal').modal('show')
}

// This function is called when the user presses the "Delete" button
// in the modal dialog box that requests the user to confirm the deletion
function docDeleteExec (element) {

	var dbDocId = document.getElementById("doc_db_id").innerHTML;
	var dbDocToken = document.getElementById("doc_token").innerHTML
	
	console.log('Deleting Document with ID ' + dbDocId + 
			    ' and token ID ' + dbDocToken);

	// AJAX query to the server to delete the document, including the security
	// token as a HTTP POST parameter.
	$.post("index.php?content=record_delete_final&id=" + dbDocId,
	       {token: dbDocToken}, docDeleteCallback);
     
}

// This function gets called once the AJAX call to delete the document
// in docDeleteExec() returns
function docDeleteCallback(data, status) {

 var resultText = "";
	
 if (status == "success") {
     // Get result text from the returned document deletion web page
     // that is in the paragraph with id 'document-del-result' and
     // put it into the dialog box.
 	$("#delete-final-text").html($("#doc-del-result", data));
 	$("#delete-final-text").append("<p>Please click on OK to close " + 
 	  "this window.<br>IMPORTANT: Reload the previous <?php echo DatabaseConfigStorage::getCfgParam('doc-name-ui');?> list!</p>");	    	
	}
	else {
		$("#delete-final-text").html("<p>Unable to contact the web server, " + 
				                     "<?php echo DatabaseConfigStorage::getCfgParam('doc-name-ui');?> not deleted</p>");
	}

	// In case an editor window was open before the document was deleted
	// reset the hasBeenEdited variable to prevent a dialog box poping up
	// that asks the user to save the data (which no longer exists). 
	hasBeenEdited = false;	
 
	// Show the dialog box
	$('#deleteDocExecModal').modal('show')

}

$(document).ready(function(){
	$('#deldoc').click(deleteDoc);
	$('#del-button').click(docDeleteExec);

});


