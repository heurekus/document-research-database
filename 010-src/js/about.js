/**
 * about.js
 *

 * @version    2.0 2018 11 19
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


/**
 * windowClose()
 *
 *
 * @param none
 *
 * @return none
 */

function windowClose() {
	window.close();
}


/**
 *
 * @param NA
 *
 * @return NA
 * 
 */

$(document).ready(function(){
    // Add event listeners for left column menu buttons
	$('#closetab').click(windowClose);
});
