/**
 * record_details_load_unload.js
 *
 * Contains functions that are called when the page is loaded and when
 * the user wants to leave it again such as:
 * 
 *  - If an editor is open, warn the user that data is unsaved.
 *  
 *  - If the user leaves the page and does not save the data, release the 
 *    lock on the database record.
 *    
 *
 * @version    2.0 2018 10 24
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

//==========================================================================
//
// Misc stuff that needs to be done once the page is fully loaded
//
// ==========================================================================

$(document).ready(function(){

	// A jQuery function to fade-out alerts that have "ms-fade" in their class!
	$(".ms-fade").delay(200).addClass("in").fadeOut(4000);

	// Set tab title
	document.title = document.getElementById("title-content").innerText;

	// In mobile touch browsers, other input options are available. Change
	// info text accordingly. Note: isMobile defined in 
	// record_details_keys_and_swipe.js
	if (isMobile == true) {
		$('#info-text').html('Note: Tap on text to edit. Swipe left and right to ' + 
				             'move between records.');
	}

	activateSwipeIfOnMobile(); // in record_details_keys_and_swipe.js
	
	// Handler for all 'close' buttons that are in class "closetab"
	$('.closetab').click(windowClose);
});

/**
 * windowClose()
 *
 *
 * @param none
 *
 * @return none
 */

function windowClose() {
	window.close();
}

//==========================================================================
//
// Things to be done before leaving the page
//
// Note: The 'hasBeenEdited' variable used below is defined in 
// record_details_edit_field.js
//
// ==========================================================================


function confirmleaving() {
	if (hasBeenEdited == true) {		
        return "Your changes are not saved!";
	}
}

function beforeUnload(evt) {
    // TODO: Better to set this in the editor html, as it does not make
    // sense elsehwere.
    // confirmleaving is available when editing
    try {return confirmleaving();}
    catch (e) {}
}

// ==========================================================================
// This function is called when the page is unloaded AFTER the dialog box
// that asks the user if he really wants to leave if data is unsaved.
// It checks if data is still being edited and if so, it sends an AJAX
// query to release the lock to the database record (no data is saved).
// ==========================================================================

function checkAndReleaseDatabaseLock(evt) {

	var dbDocId = document.getElementById("doc_db_id").innerHTML;
	var dbDocToken = document.getElementById("doc_token").innerHTML;

	if (hasBeenEdited != false) {	
		// AJAX query to the server to delete the document, including the 
		// security token as a HTTP POST parameter.
		$.post("index.php?content=unlock_doc_record&id=" + dbDocId,		   
		       {token: dbDocToken});

	    // Give the POST request above a chance to be sent!
	    sleep(1000);
	}
}

// Really ugly way to introduce a delay. It's necessary to give the AJAX 
// request a chance to be sent out in the "onunload" functionality.
function sleep(milliseconds) {
	var start = new Date().getTime();	
	for (var i = 0; i < 1e7; i++) {
	    if ((new Date().getTime() - start) > milliseconds){
 			break;
 		}
	}
}

// Catch before unloading the page to check if the user has unsaved data
// on the page. beforeUnload() is put in as a handler which is defined above.
window.onbeforeunload = beforeUnload;

// Catch the unload event to release which comes AFTER the 'onbeforeunload' 
// Event. If there is still a lock on the current database record it is 
// released.
window.onunload = checkAndReleaseDatabaseLock;




