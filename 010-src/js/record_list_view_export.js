/**
 * record_list_view_export.js
 *
 *  Functions for handling the 'Show Export to Libreoffice SETTINGS
 *  modal dialog box.
 *
 * @version    2.0 2018 10 27
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$(document).ready(function(){

    // Add event listeners
	// Note: jquery uses '.' for CLASSES and '#' for IDs.
	$('.show-print-export').click(showExportDialog);
	$('.ms-save-export-settings').click(saveExportSettings);

	$('#export-help').click(exportHelpButtonHandler);
	$('#list-view-help').click(listViewHelpButtonHandler);
	
});

function showExportDialog() {

	$('.show-id').prop('checked', false);
	$('.include-link').prop('checked', false);
	$('.page-break').prop('checked', false);

	// AJAX query to the server to get the print configuration, 
	// including the security token as a HTTP POST parameter.
	var dbDocToken = document.getElementById("doc_token").innerHTML
		
	$.post("index-json.php?content=get_print_config",
		   {token: dbDocToken},
		   docExportConfigCallback
		  );

	// Note: The Export dialog is NOT yet shown, it only appears once
	// the server has delivered the configuration parameter settings to
	// docExportConfigCallback()
}

function docExportConfigCallback(data, status) {

	if (status == "success") {

		// Covert the received print/export to Libreoffice parameters
		// from JSON format to a JS object
		console.log('Print/Export config received: ' + data);
		var params = JSON.parse(data);

		$('.show-id').prop('checked', params.showId);
		$('.include-link').prop('checked', params.includeLink);
		$('.page-break').prop('checked', params.pageBreak);

		// Now everything is ready, show the modal dialog box so the user
		// can change the settings.
		$('#myExportModal').modal('show');
		
	}
	else {
        alert('Unable to contact the server to get the print/export settings');
	}
}

function saveExportSettings() {

	// Create an empty object which will be used as dynamic associative
	// array (which is not supported as a datatype in Javascript natively)
	var params = {};

	// Add the checkbox settings from the modal dialob box
	// Each checkbox can either be checked (=true) or not (=false)
	params['showId'] = $('.show-id').is(":checked");
	params['includeLink'] = $('.include-link').is(":checked");
	params['pageBreak'] = $('.page-break').is(":checked");
	
	var paramsJSON = JSON.stringify(params);
	console.log(paramsJSON);

	// AJAX query to the server to delete the document, including the security
	// token as a HTTP POST parameter.
	var dbDocToken = document.getElementById("doc_token").innerHTML
	
	$.post("index-json.php?content=set_print_config",
		   {
		       print_settings: paramsJSON,
	           token: dbDocToken
	       });
    
}

function exportHelpButtonHandler() {
	
	// Open help in a new window instead of in a tab so the modal
	// print dialog box remains open.
	window.open('/drdb-manual/export/', 
                'newwindow', 
                'left=100,top=100');
	
}

function listViewHelpButtonHandler() {
	
	// Open help in a new window instead of in a tab so the modal
	// print dialog box remains open.
	window.open('/drdb-manual/CreateAndEditEntry/#move-document-records', 
                'newwindow', 
                'left=100,top=100');
	
}

// Add listener even for tab changes. This way, the modal EXPORT dialog
// box is closed automatically if it is still open. This will happen in two
// cases: 1) The user has pressed one of the two export buttons. 2) The 
// user has changed the tab to look for something else.
document.addEventListener('visibilitychange', function(){
	
	$('#myExportModal').modal('hide');
});



