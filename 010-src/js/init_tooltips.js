/**
 * init_tooltips.js
 *    
 * A Javascript include file for pages that use tooltips. These
 * need to be initialized.
 *
 * @version    2.0 2019 05 10
 * @package    DRDB
 * @copyright  Copyright (c) 2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$(document).ready(function(){

	// Enable tooltips for mouse-over events, e.g. to show conventions
	// to fill out a field
    $('[data-toggle="tooltip"]').tooltip();

});

