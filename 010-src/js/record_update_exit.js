/**
 * record_update_exit.js
 *
 * Functionality called before the bs_record_update.php page is closed. If data
 * was not yet saved the user is warned and remain on the page.
 *
 * @version    2.0 2018 10 25
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

var hasBeenEdited = true;

function data_was_saved() {
	hasBeenEdited = false;
}

function confirmleaving() {	
	if (hasBeenEdited == true) {		
        return "Your changes are not saved!";
	}
}

function before_unload(evt) {
    // TODO: Better to set this in the editor html, as it does not make
    // sense elsehwere.
    // confirmleaving is available when editing
    try {return confirmleaving();}
    catch (e) {}
}

// This function is called when the page is unloaded AFTER the dialog box
// that asks the user if he really wants to leave if data is unsaved.
// It checks if data is still being edited and if so, it sends an AJAX
// query to release the lock to the database record (no data is saved).
//
// Note: In practice it seems this event is not fired reliably...
function checkAndReleaseDatabaseLock(evt) {

	var dbDocId = document.getElementById("doc_db_id").innerHTML;
	var dbDocToken = document.getElementById("doc_token").innerHTML;

	if (hasBeenEdited == true) {
	    // AJAX query to the server to delete the document, including the security
		// token as a HTTP POST parameter.
		$.post("index.php?content=unlock_doc_record&id=" + dbDocId,		   
		       {token: dbDocToken});	

		// Give the POST request above a chance to be sent!
	    sleep(1000);
	}
}

// Really ugly way to introduce a delay. It's necessary to give the AJAX 
// request a chance to be sent out in the "onunload" functionality.
function sleep(milliseconds) {
	var start = new Date().getTime();	
	for (var i = 0; i < 1e7; i++) {
	    if ((new Date().getTime() - start) > milliseconds){
    	break;
    }
  }
}

// Catch before unloading the page
window.onbeforeunload = before_unload;

//Catch the unload event to release which comes AFTER the 'onbeforeunload' 
//Event. If there is still a lock on the current database record it is 
//released.
window.onunload = checkAndReleaseDatabaseLock;


/**
*
* Misc stuff that needs to be done once the page is fully loaded
*
*/
	
$(document).ready(function(){
	
    // Add event listeners
	$('#mark-data-saved').click(data_was_saved);

});