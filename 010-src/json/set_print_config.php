<?php
/**
 * set_print_config.php
 *
 * This module is called when a web client wants to push "print/export to
 * Libre Office" configuration parameters to the server. The variables
 * are sent by the client in JSON format, extracted and checked by this
 * module, put into session variables and finally saved to the user
 * configuration table in the database.
 *
 * @version    1.0 2017-04-18
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


$log = new Logging();
$log->lwrite('set_print_config.php: Updating printing configuration');

// Web security check
if (checkAntiCsrfProtection() == false) {
	$log->lwrite("set_print_config.php: security error, exiting");
	exit;
}	

// If the settings are not included in the POST request, exit
if (!isset($_POST['print_settings'])) {
	$log->lwrite("set_print_config.php: 'print_settings' not present");
	exit;
}

// Get the printer settings from the POST variable and convert from JSON
// format to an associative array
$print_settings_JSON = $_POST['print_settings'];
$print_settings = json_decode($print_settings_JSON, true);

$log->lwrite('set_print_config.php: print_settings: ' . $print_settings_JSON);

// Now loop through all allowed variable names to see if they exist. If they
// do exist, put true/false into the corresponding session variables.

$allowed_print_variable_names = array ("showId", "includeLink", "pageBreak");

foreach ($allowed_print_variable_names as $name) {

	if (isset($print_settings[$name])) {
	
		$_SESSION['blt_user_' . $name] = $print_settings[$name] ? true : false;
		$log->lwrite('set_print_config.php: setting "' . $name . '" to: ' .
				$_SESSION['blt_user_' . $name]);
	}
	else {
		$log->lwrite('set_print_config.php: "' . $name . '" not found!');
	} 
			
}

// Save the changed configuration
UserConfigStorage::saveUserConfig();


echo 'ok';

?>






