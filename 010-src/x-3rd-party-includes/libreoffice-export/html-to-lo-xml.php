<?php
/**
 * html-to-lo-xml.php
 *
 * This class to converts a HTML formatted string to Libreoffice XML output.
 * The class is intended to be 'extended' by other classes which contain
 * functionality to write the output of this class to different kinds of
 * Libreoffice document types, e.g. Writer or Calc. 
 *
 * The "Export to Libreoffice Writer' functionality, for example, is 
 * implemented in lo-writer-export.php.
 * 
 * Supported HTML tags:
 * 
 *  - Bold, (both 'b' and 'strong' tags)
 *  - Italics, i.e. 'emphasis'
 *  - Superscript
 *  - Foreground color span 
 *  - Background color span
 *  - The HTML 'br' tag without closing
 *  - HTML links 'a href'
 *
 * @version    1.0 2019-01-13
 * @package    libreoffice-export
 * @copyright  Copyright (c) 2014-19 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 * 
 */

require_once 'lo-export-log.php';

/**
 * class html_to_loffice_xml
 *
 */

class Html_To_LibreOffice_XML {
	
// Define the different types of HTML tags and a value for plain text.
const LO_CONVERT_START_PARAGRAPH = 1;
const LO_CONVERT_END_PARAGRAPH =   2;

const LO_CONVERT_START_FORMAT =    3;
const LO_CONVERT_END_FORMAT =      4;

// All spans end with a generic </span> which always terminates the most
// recent opened span.
const LO_CONVERT_END_SPAN =       60;

const LO_CONVERT_TEXT =          100;

/* 
 * HTML tags this class recognizes and actions taken when one of them is 
 * encountered. Additional tags can be added to this array to treat additional 
 * HTML tags as required.
 *
 * The inner array has the following fields:
 * 
 * 0 = TAG_TYPE:  LO_CONVERT....
 * 
 * 1 = TAG_STATE: true/false, ONLY used for LO_CONVERT_START_FORMAT entries.
 * 
 *                False indicates that this start tag was not encountered in 
 *                the text so far or has already been closed again.
 *                True indicates the tag has been encountered and has not
 *                yet been closed. This needs to be known when assembling
 *                an XML inline tag that contains several HTML values such as
 *                bold, italics, color, etc.
 *                 
 * 2 = DEBUG_INF: Text for debug output
 * 
 * 3 = XML_TXT:   Text required to assemble part of the XML tag               
 * 
 */

protected $html_tags = array (

	//
	// In the first part of the array, normal HTML style tags are defined
	// 	
		
	// Note: PNAME is replaced by the paragraph name given to the convert method
	"<p>"       => array(self::LO_CONVERT_START_PARAGRAPH,
			             false, 
			             'start para',
	                     '<text:p text:style-name="PNAME">'),
	
	"</p>"      => array(self::LO_CONVERT_END_PARAGRAPH,
			             false,
			             'end para',
	                     '</text:p>'),

	"<em>"      => array(self::LO_CONVERT_START_FORMAT,
			             false,
			             'italics',
	                     'fo:font-style="italic" style:font-style-asian="italic" ' . 
	                     'style:font-style-complex="italic"'),		

	"</em>"     => array(self::LO_CONVERT_END_FORMAT,
			             false,
			             'end of italics', 
	                     ''),

	"<strong>"  => array(self::LO_CONVERT_START_FORMAT,
			             false,
			             'bold',
	                     'fo:font-weight="bold" style:font-weight-asian="bold" ' . 
	                     'style:font-weight-complex="bold"'),

	"</strong>" => array(self::LO_CONVERT_END_FORMAT,
			             false,
			             'end of bold',
	                     ''),
		
	"<b>"       => array(self::LO_CONVERT_START_FORMAT,
				         false,
				         'bold',
				         'fo:font-weight="bold" style:font-weight-asian="bold" ' .
		                 'style:font-weight-complex="bold"'),
		
	"</b>"      => array(self::LO_CONVERT_END_FORMAT,
				         false,
				         'end of bold',
		                 ''),		

	"<sup>"  => array(self::LO_CONVERT_START_FORMAT,
				         false,
				         'superscript',
						 'style:text-position="super 58%"'),
		
	"</sup>" => array(self::LO_CONVERT_END_FORMAT,
						 false,
						 'end of superscript',
						 ''),

	// 
	// Only the end of a span defined here. The start span tag needs to be 
	// handled differently as it is a complex html element.
	//	
		
	'</span>'   => array(self::LO_CONVERT_END_SPAN,
						false,
						'end of a span',
				        ''),

);

// Constants to identify the fields of array entries above. 

const TAG_TYPE  = 0;
const TAG_STATE = 1;
const DEBUG_INF = 2;
const XML_TXT   = 3;

/*
 * Constants and an array of XML texts to translate HTML SPAN elements
 * to Libreoffice XML text style properties.  
 * 
 * Note: This array is different from $html_tags above as it doesn't define
 * actions to take. It just defines the output text with placeholders 
 * (e.g. CCCCCCC) that are replaced. 
 *  
 * That means that for adding a new span element requires to put a new 
 * entry here and extra code in Handle_Span_Tags()
 *   
 */
const SPAN_BG_COLOR    = 0;
const SPAN_FONT_COLOR  = 1;

protected $html_spans = array (

		'fo:background-color="CCCCCCC" loext:char-shading-value="0"',
		'fo:color="CCCCCCC"',
);

protected $span_stack = array();

// A string buffer to hold the generated xml
protected $xml_buf = "";

// Set to false if a paragraph shall contain a page break 'before' element
protected $insert_page_break = false;

/*
 * An associative array to hold the different formatting combinations 
 * that are found while traversing through the HMTL strings.
 * 
 * IMPORTANT: An object created from this class must be called for all
 * HTML strings that are converted to XML for one LibreOffice document
 * as otherwise the text span names would be duplicated which in turn
 * would mess up the Libreoffice document.
 * 
 * Array structure:
 * 
 *  - The index of the array is the style name
 *  - Each entry is an array of strings with all style names
 * 
 */

protected $text_styles = array();

/*
 * An array to keep page break paragraph names
 *  
 */

protected $page_brk_para_names = array();

/*
 * $href_link is filled when an 'a href' link tag is found. It is reset
 * when an end of link tag is found. 
 *
 */

protected $href_link = "";

/**
 * Convert_Html_To_LO_XML()
 *
 * Converts the HTML formatted text to a LibreOffice XML string and
 * the associated format description.   
 *
 * @param string $html_str The HTML formatted string that has to start and 
 *        end with 'p' and '/p'
 * @param string $paragraph_name The paragraph name, i.e. the general 
 *        formatting name contained in the Libreoffice that shall be applied 
 *        to the paragraphs in the HTML string.
 * @param boolean $page_break Optional, true if a page break shall be inserted 
 *        before the first paragraph.  
 *
 * @return string Log buffer for debugging purposes. 
 */

public function Convert_Html_To_LO_XML ($html_str, 
		                                $paragraph_name, 
		                                $page_break = false){

	$output_str = "";
	$log = "";

	// Copy info whether to start with a page break paragraph into this
	// object's page break info variable.
	$this->insert_page_break = $page_break;		
		
	// <br> are part of the text and do not change formatting. Therefore
	// replace the tag with what will later become an XML tag. This way
	// it is not split into an individual tag that needs to be treated.
	$html_str = str_replace("<br>", 'xxxtext:line-break/xxx', $html_str);
	
	// split the input string that contains html formated text 
	// into html tags and text in betweeen. 
	$parts = preg_split('/(<[^>]*[^\/]>)/i', $html_str, -1, 
			            PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
		
	// Go through all the parts
	foreach ($parts as $part) {

		LoxLog::add($log, "---------------------");
		
		// Output the part if it is text
		if ($part[self::TAG_TYPE] != "<") {
			
			$this->Handle_Text($output_str, $part, $log);
			continue;
		}
		
		// Tags can be upper and lower case, lets convert to lower case so
		// we catch both.
		$part = strtolower($part);
		
		
		// If not text, $part must be a tag. Check if the tag exists
		// in $html_tags.
		if (array_key_exists($part, $this->html_tags)) {
			$this->Handle_Normal_Tags($output_str, 
					                  $part,  
					                  $paragraph_name, 
					                  $log);
		} else {

		    // Handle tags with attributes such as spans and links
		    $this->Handle_Tags_With_Attributes($part, $log);
		}
			
	} // end for each loop
	
	// Replace all provisional XML line breaks in the output string with
	// a real XML tag for line break.
	$this->xml_buf .= str_replace('xxxtext:line-break/xxx', '<text:line-break/>',
			                      $output_str);
	return $log;
}

/**
 * Get_XML_Text()
 *
 * This function returns the XML text that has so far been collected as it
 * will be inserted into the Libreoffice document. Its main use is for
 * debugging purposes.
 *
 * @return string XML formatted text for Libreoffice
 *
 */

public function Get_XML_Text() {

	return $this->xml_buf;
}

/**
 * Get_XML_Style_Description()
 *
 * Assembles all style information stored in the global array $text_styles
 * into XML formatted style instructions in a single return string that
 * can be inserted into a Libreoffice Writer document in content.xml in
 * the style section.
 *
 * @return string XML formatted information about all styles that are used
 *         in the HTML input string.
 *
 */

public function Get_XML_Style_Description() {

	$style_desc = "";

	// Now assemble the text styles	
	foreach($this->text_styles as $name => $styles) {
		$style_desc .= '<style:style style:name="' . $name .
		'" style:family="text">';
		$style_desc .= '<style:text-properties';

		foreach($styles as $style) {
			$style_desc .= ' ' . $style;
		}

		$style_desc .= '/>';
		$style_desc .= '</style:style>';
		
		$style_desc .= "\n";
	}
	
	$style_desc .= "\n";

	return $style_desc;
}

/**
 *
 * Get_XML_Page_Break_Para_Styles()
 *
 * Creates the XML page break paragraph styles that are descenants of
 * normal paragraph styles and returns them as an XML formatted string that
 * can then be inserted in content.xml of a Libreoffice document.
 *
 * @return string The XML formater page break paragraph styles
 *
 */

public function Get_XML_Page_Break_Para_Styles() {

	$xml = "";

	foreach ($this->page_brk_para_names as $brk_name => $parent_name) {

		$xml .= '<style:style style:name="' . $brk_name .
		'" style:family="paragraph" ' .
		'style:parent-style-name="' . $parent_name . '">' .
		'<style:paragraph-properties fo:break-before="page"/>' .
		'</style:style>';
		
		$xml .= "\n";
	}

	return $xml;
}


/**
 * Handle_Tags_With_Attributes()
 *
 * Tags with attributes can either be <span> elements or link (<a href...>).
 * The function first checks if it is a span and if not checks if it is a 
 * link. If it is neither, the tag is ignored and an error message is put
 * into the log.
 * 
 * @param string $part The tag
 * @param string $log The log buffer, given by reference
 * 
 * @return boolean True if the tag was handled, false if the tag is unknown.
 *
 */

private function Handle_Tags_With_Attributes($part, &$log) {
    
    // Let's see if the tag is a span
    if ($this->Handle_Span_Tags($part, $log)) return true;
    
    // If not a span, perhaps its a link    
    if ($this->Handle_Link_Tags($part, $log)) return true;
    
    LoxLog::add($log, "ERROR: The tag is unknown: " . $part, true);
    return false;
}

/**
 * Handle_Link_Tags()
 *
 * Checks if the given tag is of type 'a href' and if so extracts the link
 * target and saves it into an object variable for later use.
 * 
 * If the given tag is of type '/a', the function removes the extracted link
 * from the object variable as the text that now follows no longer belongs to
 * the link.
 * 
 * Any other tag will not be treated and handled as unknown.
 *
 * @param string $part The tag
 * @param string $log The log buffer, given by reference
 * 
 * @return boolean True if the tag was handled, false if the tag is unknown.
 *
 */

private function Handle_Link_Tags($part, &$log) {

    
    // Replace & characters, with HTML encoding to make the a href tag
    // XML compatible
    $part = str_replace("&", "&amp;", $part);
    
    // Handle Beginning of a Link
    if (strstr($part, '<a')) {
        LoxLog::add($log, "Beginning of Link found: " . $part);
        
        //Extract the link target
        $link = "";
        
        if (preg_match('/"([^"]+)"/', $part, $link)) { 
            $this->href_link = $link[1];
            LoxLog::add($log, "Link is: ".  $this->href_link);            
        } else {
            LoxLog::add($log, "ERROR: Unable to extract HREF link", true);
            return false;
        }
        
        return true;
    }
    
    // Handle End of a Link
    if (strstr($part, '</a')) {
        $this->href_link = "";
        LoxLog::add($log, "End of link tag found");
        return true;
    }
    
    return false;
}

/**
 * Handle_Span_Tags()
 *
 * This function is called to handle a span element. Span elements are different
 * from HTML style tags as they have a value inside (e.g. a color value) that
 * has to be extracted and put into the XML output in a different place. 
 * Further, span elements only have a generic 'end span', i.e. the end does
 * not contain the information to which span element it refers to. As a
 * consequence an 'end span' element closes the most recent span element that
 * was opened. As span elements can be cascaded, this function pushes a new
 * span element to an object array that acts as a stack. In other parts of this
 * class, the span details, already in XML format, can be removed from the 
 * top-most position of the stack again.
 * 
 * If the part is unkown, an error message is appended to the log string 
 * and the function returns without doing anything.
 *
 * @param string $part The span to be analyzed
 * @param string $log The log buffer, called by reference
 *
 * @return boolean True if this was a span and could be handled and false if the
 *         span was unknown / input was no span at all.
 */

private function Handle_Span_Tags($part, &$log) {

	switch (true) {
		
		// Background color span
		case (strstr($part, '<span style="background-color:#')):
			$span_type = self::SPAN_BG_COLOR;
			LoxLog::add($log, "Background color span found: " . $part);			
			break;
			
		// Font color span
		case (strstr($part, '<span style="color:#')):
			$span_type = self::SPAN_FONT_COLOR;
			LoxLog::add($log, "Font color span found: " . $part);
			break;	
				
		
		default: 
			return false;
	}
	
	// Now do the stuff that is common for background color and text color

	// Extract the color value from the span
	if (($pos = strpos($part, "#")) !== FALSE) {
		$color = substr($part, $pos, 7);
		if (strlen($color) != 7) {
			LoxLog::add($log, "ERROR, color length invalid: " . $color . 
					          " using default black", true);
			$color = "#000000";
		}
	}
		
	$xml_attr = str_replace("CCCCCCC", $color, $this->html_spans[$span_type]);
	LoxLog::add($log, "XML attribute: " . $xml_attr);
		
	// Push the XML attribute to the span stack (first in/first out)
	array_push($this->span_stack, $xml_attr);
	
	return true;	
}

/**
 * Handle_Normal_Tags()
 *
 * This function is called to take action when an HTML tag is discovered in
 * the input string. The beginning and end of a paragraph are translated
 * directly into the output string. Normal HTML style tags such as bold, 
 * italics, etc. are handled by setting a flag in the object's $html_tags
 * array to true. This is later used in another method of this class
 * when a text part is detected in the input HTML string to create an XML 
 * style tag around it.
 *
 * @param string $output_str The output string, called by reference
 * @param string $part The HTML tag to be analyzed
 * @param string $paragraph_name The style name for the paragraph 
 *               (as defined in the Libreoffice document)
 * @param string $log The log buffer 
 *
 * @return null The output string variable (called by reference) and object
 *         variables.
 */

private function Handle_Normal_Tags (&$output_str, $part, $paragraph_name, &$log) {

	// If the tag is unknown, put an error message into the log buffer
	// and exit
	if (!array_key_exists($part, $this->html_tags)) {
		
		LoxLog::add($log, "ERROR: Unknown tag: " . $part, true);
		return;		
	}

	// The tag is in the $html_tags array, do a standard conversion from 
	// HTML to Libreoffice XML format as described in the $html_tags array.
	$tag_type = $this->html_tags[$part][self::TAG_TYPE];

	LoxLog::add($log, "TAG Type: " . $tag_type);
	LoxLog::add($log, "Current part is: " .
			          $this->html_tags[$part][self::DEBUG_INF]  .
			          " --> " . $part);
		
	switch($tag_type) {
	
		case self::LO_CONVERT_START_PARAGRAPH:
			$this->Handle_Start_Paragraph($output_str,
			                              $part,
			                              $paragraph_name,
										  $log);					
			break;
	
		case self::LO_CONVERT_END_PARAGRAPH:
			// This one is simple, just end the closing paragraph XML tag
			$output_str = $output_str . $this->html_tags[$part][self::XML_TXT];
			
			// In case there was an error, some formatting might still
			// be active. To prevent it to spill into the next paragraph,
			// reset all formatting.
			$this->Check_And_Reset_All_Formatting($log);
			break;
				
		case self::LO_CONVERT_START_FORMAT:
			$this->html_tags[$part][self::TAG_STATE] = true;
			break;
				
		case self::LO_CONVERT_END_FORMAT:
	
			// We have found an END Format, set start format
			// that belongs to this end format tag to false
			$corresponding_start_tag = str_replace("/", "", $part);
			$this->html_tags[$corresponding_start_tag][self::TAG_STATE] = false;
			break;
			
		case self::LO_CONVERT_END_SPAN:
			// Remove the most recent HTML span element from the span stack
			array_pop($this->span_stack);

			LoxLog::add($log, "Removed topmost span from stack!");			
			break;
						
		default:
			// Note: This theoreticall can't happen because we check if
			// the tag exists before we hop into the switsch statement.
			LoxLog::add($log, "ERROR: The tag is unknown in the " . 
			            "switch section: " . $part, true);
			
	} // end switch
	
}


/**
 * Check_And_Reset_All_Formatting()
 *
 * At the end of a paragraph, all HTML formatting should have terminated.
 * To detect logic errors in this code or error in the HTML input data,
 * this function checks if an HTML formatting tag or span is still active.
 * If so it resets the indication so the issue will not spill over into
 * the next paragraph. Also, an error message will be added to the log
 * 
 * @param string $log Reference to log buffer
 *
 * @return null
 *
 */

private function Check_And_Reset_All_Formatting(&$log) {

	// Check and reset all normal html tag active flags
	foreach($this->html_tags as $tag => &$attributes) {
				
		if ($attributes[self::TAG_STATE]) {
			LoxLog::add($log, "ERROR: Tag: " . 
					    $tag . " was still set to 'true'", true);
			$attributes[self::TAG_STATE] = false;
		}						
	}
	
	// Check and reset the span elment stack.
	if (count($this->span_stack) > 0) {
		$this->span_stack = array();
		LoxLog::add($log, "ERROR: The span stack " .
				    "was not empty at the end of a paragraph", true);
	}
	
} 

/**
 * Handle_Start_Paragraph()
 *
 * Creates a start paragraph XML tag and appends it to the already
 * existing output string.
 *
 * @param string $output_str The already existing output
 * @param string $part The current part of the input string
 * @param string $paragraph_name The paragraph name, e.g. "P3" or "aa-3" or any
 *               other paragraph name defined in the Libreoffice document.
 * @param string $log Reference to the log string
 *
 * @return null The output string is called by reference and thus implicitly 
 *              modified.
 */

private function Handle_Start_Paragraph (&$output_str, $part, $paragraph_name, &$log){
	
	// Create paragraph style with page break from the current paragrah
	// name if the object's 'insert page break' variable is set.
	if ($this->insert_page_break) {
		
		$paragraph_name = $this->Create_Page_Break_Para_Style($paragraph_name, $log);		
		
		// Only do this for the first paragraph in the current HTML input
		$this->insert_page_break = false;
	}
		
	// Replace PNAME in the generic paragraph start XML tag with the paragraph
	// name given to this function.
	$start_str = str_replace("PNAME", $paragraph_name, 
			                 $this->html_tags[$part][self::XML_TXT]);
			
	// Concatenate previous output with start paragraph tag and return new string.
	$output_str = $output_str . $start_str;
}

/**
 * Handle_Text()
 *
 * Handles the actions to be taken when an HTML text part is encountered in the
 * input string as follows:
 * 
 *  * Go thorough the list of formatting tags and span info to see which 
 *    ones are activated.
 *  * Assemble the formatting XML tag from all activated HTML tags
 *  * Append XML tag + text to output string
 * 
 * @param string $out_str Already existing output, called by reference, modified
 * @param string $part The current part of the HTML string
 * @param string $log The log buffer, called by reference, modified.
 *
 * @return null Calling parameters that are referred to by reference.
 *
 */

private function Handle_Text (&$out_str, $part, &$log) {
	
	LoxLog::add($log, "Part is text: " . $part);

	// Convert the HTML formatted text into UTF-8
	$part = html_entity_decode($part, ENT_QUOTES, "utf-8");
	
	// 'Escape' characters in the UTF-8 text such as <,>,&, etc. that must
	// not be present in XML encapsulated text.
	$part = htmlspecialchars($part, ENT_XML1 | ENT_COMPAT, 'UTF-8');
	
	LoxLog::add($log, "Part in UTF-8: " . $part);
	
	// Get all 'normal' HTML styles in an array
	$cur_styles = $this->Get_All_Active_HTML_Styles($log);
	LoxLog::add($log, 'Number of styles: ' . count($cur_styles));
	
	// Get all 'special' HTML styles, i.e. active HTML spans (e.g. color) 
	// and add them to the $cur_styles array. 
	foreach ($this->span_stack as $span_attrib) {
		$this->Add_Style_To_Array_If_Not_Duplicate($cur_styles, $span_attrib, $log);
	}

	// If the text is part of a link, put a XML link info at the beginning
	if ($this->href_link != "") {
	    $this->Add_XML_Link_Start($out_str, $log);
	}	   
	
	// Include tags with styles if any
	if (count($cur_styles) > 0) {

		$style_name = $this->Create_And_Get_Style_Name($cur_styles, $log);		
		$out_str = $out_str . '<text:span text:style-name="'. $style_name . '">';
	}
	
	// Now add the text to the output string.
	$out_str = $out_str . $part;
	
	if (count($cur_styles) > 0) {
		$out_str = $out_str . '</text:span>';
	}
	
	// If the text is part of a link, put the 'end link' XML tag in place
	if ($this->href_link != "") {
	    $out_str .= '</text:a>';
	}
}


/**
 * Add_Style_To_Array_If_Not_Duplicate()
 *
 * This function adds a span attribute to the array of previously found
 * styles. If the type of style is already in the array, the existing 
 * entry will be replaced by the new span attribute. This is necessary 
 * as Libreoffice only accepts one instance of a particular style type,
 * while it is possible to have nested styles of the same type in HTML.
 *
 * @param array, styles already found
 * @param string, the new span attribute to be added
 * @param string, error log
 *
 * @return null, first parameter and the log are modified
 *
 */
private function Add_Style_To_Array_If_Not_Duplicate(&$cur_styles, $span_attrib, &$log) {

    $duplicate_found = false;
    
    // Get the type of the span element which to compare it to already 
    // existing style elements in the $cur_styles array. 
    // Note: Only this part can be compared because the value of the span 
    // element of the same type can be different.
    $new_style_type = substr($span_attrib, 0, strpos($span_attrib, '='));
    
    foreach ($cur_styles as &$style) {
        
        $cur_style_type = substr($style, 0, strpos($style, '='));
        if (strcmp($cur_style_type, $new_style_type) == 0) {
            LoxLog::add($log, "Note: Style already present, replacing instead " . 
                        "of adding array entry", true);
            $style = $span_attrib;
            $duplicate_found = true;
            break;
        }
    }
    
    if ($duplicate_found == false) {
        array_push($cur_styles, $span_attrib);
    }
}


/**
 * Add_XML_Link_Start()
 *
 * Adds the XML tag for beginning of 'link' to the output string given
 * as first parameter. 
 * 
 * @param string $out_str XML link description is added in front
 * @param string $log The log buffer
 * 
 * @return null $out_str is modified
 *
 */

private function Add_XML_Link_Start (&$out_str, &$log) {

   $XML_link = '<text:a xlink:type="simple" xlink:href="' . $this->href_link .  
               '" text:style-name="Internet_20_link" '. 
               'text:visited-style-name="Visited_20_Internet_20_Link">';
   
   LoxLog::add($log, 'XML Link created: ' . $XML_link);
   
   $out_str .= $XML_link;
   
}

/**
 * Get_All_Active_HTML_Styles()
 *
 * Searches the global $html_tags variable to find all currently active
 * styles and returns them in an array
 * 
 * @param string $log Buffer to add log information to, called by reference
 *
 * @return array All styles in XML notation
 *
 */

private function Get_All_Active_HTML_Styles (&$log) {
	
	$cur_styles = array();
	
	// Find all activated HTML formatting elements
	foreach($this->html_tags as $tag => $attributes) {
	
		// Skip this round if current attribute is not a start format attribute
		if ($attributes[self::TAG_TYPE] != self::LO_CONVERT_START_FORMAT) {
			continue;
		}
			
		$state = $attributes[self::TAG_STATE];
	
		LoxLog::add($log, "Tag '" . $tag . "' is set to: " .
				    ($state ? "true" : "false"));
	
		// If current HTML formatting element is not active go to next one
		if ($state != true) {
			continue;
		}
	
		array_push($cur_styles, $attributes[self::XML_TXT]);
	}	
	
	return $cur_styles;
}

/**
 * Create_And_Get_Style_Name()
 *
 * Compares the array of given styles + the stack of span elements currently
 * active to the global array of styles that were used before. If the 
 * combination already exists it will return the name of a previously created 
 * style combination. If the combination does not exist, a new style name
 * will be generated and the new style/span combination will be put into
 * the global array.
 *
 * @param string $cur_styles The current style. Note: the current stack of 
 *               HTML span elements from the global variable is used. A global
 *               variable is used for easy access from several private
 *               functions in this class.
 * @param string $log Log buffer called by reference.
 *
 * @return string The style name to use for this combination
 * 
 */

private function Create_And_Get_Style_Name ($cur_styles, &$log) {

	$return_style_name = "";
	
	// Compare the given style list to already existing style lists
	// to see if the combination already exists and can thus be reused.
	foreach ($this->text_styles as $style_name => $styles) {

		// If the number of styles in the current array is not the same
		// as in the current styles variable, skip this iteration as
		// they can't be the same.
		if (count($styles) != count($cur_styles)) continue;
		
		$matches = array_intersect ($styles, $cur_styles);
				
		if (count($matches) == count($cur_styles)) {
			$return_style_name = $style_name;			
			LoxLog::add($log, "Existing style: " . $style_name);			
			break;
		}
	} 
	
	// If the current style list does not match an existing one, add it!
	if ($return_style_name == "") {

		// Put the new style name + style combination into the global styles 
		// array. Note: TLOX = Text LibreOffice Export, could be any other 
		// identifier as well.
		$return_style_name = 'TLOX' . (count($this->text_styles) + 1);
		$this->text_styles[$return_style_name] = $cur_styles;
		LoxLog::add($log, "NEW text style: " . $return_style_name);
	}	
	
	return $return_style_name;
}

/**
 *
 * Create_Page_Break_Para_Style()
 *
 * Creates a paragraph style with page break information inside by prepending 
 * "PBreak-" before the given paragraph name and by storing the information
 * in an object array for later use.
 * 
 * @param string $para_name The paragraph name
 * @param string $log The log buffer
 *
 * @return string The page break paragraph name
 * 
 */

private function Create_Page_Break_Para_Style($para_name, &$log) {

	$break_para_name = "PBreak-" . $para_name;

	LoxLog::add($log, "Creating new page break para name: " . $break_para_name);

	// Creates new or overwrites already existing entry in array
	$this->page_brk_para_names[$break_para_name] = $para_name;		

	return $break_para_name;
}

} // end of class

