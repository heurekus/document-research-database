<?php

/**
 * x-demo-calc-export.php
 *
 * This demo can be run directly from the shell. It converts and outputs
 * the HTML text given into a Libreoffice Calc file based on an existing
 * template file in this directory.
 * 
 * Example shell command with XAMPP installed:
 * 
 *    /opt/lampp/bin/php x-demo-calc-export.php
 * 
 * Output file: 
 *  
 *    x-demo-calc-output.ods  --> Open with Libreoffice Writer
 *
 * IMPORTANT: This class requires the 'php-zip' module to be installed
 * which might have to be installed on the server separately from the
 * standard repositories (e.g. apt install php-zip)
 *
 * @version    1.1 2019-03-04
 * @package    libreoffice-export
 * @copyright  Copyright (c) 2018-2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'lo-calc-export.php';

$html_str1 = '<p>Before <span style="color:#ff0000">the first cell</span> and after</p>';
$html_str2 = '<p><span style="background-color:#00ffff">the second cell</span></p>';
$html_str3 = '<p><strong><em>the fourth cell</em></strong></p>';
$html_str4 = "<p>one more...</p>";

$lo_export = new LO_Calc_Export;

$lo_export->New_Table_Row();
$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($html_str1);
$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($html_str2);
$lo_export->End_Of_Table_Row();

$lo_export->New_Table_Row();
$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content('');
$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($html_str3);
$log_buf = $lo_export->Convert_HTML_To_LO_Calc_Cell_Content($html_str4);

$lo_export->End_Of_Table_Row();

$src = 'x-demo-calc-template.ods';
$dst = 'x-demo-calc-output.ods';

$log_buf .= $lo_export->Save_To_Calc_Document($src, $dst);

// Output error debug messages if any.
var_dump($log_buf);

