# PHP HTML to Libreoffice Writer and Calc Export
Export HTML formatted text to Libreoffice


### Overview

This project converts a HTML formatted strings to Libreoffice documents. The base class 'html-to-lo-xml.php' can be 'extended' by other classes which contain functionality to write its output to different kinds of Libreoffice document types. At the moment, Writer and Calc output is implemented.

The "Export to Libreoffice Writer' functionality is implemented in lo-writer-export.php. It uses a Libreoffice Writer file as template for structure and predefined paragraph styles. Basically, an '.odt' file is a ZIP file with a number of files inside, so it is easy to extend and change the content.

Libreoffice Calc Export is implemented in lo-calc-export.php. Like the Writer equivalent, it uses a template Calc file for the general structure. 

### Demo Programs

Two demo programs are provided to try out the functionality. Run directly from the shell, they convert and output the HTML text given in a number of variables and create a Libreoffice Writer and Calc spreadsheet:

Example shell commands with XAMPP installed:

`     /opt/lampp/bin/php x-demo-writer-export.php`

`     /opt/lampp/bin/php x-demo-calc-export.php`

Output files: 

`     x-demo-writer-output.odt  --> Open with Libreoffice Writer`

`     x-demo-calc-output.odt  --> Open with Libreoffice Calc`


### Required PHP librarries

IMPORTANT: This class requires the 'php-zip' module to be installed which might have to be installed on the server separately from the standard repositories (e.g. apt install php-zip)


### Supported HTML tags:

 *  Bold, (both 'b' and 'strong' tags)
 *  Italics, i.e. 'emphasis'
 *  Superscript
 *  Foreground color span 
 *  Background color span (Note: Calc does not support background color of parts of a cell, it is ignored)
 *  The HTML 'br' tag without closing
 *  HTML links 'a href'

### Similar Projects

Before writing this library, I was looking for already available alternatives. One intersting project is [Open Tiny But Strong](https://github.com/Skrol29/opentbs) which offers great functinality to insert text, graphics, etc. into Libreoffice documents. However, it is not possible to use different styles in a paragraph and the project also offers no HTML translation capabilities as its intended purpose is different. In other words, it was not an option for me. However, depending on your needs, it might be worth a look!
