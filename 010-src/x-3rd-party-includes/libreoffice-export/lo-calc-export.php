<?php

/**
 * lo-calc-export.php
 *
 * The class in this file extends the Html_To_LibreOffice_Calc_XML class
 * and contains the code to save text with style information to a 
 * Libreoffice Calc template document.
 * 
 * For details which style information is implemented have a look at the
 * corresponding writer PHP modules in this project.
 * 
 * Limitations: 
 * 
 *  - All existing text in cells is removed from the template
 *    file as Calc documents contain 'default' formatting for
 *    used rows so content can't directly be added after existing
 *    content. Should such functionality be desired in the
 *    future then this module has to treat them.  
 * 
 * IMPORTANT:
 *  
 *     This class requires the 'php-zip' module to be installed
 *     which might have to be installed on the server separately from
 *     the standard repositories (e.g. apt install php-zip)
 *
 * @version    1.1 2019-01-24
 * @package    libreoffice-export
 * @copyright  Copyright (c) 2014-2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'lo-export-log.php';
require_once 'html-to-lo-calc-xml.php';

class LO_Calc_Export extends Html_To_LibreOffice_Calc_XML {

/**
 * LO_Calc_Export()
 *
 *
 * @param string $src Path and filename to the template Libreoffice Calc document
 * @param string $dst Path and filename to which the output Calc document is to 
 *               be written to.
 *
 * @return string Log information for debugging
 */	
	
public function Save_To_Calc_Document ($src, $dst) {

	$log = "";
	
	if (!copy($src, $dst)) {
		LoxLog::add($log, "ERROR: Unable to copy file, aborting");
		return $log;
	}
	
	$zip = new ZipArchive;
	$res = $zip->open($dst);
	if ($res !== TRUE) {
		LoxLog::add($log, "ERROR: Couldn't open Calc template copy");
		return $log;
	}
	
	LoxLog::add($log, "Calc template open successfull");
	
	$content = $zip->getFromName('content.xml');

	$xml_styles = $this->Get_XML_Page_Break_Para_Styles();
	$xml_styles .= $this->Get_XML_Style_Description();
			
	$content = str_replace("</office:automatic-styles>", 
			               $xml_styles . "</office:automatic-styles>", 
						   $content);
	

	// Get the beginning of content.xml and thus REMOVE any exisiting table
	// cells and rows. This is important as formatting etc. could be inside
	// applying to ALL empty cells which would make inserting after the 
	// last row that was populated impossible without further looking into
	// what the formatting is doing and adapting it.
	$content = strstr ($content, '<table:table-row', true);
	
	// Add our content
	$content .=  $this->xml_buf; 
	
	// And now close the document with end XML tags
	$content .= '</table:table><table:named-expressions/>' . 
	            '</office:spreadsheet></office:body>' . 
	            '</office:document-content>';
		
	$zip->addFromString("content.xml", $content);
	$zip->close();
		
	return $log;
} 
	
}